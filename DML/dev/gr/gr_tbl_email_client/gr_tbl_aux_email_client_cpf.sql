With tab_aux_email as (
select distinct num_document, ds_email, ds_email_type, prioridade, dth_create, dth_update
from (
select num_cpf as num_document, ds_email, 1 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` a	
 where num_cpf is not null and ds_email is not null	and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true
union all
select num_cpf as num_document, ds_email, 2 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` a	
 where num_cpf is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true
union all
select num_cpf as num_document, ds_email, 3 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` 	a
 where num_cpf is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true		  
union all
select num_document, ds_email, 4 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` a	
 where num_document is not null and ds_document_type = 'CPF' and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true		  
union all
select num_cpf as num_document, ds_email, 6 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client` a	
 where num_cpf is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true		  
union all
select num_cpf as num_document, ds_email, 7 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client` a	
 where num_cpf is not null and ds_email is not null	and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  
union all
select num_document as num_document, ds_email, 9 as prioridade, ds_email_type, dth_create, dth_update 
  from (
  select a.num_document, b.ds_email, b.ds_email_type, b.dth_create, b.dth_update
  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_email` b 
  inner join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` a 
  on a.num_unique_client = b.num_unique_client
  and b.ds_email_type = 'PESSOAL'
   	) a 
 where num_document is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  
union all
select num_document as num_document, ds_email, 8 as prioridade, 'PESSOAL' as ds_email_type, dth_create, dth_update
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` a	
 where num_document is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  

) as tab_aux_1	
),
tab_aux_main_email as (
select num_document, ds_email_type, min(prioridade) as prioridade
from   tab_aux_email
where trim(ds_email) != 'NONE@NONE.COM'
Group by num_document, ds_email_type 		
),
tab_selected_emails as (
    Select a.*
      from tab_aux_email a
      inner join tab_aux_main_email b 
         on a.num_document  = b.num_document 
        and a.ds_email_type = b.ds_email_type
        and a.prioridade    = b.prioridade  
)
Select * except(ind_bounce_email),
       case when ind_bounce_email = '0' then false
	        when ind_bounce_email = '1' then true
			else null 
	    end as ind_bounce_email 
from (	
		select c.num_unique_client, COALESCE(c.num_version_golden_record,
			1) AS num_version_golden_record,
			   a.ds_email, SHA256(a.ds_email) as hash_email, a.ds_email_type, 
				case when a.ds_email_type = 'PESSOAL' then True Else False end as ind_main_email,
			   case when a.prioridade = 1 and a.ds_email_type = 'PESSOAL'   then 'VTEX'
					when a.prioridade = 1 and a.ds_email_type = 'COMERCIAL' then 'RH'
					when a.prioridade = 2 then 'CSF'
					when a.prioridade = 3 then 'RH'
					when a.prioridade = 4 then 'SVA'
		 --           when a.prioridade = 5 then 'PROPZ'
					when a.prioridade = 6 then 'NEOASSIST'
					when a.prioridade = 7 then 'QUALIBEST'	   
					when a.prioridade = 8 then 'HIST'	   
					when a.prioridade = 9 then 'OLDGR'	   
				end as ds_source_system,
			  a.dth_create, 
        a.dth_update,
			   True                     as ind_document,
		  (select ind_hard_bounce_email 
			 from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
			where a.num_document = b.num_cpf and a.ds_email = b.ds_email) as ind_bounce_email	   	   
		from tab_selected_emails as a 
		inner join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` as c 
				on c.num_document = a.num_document
	)
;
