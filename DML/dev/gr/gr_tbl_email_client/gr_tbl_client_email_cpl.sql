With tab_aux_email as (
select distinct num_document, ds_email, ds_email_type, ind_hard_bounce_email, prioridade, dth_create, dth_update 
from (
select a.num_cpf as num_document, a.ds_email, 1 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_cpf = b.num_cpf and a.ds_email = b.ds_email 
 where a.num_cpf is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  
union all
select a.num_cpf as num_document, a.ds_email, 2 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_cpf = b.num_cpf and a.ds_email = b.ds_email 
 where a.num_cpf is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true    
union all
select a.num_cpf as num_document, a.ds_email, 3 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` 	a
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_cpf = b.num_cpf and a.ds_email = b.ds_email 
 where a.num_cpf is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
union all
select a.num_document as num_document, a.ds_email, 4 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_document = b.num_cpf and a.ds_email = b.ds_email 
 where a.num_document is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
union all
select a.num_cpf as num_document, a.ds_email, 6 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_cpf = b.num_cpf and a.ds_email = b.ds_email 
where a.num_cpf is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true   
union all
select a.num_cpf as num_document, a.ds_email, 7 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update  
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_cpf = b.num_cpf and a.ds_email = b.ds_email 
where a.num_cpf is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  
union all
select a.num_document as num_document, a.ds_email, 8 as prioridade, ds_email_type, b.ind_hard_bounce_email, dth_create, dth_update  
  from (
  select a.num_document, b.ds_email, b.ds_email_type
  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_email` b 
  inner join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` a 
  on a.num_unique_client = b.num_unique_client
  and b.ds_email_type = 'PESSOAL'
   	) a 
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_document = b.num_cpf and a.ds_email = b.ds_email 
  where a.num_document is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
union all
select num_document as num_document, a.ds_email, 9 as prioridade, 'PESSOAL' as ds_email_type, b.ind_hard_bounce_email, a.dth_create, a.dth_update 
  from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` a	
  left join `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
         on a.num_document = b.num_cpf and a.ds_email = b.ds_email 
  where a.num_document is not null and a.ds_email is not null	and REGEXP_CONTAINS(trim(a.ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  

) as tab_aux_1	
),
tab_aux_main_email as (
select num_document, ds_email_type, ds_email, ind_hard_bounce_email, prioridade, dth_create, dth_update, ROW_NUMBER() OVER (PARTITION BY ds_email_type, ds_email order by prioridade) RANK
from   tab_aux_email
where trim(ds_email) != 'NONE@NONE.COM'
and trim(ds_email) not in (select trim(ds_email) from  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_email` )
),
tab_aux_main_email2 as (
SELECT * EXCEPT (RANK) 
from   tab_aux_main_email
where RANK = 1 
)
Select * except(ind_hard_bounce_email),
       case when ind_hard_bounce_email = '0' then false
	        when ind_hard_bounce_email = '1' then true
			else null 
	    end as ind_bounce_email 
from (	
		select c.num_unique_client, COALESCE(c.num_version_golden_record,
			1) AS num_version_golden_record,
			   a.ds_email, SHA256(a.ds_email) as hash_email, a.ds_email_type, 
				false as ind_main_email,
			   case when a.prioridade = 1 and a.ds_email_type = 'PESSOAL'   then 'VTEX'
					when a.prioridade = 1 and a.ds_email_type = 'COMERCIAL' then 'RH'
					when a.prioridade = 2 then 'CSF'
					when a.prioridade = 3 then 'RH'
					when a.prioridade = 4 then 'SVA'
		 --           when a.prioridade = 5 then 'PROPZ'
					when a.prioridade = 6 then 'NEOASSIST'
					when a.prioridade = 7 then 'QUALIBEST'	   
					when a.prioridade = 8 then 'OLDGR'	   
					when a.prioridade = 9 then 'HIST'	   
				end as ds_source_system,
			  a.dth_create,
			  a.dth_update,
			  --  True                     as ind_document
		  a.ind_hard_bounce_email
		from tab_aux_main_email2 as a 
		inner join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` as c 
				on c.num_document = a.num_document
	)