Select * except(ind_bounce_email),
       case when ind_bounce_email = '0' then false
	        when ind_bounce_email = '1' then true
			else null 
	    end as ind_bounce_email 
from (	
		SELECT
		  a.num_unique_client,
		  COALESCE(a.num_version_golden_record,
			1) AS num_version_golden_record,
		  b.ds_email_business AS ds_email,
		  SHA256(b.ds_email_business) AS hash_email,
		  'COMERCIAL' AS ds_email_type,
		  TRUE AS ind_main_email,
		  'RH' AS ds_source_system,
			b.dth_create,
			b.dth_update,
		  False                    as ind_document,
		  c.ind_bounce_email
		FROM
		  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_email_process` AS a
		INNER JOIN
		  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` AS b
		ON
		  TRIM(a.ds_email) = TRIM(b.ds_email)
		LEFT JOIN (
					Select ds_email, max(ind_hard_bounce_email) as ind_bounce_email
					  From `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client`  
					group by ds_email
				  ) c 
		ON  TRIM(a.ds_email) = TRIM(c.ds_email)
		WHERE NOT a.ind_document 
		  and b.ds_email_business is not null
		  and trim(a.ds_email) != 'NONE@NONE.COM'
	)
;