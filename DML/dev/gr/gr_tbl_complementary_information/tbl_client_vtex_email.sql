SELECT
  c.num_unique_client,
  a.uuid_client,
  b.uuid_client AS uuid_address,
  a.ds_channel,
  a.dth_create,
  a.dth_update,
  b.dth_create_system_origin AS dth_create_system_address,
  b.dth_update_system_origin AS dth_update_system_address
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_address` AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN (
			  select distinct num_unique_client, ds_email, ds_source_system 
			  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email`
			  where ds_source_system = 'VTEX'	
			) c
ON
  UPPER(TRIM(a.ds_email)) = UPPER(TRIM(c.ds_email))
LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL;  