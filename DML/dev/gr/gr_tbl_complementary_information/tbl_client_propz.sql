CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_propz`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  ds_buy_phusical_virtual STRING OPTIONS(description="Purchase Individual or Virtual / Compra Física Ou Virtual"),
  num_children NUMERIC OPTIONS(description="Number Children Client Has / Quantidade De Filhos Que O Cliente Tem"),
  num_coupons_used STRING OPTIONS(description="Number Coupons That The Person Used / Número De Cupons Que A Pessoa Utilizou"),
  num_advertisements_customer_interacted_email NUMERIC OPTIONS(description="Number Advertisements The Client Interacted In The E-Mail / Quantidade De Propagandas Que O Cliente Interagiu No E-Mail"),
  dth_first_buy TIMESTAMP OPTIONS(description="Date First Purchase Client / Data Da Primeira Compra Do Cliente"),
  dth_last_buy TIMESTAMP OPTIONS(description="Date Last Purchase Client / Data Da Última Compra Do Cliente"),
  dth_received_last_email_advertisement TIMESTAMP OPTIONS(description="Date Received Last E-Mail Advertisement / Data Recebeu A Última Propaganda E-Mail"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente"),
  dth_partition TIMESTAMP OPTIONS(description="Partição da tabela Origem / Origin Table partition")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Table Propz Information Data / Tabela Dados De Informações De Pessoas Da Propz"
)
AS
SELECT
  c.num_unique_client,
  a.ds_buy_phusical_virtual,
  a.num_children,
  a.num_coupons_used,
  a.num_advertisements_customer_interacted_email,
  a.dth_first_buy,
  a.dth_last_buy,
  a.dth_received_last_email_advertisement,
  a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NOT NULL;