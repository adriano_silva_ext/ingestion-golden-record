SELECT
  c.num_unique_client,
  a.uuid_client,
  a.num_state_subscription,
  a.ind_loyalty,
  a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` AS a
INNER JOIN (
			  select distinct num_unique_client, ds_email, ds_source_system 
			  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email`
			  where ds_source_system = 'SVA'	
			) c
ON
  UPPER(TRIM(a.ds_email)) = UPPER(TRIM(c.ds_email))
LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_document IS NULL
  AND a.ds_email IS NOT NULL;