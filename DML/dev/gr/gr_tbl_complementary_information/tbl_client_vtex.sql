CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_vtex`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  uuid_client STRING OPTIONS(description="Id Consumer Ecommerce (Vtex) / Id Consumidor Ecommerce(Vtex)"),
  uuid_address STRING OPTIONS(description="Id Address / Id Do Endereço"),
  ds_channel STRING OPTIONS(description="Name Of The Channel Through Which The Customer Was Registered / Nome Do Canal Pelo Qual O Cliente Foi Cadastrado"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente"),
  dth_create_system_address TIMESTAMP OPTIONS(description="Date Create System Source / Data Criação Sistema Origem"),
  dth_update_system_address TIMESTAMP OPTIONS(description="Update Source System / Data Alteração Sistema Origem")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Table VTEX People Information Data / Tabela Dados De Informações De Pessoas Da Vtex"
)
AS
SELECT
  c.num_unique_client,
  a.uuid_client,
  b.uuid_client AS uuid_address,
  a.ds_channel,
  a.dth_create,
  a.dth_update,
  b.dth_create_system_origin AS dth_create_system_address,
  b.dth_update_system_origin AS dth_update_system_address
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_address` AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
ON
  c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NOT NULL;