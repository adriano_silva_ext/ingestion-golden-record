SELECT
  c.num_unique_client,
  a.ds_buy_phusical_virtual,
  a.num_children,
  a.num_coupons_used,
  a.num_advertisements_customer_interacted_email,
  a.dth_first_buy,
  a.dth_last_buy,
  a.dth_received_last_email_advertisement,
  a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` AS a
INNER JOIN (
			  select distinct num_unique_client, ds_email, ds_source_system 
			  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email`
			  where ds_source_system = 'PROPZ'	
			) c
ON
  UPPER(TRIM(a.ds_email)) = UPPER(TRIM(c.ds_email))
LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL;