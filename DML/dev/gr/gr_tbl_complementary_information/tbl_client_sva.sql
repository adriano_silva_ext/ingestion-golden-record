CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_sva`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  uuid_client STRING OPTIONS(description="Client Code / Código Cliente"),
  num_state_subscription STRING OPTIONS(description="State Registrater Number / Número De Incrição Estadual"),
  ind_loyalty STRING OPTIONS(description="My Carrefour Client / Cliente Meu Carrefour"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente"),
  dth_partition TIMESTAMP OPTIONS(description="Partição da tabela Origem / Origin Table partition")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Table Related to Physical Store Sales / Tabela Dados De Informações De Pessoas Relacionado A Vendas De Loja Físicas"
)
AS
SELECT
  c.num_unique_client,
  a.uuid_client,
  a.num_state_subscription,
  a.ind_loyalty,
    a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_document = c.num_document
  AND c.ds_document_type = a.ds_document_type
LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_document IS NOT NULL;