WITH
  tab_aux_phone AS (
  SELECT
    num_document,
    num_phone,
    ds_phone_type,
    MIN(prioridade) AS prioridade,
    dth_create,
    dth_update
  FROM (
    SELECT
      num_cpf AS num_document,
      num_phone,
      1 AS prioridade,
      CASE
        WHEN LENGTH(num_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_phone IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_mobile_phone AS num_phone,
      2 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_phone,
      3 AS prioridade,
      CASE
        WHEN LENGTH(num_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_phone IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_mobile_phone AS num_phone,
      4 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_cpf as num_document,
      num_mobile_phone AS num_phone,
      5 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_document,
      num_mobile_phone AS num_phone,
      6 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` a 
    WHERE
      num_document IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_document,
      num_landline AS num_phone,
      7 AS prioridade,
      CASE
        WHEN LENGTH(num_landline) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_landline) IN (13, 10,8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client` a
    WHERE
      num_document IS NOT NULL
      AND num_landline IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_mobile_phone AS num_phone,
      9 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_mobile_phone_2 AS num_phone,
      10 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone_2) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone_2) IN (13, 10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone_2 IS NOT NULL
    UNION ALL
    SELECT
      num_cpf AS num_document,
      num_mobile_phone AS num_phone,
      11 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client` a
    WHERE
      num_cpf IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_document,
      num_mobile_phone AS num_phone,
      12 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (13, 10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` a
    WHERE
      num_document IS NOT NULL
      AND num_mobile_phone IS NOT NULL
    UNION ALL
    SELECT
      num_document,
      num_phone AS num_phone,
      13 AS prioridade,
      CASE
        WHEN LENGTH(num_phone) IN (14, 11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_phone) IN (13, 10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` a
    WHERE
      num_document IS NOT NULL
      AND num_phone IS NOT NULL
  ) AS tab_aux_1
  GROUP BY
    num_document,
    num_phone,
    ds_phone_type,
    dth_create,
    dth_update ),
  tab_aux_main_phone AS (
  SELECT
    num_document,
    ds_phone_type,
    MIN(prioridade) AS prioridade
  FROM
    tab_aux_phone
  GROUP BY
    num_document,
    ds_phone_type ),
tab_selected_phone as (
    Select a.*
      from tab_aux_phone a
      inner join tab_aux_main_phone b 
         on a.num_document  = b.num_document 
        and a.ds_phone_type = b.ds_phone_type
        and a.prioridade    = b.prioridade  
)
Select * except(ind_bounce_phone),
       case when ind_bounce_phone = '0' then false
	        when ind_bounce_phone = '1' then true
			else null 
	    end as ind_bounce_phone 
from (		
		SELECT
		  c.num_unique_client,
		  a.num_phone,
		  SHA256(a.num_phone) AS hash_phone,
		  a.ds_phone_type,
		  CASE
			WHEN a.ds_phone_type = 'CELULAR PESSOAL' or a.ds_phone_type = 'FIXO PESSOAL' THEN TRUE ELSE FALSE
		  END AS ind_main_phone,
		  CASE
			WHEN a.prioridade IN (1, 2) THEN 'VTEX'
			WHEN a.prioridade IN (3,
			4) THEN 'CSF'
			WHEN a.prioridade = 5 THEN 'RH'
			WHEN a.prioridade IN (6, 7) THEN 'SVA'
		--    WHEN a.prioridade = 8 THEN 'PROPZ'
			WHEN a.prioridade IN (9, 10) THEN 'NEOASSIST'
			WHEN a.prioridade = 11 THEN 'QUALIBEST'
		END
		  AS ds_source_system,
		  a.dth_create,
      a.dth_update,				
		  TRUE AS ind_document,
		  (select ind_hard_bounce_sms 
			 from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
			where a.num_document = b.num_cpf and a.num_phone = b.num_mobile_phone) as ind_bounce_phone  
		FROM
		  tab_selected_phone AS a
		INNER JOIN
		  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
		ON
		  c.num_document = a.num_document
		left join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` 	 as oldgr 
			   ON  c.num_unique_client = oldgr.num_unique_client
		where c.num_document is not null
		UNION ALL
		SELECT
		  c.num_unique_client,
		  a.num_phone,
		  SHA256(a.num_phone) AS hash_phone,
		  a.ds_phone_type,
		  False				  AS ind_main_phone,
		  CASE
			WHEN a.prioridade IN (1, 2) THEN 'VTEX'
			WHEN a.prioridade IN (3,
			4) THEN 'CSF'
			WHEN a.prioridade = 5 THEN 'RH'
			WHEN a.prioridade IN (6, 7) THEN 'SVA'
		--    WHEN a.prioridade = 8 THEN 'PROPZ'
			WHEN a.prioridade IN (9, 10) THEN 'NEOASSIST'
			WHEN a.prioridade = 11 THEN 'QUALIBEST'
      WHEN a.prioridade IN (12, 13) THEN 'HIST'
		  END
		  AS ds_source_system,
		  a.dth_create,
      a.dth_update,				
		  TRUE 																	 AS ind_document,
		  (select ind_hard_bounce_sms 
			 from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client` b 
			where a.num_document = b.num_cpf and a.num_phone = b.num_mobile_phone) as ind_bounce_phone
		FROM (
		  SELECT num_document, num_phone, ds_phone_type, prioridade, dth_create, dth_update
		   from tab_aux_phone
		  except distinct
		  SELECT num_document, num_phone, ds_phone_type, prioridade, dth_create, dth_update
		   from tab_selected_phone
		) a 
		INNER JOIN
		  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
		ON
		  c.num_document = a.num_document
		left join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` 	 as oldgr 
			   ON  c.num_unique_client = oldgr.num_unique_client
		where c.num_document is not null
	)
;

