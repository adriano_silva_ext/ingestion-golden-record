SELECT  
  a.num_unique_client,
  a.num_phone,
  a.cod_consent_offer_sms,
  a.dth_consent_offer_sms,
  a.cod_consent_offer_whatsapp,
  a.dth_consent_offer_whatsapp,
  a.cod_source,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_phone_optin` a; 
