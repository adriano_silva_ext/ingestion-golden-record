CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_phone_optin`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  num_phone STRING OPTIONS(description="Telephone / Telefone"),
  cod_consent_offer_sms STRING OPTIONS(description="Consent Code Message Offer SMS / Código Do Consentimento Oferta SMS"),
  dth_consent_offer_sms TIMESTAMP OPTIONS(description="Consent Date Offer SMS / Data Do Consentimento Das Ofertas SMS"),
  cod_consent_offer_whatsapp STRING OPTIONS(description="Consent Code Offer WhatsApp / Código De Consentimento Oferta WhatsApp"),
  dth_consent_offer_whatsapp TIMESTAMP OPTIONS(description="Consent Date Offer Whatsapp / Data Do Consentimento Ofertas Whatsapp"),
  cod_source STRING OPTIONS(description="Source Code / Código De Origem"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client, num_phone
OPTIONS(
  description="Consent Table People's Phone Data / Tabela De Consentimento Dados De Telefones De Pessoas"
)
AS
WITH
  tab_phone_optin AS (
  SELECT
    b.num_unique_client,
    a.num_phone,
    CASE
      WHEN d.cod_consent_offer_sms IS NULL THEN a.cod_consent_offer_sms
      WHEN a.dth_consent_offer_sms > d.dth_consent_offer_sms THEN a.cod_consent_offer_sms
    ELSE
    d.cod_consent_offer_sms
  END
    AS cod_consent_offer_sms,
    CASE
      WHEN d.cod_consent_offer_sms IS NULL THEN a.dth_consent_offer_sms
      WHEN a.dth_consent_offer_sms > d.dth_consent_offer_sms THEN a.dth_consent_offer_sms
    ELSE
    d.dth_consent_offer_sms
  END
    AS dth_consent_offer_sms,
    CASE
      WHEN d.cod_consent_offer_whatsapp IS NULL THEN a.cod_consent_offer_whatsapp
      WHEN a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp THEN a.cod_consent_offer_whatsapp
    ELSE
    d.cod_consent_offer_whatsapp
  END
    AS cod_consent_offer_whatsapp,
    CASE
      WHEN d.cod_consent_offer_whatsapp IS NULL THEN a.dth_consent_offer_whatsapp
      WHEN a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp THEN a.dth_consent_offer_whatsapp
    ELSE
    d.dth_consent_offer_whatsapp
  END
    AS dth_consent_offer_whatsapp,
    CASE
      WHEN d.cod_source IS NULL THEN a.cod_source
      WHEN (a.dth_consent_offer_sms > d.dth_consent_offer_sms)
    OR (a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp) THEN a.cod_source
    ELSE
    d.cod_source
  END
    AS cod_source,
    a.dth_create,
    a.dth_update,
    row_number() over(partition by b.num_unique_client) as num_row	
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_onetrust_client` a
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` b
  ON
    a.num_cpf = b.num_document
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_phone` c
  ON
    b.num_unique_client = c.num_unique_client
  and a.num_phone = c.num_phone
  LEFT JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_phone_optin` d
  ON
    b.num_unique_client = d.num_unique_client
    AND a.num_phone = d.num_phone
  WHERE
    a.num_cpf IS NOT NULL
    AND b.num_document IS NOT NULL
    AND a.num_phone IS NOT NULL ),
	
	
  tab_max_optin_sms AS (
  SELECT
    a.num_unique_client,
    a.num_phone,
    MAX(a.dth_consent_offer_sms) AS dth_consent_offer_sms
  FROM
    tab_phone_optin a
  GROUP BY
    a.num_unique_client,
    a.num_phone
   ),
   
tab_selected_1 as (
Select a.*
  from tab_phone_optin a
  inner join tab_max_optin_sms b
     on a.num_unique_client = b.num_unique_client
    and a.num_phone         = b.num_phone
    and a.dth_consent_offer_sms = b.dth_consent_offer_sms
),

  tab_max_optin_sms_zap AS (
  SELECT
    a.num_unique_client,
    a.num_phone,
    MAX(a.dth_consent_offer_whatsapp) AS dth_consent_offer_whatsapp
  FROM
    tab_selected_1 a
  GROUP BY
    a.num_unique_client,
    a.num_phone
    ),
	
tab_selected_2 as (
Select a.*
  from tab_selected_1 a
  inner join tab_max_optin_sms_zap b
     on a.num_unique_client = b.num_unique_client
    and a.num_phone          = b.num_phone
    and a.dth_consent_offer_whatsapp  = b.dth_consent_offer_whatsapp
),

  tab_unique_row_create AS (
  SELECT
    a.num_unique_client,
    a.num_phone,
    max(a.num_row)      AS num_row
  FROM
    tab_selected_2 a
  GROUP BY
    1,
    2
      )   
	
SELECT distinct
  a.num_unique_client,
  a.num_phone,
  a.cod_consent_offer_sms,	
  a.dth_consent_offer_sms,	
  a.cod_consent_offer_whatsapp,	
  a.dth_consent_offer_whatsapp,	
  a.cod_source,
  a.dth_create,	
  a.dth_update
FROM
  tab_selected_2 a
INNER JOIN
  tab_unique_row_create b
ON
  a.num_unique_client = b.num_unique_client
  AND a.num_phone = b.num_phone
  AND a.num_row   = b.num_row;