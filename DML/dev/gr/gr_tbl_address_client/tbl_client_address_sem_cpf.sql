select
a.num_unique_client,
a.num_cep,
a.ds_street,
a.ds_neighborhood_name,
a.ds_city_name,
a.ini_state,
a.ds_state_name,
a.ds_country_name,
a.num_house,
a.ds_complement_street,
a.ds_address_type,
a.ds_reference,
a.ds_source_system,
true as ind_main_address,
a.dth_create,
a.dth_update
from (
Select * from (
			  SELECT
				a.*
			  FROM
				( 
				  SELECT
					a.*
				  FROM
					(
					  SELECT
						a.*
					  FROM
						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
					  inner join (
								  SELECT
									num_unique_client,
									MIN(prioridade) AS prioridade
								  FROM
									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
								  WHERE 
									ds_address_type = 'RESIDENCIAL'
								  GROUP BY
									num_unique_client
								 ) b 
						 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
					  WHERE 
						ds_address_type = 'RESIDENCIAL'
					 ) a 
				  inner join ( 
							  SELECT
								num_unique_client,
								prioridade,
								MIN(dt_create_ori) AS dt_create_ori
							  FROM
								(
								  SELECT
									a.*
								  FROM
									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
								  inner join (
											  SELECT
												num_unique_client,
												MIN(prioridade) AS prioridade
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
											  WHERE 
												ds_address_type = 'RESIDENCIAL'
											  GROUP BY
												num_unique_client
											 ) b 
									 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
								WHERE 
									ds_address_type = 'RESIDENCIAL'	
								)
							WHERE 
								dt_create_ori is not null 
							  GROUP BY
								num_unique_client,
								prioridade  
							)b 
					 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
				) a 
			  inner join (
						  SELECT
							num_unique_client,
							prioridade,
							coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
							MIN(num_row) AS num_row
						  FROM (
							select * from (
											  SELECT
												a.*
											  FROM
												(
												  SELECT
													a.*
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
												  inner join (
															  SELECT
																num_unique_client,
																MIN(prioridade) AS prioridade
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
															  WHERE 
																ds_address_type = 'RESIDENCIAL'
															  GROUP BY
																num_unique_client
															 ) b 
													 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
												  WHERE 
													ds_address_type = 'RESIDENCIAL'
												 ) a 
											  inner join ( 
														  SELECT
															num_unique_client,
															prioridade,
															MIN(dt_create_ori) AS dt_create_ori
														  FROM
															(
															  SELECT
																a.*
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
															  inner join (
																		  SELECT
																			num_unique_client,
																			MIN(prioridade) AS prioridade
																		  FROM
																			`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																		  WHERE 
																			ds_address_type = 'RESIDENCIAL'
																		  GROUP BY
																			num_unique_client
																		 ) b 
																 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
															WHERE 
																ds_address_type = 'RESIDENCIAL'	
															)
														WHERE 
															dt_create_ori is not null 
														  GROUP BY
															num_unique_client,
															prioridade  
														)b 
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
										  )
							union all select * 
										from (
											  SELECT
												a.*
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
											  inner join (
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
														  WHERE 
															ds_address_type = 'RESIDENCIAL'
														  GROUP BY
															num_unique_client
														 ) b 
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
											WHERE 
												ds_address_type = 'RESIDENCIAL'
											 )
										WHERE dt_create_ori is null ) a  
						  GROUP BY
							1,
							2,
							3
						  ) b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
              )
union all 
Select * from (
			  SELECT
				a.*
			  FROM  
				(
				  SELECT
					a.*
				  from  
					( 
					  SELECT
						a.*
					  FROM
						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
					  inner join ( 
										  SELECT
											num_unique_client,
											MIN(prioridade) AS prioridade
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
										WHERE 
											ds_address_type = 'COBRANÇA'
										and not exists (
											select * from ( 
															  SELECT
																num_unique_client,
																MIN(prioridade) AS prioridade
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
															  WHERE 
																ds_address_type = 'RESIDENCIAL'
															  GROUP BY
																num_unique_client		
														  ) b 
											where a.num_unique_client = b.num_unique_client 
										)
										  GROUP BY
											num_unique_client	  
								 ) b  
						 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
					WHERE 
						ds_address_type = 'COBRANÇA'
					) a 
				  inner join (  
								  SELECT
									num_unique_client,
									prioridade,
									MIN(dt_create_ori) AS dt_create_ori
								  FROM
									(	
									  SELECT
										a.*
									  FROM
										`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
									  inner join ( 
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
														WHERE 
															ds_address_type = 'COBRANÇA'
														and not exists (
															select * from ( 
																			  SELECT
																				num_unique_client,
																				MIN(prioridade) AS prioridade
																			  FROM
																				`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																			  WHERE 
																				ds_address_type = 'RESIDENCIAL'
																			  GROUP BY
																				num_unique_client		
																		  ) b 
															where a.num_unique_client = b.num_unique_client 
														)
														  GROUP BY
															num_unique_client	  
												 ) b  
										 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
									WHERE 
										ds_address_type = 'COBRANÇA'
									)
								WHERE 
									dt_create_ori is not null 
								  GROUP BY
									num_unique_client,
									prioridade  
							 ) b 
					 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
				) a 
			  inner join ( 
						  SELECT
							num_unique_client,
							prioridade,
							coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
							MIN(num_row) AS num_row
						  FROM (
							select * from (
										  SELECT
											a.*
										  from  
											( 
											  SELECT
												a.*
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
											  inner join ( 
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																WHERE 
																	ds_address_type = 'COBRANÇA'
																and not exists (
																	select * from ( 
																					  SELECT
																						num_unique_client,
																						MIN(prioridade) AS prioridade
																					  FROM
																						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																					  WHERE 
																						ds_address_type = 'RESIDENCIAL'
																					  GROUP BY
																						num_unique_client		
																				  ) b 
																	where a.num_unique_client = b.num_unique_client 
																)
																  GROUP BY
																	num_unique_client	  
														 ) b  
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
											WHERE 
												ds_address_type = 'COBRANÇA'
											) a 
										  inner join (  
														  SELECT
															num_unique_client,
															prioridade,
															MIN(dt_create_ori) AS dt_create_ori
														  FROM
															(	
															  SELECT
																a.*
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
															  inner join ( 
																				  SELECT
																					num_unique_client,
																					MIN(prioridade) AS prioridade
																				  FROM
																					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																				WHERE 
																					ds_address_type = 'COBRANÇA'
																				and not exists (
																					select * from ( 
																									  SELECT
																										num_unique_client,
																										MIN(prioridade) AS prioridade
																									  FROM
																										`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																									  WHERE 
																										ds_address_type = 'RESIDENCIAL'
																									  GROUP BY
																										num_unique_client		
																								  ) b 
																					where a.num_unique_client = b.num_unique_client 
																				)
																				  GROUP BY
																					num_unique_client	  
																		 ) b  
																 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
															WHERE 
																ds_address_type = 'COBRANÇA'
															)
														WHERE 
															dt_create_ori is not null 
														  GROUP BY
															num_unique_client,
															prioridade  
													 ) b 
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
										  )
							union all select * 
										from (
											  SELECT
												a.*
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
											  inner join ( 
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																WHERE 
																	ds_address_type = 'COBRANÇA'
																and not exists (
																	select * from ( 
																					  SELECT
																						num_unique_client,
																						MIN(prioridade) AS prioridade
																					  FROM
																						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																					  WHERE 
																						ds_address_type = 'RESIDENCIAL'
																					  GROUP BY
																						num_unique_client		
																				  ) b 
																	where a.num_unique_client = b.num_unique_client 
																)
																  GROUP BY
																	num_unique_client	  
														 ) b  
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
											WHERE 
												ds_address_type = 'COBRANÇA'
											 )
							WHERE dt_create_ori is null ) a  
						  GROUP BY
							1,
							2,
							3  
						  ) b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
			  )
union all 
Select * from (
			  SELECT
				a.*
			  FROM
				(
				  SELECT
					a.*
				  FROM
					(
				     SELECT
						a.*
					  FROM
						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
					  inner join ( 
									SELECT
										num_unique_client,
										MIN(prioridade) AS prioridade
									  FROM
										`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
									WHERE 
										ds_address_type = 'OUTROS'
									and not exists (
										select * from (
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
														WHERE 
															ds_address_type = 'RESIDENCIAL'
														  GROUP BY
															num_unique_client	
													  ) b 
										 where a.num_unique_client = b.num_unique_client 
									)
									and not exists (
										select * from ( 
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
														  WHERE 
															ds_address_type = 'COBRANÇA'
															and not exists (
																  select * from ( 
																				  SELECT
																					num_unique_client,
																					MIN(prioridade) AS prioridade
																					FROM
																					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																				   WHERE 
																					ds_address_type = 'RESIDENCIAL'
																				  GROUP BY
																					num_unique_client							  
																				) b 
																   where a.num_unique_client = b.num_unique_client 
														)
														  GROUP BY
															num_unique_client	
													  ) b 
										 where a.num_unique_client = b.num_unique_client 
									)
									  GROUP BY
										num_unique_client					  
					             ) b  
						on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
					  WHERE 
						ds_address_type = 'OUTROS'
					) a 
				  inner join ( 
								  SELECT
									num_unique_client,
									prioridade,
									MIN(dt_create_ori) AS dt_create_ori
								  FROM
									(
									  SELECT
										a.*
									  FROM
										`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
									  inner join ( 
												  SELECT
													num_unique_client,
													MIN(prioridade) AS prioridade
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
												WHERE 
													ds_address_type = 'OUTROS'
												and not exists (
													select * from ( 
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																	  WHERE 
																		ds_address_type = 'RESIDENCIAL'
																	  GROUP BY
																		num_unique_client	
																  ) b 
													 where a.num_unique_client = b.num_unique_client 
												)
												and not exists (
													select * from ( 
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																	WHERE 
																		ds_address_type = 'COBRANÇA'
																	and not exists (
																		select * from ( 
																						  SELECT
																							num_unique_client,
																							MIN(prioridade) AS prioridade
																						  FROM
																							`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																						  WHERE 
																							ds_address_type = 'RESIDENCIAL'
																						  GROUP BY
																							num_unique_client		
																					  ) b 
																		where a.num_unique_client = b.num_unique_client 
																	)
																	  GROUP BY
																		num_unique_client	
																  ) b 
													 where a.num_unique_client = b.num_unique_client 
												)
												  GROUP BY
													num_unique_client
												 ) b  
										on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
									WHERE 
										ds_address_type = 'OUTROS'
									)
								WHERE 
									dt_create_ori is not null 
								  GROUP BY
									num_unique_client,
									prioridade  
							 ) b 
					 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
				) a 
			  inner join (  
						  SELECT
							num_unique_client,
							prioridade,
							coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
							MIN(num_row) AS num_row
						  FROM (
							select * from (
											  SELECT
												a.*
											  FROM
												( 
												  SELECT
													a.*
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
												  inner join (

												  SELECT
													num_unique_client,
													MIN(prioridade) AS prioridade
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
												  WHERE 
														ds_address_type = 'OUTROS'
													and not exists (
														select * from (
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																	  WHERE 
																		ds_address_type = 'RESIDENCIAL'
																	  GROUP BY
																		num_unique_client
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													and not exists (
														select * from ( 
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																	 WHERE 
																		ds_address_type = 'COBRANÇA'
																		and not exists (
																		select * from (
																						SELECT
																							num_unique_client,
																							MIN(prioridade) AS prioridade
																						  FROM
																							`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																						WHERE 
																							ds_address_type = 'RESIDENCIAL'
																						  GROUP BY
																							num_unique_client
																					  ) b 
																		where a.num_unique_client = b.num_unique_client 
																		)
																	  GROUP BY
																		num_unique_client		
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													  GROUP BY
														num_unique_client
															 ) b  
													on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
												WHERE 
													ds_address_type = 'OUTROS'
												) a 
											  inner join ( 
															  SELECT
																num_unique_client,
																prioridade,
																MIN(dt_create_ori) AS dt_create_ori
															  FROM
																(
																  SELECT
																	a.*
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
																  inner join ( 
																			  SELECT
																				num_unique_client,
																				MIN(prioridade) AS prioridade
																			  FROM
																				`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																			WHERE 
																				ds_address_type = 'OUTROS'
																			and not exists (
																				select * from ( 
																								  SELECT
																									num_unique_client,
																									MIN(prioridade) AS prioridade
																								  FROM
																									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																								  WHERE 
																									ds_address_type = 'RESIDENCIAL'
																								  GROUP BY
																									num_unique_client	
																							  ) b 
																				 where a.num_unique_client = b.num_unique_client 
																			)
																			and not exists (
																				select * from ( 
																								  SELECT
																									num_unique_client,
																									MIN(prioridade) AS prioridade
																								  FROM
																									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																								WHERE 
																									ds_address_type = 'COBRANÇA'
																								and not exists (
																									select * from ( 
																													  SELECT
																														num_unique_client,
																														MIN(prioridade) AS prioridade
																													  FROM
																														`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																													  WHERE 
																														ds_address_type = 'RESIDENCIAL'
																													  GROUP BY
																														num_unique_client		
																												  ) b 
																									where a.num_unique_client = b.num_unique_client 
																								)
																								  GROUP BY
																									num_unique_client	
																							  ) b 
																				 where a.num_unique_client = b.num_unique_client 
																			)
																			  GROUP BY
																				num_unique_client
																			 ) b  
																	on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
																WHERE 
																	ds_address_type = 'OUTROS'
																)
															WHERE 
																dt_create_ori is not null 
															  GROUP BY
																num_unique_client,
																prioridade  
														 ) b 
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
										  )
							union all select * from (
												  SELECT
													a.*
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
												  inner join (

												  SELECT
													num_unique_client,
													MIN(prioridade) AS prioridade
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
												  WHERE 
														ds_address_type = 'OUTROS'
													and not exists (
														select * from (
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																	  WHERE 
																		ds_address_type = 'RESIDENCIAL'
																	  GROUP BY
																		num_unique_client
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													and not exists (
														select * from ( 
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																	 WHERE 
																		ds_address_type = 'COBRANÇA'
																		and not exists (
																		select * from (
																						SELECT
																							num_unique_client,
																							MIN(prioridade) AS prioridade
																						  FROM
																							`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																						WHERE 
																							ds_address_type = 'RESIDENCIAL'
																						  GROUP BY
																							num_unique_client
																					  ) b 
																		where a.num_unique_client = b.num_unique_client 
																		)
																	  GROUP BY
																		num_unique_client		
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													  GROUP BY
														num_unique_client
															 ) b  
													on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
												WHERE 
													ds_address_type = 'OUTROS'
													)
							WHERE dt_create_ori is null ) a  
						  GROUP BY
							1,
							2,
							3
						 ) b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
			  )
) a 
union all 
select
a.num_unique_client,
a.num_cep,
a.ds_street,
a.ds_neighborhood_name,
a.ds_city_name,
a.ini_state,
a.ds_state_name,
a.ds_country_name,
a.num_house,
a.ds_complement_street,
a.ds_address_type,
a.ds_reference,
a.ds_source_system,
false as ind_main_address,
a.dth_create,
a.dth_update
from (
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
from(
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a  
   where ds_address_type = 'RESIDENCIAL'
except distinct 
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from ( 
		  SELECT
			a.*
		  FROM
			( 
			  SELECT
				a.*
			  FROM
				(
				  SELECT
					a.*
				  FROM
					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
				  inner join (
							  SELECT
								num_unique_client,
								MIN(prioridade) AS prioridade
							  FROM
								`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
							  WHERE 
								ds_address_type = 'RESIDENCIAL'
							  GROUP BY
								num_unique_client
							 ) b 
					 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
				  WHERE 
					ds_address_type = 'RESIDENCIAL'
				 ) a 
			  inner join ( 
						  SELECT
							num_unique_client,
							prioridade,
							MIN(dt_create_ori) AS dt_create_ori
						  FROM
							(
							  SELECT
								a.*
							  FROM
								`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
							  inner join (
										  SELECT
											num_unique_client,
											MIN(prioridade) AS prioridade
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
										  WHERE 
											ds_address_type = 'RESIDENCIAL'
										  GROUP BY
											num_unique_client
										 ) b 
								 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
							WHERE 
								ds_address_type = 'RESIDENCIAL'	
							)
						WHERE 
							dt_create_ori is not null 
						  GROUP BY
							num_unique_client,
							prioridade  
						)b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
			) a 
		  inner join (
					  SELECT
						num_unique_client,
						prioridade,
						coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
						MIN(num_row) AS num_row
					  FROM (
						select * from (
										  SELECT
											a.*
										  FROM
											(
											  SELECT
												a.*
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
											  inner join (
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
														  WHERE 
															ds_address_type = 'RESIDENCIAL'
														  GROUP BY
															num_unique_client
														 ) b 
												 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
											  WHERE 
												ds_address_type = 'RESIDENCIAL'
											 ) a 
										  inner join ( 
													  SELECT
														num_unique_client,
														prioridade,
														MIN(dt_create_ori) AS dt_create_ori
													  FROM
														(
														  SELECT
															a.*
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
														  inner join (
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																	  WHERE 
																		ds_address_type = 'RESIDENCIAL'
																	  GROUP BY
																		num_unique_client
																	 ) b 
															 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
														WHERE 
															ds_address_type = 'RESIDENCIAL'	
														)
													WHERE 
														dt_create_ori is not null 
													  GROUP BY
														num_unique_client,
														prioridade  
													)b 
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
									  )
						union all select * 
									from (
										  SELECT
											a.*
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
										  inner join (
													  SELECT
														num_unique_client,
														MIN(prioridade) AS prioridade
													  FROM
														`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
													  WHERE 
														ds_address_type = 'RESIDENCIAL'
													  GROUP BY
														num_unique_client
													 ) b 
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
										WHERE 
											ds_address_type = 'RESIDENCIAL'
										 )
									WHERE dt_create_ori is null ) a  
					  GROUP BY
						1,
						2,
						3
					  ) b 
			 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
		 ) a  
   where ds_address_type = 'RESIDENCIAL'
)
union all 
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
from(
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a  
   where ds_address_type = 'COBRANÇA'
except distinct 
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from (
		  SELECT
			a.*
		  FROM  
			(
			  SELECT
				a.*
			  from  
				( 
				  SELECT
					a.*
				  FROM
					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
				  inner join ( 
									  SELECT
										num_unique_client,
										MIN(prioridade) AS prioridade
									  FROM
										`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
									WHERE 
										ds_address_type = 'COBRANÇA'
									and not exists (
										select * from ( 
														  SELECT
															num_unique_client,
															MIN(prioridade) AS prioridade
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
														  WHERE 
															ds_address_type = 'RESIDENCIAL'
														  GROUP BY
															num_unique_client		
													  ) b 
										where a.num_unique_client = b.num_unique_client 
									)
									  GROUP BY
										num_unique_client	  
							 ) b  
					 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
				WHERE 
					ds_address_type = 'COBRANÇA'
				) a 
			  inner join (  
							  SELECT
								num_unique_client,
								prioridade,
								MIN(dt_create_ori) AS dt_create_ori
							  FROM
								(	
								  SELECT
									a.*
								  FROM
									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
								  inner join ( 
													  SELECT
														num_unique_client,
														MIN(prioridade) AS prioridade
													  FROM
														`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
													WHERE 
														ds_address_type = 'COBRANÇA'
													and not exists (
														select * from ( 
																		  SELECT
																			num_unique_client,
																			MIN(prioridade) AS prioridade
																		  FROM
																			`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																		  WHERE 
																			ds_address_type = 'RESIDENCIAL'
																		  GROUP BY
																			num_unique_client		
																	  ) b 
														where a.num_unique_client = b.num_unique_client 
													)
													  GROUP BY
														num_unique_client	  
											 ) b  
									 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
								WHERE 
									ds_address_type = 'COBRANÇA'
								)
							WHERE 
								dt_create_ori is not null 
							  GROUP BY
								num_unique_client,
								prioridade  
						 ) b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
			) a 
		  inner join ( 
					  SELECT
						num_unique_client,
						prioridade,
						coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
						MIN(num_row) AS num_row
					  FROM (
						select * from (
									  SELECT
										a.*
									  from  
										( 
										  SELECT
											a.*
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
										  inner join ( 
															  SELECT
																num_unique_client,
																MIN(prioridade) AS prioridade
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
															WHERE 
																ds_address_type = 'COBRANÇA'
															and not exists (
																select * from ( 
																				  SELECT
																					num_unique_client,
																					MIN(prioridade) AS prioridade
																				  FROM
																					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																				  WHERE 
																					ds_address_type = 'RESIDENCIAL'
																				  GROUP BY
																					num_unique_client		
																			  ) b 
																where a.num_unique_client = b.num_unique_client 
															)
															  GROUP BY
																num_unique_client	  
													 ) b  
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
										WHERE 
											ds_address_type = 'COBRANÇA'
										) a 
									  inner join (  
													  SELECT
														num_unique_client,
														prioridade,
														MIN(dt_create_ori) AS dt_create_ori
													  FROM
														(	
														  SELECT
															a.*
														  FROM
															`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
														  inner join ( 
																			  SELECT
																				num_unique_client,
																				MIN(prioridade) AS prioridade
																			  FROM
																				`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																			WHERE 
																				ds_address_type = 'COBRANÇA'
																			and not exists (
																				select * from ( 
																								  SELECT
																									num_unique_client,
																									MIN(prioridade) AS prioridade
																								  FROM
																									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																								  WHERE 
																									ds_address_type = 'RESIDENCIAL'
																								  GROUP BY
																									num_unique_client		
																							  ) b 
																				where a.num_unique_client = b.num_unique_client 
																			)
																			  GROUP BY
																				num_unique_client	  
																	 ) b  
															 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
														WHERE 
															ds_address_type = 'COBRANÇA'
														)
													WHERE 
														dt_create_ori is not null 
													  GROUP BY
														num_unique_client,
														prioridade  
												 ) b 
										 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
									  )
						union all select * 
									from (
										  SELECT
											a.*
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
										  inner join ( 
															  SELECT
																num_unique_client,
																MIN(prioridade) AS prioridade
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
															WHERE 
																ds_address_type = 'COBRANÇA'
															and not exists (
																select * from ( 
																				  SELECT
																					num_unique_client,
																					MIN(prioridade) AS prioridade
																				  FROM
																					`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																				  WHERE 
																					ds_address_type = 'RESIDENCIAL'
																				  GROUP BY
																					num_unique_client		
																			  ) b 
																where a.num_unique_client = b.num_unique_client 
															)
															  GROUP BY
																num_unique_client	  
													 ) b  
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
										WHERE 
											ds_address_type = 'COBRANÇA'
										 )
						WHERE dt_create_ori is null ) a  
					  GROUP BY
						1,
						2,
						3  
					  ) b 
			 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
		 ) a  
   where ds_address_type = 'COBRANÇA'
)
union all 
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
from(
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a  
   where ds_address_type = 'OUTROS'
except distinct 
  select num_unique_client,num_cep,ds_street,ds_neighborhood_name,ds_city_name,ini_state,ds_state_name,
         ds_country_name,num_house,ds_complement_street,ds_address_type,ds_reference,ds_source_system, dth_create, dth_update
    from ( 
		  SELECT
			a.*
		  FROM
			(
			  SELECT
				a.*
			  FROM
				(
					  SELECT
						a.*
					  FROM
						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
					  inner join (

					  SELECT
						num_unique_client,
						MIN(prioridade) AS prioridade
					  FROM
						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
					  WHERE 
							ds_address_type = 'OUTROS'
						and not exists (
							select * from (
										  SELECT
											num_unique_client,
											MIN(prioridade) AS prioridade
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
										  WHERE 
											ds_address_type = 'RESIDENCIAL'
										  GROUP BY
											num_unique_client
										  ) b 
							 where a.num_unique_client = b.num_unique_client 
						)
						and not exists (
							select * from ( 
										  SELECT
											num_unique_client,
											MIN(prioridade) AS prioridade
										  FROM
											`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
										 WHERE 
											ds_address_type = 'COBRANÇA'
											and not exists (
											select * from (
															SELECT
																num_unique_client,
																MIN(prioridade) AS prioridade
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
															WHERE 
																ds_address_type = 'RESIDENCIAL'
															  GROUP BY
																num_unique_client
														  ) b 
											where a.num_unique_client = b.num_unique_client 
											)
										  GROUP BY
											num_unique_client		
										  ) b 
							 where a.num_unique_client = b.num_unique_client 
						)
						  GROUP BY
							num_unique_client
								 ) b  
						on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
					WHERE 
						ds_address_type = 'OUTROS'				
				) a 
			  inner join ( 
							  SELECT
								num_unique_client,
								prioridade,
								MIN(dt_create_ori) AS dt_create_ori
							  FROM
								(
								  SELECT
									a.*
								  FROM
									`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
								  inner join ( 
											  SELECT
												num_unique_client,
												MIN(prioridade) AS prioridade
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
											WHERE 
												ds_address_type = 'OUTROS'
											and not exists (
												select * from ( 
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																  WHERE 
																	ds_address_type = 'RESIDENCIAL'
																  GROUP BY
																	num_unique_client	
															  ) b 
												 where a.num_unique_client = b.num_unique_client 
											)
											and not exists (
												select * from ( 
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																WHERE 
																	ds_address_type = 'COBRANÇA'
																and not exists (
																	select * from ( 
																					  SELECT
																						num_unique_client,
																						MIN(prioridade) AS prioridade
																					  FROM
																						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																					  WHERE 
																						ds_address_type = 'RESIDENCIAL'
																					  GROUP BY
																						num_unique_client		
																				  ) b 
																	where a.num_unique_client = b.num_unique_client 
																)
																  GROUP BY
																	num_unique_client	
															  ) b 
												 where a.num_unique_client = b.num_unique_client 
											)
											  GROUP BY
												num_unique_client
											 ) b  
									on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
								WHERE 
									ds_address_type = 'OUTROS'
								)
							WHERE 
								dt_create_ori is not null 
							  GROUP BY
								num_unique_client,
								prioridade  
						 ) b 
				 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
			) a 
		  inner join (  
					  SELECT
						num_unique_client,
						prioridade,
						coalesce(dt_create_ori,PARSE_TIMESTAMP('%d/%m/%Y %H:%M:%S,000000', '01/01/1900 00:00:01,000000')) as dt_create_ori,
						MIN(num_row) AS num_row
					  FROM (
						select * from (
										  SELECT
											a.*
										  FROM
											(
											  SELECT
												a.*
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
											  inner join (

											  SELECT
												num_unique_client,
												MIN(prioridade) AS prioridade
											  FROM
												`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
											  WHERE 
													ds_address_type = 'OUTROS'
												and not exists (
													select * from (
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																  WHERE 
																	ds_address_type = 'RESIDENCIAL'
																  GROUP BY
																	num_unique_client
																  ) b 
													 where a.num_unique_client = b.num_unique_client 
												)
												and not exists (
													select * from ( 
																  SELECT
																	num_unique_client,
																	MIN(prioridade) AS prioridade
																  FROM
																	`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																 WHERE 
																	ds_address_type = 'COBRANÇA'
																	and not exists (
																	select * from (
																					SELECT
																						num_unique_client,
																						MIN(prioridade) AS prioridade
																					  FROM
																						`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																					WHERE 
																						ds_address_type = 'RESIDENCIAL'
																					  GROUP BY
																						num_unique_client
																				  ) b 
																	where a.num_unique_client = b.num_unique_client 
																	)
																  GROUP BY
																	num_unique_client		
																  ) b 
													 where a.num_unique_client = b.num_unique_client 
												)
												  GROUP BY
													num_unique_client
														 ) b  
												on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
											WHERE 
												ds_address_type = 'OUTROS'															
											) a 
										  inner join ( 
														  SELECT
															num_unique_client,
															prioridade,
															MIN(dt_create_ori) AS dt_create_ori
														  FROM
															(
															  SELECT
																a.*
															  FROM
																`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
															  inner join ( 
																		  SELECT
																			num_unique_client,
																			MIN(prioridade) AS prioridade
																		  FROM
																			`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																		WHERE 
																			ds_address_type = 'OUTROS'
																		and not exists (
																			select * from ( 
																							  SELECT
																								num_unique_client,
																								MIN(prioridade) AS prioridade
																							  FROM
																								`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																							  WHERE 
																								ds_address_type = 'RESIDENCIAL'
																							  GROUP BY
																								num_unique_client	
																						  ) b 
																			 where a.num_unique_client = b.num_unique_client 
																		)
																		and not exists (
																			select * from ( 
																							  SELECT
																								num_unique_client,
																								MIN(prioridade) AS prioridade
																							  FROM
																								`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																							WHERE 
																								ds_address_type = 'COBRANÇA'
																							and not exists (
																								select * from ( 
																												  SELECT
																													num_unique_client,
																													MIN(prioridade) AS prioridade
																												  FROM
																													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																												  WHERE 
																													ds_address_type = 'RESIDENCIAL'
																												  GROUP BY
																													num_unique_client		
																											  ) b 
																								where a.num_unique_client = b.num_unique_client 
																							)
																							  GROUP BY
																								num_unique_client	
																						  ) b 
																			 where a.num_unique_client = b.num_unique_client 
																		)
																		  GROUP BY
																			num_unique_client
																		 ) b  
																on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
															WHERE 
																ds_address_type = 'OUTROS'
															)
														WHERE 
															dt_create_ori is not null 
														  GROUP BY
															num_unique_client,
															prioridade  
													 ) b 
											 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori
									  )
						union all select * from (
												  SELECT
													a.*
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
												  inner join (

												  SELECT
													num_unique_client,
													MIN(prioridade) AS prioridade
												  FROM
													`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
												  WHERE 
														ds_address_type = 'OUTROS'
													and not exists (
														select * from (
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																	  WHERE 
																		ds_address_type = 'RESIDENCIAL'
																	  GROUP BY
																		num_unique_client
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													and not exists (
														select * from ( 
																	  SELECT
																		num_unique_client,
																		MIN(prioridade) AS prioridade
																	  FROM
																		`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
																	 WHERE 
																		ds_address_type = 'COBRANÇA'
																		and not exists (
																		select * from (
																						SELECT
																							num_unique_client,
																							MIN(prioridade) AS prioridade
																						  FROM
																							`br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
																						WHERE 
																							ds_address_type = 'RESIDENCIAL'
																						  GROUP BY
																							num_unique_client
																					  ) b 
																		where a.num_unique_client = b.num_unique_client 
																		)
																	  GROUP BY
																		num_unique_client		
																	  ) b 
														 where a.num_unique_client = b.num_unique_client 
													)
													  GROUP BY
														num_unique_client
															 ) b  
													on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade
												WHERE 
													ds_address_type = 'OUTROS'				
						                        ) 
						WHERE dt_create_ori is null ) a  
					  GROUP BY
						1,
						2,
						3
					 ) b 
			 on a.num_unique_client = b.num_unique_client and a.prioridade = b.prioridade and a.dt_create_ori = b.dt_create_ori and a.num_row = b.num_row
		 ) a  
   where ds_address_type = 'OUTROS'
)
) a

