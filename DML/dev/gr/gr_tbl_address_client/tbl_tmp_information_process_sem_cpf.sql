Select a.*
      ,row_number() over(partition by num_unique_client) as num_row
from (
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  ds_country_name,
  a.num_house,
  a.ds_complement_street,
  CASE
    WHEN TRIM(a.ds_address_type) = 'RESIDENTIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'INVOICE'     THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) = 'RESIDENCIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'COBRANÇA'    THEN 'COBRANÇA'	
/*    WHEN TRIM(a.ds_address_type) IN ('RESIDENTIAL', 'APARTAMENTO', 'CASA') THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) IN ('BILLING','INVOICE') THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) IN ('SHIPPING') THEN 'ENTREGA'
    WHEN TRIM(a.ds_address_type) IN ('COMERCIAL') THEN 'COMERCIAL'
*/	
  ELSE
  'OUTROS' 
END
  AS ds_address_type,
  a.ds_reference,
  'VTEX' AS ds_source_system,
  '1' AS prioridade,
  coalesce(a.dth_create_system_origin,a.dth_partition) as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_address` AS a
INNER JOIN (
  SELECT
    ds_email,
    uuid_client
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client`
  WHERE
    num_cpf IS NULL
    AND ds_email IS NOT NULL ) AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN (
  SELECT
    a.num_unique_client,
    a.ds_email,
    a.ds_source_system,
    a.dth_create,
  a.dth_update
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email` b
  ON
    a.num_unique_client = b.num_unique_client
    AND a.ds_email_type = b.ds_email_type
    AND a.ds_email = b.ds_email
  WHERE
    NOT a.ind_document ) AS c
ON
  TRIM(b.ds_email) = TRIM(c.ds_email)
  AND c.ds_source_system = 'VTEX'
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  'BRA' AS ds_country_name,
  num_house,
  ds_complement_street,
  'RESIDENCIAL' AS ds_address_type,
  NULL AS ds_reference,
  'CSF' AS ds_source_system,
  '2' AS prioridade,
  a.dth_partition as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` AS a
INNER JOIN (
  SELECT
    a.num_unique_client,
    a.ds_email,
    a.ds_source_system,
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email` b
  ON
    a.num_unique_client = b.num_unique_client
    AND a.ds_email_type = b.ds_email_type
    AND a.ds_email = b.ds_email
  WHERE
    NOT a.ind_document ) AS c
ON
  TRIM(a.ds_email) = TRIM(c.ds_email)
  AND c.ds_source_system = 'CSF'
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  a.ini_state,
  d.ds_state AS ds_state_name,
  ds_country_name,
  a.num_house,
  a.ds_complement_street,
  CASE
    WHEN TRIM(a.ds_address_type) = 'RESIDENTIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'INVOICE'     THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) = 'RESIDENCIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'COBRANÇA'    THEN 'COBRANÇA'	
/*    WHEN TRIM(a.ds_address_type) IN ('RESIDENTIAL', 'APARTAMENTO', 'CASA') THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) IN ('BILLING','INVOICE') THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) IN ('SHIPPING') THEN 'ENTREGA'
    WHEN TRIM(a.ds_address_type) IN ('COMERCIAL') THEN 'COMERCIAL'
*/	
  ELSE
  'OUTROS' 
END
  AS ds_address_type,
  a.ds_reference,
  'SVA' AS ds_source_system,
  '3' AS prioridade,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_address` AS a
INNER JOIN (
  SELECT
    ds_email,
    uuid_client
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client`
  WHERE
    num_document IS NULL
    AND ds_email IS NOT NULL ) AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN (
  SELECT
    a.num_unique_client,
    a.ds_email,
    a.ds_source_system,
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email` b
  ON
    a.num_unique_client = b.num_unique_client
    AND a.ds_email_type = b.ds_email_type
    AND a.ds_email = b.ds_email
  WHERE
    NOT a.ind_document ) AS c
ON
  TRIM(b.ds_email) = TRIM(c.ds_email)
  AND c.ds_source_system = 'SVA'
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ini_state)) = UPPER(TRIM(d.ini_state))
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  'BRA' AS ds_country_name,
  num_house,
  ds_complement_street,
  'RESIDENCIAL' AS ds_address_type,
  NULL AS ds_reference,
  'QUALIBEST' AS ds_source_system,
  '4' AS prioridade,
  a.dth_partition as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client` AS a
INNER JOIN (
  SELECT
    a.num_unique_client,
    a.ds_email,
    a.ds_source_system,
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
  INNER JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email` b
  ON
    a.num_unique_client = b.num_unique_client
    AND a.ds_email_type = b.ds_email_type
    AND a.ds_email = b.ds_email
  WHERE
    NOT a.ind_document ) AS c
ON
  TRIM(a.ds_email) = TRIM(c.ds_email)
  AND c.ds_source_system = 'QUALIBEST'
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL
) a;