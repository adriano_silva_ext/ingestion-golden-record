WITH
  tab_aux_address AS (
  SELECT
    num_unique_client,
    ds_address_type,
    MIN(prioridade) AS prioridade
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
  GROUP BY
    num_unique_client,
    ds_address_type ),
  tab_selected_address AS (
  SELECT
    a.*
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
  INNER JOIN
    tab_aux_address b
  ON
    a.num_unique_client   = b.num_unique_client
    AND a.ds_address_type = b.ds_address_type
    AND	a.prioridade      = b.prioridade),
  tab_address_main_address AS (
  SELECT
    num_unique_client,
    MIN(prioridade_main) AS prioridade_main
  FROM (
    SELECT
      num_unique_client,
      ds_address_type,
      CASE
        WHEN ds_address_type = 'RESIDENCIAL' THEN 1
        WHEN ds_address_type = 'COBRANÇA' THEN 2
        ELSE 3 
    END
      AS prioridade_main
    FROM
      tab_selected_address )
  GROUP BY
    num_unique_client )
SELECT
  a.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  a.ini_state,
  a.ds_state_name,
  a.ds_country_name,
  a.num_house,
  a.ds_complement_street,
  a.ds_address_type,
  CASE
    WHEN b.prioridade_main IS NULL THEN FALSE
  ELSE
  TRUE
END
  AS ind_main_address,
  a.ds_reference,
  a.ds_source_system,
  a.dth_create,
  a.dth_update
FROM (
  SELECT
    a.num_unique_client,
    num_cep,
    ds_street,
    ds_neighborhood_name,
    ds_city_name,
    ini_state,
    ds_state_name,
    ds_country_name,
    num_house,
    ds_complement_street,
    ds_address_type,
    CASE
        WHEN ds_address_type = 'RESIDENCIAL' THEN 1
        WHEN ds_address_type = 'COBRANÇA' THEN 2
        ELSE 3 
  END
    AS prioridade_main,
    ds_reference,
    ds_source_system,
    coalesce(b.dth_create,
      CURRENT_TIMESTAMP()) AS dth_create,
    CASE
      WHEN b.dth_create IS NOT NULL THEN COALESCE(b.dth_update, CURRENT_TIMESTAMP())
  END
    AS dth_update
  FROM
    tab_selected_address AS a
  LEFT JOIN
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS b
  ON
    a.num_unique_client = b.num_unique_client ) a
LEFT JOIN
  tab_address_main_address b
ON
  a.num_unique_client = b.num_unique_client
  AND a.prioridade_main = b.prioridade_main