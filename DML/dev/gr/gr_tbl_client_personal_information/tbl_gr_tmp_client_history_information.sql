SELECT distinct 
  hist.num_unique_client,
  1 				AS num_version_golden_record,                   -- vamos ter que rever esse tratamento
  case
    when hist.ds_full_name is not null then hist.ds_full_name
    when hist.ds_full_name is null and hist.ds_first_name is not null and hist.ds_last_name is null then hist.ds_first_name
    when hist.ds_full_name is null and hist.ds_first_name is null and hist.ds_last_name is not null then hist.ds_last_name
    when hist.ds_full_name is null and hist.ds_first_name is not null and hist.ds_last_name is not null then concat(hist.ds_first_name,' ',hist.ds_last_name)
  end AS ds_full_name,
  CASE
    WHEN hist.ds_first_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_first_name),substr(hist.ds_full_name,1,case when STRPOS(hist.ds_full_name, ' ') > 0 then STRPOS(hist.ds_full_name, ' ') else length(hist.ds_full_name)+1 end - 1))	
  END
  AS ds_first_name,
  CASE
    WHEN hist.ds_last_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_last_name),ARRAY_REVERSE(SPLIT(replace(hist.ds_full_name,' ',',')))[SAFE_OFFSET(0)])
  END
  AS ds_last_name,
  'HIST' AS ds_name_source_system,
  hist.num_document AS num_document,
  SHA256(COALESCE(CAST(hist.num_document AS STRING))) AS hash_document,
  'CPF'  AS ds_document_type,
  'HIST' AS ds_document_source_system,
  hist.dth_birth AS dth_birth,
  'HIST' AS ds_birth_source_system,
  CASE
    WHEN hist.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
  END
  AS ini_gender,
  CASE
    WHEN hist.ds_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
  END
  AS ds_gender,
  'HIST' AS ds_gender_source_system,
  CAST(null AS STRING) as ini_marital_status,
  CAST(null AS STRING) as ds_marital_status,
  CAST(null AS STRING) as ds_marital_status_source_system,
  CAST(null AS STRING) as ds_nationality,
  CAST(null AS STRING) as ds_country_birth_name,
  CAST(null AS STRING) as ds_nationality_source_system,
  trim(hist.ind_status_client_csf_carrefour) AS ind_status_client_csf_carrefour,
  trim(hist.nm_status_client_csf_carrefour) AS ds_status_client_csf_carrefour,
  loyat.dth_join_loyalty AS dt_admission_minhas_recompensas,
  case
    WHEN (
	      Select true
		    from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` rh
		   where hist.num_document = rh.num_cpf
	        and STARTS_WITH(ds_status_employee, 'RESC') = FALSE 
			and ds_status_employee is not null 
			and CONTAINS_SUBSTR(ds_status_employee, 'ESTAGI') = False
         ) = True 			 
	THEN TRUE
    ELSE FALSE
  end AS ind_employee,
  FALSE AS ind_has_atacadao,
  case when hist.ind_has_carrefour = 1 then TRUE when hist.ind_has_carrefour = 0 then false else null  
  end AS ind_has_carrefour,
  FALSE AS ind_client_blocked,
  coalesce(hist.ind_portfolio,'0') as ind_portfolio,
  FALSE AS ind_bad_golden_record,
  coalesce(case when hist.ind_deleted = 1 then true when hist.ind_deleted = 0 then false else null end,FALSE) AS ind_deleted,
   hist.ds_channel_create_client      AS ds_channel_create_client,
    coalesce(hist.dth_create,CURRENT_TIMESTAMP()) AS dth_create,
  hist.dth_update,
  coalesce(devid.uuid_token_firebase,
    hist.uuid_token_firebase) AS uuid_token_firebase,
  hist.dth_create_origin      AS dth_create_system_origin,
  hist.dth_last_change        AS dth_update_system_origin
FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` AS hist
LEFT JOIN  (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_device_id_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'DEVICE_ID')      
          ) AS devid
ON
  hist.num_document = devid.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_loyalty_person`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'LOYALTY')   
          ) AS loyat

ON
  hist.num_document = loyat.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_onetrust_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'ONETRUST')    
          ) AS one
ON
  hist.num_document = one.num_cpf
WHERE hist.num_document is not null and
  NOT EXISTS (
  SELECT
    *
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` AS gr
  WHERE
    hist.num_document = gr.num_document
    AND 'CPF' = gr.ds_document_type
    )