With tb_cpfs_multicli as ( 
	Select ds_document_type, num_document, min(num_unique_client) num_unique_client 
	  From `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a
	 Where exists (
				   Select ds_document_type, num_document	
					from (  Select ds_document_type, num_document, count(0) as total
							  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`
							group by ds_document_type, num_document
							having count(0)>1
						 ) b
					where a.ds_document_type    = b.ds_document_type 
					  and a.num_document  = b.num_document 	 	 
				  )
	 Group by ds_document_type, num_document
)
Select  
  a.num_unique_client,
  a.num_version_golden_record,
  a.ds_full_name,
  a.ds_first_name,
  a.ds_last_name,
  a.ds_name_source_system,
  a.num_document,
  a.hash_document,
  a.ds_document_type,
  a.ds_document_source_system,
  a.dth_birth,
  a.ds_birth_source_system,
  a.ini_gender,
  a.ds_gender,
  a.ds_gender_source_system,
  a.ini_marital_status,
  a.ds_marital_status,
  a.ds_marital_status_source_system,
  a.ds_nationality,
  a.ds_country_birth_name,
  a.ds_nationality_source_system,
  a.ind_status_client_csf_carrefour,
  a.ds_status_client_csf_carrefour,
  a.dt_admission_minhas_recompensas,
  a.ind_employee,
  a.ind_has_atacadao,
  a.ind_has_carrefour,
  a.ind_client_blocked,
  a.ind_portfolio,
  case when a.ds_full_name is null or a.ds_first_name is null or a.ds_last_name is null or a.ind_deleted is null or a.num_version_golden_record is null 
         or a.ds_channel_create_client is null or a.dth_create is null or not coalesce(c.cpf_valido,true) 
		 or REGEXP_CONTAINS(trim(a.ds_last_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
		 or REGEXP_CONTAINS(trim(a.ds_first_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
		 or REGEXP_CONTAINS(trim(a.ds_full_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
         or not REGEXP_CONTAINS(trim(a.ds_last_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")
         or not REGEXP_CONTAINS(trim(a.ds_first_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")
         or not REGEXP_CONTAINS(trim(a.ds_full_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")		 
		 then True else False 
  end as ind_bad_golden_record,				  
  a.ind_deleted,
  a.ds_channel_create_client,
  a.dth_create,
  a.dth_update,
  a.uuid_token_firebase,
  a.dth_create_system_origin,
  a.dth_update_system_origin,  
  coalesce(b.lst_client_origins,hist.lst_client_origins) as lst_client_origins
  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` a 
  inner join tb_cpfs_multicli d
			on a.ds_document_type   = d.ds_document_type
		   and a.num_document = d.num_document
		   and a.num_unique_client   = d.num_unique_client
 LEFT JOIN `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` AS hist
 ON a.num_document = hist.num_document
 LEFT JOIN (
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('RH' AS STRING), CAST('DEVICE ID' AS STRING), CAST('LOYALTY PERSON' AS STRING)]) AS origem
   where uuid_token_firebase is not null and ind_employee and dt_admission_minhas_recompensas is not null  
   GROUP BY
     num_unique_client 
   union all
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('RH' AS STRING), CAST('DEVICE ID' AS STRING)]) AS origem
   where uuid_token_firebase is not null and ind_employee and dt_admission_minhas_recompensas is null  
   GROUP BY
     num_unique_client 
   union all
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('DEVICE ID' AS STRING), CAST('LOYALTY PERSON' AS STRING)]) AS origem
   where uuid_token_firebase is not null and not ind_employee and dt_admission_minhas_recompensas is not null  
   GROUP BY
     num_unique_client 
   union all 
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('RH' AS STRING), CAST('LOYALTY PERSON' AS STRING)]) AS origem
   where uuid_token_firebase is null and ind_employee and dt_admission_minhas_recompensas is not null  
   GROUP BY
     num_unique_client 
   union all 
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('DEVICE ID' AS STRING)]) AS origem
   where uuid_token_firebase is not null and not ind_employee and dt_admission_minhas_recompensas is null  
   GROUP BY
     num_unique_client 
   union all 
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('LOYALTY PERSON' AS STRING)]) AS origem
   where uuid_token_firebase is null and not ind_employee and dt_admission_minhas_recompensas is not null  
   GROUP BY
     num_unique_client 
   union all 
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING), CAST('RH' AS STRING)]) AS origem
   where uuid_token_firebase is null and ind_employee and dt_admission_minhas_recompensas is null  
   GROUP BY
     num_unique_client 
   union all 
   SELECT
     num_unique_client,
     ARRAY_AGG(DISTINCT origem IGNORE NULLS) AS lst_client_origins
   FROM
     `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`,
     UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST('CSF' AS STRING)]) AS origem
   where uuid_token_firebase is null and not ind_employee and dt_admission_minhas_recompensas is null  
   GROUP BY
     num_unique_client    
     ) b
  ON
    a.num_unique_client = b.num_unique_client
  LEFT JOIN (
    Select num_unique_client
          ,case when dig1 = cast(substr(num_cpf,10,1) as integer) and
                   cast(substr(num_cpf,11,1) as integer) = 
                   mod(mod(
                      cast(SUBSTR(num_cpf,2,1) as integer)   + cast(SUBSTR(num_cpf,3,1)as integer) *2 + cast(SUBSTR(num_cpf,4,1) as integer)*3 + 
                      cast(SUBSTR(num_cpf,5,1) as integer)*4 + cast(SUBSTR(num_cpf,6,1)as integer) *5 + cast(SUBSTR(num_cpf,7,1) as integer)*6 + 
                      cast(SUBSTR(num_cpf,8,1) as integer)*7 + cast(SUBSTR(num_cpf,9,1)as integer) *8 + dig1          *9, 11) , 10)
              then true else false end as cpf_valido
  	From (Select num_unique_client, 
  			     num_cpf,
  				 mod(mod(
  				 cast(SUBSTR(num_cpf,1,1) as integer)   + cast(SUBSTR(num_cpf,2,1) as integer) *2 + cast(SUBSTR(num_cpf,3,1) as integer) *3+
  				 cast(SUBSTR(num_cpf,4,1) as integer)*4 + cast(SUBSTR(num_cpf,5,1) as integer)*5 + cast(SUBSTR(num_cpf,6,1) as integer)*6+
  				 cast(SUBSTR(num_cpf,7,1) as integer)*7 + cast(SUBSTR(num_cpf,8,1) as integer)*8 + cast(SUBSTR(num_cpf,9,1) as integer)*9, 11), 10)  as dig1
  		   From (Select  num_unique_client,
  					     lpad(cast(num_document as string),11,'0') as num_cpf  
  				   FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process`) x ) x 
  )  c
  ON
    a.num_unique_client = c.num_unique_client;