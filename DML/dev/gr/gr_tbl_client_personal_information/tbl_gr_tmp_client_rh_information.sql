SELECT DISTINCT
  COALESCE(hist.num_unique_client,oldgr.num_unique_client,generate_uuid()) AS num_unique_client,
  COALESCE(oldgr.num_version_golden_record+1,1) AS num_version_golden_record,                   -- vamos ter que rever esse tratamento
  COALESCE(rh.ds_full_name,
    sva.ds_full_name,
--    CONCAT(propz.ds_first_name,' ',propz.ds_last_name),
    neo.ds_full_name,
    quali.ds_full_name,
	oldgr.ds_full_name,	
	hist.ds_full_name,CONCAT(hist.ds_first_name,' ',hist.ds_last_name)) AS ds_full_name,
  CASE
    WHEN rh.ds_full_name IS NOT NULL THEN substr(rh.ds_full_name,1,case when STRPOS(rh.ds_full_name, ' ') > 0 then STRPOS(rh.ds_full_name, ' ') else length(rh.ds_full_name)+1 end - 1)
    WHEN sva.ds_full_name IS NOT NULL THEN substr(sva.ds_full_name,1,case when STRPOS(sva.ds_full_name, ' ') > 0 then STRPOS(sva.ds_full_name, ' ') else length(sva.ds_full_name)+1 end - 1)
--    WHEN propz.ds_first_name IS NOT NULL THEN propz.ds_first_name
    WHEN neo.ds_full_name IS NOT NULL THEN substr(neo.ds_full_name,1,case when STRPOS(neo.ds_full_name, ' ') > 0 then STRPOS(neo.ds_full_name, ' ') else length(neo.ds_full_name)+1 end - 1)
    WHEN quali.ds_full_name IS NOT NULL THEN substr(quali.ds_full_name,1,case when STRPOS(quali.ds_full_name, ' ') > 0 then STRPOS(quali.ds_full_name, ' ') else length(quali.ds_full_name)+1 end - 1)
    WHEN oldgr.ds_full_name IS NOT NULL THEN substr(oldgr.ds_full_name,1,case when STRPOS(oldgr.ds_full_name, ' ') > 0 then STRPOS(oldgr.ds_full_name, ' ') else length(oldgr.ds_full_name)+1 end - 1)
    WHEN hist.ds_first_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_first_name),substr(hist.ds_full_name,1,case when STRPOS(hist.ds_full_name, ' ') > 0 then STRPOS(hist.ds_full_name, ' ') else length(hist.ds_full_name)+1 end - 1))	
END
  AS ds_first_name,
  CASE
    WHEN rh.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(rh.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
    WHEN sva.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(sva.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
--    WHEN propz.ds_last_name IS NOT NULL THEN propz.ds_last_name
    WHEN neo.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(neo.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
    WHEN quali.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(quali.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
    WHEN oldgr.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(oldgr.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
    WHEN hist.ds_last_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_last_name),ARRAY_REVERSE(SPLIT(replace(hist.ds_full_name,' ',',')))[SAFE_OFFSET(0)])
END
  AS ds_last_name,
  CASE
    WHEN rh.ds_full_name IS NOT NULL THEN 'RH'
    WHEN sva.ds_full_name IS NOT NULL THEN 'SVA'
--    WHEN CONCAT(propz.ds_first_name,' ',propz.ds_last_name) IS NOT NULL THEN 'PROPZ'
    WHEN neo.ds_full_name IS NOT NULL THEN 'NEOASSIST'
    WHEN quali.ds_full_name IS NOT NULL THEN 'QUALIBEST'
    WHEN oldgr.ds_full_name IS NOT NULL THEN oldgr.ds_name_source_system
    WHEN hist.ds_full_name IS NOT NULL or hist.ds_first_name IS NOT NULL or hist.ds_last_name IS NOT NULL THEN 'HIST'
	ELSE 'HIST'	
END
  AS ds_name_source_system,
  COALESCE(quali.num_cpf,
    rh.num_cpf,
    sva.num_document,
--    propz.num_cpf,
    neo.num_cpf,
	oldgr.num_document,
	hist.num_document) AS num_document,
  SHA256(COALESCE(CAST(quali.num_cpf AS STRING),
      CAST(rh.num_cpf AS STRING),
      CAST(sva.num_document AS STRING),
--     CAST(propz.num_cpf AS STRING),
      CAST(neo.num_cpf AS STRING),
  	  CAST(oldgr.num_document AS STRING),
	  CAST(hist.num_document AS STRING))) AS hash_document,
  CASE
    WHEN quali.num_cpf IS NOT NULL THEN 'CPF'
    WHEN rh.num_cpf IS NOT NULL THEN 'CPF'
    WHEN sva.num_document IS NOT NULL THEN sva.ds_document_type
  ELSE
  'CPF'
END
  AS ds_document_type,
  CASE
    WHEN quali.num_cpf IS NOT NULL THEN 'QUALIBEST'
    WHEN rh.num_cpf IS NOT NULL THEN 'RH'
    WHEN sva.num_document IS NOT NULL THEN 'SVA'
--    WHEN propz.num_cpf IS NOT NULL THEN 'PROPZ'
    WHEN neo.num_cpf IS NOT NULL THEN 'NEOASSIST'
	WHEN oldgr.ds_full_name IS NOT NULL THEN oldgr.ds_document_source_system
	WHEN hist.num_document is not null then 'HIST'
END
  AS ds_document_source_system,
  COALESCE(quali.dth_birth,
    rh.dth_birth,
    sva.dth_birth,
--    propz.dth_birth,
    oldgr.dth_birth,
	hist.dth_birth) AS dth_birth,
  CASE
    WHEN quali.dth_birth IS NOT NULL THEN 'QUALIBEST'
    WHEN rh.dth_birth IS NOT NULL THEN 'RH'
    WHEN sva.dth_birth IS NOT NULL THEN 'SVA'
--    WHEN propz.dth_birth IS NOT NULL THEN 'PROPZ'
	WHEN oldgr.dth_birth IS NOT NULL THEN oldgr.ds_birth_source_system
    WHEN hist.dth_birth IS NOT NULL THEN 'HIST'
END
  AS ds_birth_source_system,
  CASE
    WHEN sva.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = sva.ini_gender AND ds_source_system = 'SVA')
--    WHEN propz.ini_gender IS NOT NULL THEN (
--  SELECT
--    ini_gender
--  FROM
--    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender`
--  WHERE
--    ini_gender_origin = propz.ini_gender
--    AND ds_source_system = 'PROPZ')
	WHEN oldgr.ini_gender is not null then oldgr.ini_gender
    WHEN hist.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
END
  AS ini_gender,
  CASE
    WHEN sva.ini_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = sva.ini_gender AND ds_source_system = 'SVA')
--    WHEN propz.ini_gender IS NOT NULL THEN (
--  SELECT
--    ds_gender
--  FROM
--    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender`
--  WHERE
--    ini_gender_origin = propz.ini_gender
--    AND ds_source_system = 'PROPZ')
	WHEN oldgr.ds_gender is not null then oldgr.ds_gender
    WHEN hist.ds_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
END
  AS ds_gender,
  CASE
    WHEN sva.ini_gender IS NOT NULL THEN 'SVA'
--    WHEN propz.ini_gender IS NOT NULL THEN 'PROPZ'
	WHEN oldgr.ini_gender IS NOT NULL or oldgr.ds_gender IS NOT NULL THEN oldgr.ds_gender_source_system
    WHEN hist.ini_gender IS NOT NULL THEN 'HIST'
END
  AS ds_gender_source_system,
  CASE
    WHEN sva.ini_marital_status IS NOT NULL THEN ( SELECT sva.ini_marital_status FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_marital` WHERE ini_marital_status_origin = sva.ini_marital_status AND ds_source_system = 'SVA')
END
  AS ini_marital_status,
  CASE
    WHEN sva.ini_marital_status IS NOT NULL THEN ( SELECT ds_marital_status FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_aux_marital` WHERE ini_marital_status_origin = sva.ini_marital_status AND ds_source_system = 'SVA')
END
  AS ds_marital_status,
  CASE
    WHEN sva.ini_marital_status IS NOT NULL THEN 'SVA'
END
  AS ds_marital_status_source_system,
  coalesce(sva.ds_nationality,oldgr.ds_nationality) 				as ds_nationality,
  coalesce(sva.ds_country_birth_name,oldgr.ds_country_birth_name)	as ds_country_birth_name,
  CASE
    WHEN sva.ds_nationality IS NOT NULL OR sva.ds_country_birth_name IS NOT NULL THEN 'SVA'
	WHEN oldgr.ds_nationality IS NOT NULL THEN oldgr.ds_nationality_source_system
END
  AS ds_nationality_source_system,
  coalesce(oldgr.ind_status_client_csf_carrefour,hist.ind_status_client_csf_carrefour) AS ind_status_client_csf_carrefour,	
  coalesce(oldgr.ds_status_client_csf_carrefour,trim(hist.nm_status_client_csf_carrefour)) AS ds_status_client_csf_carrefour,
  coalesce(loyat.dth_join_loyalty,
  oldgr.dt_admission_minhas_recompensas) AS dt_admission_minhas_recompensas,
  CASE
    WHEN STARTS_WITH(ds_status_employee, 'RESC') = FALSE and ds_status_employee is not null and CONTAINS_SUBSTR(ds_status_employee, 'ESTAGI') = False THEN TRUE
    ELSE FALSE
END
  AS ind_employee,
  coalesce(oldgr.ind_has_atacadao,
    FALSE) AS ind_has_atacadao,
  case when oldgr.ind_has_carrefour is not null then oldgr.ind_has_carrefour
       when hist.ind_has_carrefour = 1 then TRUE when hist.ind_has_carrefour = 0 then false else null 
  end AS ind_has_carrefour,
  coalesce(oldgr.ind_client_blocked,
    FALSE) AS ind_client_blocked,
  coalesce(oldgr.ind_portfolio,hist.ind_portfolio,'0') as ind_portfolio,
  FALSE AS ind_bad_golden_record,
  coalesce(oldgr.ind_deleted,case when hist.ind_deleted = 1 then true when hist.ind_deleted = 0 then false else null end,
    FALSE) AS ind_deleted,
  CASE
    WHEN rh.num_cpf IS NOT NULL THEN 'RH'
    WHEN sva.num_document IS NOT NULL THEN 'SVA'
--    WHEN propz.num_cpf IS NOT NULL THEN 'PROPZ'
    WHEN neo.num_cpf IS NOT NULL THEN 'NEOASSIST'
    WHEN quali.num_cpf IS NOT NULL THEN 'QUALIBEST'
    WHEN hist.ds_channel_create_client is not null then hist.ds_channel_create_client  
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.ds_channel_create_client
END
  AS ds_channel_create_client,
    coalesce(oldgr.dth_create,
    CURRENT_TIMESTAMP()) AS dth_create,
  CASE
    WHEN oldgr.dth_create IS NOT NULL THEN CURRENT_TIMESTAMP()
END
  AS dth_update,
  coalesce(devid.uuid_token_firebase,oldgr.uuid_token_firebase,
    hist.uuid_token_firebase) AS uuid_token_firebase,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_create_origin
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_create_system_origin
    WHEN rh.num_cpf IS NOT NULL THEN rh.dth_partition
    WHEN sva.num_document IS NOT NULL THEN sva.dth_create
--    WHEN propz.num_cpf IS NOT NULL THEN propz.dth_create
    WHEN neo.num_cpf IS NOT NULL THEN neo.dth_create
    WHEN quali.num_cpf IS NOT NULL THEN quali.dth_partition
END
  AS dth_create_system_origin,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_last_change
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_update_system_origin
    WHEN rh.num_cpf IS NOT NULL THEN rh.dth_partition
    WHEN sva.num_document IS NOT NULL THEN sva.dth_update
--    WHEN propz.num_cpf IS NOT NULL THEN propz.dth_update
    WHEN neo.num_cpf IS NOT NULL THEN neo.dth_update
    WHEN quali.num_cpf IS NOT NULL THEN quali.dth_partition
END
  AS dth_update_system_origin
FROM (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'RH')  
     ) AS rh
LEFT JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS oldgr
ON
  rh.num_cpf = oldgr.num_document
  AND oldgr.ds_document_type = 'CPF'
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'SVA')  
          ) AS sva
ON
  rh.num_cpf = sva.num_document
  AND sva.ds_document_type = 'CPF'
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'PROPZ')  
          ) AS propz
ON
  rh.num_cpf = propz.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'NEOASSIST')  
          ) AS neo
ON
  rh.num_cpf = neo.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'QUALIBEST')  
          ) AS quali
ON
  rh.num_cpf = quali.num_cpf
LEFT JOIN 
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` AS hist
ON
  rh.num_cpf = hist.num_document    
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_device_id_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'DEVICE_ID')    
          ) AS devid
ON
  rh.num_cpf = devid.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_loyalty_person`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'LOYALTY')  
          ) AS loyat
ON
  rh.num_cpf = loyat.num_cpf
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_onetrust_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'ONETRUST')  
		  ) AS one
ON
  rh.num_cpf = one.num_cpf
WHERE
  rh.num_cpf IS NOT NULL
  AND NOT EXISTS (
  SELECT
    *
  FROM
    `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_tmp_information_process` AS gr
  WHERE
    rh.num_cpf = gr.num_document
    AND 'CPF' = gr.ds_document_type
    )