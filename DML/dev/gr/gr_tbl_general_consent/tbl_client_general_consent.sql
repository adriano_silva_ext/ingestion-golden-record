CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_general_consent`
(
  num_unique_client STRING OPTIONS(description=" NUC - Unique Customer Number / NUC - Número Único Cliente"),
  cod_consent_privacy_policy STRING OPTIONS(description="Consent Code Privacy Policy / Código Do Consentimento Da Política De Privacidade"),
  dth_consent_privacy_policy TIMESTAMP OPTIONS(description="Consent Date Privacy Policy / Data Do Consentimento Da Política De Privacidade"),
  cod_consent_purchase_status STRING OPTIONS(description="Consent Code Purchase Status / Código Do Consentimento Do Status Compra"),
  dth_consent_purchase_status TIMESTAMP OPTIONS(description="Consent Date Purchase Status / Data Do Consentimento Do Status Compra"),
  cod_consent_sharing_third STRING OPTIONS(description="Consent Code Share Data with Third Parties / Código Do Consentimento De Compartilhamento De Dados Com Terceiros"),
  dth_consent_sharing_third TIMESTAMP OPTIONS(description="Consent Date Share Data with Third Parties / Data Do Consentimento De Compartilhamento De Dados Com Terceiros"),
  cod_source STRING OPTIONS(description="Source Code / Código De Origem"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Consent Table Privacy Data / Tabela De Consentimento Dados De Privacidade De Pessoas"
)
AS
SELECT DISTINCT 
  b.num_unique_client,
  a.cod_consent_privacy_policy,
  a.dth_consent_privacy_policy,
  a.cod_consent_purchase_status,
  a.dth_consent_purchase_status,
  a.cod_consent_sharing_third,
  a.dth_consent_sharing_third,
  a.cod_source,
  a.dth_create,
  a.dth_update 
FROM
   `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_onetrust_client` a
INNER JOIN
  `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` b
ON
  a.num_cpf = b.num_document
WHERE a.num_cpf IS NOT NULL
  AND b.num_document IS NOT NULL