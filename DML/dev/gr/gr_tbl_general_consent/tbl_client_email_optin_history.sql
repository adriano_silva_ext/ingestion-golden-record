select distinct
      a.num_unique_client,
      a.ds_email,
      case when a.cod_consent_offer_email = 0 then 'REV'
           when a.cod_consent_offer_email = 1 then 'CON'
      end                   as cod_consent_offer_email,     
      a.dth_create_origin   as dth_consent_offer_email,
      'CSF'                 as cod_source,
      a.dth_create,
      a.dth_update
from `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_history_client` a
inner join `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_personal_information` b
        on a.num_unique_client = b.num_unique_client and a.num_document = b.num_document
where a.cod_consent_offer_email is not null and a.ds_email is not null 
  and not exists (
    Select *
	  from `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_client_email_optin` c 
	 where a.num_unique_client = c.num_unique_client
       and upper(trim(a.ds_email)) = upper(trim(c.ds_email)) 	 
  );