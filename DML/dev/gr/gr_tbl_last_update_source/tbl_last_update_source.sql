CREATE OR REPLACE TABLE `br-apps-bi-customermdm-dev.db_dolphin_target_clientmdm.tbl_last_update_source`
(
  ds_source_name STRING OPTIONS(description="Source System / Sistema Origem"),
  dth_partition TIMESTAMP OPTIONS(description="Partição da tabela Origem / Origin Table partition")
)
OPTIONS(
  description="Customer base last update table / Tabela da última atualização da base de clientes"
)
AS
SELECT
  'VTEX'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_vtex_client`
UNION ALL 
SELECT
  'CSF'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client`
UNION ALL 
SELECT
  'RH'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client`
UNION ALL 
SELECT
  'SVA'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_sva_client`
UNION ALL 
SELECT
  'PROPZ'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_propz_client`
UNION ALL 
SELECT
  'NEOASSIST'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client`
UNION ALL 
SELECT
  'QUALIBEST'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_qualibest_client`
UNION ALL 
SELECT
  'DEVICE_ID'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_device_id_client`
UNION ALL 
SELECT
  'ONETRUST'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_onetrust_client`
UNION ALL 
SELECT
  'LOYALTY'                         as ds_source_name,
  max(dth_partition) as dth_partition
FROM `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_loyalty_person`
;
