MERGE
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_device_id_client` tgt
USING
  (
  WITH device_id_client AS
(
SELECT 
  UPPER(email) as ds_email
  ,UPPER(platform) as ind_android_ios
  ,SAFE_CAST(REGEXP_REPLACE(customerId, r'[.,-]', '') AS NUMERIC) AS num_cpf
  ,UPPER(deviceId) as uuid_device
  ,UPPER(uid) as uuid
  ,UPPER(firebasedevicetoken) as uuid_token_firebase
  ,dth_partition
  ,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(customerId, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
  FROM  `br-apps-bi-customermdm-dev.db_dolphin_stage_client_base.tbl_device_id_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    device_id_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET ds_email = stg.ds_email,
  ind_android_ios = stg.ind_android_ios, 
  uuid_device = stg.uuid_device,
  uuid = stg.uuid,
  uuid_token_firebase = stg.uuid_token_firebase,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (ds_email,ind_android_ios,num_cpf,uuid_device,uuid,uuid_token_firebase,dth_partition,dth_create)
VALUES
  (stg.ds_email, stg.ind_android_ios, stg.num_cpf, stg.uuid_device, stg.uuid, stg.uuid_token_firebase, stg.dth_partition,stg.dth_partition)