MERGE
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_neoassist_client` tgt
USING
  (
  WITH neoassist_client AS
(
SELECT
  UPPER(customerid) as uuid_consumer
  ,'' as lst_personalization_field
  ,UPPER(fieldc) as ds_city_origin
  ,'' as ds_client_classification
  ,SAFE_CAST(REGEXP_REPLACE(fieldu, r'[.,-]', '') AS NUMERIC) as num_cpf
  ,timestamp(date  , "America/Sao_Paulo") as dth_create_system_origin
  ,timestamp(dateupdate , "America/Sao_Paulo") as dth_update_system_origin
  ,UPPER(fielda) as ds_street
  ,UPPER(fieldd) as ds_state
  ,UPPER(cpfs) as lst_cpfs
  ,UPPER(email) as ds_email
  ,UPPER(telefones) as lst_phone_number
  ,UPPER(name) as ds_full_name
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(fieldb, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(fieldb, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(fieldb, r'[\s()-]', '') END as num_mobile_phone  
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(fielde, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(fielde, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(fielde, r'[\s()-]', '') END as num_mobile_phone_2
  ,UPPER(emails) as lst_email
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(fieldu, r'[.,-]', '') AS NUMERIC) ORDER BY SAFE_CAST(dateupdate AS TIMESTAMP) DESC) RANK
FROM  `br-apps-bi-customermdm-dev.db_dolphin_stage_client_base.tbl_neoassist_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    neoassist_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET lst_personalization_field = stg.lst_personalization_field,
  uuid_consumer = stg.uuid_consumer, 
  ds_city_origin = stg.ds_city_origin,
  ds_client_classification = stg.ds_client_classification,
  num_cpf = stg.num_cpf,
  dth_create_system_origin = stg.dth_create_system_origin,
  dth_update_system_origin = stg.dth_update_system_origin,
  ds_street = stg.ds_street,
  ds_state = stg.ds_state,
  lst_cpfs = stg.lst_cpfs,
  ds_email = stg.ds_email,
  lst_phone_number = stg.lst_phone_number,
  ds_full_name = stg.ds_full_name,
  num_mobile_phone = stg.num_mobile_phone,
  num_mobile_phone_2 = stg.num_mobile_phone_2,
  lst_email = stg.lst_email,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (lst_personalization_field,uuid_consumer,ds_city_origin,ds_client_classification,num_cpf,dth_create_system_origin,dth_update_system_origin,ds_street,ds_state,lst_cpfs,ds_email,lst_phone_number,ds_full_name,num_mobile_phone,num_mobile_phone_2,lst_email,dth_partition,dth_create)
VALUES
  (stg.lst_personalization_field, stg.uuid_consumer, stg.ds_city_origin, stg.ds_client_classification, stg.num_cpf,stg.dth_create_system_origin,stg.dth_update_system_origin,stg.ds_street,stg.ds_state,stg.lst_cpfs,stg.ds_email,stg.lst_phone_number,stg.ds_full_name,stg.num_mobile_phone,stg.num_mobile_phone_2,stg.lst_email,stg.dth_partition,stg.dth_partition)