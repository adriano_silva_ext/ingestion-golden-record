MERGE
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_rh_client` tgt
USING
  (
  WITH rh_client AS
(
SELECT 
  UPPER(ns0cod_empresa) as cod_company
  ,UPPER(ns0matricula_func) as cod_registration
  ,UPPER(ns0cod_sit_func) as cod_status
  ,UPPER(ns0cod_lotacao) as cod_stocking
  ,UPPER(ns0email_pessoal) as ds_email
  ,UPPER(ns0email_corp) as ds_email_business
  ,UPPER(ns0desc_tipo_situacao) as ds_situation_type
  ,UPPER(ns0desc_situacao) as ds_status_employee
  ,CASE WHEN CONTAINS_SUBSTR(ns0data_contratacao, '-') THEN TIMESTAMP(ns0data_contratacao) ELSE TIMESTAMP(CONCAT(SUBSTR(ns0data_contratacao,0,4),'-',SUBSTR(ns0data_contratacao,5,2),'-',SUBSTR(ns0data_contratacao,7,2)) , "America/Sao_Paulo") END AS dth_admission
  ,CASE WHEN CONTAINS_SUBSTR(ns0data_nasc, '-') THEN TIMESTAMP(ns0data_nasc) ELSE TIMESTAMP(CONCAT(SUBSTR(ns0data_nasc,0,4),'-',SUBSTR(ns0data_nasc,5,2),'-',SUBSTR(ns0data_nasc,7,2)) , "America/Sao_Paulo") END AS dth_birth
  ,UPPER(ns0tipo_situacao) as ind_situation_type
  ,UPPER(ns0nome) as ds_full_name
  ,UPPER(ns0desc_empresa) as ds_company_name
  ,UPPER(ns0name_lotacao) as ds_stocking_name
  ,UPPER(ns0local_trabalho) as ds_workplace_name
  ,SAFE_CAST(REGEXP_REPLACE(ns0cpf, r'[.,-]', '')AS NUMERIC) as num_cpf
  ,SAFE_CAST(REGEXP_REPLACE(ns0drt, r'[.,-]', '')AS NUMERIC) as num_employe_drt
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(ns0cellphone, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(ns0cellphone, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(ns0cellphone, r'[\s()-]', '') END as num_mobile_phone
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(ns0cpf, r'[.,-]', '')AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-dev.db_dolphin_stage_client_base.tbl_rh_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    rh_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET 
  cod_company = stg.cod_company,
  cod_registration = stg.cod_registration, 
  cod_status = stg.cod_status,
  cod_stocking = stg.cod_stocking,
  ds_email = stg.ds_email,
  ds_email_business = stg.ds_email_business,
  ds_situation_type = stg.ds_situation_type,
  ds_status_employee = stg.ds_status_employee,
  dth_admission = stg.dth_admission,
  dth_birth = stg.dth_birth,
  ind_situation_type = stg.ind_situation_type,
  ds_full_name = stg.ds_full_name,
  ds_company_name = stg.ds_company_name,
  ds_stocking_name = stg.ds_stocking_name,
  ds_workplace_name = stg.ds_workplace_name,
  num_cpf = stg.num_cpf,
  num_employe_drt = stg.num_employe_drt,
  num_mobile_phone = stg.num_mobile_phone,
  dth_partition = CASE WHEN tgt.cod_company != stg.cod_company OR tgt.cod_registration != stg.cod_registration OR tgt.cod_status != stg.cod_status OR tgt.cod_stocking != stg.cod_stocking OR tgt.ds_email != stg.ds_email OR tgt.ds_email_business != stg.ds_email_business OR tgt.ds_situation_type != stg.ds_situation_type OR tgt.ds_status_employee != stg.ds_status_employee OR tgt.dth_admission != stg.dth_admission OR tgt.dth_birth != stg.dth_birth OR tgt.ind_situation_type != stg.ind_situation_type OR tgt.ds_full_name != stg.ds_full_name OR tgt.ds_company_name != stg.ds_company_name OR tgt.ds_stocking_name != stg.ds_stocking_name OR tgt.ds_workplace_name != stg.ds_workplace_name OR tgt.num_employe_drt != stg.num_employe_drt OR tgt.num_mobile_phone != stg.num_mobile_phone THEN stg.dth_partition ELSE tgt.dth_partition END,
  dth_update = CASE WHEN tgt.cod_company != stg.cod_company OR tgt.cod_registration != stg.cod_registration OR tgt.cod_status != stg.cod_status OR tgt.cod_stocking != stg.cod_stocking OR tgt.ds_email != stg.ds_email OR tgt.ds_email_business != stg.ds_email_business OR tgt.ds_situation_type != stg.ds_situation_type OR tgt.ds_status_employee != stg.ds_status_employee OR tgt.dth_admission != stg.dth_admission OR tgt.dth_birth != stg.dth_birth OR tgt.ind_situation_type != stg.ind_situation_type OR tgt.ds_full_name != stg.ds_full_name OR tgt.ds_company_name != stg.ds_company_name OR tgt.ds_stocking_name != stg.ds_stocking_name OR tgt.ds_workplace_name != stg.ds_workplace_name OR tgt.num_employe_drt != stg.num_employe_drt OR tgt.num_mobile_phone != stg.num_mobile_phone THEN stg.dth_partition ELSE tgt.dth_partition END
  WHEN NOT MATCHED
  THEN
INSERT
  (
  cod_company,
  cod_registration,
  cod_status,
  cod_stocking,
  ds_email,
  ds_email_business,
  ds_situation_type,
  ds_status_employee,
  dth_admission,
  dth_birth,
  ind_situation_type,
  ds_full_name,
  ds_company_name,
  ds_stocking_name,
  ds_workplace_name,
  num_cpf,
  num_employe_drt,
  num_mobile_phone,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.cod_company,
  stg.cod_registration,
  stg.cod_status,
  stg.cod_stocking,
  stg.ds_email,
  stg.ds_email_business,
  stg.ds_situation_type,
  stg.ds_status_employee,
  stg.dth_admission,
  stg.dth_birth,
  stg.ind_situation_type,
  stg.ds_full_name,
  stg.ds_company_name,
  stg.ds_stocking_name,
  stg.ds_workplace_name,
  stg.num_cpf,
  stg.num_employe_drt,
  stg.num_mobile_phone,
  stg.dth_partition,
  stg.dth_partition)