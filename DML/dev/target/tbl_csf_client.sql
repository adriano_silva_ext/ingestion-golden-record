MERGE
  `br-apps-bi-customermdm-dev.db_dolphin_target_client_replica.tbl_csf_client` tgt
USING
  (
  WITH csf_client AS
(
SELECT
  UPPER(emailaddress) as ds_email
  ,UPPER(emailoptin) AS ind_optin_email
  ,UPPER(facets_0_id) AS id_facets_0
  ,UPPER(facets_0_idorigin) AS id_facets_0_id_origin
  ,CAST(facets_0_timestamp AS TIMESTAMP) as dth_facets_0_timestamp
  ,UPPER(facets_1_id) AS id_facets_1
  ,UPPER(facets_1_idorigin) AS id_facets_1_id_origin
  ,UPPER(facets_1_optin) AS ds_facets_1_optin
  ,CAST(facets_1_timestamp AS TIMESTAMP) as dth_facets_1_timestamp
  ,UPPER(firstname) as ds_first_name
  ,UPPER(fullname) as ds_full_name
  ,SAFE_CAST(REGEXP_REPLACE(id, r'[.,-]', '') AS NUMERIC) as num_cpf
  ,UPPER(idorigin) as id_origin
  ,UPPER(isconsumer) as ind_client
  ,UPPER(iscontact) as ind_contact
  ,UPPER(marketingpermissions_0_communicationdirection) as ds_mktperm_origin_cmm_dir
  ,UPPER(marketingpermissions_0_id) as uuid_marketing_permissions
  ,UPPER(marketingpermissions_0_idorigin) as ds_origin_marketing_permissions
  ,UPPER(marketingpermissions_0_optin) as ds_mktperm_origin_optin
  ,UPPER(marketingpermissions_0_outboundcommunicationmedium) as ds_mktperm_origin_cmm_medium
  ,CAST(marketingpermissions_0_timestamp AS TIMESTAMP) as dth_marketing_permissions
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(mobilephonenumber, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(mobilephonenumber, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(mobilephonenumber, r'[\s()-]', '') END as num_mobile_phone
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(phonenumber, r'[\s()-]', ''))
    WHEN 10 THEN CONCAT('+55',REGEXP_REPLACE(phonenumber, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(phonenumber, r'[\s()-]', '') END as num_phone
  ,CAST(timestamp AS TIMESTAMP) as dth_timestamp
  ,UPPER(yy1_csf_city_enh) as ds_city_name
  ,UPPER(yy1_csf_complement_enh) as ds_complement_street
  ,UPPER(yy1_csf_neighborhood_enh) as ds_neighborhood_name
  ,UPPER(yy1_csf_number_house_enh) as num_house
  ,SAFE_CAST(yy1_csf_optin_enh as BOOLEAN) as ind_optin_mailing
  ,UPPER(yy1_csf_postal_code1_enh) as num_cep
  ,UPPER(yy1_csf_state_enh) as ds_state
  ,UPPER(yy1_csf_status_enh) as cod_client_status
  ,UPPER(yy1_csf_street_enh) as ds_street
  ,UPPER(yy1_fg_grupo_controle_enh) as ind_grupo_controle
  ,UPPER(yy1_fg_portfolio_enh) as ind_portfolio
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(id, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-dev.db_dolphin_stage_client_base.tbl_csf_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
SELECT 
  * EXCEPT (RANK)
FROM 
  csf_client 
WHERE 
  RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET 
  ds_email = stg.ds_email,
  ind_optin_email = stg.ind_optin_email,
  id_facets_0 = stg.id_facets_0,
  id_facets_0_id_origin = stg.id_facets_0_id_origin,
  dth_facets_0_timestamp = stg.dth_facets_0_timestamp,
  id_facets_1 = stg.id_facets_1,
  id_facets_1_id_origin = stg.id_facets_1_id_origin,
  ds_facets_1_optin = stg.ds_facets_1_optin,
  dth_facets_1_timestamp = stg.dth_facets_1_timestamp,
  ds_first_name = stg.ds_first_name,
  ds_full_name = stg.ds_full_name,
  num_cpf = stg.num_cpf,
  id_origin = stg.id_origin,
  ind_client = stg.ind_client,
  ind_contact = stg.ind_contact,
  ds_mktperm_origin_cmm_dir = stg.ds_mktperm_origin_cmm_dir,
  uuid_marketing_permissions = stg.uuid_marketing_permissions,
  ds_origin_marketing_permissions = stg.ds_origin_marketing_permissions,
  ds_mktperm_origin_optin = stg.ds_mktperm_origin_optin,
  ds_mktperm_origin_cmm_medium = stg.ds_mktperm_origin_cmm_medium,
  dth_marketing_permissions = stg.dth_marketing_permissions,
  num_mobile_phone = stg.num_mobile_phone,
  num_phone = stg.num_phone,
  dth_timestamp = stg.dth_timestamp,
  ds_city_name = stg.ds_city_name,
  ds_complement_street = stg.ds_complement_street,
  ds_neighborhood_name = stg.ds_neighborhood_name,
  num_house = stg.num_house,
  ind_optin_mailing = stg.ind_optin_mailing,
  num_cep = stg.num_cep,
  ds_state = stg.ds_state,
  cod_client_status = stg.cod_client_status,
  ds_street = stg.ds_street,
  ind_grupo_controle = stg.ind_grupo_controle,
  ind_portfolio = stg.ind_portfolio,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  ds_email,
  ind_optin_email,
  id_facets_0,
  id_facets_0_id_origin,
  dth_facets_0_timestamp,
  id_facets_1,
  id_facets_1_id_origin,
  ds_facets_1_optin,
  dth_facets_1_timestamp,
  ds_first_name,
  ds_full_name,
  num_cpf,
  id_origin,
  ind_client,
  ind_contact,
  ds_mktperm_origin_cmm_dir,
  uuid_marketing_permissions,
  ds_origin_marketing_permissions,
  ds_mktperm_origin_optin,
  ds_mktperm_origin_cmm_medium,
  dth_marketing_permissions,
  num_mobile_phone,
  num_phone,
  dth_timestamp,
  ds_city_name,
  ds_complement_street,
  ds_neighborhood_name,
  num_house,
  ind_optin_mailing,
  num_cep,
  ds_state,
  cod_client_status,
  ds_street,
  ind_grupo_controle,
  ind_portfolio,
  dth_partition,
  dth_create)
VALUES
  (  
  stg.ds_email,
  stg.ind_optin_email,
  stg.id_facets_0,
  stg.id_facets_0_id_origin,
  stg.dth_facets_0_timestamp,
  stg.id_facets_1,
  stg.id_facets_1_id_origin,
  stg.ds_facets_1_optin,
  stg.dth_facets_1_timestamp,
  stg.ds_first_name,
  stg.ds_full_name,
  stg.num_cpf,
  stg.id_origin,
  stg.ind_client,
  stg.ind_contact,
  stg.ds_mktperm_origin_cmm_dir,
  stg.uuid_marketing_permissions,
  stg.ds_origin_marketing_permissions,
  stg.ds_mktperm_origin_optin,
  stg.ds_mktperm_origin_cmm_medium,
  stg.dth_marketing_permissions,
  stg.num_mobile_phone,
  stg.num_phone,
  stg.dth_timestamp,
  stg.ds_city_name,
  stg.ds_complement_street,
  stg.ds_neighborhood_name,
  stg.num_house,
  stg.ind_optin_mailing,
  stg.num_cep,
  stg.ds_state,
  stg.cod_client_status,
  stg.ds_street,
  stg.ind_grupo_controle,
  stg.ind_portfolio,
  stg.dth_partition,
  stg.dth_partition)