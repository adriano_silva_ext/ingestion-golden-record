SELECT
  n.num_unique_client,
  n.num_version_golden_record,
  CASE WHEN n.ds_full_name != v.ds_full_name THEN CONCAT(n.ds_full_name, '|', v.ds_full_name) END AS ds_full_name,
  CASE WHEN n.ds_first_name != v.ds_first_name THEN CONCAT(n.ds_first_name, '|', v.ds_first_name) END AS ds_first_name,
  CASE WHEN n.ds_last_name != v.ds_last_name THEN CONCAT(n.ds_last_name, '|', v.ds_last_name) END AS ds_last_name,
  CONCAT(n.ds_name_source_system, '|', v.ds_name_source_system) AS ds_name_source_system,
  CASE WHEN n.num_document != v.num_document THEN CONCAT(n.num_document, '|', v.num_document) END AS num_document,
  CASE WHEN n.hash_document != v.hash_document THEN CONCAT(TO_BASE64(n.hash_document), '|', TO_BASE64(v.hash_document)) END AS hash_document,
  CASE WHEN n.ds_document_type != v.ds_document_type THEN CONCAT(n.ds_document_type, '|', v.ds_document_type) END AS ds_document_type,
  CONCAT(n.ds_document_source_system, '|', v.ds_document_source_system) AS ds_document_source_system,
  CASE WHEN n.dth_birth != v.dth_birth THEN CONCAT(n.dth_birth, '|', v.dth_birth) END AS dth_birth,
  CONCAT(n.ds_birth_source_system, '|', v.ds_birth_source_system)  AS ds_birth_source_system,
  CASE WHEN n.ini_gender != v.ini_gender THEN CONCAT(n.ini_gender, '|', v.ini_gender) END AS ini_gender,
  CASE WHEN n.ds_gender != v.ds_gender THEN CONCAT(n.ds_gender, '|', v.ds_gender) END AS ds_gender,
  CONCAT(n.ds_gender_source_system, '|', v.ds_gender_source_system) AS ds_gender_source_system,
  CASE WHEN n.ini_marital_status != v.ini_marital_status THEN CONCAT(n.ini_marital_status, '|', v.ini_marital_status) END AS ini_marital_status,
  CASE WHEN n.ds_marital_status != v.ds_marital_status THEN CONCAT(n.ds_marital_status, '|', v.ds_marital_status) END AS ds_marital_status,
  CONCAT(n.ds_marital_status_source_system, '|', v.ds_marital_status_source_system) AS ds_marital_status_source_system,
  CASE WHEN n.ds_nationality != v.ds_nationality THEN CONCAT(n.ds_nationality, '|', v.ds_nationality) END AS ds_nationality,
  CASE WHEN n.ds_country_birth_name != v.ds_country_birth_name THEN CONCAT(n.ds_country_birth_name, '|', v.ds_country_birth_name) END AS ds_country_birth_name,
  CONCAT(n.ds_nationality_source_system, '|', v.ds_nationality_source_system) AS ds_nationality_source_system,
  CASE WHEN n.ind_status_client_csf_carrefour != v.ind_status_client_csf_carrefour THEN CONCAT(n.ind_status_client_csf_carrefour, '|', v.ind_status_client_csf_carrefour) END AS ind_status_client_csf_carrefour,
  CASE WHEN n.ds_status_client_csf_carrefour != v.ds_status_client_csf_carrefour THEN CONCAT(n.ds_status_client_csf_carrefour, '|', v.ds_status_client_csf_carrefour) END AS ds_status_client_csf_carrefour,
  CASE WHEN n.dt_admission_minhas_recompensas != v.dt_admission_minhas_recompensas THEN CONCAT(n.dt_admission_minhas_recompensas, '|', v.dt_admission_minhas_recompensas) END AS dt_admission_minhas_recompensas,
  CASE WHEN n.ind_employee != v.ind_employee THEN CONCAT(n.ind_employee, '|', v.ind_employee) END AS ind_employee,
  CASE WHEN n.ind_has_atacadao != v.ind_has_atacadao THEN CONCAT(n.ind_has_atacadao, '|', v.ind_has_atacadao) END AS ind_has_atacadao,
  CASE WHEN n.ind_has_carrefour != v.ind_has_carrefour THEN CONCAT(n.ind_has_carrefour, '|', v.ind_has_carrefour) END AS ind_has_carrefour,
  CASE WHEN n.ind_client_blocked != v.ind_client_blocked THEN CONCAT(n.ind_client_blocked, '|', v.ind_client_blocked) END AS ind_client_blocked,
  CASE WHEN n.ind_portfolio != v.ind_portfolio THEN CONCAT(n.ind_portfolio, '|', v.ind_portfolio) END AS ind_portfolio,
  CASE WHEN n.ind_bad_golden_record != v.ind_bad_golden_record THEN CONCAT(n.ind_bad_golden_record, '|', v.ind_bad_golden_record) END AS ind_bad_golden_record,
  CASE WHEN n.ind_deleted != v.ind_deleted THEN CONCAT(n.ind_deleted, '|', v.ind_deleted) END AS ind_deleted,
  CASE WHEN n.ds_channel_create_client != v.ds_channel_create_client THEN CONCAT(n.ds_channel_create_client, '|', v.ds_channel_create_client) END AS ds_channel_create_client,
  CASE WHEN n.uuid_token_firebase != v.uuid_token_firebase THEN CONCAT(n.uuid_token_firebase, '|', v.uuid_token_firebase) END AS uuid_token_firebase,
  n.lst_client_origins,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS v
ON
  n.num_unique_client = v.num_unique_client
  WHERE n.num_unique_client !=v.num_unique_client or n.ds_full_name !=v.ds_full_name or n.ds_first_name !=v.ds_first_name or n.ds_last_name !=v.ds_last_name or n.ds_name_source_system !=v.ds_name_source_system or n.num_document !=v.num_document or n.hash_document !=v.hash_document or n.ds_document_type !=v.ds_document_type or n.ds_document_source_system !=v.ds_document_source_system or n.dth_birth !=v.dth_birth or n.ds_birth_source_system !=v.ds_birth_source_system or n.ini_gender !=v.ini_gender or n.ds_gender !=v.ds_gender or n.ds_gender_source_system !=v.ds_gender_source_system or n.ini_marital_status !=v.ini_marital_status or n.ds_marital_status !=v.ds_marital_status or n.ds_marital_status_source_system !=v.ds_marital_status_source_system or n.ds_nationality !=v.ds_nationality or n.ds_country_birth_name !=v.ds_country_birth_name or n.ds_nationality_source_system !=v.ds_nationality_source_system or n.ind_status_client_csf_carrefour !=v.ind_status_client_csf_carrefour or n.ds_status_client_csf_carrefour !=v.ds_status_client_csf_carrefour or n.dt_admission_minhas_recompensas !=v.dt_admission_minhas_recompensas or n.ind_employee !=v.ind_employee or n.ind_has_atacadao !=v.ind_has_atacadao or n.ind_has_carrefour !=v.ind_has_carrefour or n.ind_client_blocked !=v.ind_client_blocked or n.ind_portfolio !=v.ind_portfolio or n.ind_bad_golden_record !=v.ind_bad_golden_record or n.ind_deleted !=v.ind_deleted or n.ds_channel_create_client !=v.ds_channel_create_client or n.dth_create !=v.dth_create or n.uuid_token_firebase !=v.uuid_token_firebase