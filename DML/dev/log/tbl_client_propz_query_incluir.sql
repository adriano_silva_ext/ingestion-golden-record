SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
  n.ds_buy_phusical_virtual,
	n.num_children,
	n.num_coupons_used,
	n.num_advertisements_customer_interacted_email,
  n.dth_first_buy,
  n.dth_last_buy,
  n.dth_received_last_email_advertisement,
  n.dth_partition,
  n.dth_create,
  n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_propz` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_propz` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null