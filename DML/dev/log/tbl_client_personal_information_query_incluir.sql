SELECT
	n.num_unique_client,
	n.num_version_golden_record,
	n.ds_full_name,
	n.ds_first_name,
	n.ds_last_name,
	n.ds_name_source_system,
	n.num_document,
	TO_BASE64(n.hash_document) AS hash_document,
	n.ds_document_type,
	n.ds_document_source_system,
	n.dth_birth,
	n.ds_birth_source_system,
	n.ini_gender,
	n.ds_gender,
	n.ds_gender_source_system,
	n.ini_marital_status,
	n.ds_marital_status,
	n.ds_marital_status_source_system,
	n.ds_nationality,
	n.ds_country_birth_name,
	n.ds_nationality_source_system,
	n.ind_status_client_csf_carrefour,
	n.ds_status_client_csf_carrefour,
	n.dt_admission_minhas_recompensas,
	n.ind_employee,
	n.ind_has_atacadao,
	n.ind_has_carrefour,
	n.ind_client_blocked,
	n.ind_portfolio,
	n.ind_bad_golden_record,
	n.ind_deleted,
	n.ds_channel_create_client,
  n.uuid_token_firebase,
  n.lst_client_origins,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS v
ON
  n.num_unique_client = v.num_unique_client
WHERE v.num_unique_client is null