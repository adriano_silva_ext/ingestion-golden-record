SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.cod_consent_privacy_policy != v.cod_consent_privacy_policy THEN CONCAT(n.cod_consent_privacy_policy, '|', v.cod_consent_privacy_policy) END AS cod_consent_privacy_policy,
  CASE WHEN n.dth_consent_privacy_policy != v.dth_consent_privacy_policy THEN CONCAT(n.dth_consent_privacy_policy, '|', v.dth_consent_privacy_policy) END AS dth_consent_privacy_policy,
  CASE WHEN n.cod_consent_purchase_status != v.cod_consent_purchase_status THEN CONCAT(n.cod_consent_purchase_status, '|', v.cod_consent_purchase_status) END AS cod_consent_purchase_status,
  CASE WHEN n.dth_consent_purchase_status != v.dth_consent_purchase_status THEN CONCAT(n.dth_consent_purchase_status, '|', v.dth_consent_purchase_status) END AS dth_consent_purchase_status,
  CASE WHEN n.cod_consent_sharing_third != v.cod_consent_sharing_third THEN CONCAT(n.cod_consent_sharing_third, '|', v.cod_consent_sharing_third) END AS cod_consent_sharing_third,
  CASE WHEN n.dth_consent_sharing_third != v.dth_consent_sharing_third THEN CONCAT(n.dth_consent_sharing_third, '|', v.dth_consent_sharing_third) END AS dth_consent_sharing_third,
  CONCAT(n.cod_source, '|', v.cod_source)  AS cod_source,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_general_consent` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_general_consent` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.cod_consent_privacy_policy !=v.cod_consent_privacy_policy or n.dth_consent_privacy_policy !=v.dth_consent_privacy_policy or n.cod_consent_purchase_status !=v.cod_consent_purchase_status or n.dth_consent_purchase_status !=v.dth_consent_purchase_status or n.cod_consent_sharing_third !=v.cod_consent_sharing_third or n.dth_consent_sharing_third !=v.dth_consent_sharing_third or n.cod_source !=v.cod_source
