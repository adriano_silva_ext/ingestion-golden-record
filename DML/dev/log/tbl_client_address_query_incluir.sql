SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  n.num_cep,
  n.ds_street,
  n.ds_neighborhood_name,
  n.ds_city_name,
  n.ini_state,
  n.ds_state_name,
  n.ds_country_name,
  n.ind_main_address,
  n.num_house,
  n.ds_complement_street,
  n.ds_address_type,
  n.ds_reference,
  n.ds_source_system,
  n.dth_create,
  n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_address` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_address` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE
  v.num_unique_client IS NULL