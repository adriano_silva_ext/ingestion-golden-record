SELECT
  n.num_unique_client,
  n.num_version_golden_record,
  CASE WHEN n.ds_email != v.ds_email THEN CONCAT(n.ds_email, '|', v.ds_email) END AS ds_email,
  CASE WHEN n.hash_email != v.hash_email THEN CONCAT(TO_BASE64(n.hash_email), '|', TO_BASE64(v.hash_email)) END AS hash_email,
  CASE WHEN n.ds_email_type != v.ds_email_type THEN CONCAT(n.ds_email_type, '|', v.ds_email_type) END AS ds_email_type,
  CASE WHEN n.ind_main_email != v.ind_main_email THEN CONCAT(n.ind_main_email, '|', v.ind_main_email) END AS ind_main_email,
  CONCAT(n.ds_source_system, '|', v.ds_source_system)  AS ds_source_system,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_email` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_email` AS v
ON
  n.num_unique_client = v.num_unique_client
  WHERE n.num_unique_client !=v.num_unique_client or n.ds_email !=v.ds_email or n.hash_email !=v.hash_email or n.ds_email_type !=v.ds_email_type or n.ind_main_email !=v.ind_main_email or n.ds_source_system !=v.ds_source_system
