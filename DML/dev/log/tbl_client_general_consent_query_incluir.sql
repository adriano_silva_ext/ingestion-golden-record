SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
  n.cod_consent_privacy_policy,
	n.dth_consent_privacy_policy,
	n.cod_consent_purchase_status,
	n.dth_consent_purchase_status,
  n.cod_consent_sharing_third,
	n.dth_consent_sharing_third,
  n.cod_source,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_general_consent` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_general_consent` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null