CREATE OR REPLACE TABLE `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_phone`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  num_phone STRING OPTIONS(description="Telephone / Telefone"),
  hash_phone BYTES OPTIONS(description="Phone Anonymization / Anonimização Telefone"),
  ds_phone_type STRING OPTIONS(description="Phone Type / Tipo De Telefone"),
  ind_main_phone BOOLEAN OPTIONS(description="Main Phone Indicator / Indicador Telefone Principal"),
  ind_bounce_phone BOOLEAN OPTIONS(description="Mobile Phone Validation Indicator / Indicador de  validação de celular"),
  ds_source_system STRING OPTIONS(description="Data Source System Indicator / Indicador De Sistema De Origem Do Dado"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client, num_phone
OPTIONS(
  description="Table Data from people telephones / Tabela Dados De Telefones De Pessoas"
)
AS
SELECT
  num_unique_client	 	,
  num_phone		,
  hash_phone 	,
  ds_phone_type	,
  ind_main_phone		,
  ind_bounce_phone ,
  ds_source_system		,
  dth_create            ,
  dth_update
from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_information_process`;