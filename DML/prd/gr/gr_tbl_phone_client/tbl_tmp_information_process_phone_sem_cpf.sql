-- INSERT INTO br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_information_process
WITH
  tab_aux_phone AS (
  SELECT
    num_unique_client,
    num_phone,
    ds_phone_type,
    MIN(prioridade) AS prioridade,
    dth_create,
    dth_update
  FROM (
    SELECT
      b.num_unique_client,
      a.num_phone,
      1 AS prioridade,
      CASE
        WHEN LENGTH(num_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_phone) IN (10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'VTEX' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      2 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'VTEX' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_phone,
      3 AS prioridade,
      CASE
        WHEN LENGTH(num_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_phone) IN (10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_csf_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'VTEX' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      4 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_csf_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'CSF' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      5 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_rh_client` a
    INNER JOIN
      (select distinct num_unique_client, ds_source_system, ds_email 
	     from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process`
		where ind_document = FALSE) b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'RH' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      6 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'SVA' 
    WHERE
      a.num_document IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client AS num_unique_client,
      num_landline AS num_phone,
      7 AS prioridade,
      CASE
        WHEN LENGTH(num_landline) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_landline) IN (10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'SVA' 
    WHERE
      a.num_document IS NULL
      AND a.num_landline IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
   and not exists (
   select * from `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_propz_client` c 
    where UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email)) and a.num_landline = c.num_mobile_phone and ind_hard_bounce_sms = '1'
   )
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      9 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10, 8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_neoassist_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'PROPZ' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone_2 AS num_phone
     ,10 AS prioridade
    ,CASE
        WHEN LENGTH(num_mobile_phone_2) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone_2) IN (10,  8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_neoassist_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'NEOASSIST' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone_2 IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE
   and not exists (
   select * from `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_propz_client` c 
    where UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email)) and a.num_mobile_phone_2 = c.num_mobile_phone and ind_hard_bounce_sms = '1'
   )
    UNION ALL
    SELECT
      b.num_unique_client,
      a.num_mobile_phone AS num_phone,
      11 AS prioridade,
      CASE
        WHEN LENGTH(num_mobile_phone) IN (11, 9) THEN 'CELULAR PESSOAL'
        WHEN LENGTH(num_mobile_phone) IN (10,
        8) THEN 'FIXO PESSOAL'
      ELSE
      'DESCONHECIDO'
    END
      AS ds_phone_type,
      a.dth_create,
      a.dth_update
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_qualibest_client` a
    INNER JOIN
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
    ON
      UPPER(TRIM(a.ds_email)) = UPPER(TRIM(b.ds_email))
	AND trim(b.ds_source_system)    = 'QUALIBEST' 
    WHERE
      a.num_cpf IS NULL
      AND a.num_mobile_phone IS NOT NULL
      AND a.ds_email IS NOT NULL
      AND b.ind_document = FALSE 
	  ) AS tab_aux_1
  GROUP BY
    num_unique_client,
    num_phone,
    ds_phone_type,
    dth_create,
    dth_update),
  tab_aux_main_phone AS (
  SELECT
    num_unique_client,
    ds_phone_type,
    MIN(prioridade) AS prioridade
  FROM
    tab_aux_phone
  GROUP BY
    num_unique_client,
    ds_phone_type ),
tab_selected_phone as (
    Select a.*
      from tab_aux_phone a
      inner join tab_aux_main_phone b 
         on a.num_unique_client  = b.num_unique_client 
        and a.ds_phone_type = b.ds_phone_type
        and a.prioridade    = b.prioridade  
)	
Select * except(ind_bounce_phone),
       case when ind_bounce_phone = '0' then false
	        when ind_bounce_phone = '1' then true
			else null 
	    end as ind_bounce_phone 
from (		
		SELECT
		  c.num_unique_client,
		  a.num_phone,
		  SHA256(a.num_phone) AS hash_phone,
		  a.ds_phone_type,
		  CASE
			WHEN a.ds_phone_type = 'CELULAR PESSOAL' or a.ds_phone_type = 'FIXO PESSOAL' THEN TRUE ELSE FALSE
		  END AS ind_main_phone,
		  CASE
			WHEN a.prioridade IN (1, 2) THEN 'VTEX'
			WHEN a.prioridade IN (3,
			4) THEN 'CSF'
			WHEN a.prioridade = 5 THEN 'RH'
			WHEN a.prioridade IN (6, 7) THEN 'SVA'
		--    WHEN a.prioridade = 8 THEN 'PROPZ'
			WHEN a.prioridade IN (9, 10) THEN 'NEOASSIST'
			WHEN a.prioridade = 11 THEN 'QUALIBEST'
		END
		  AS ds_source_system,
      a.dth_create,
      a.dth_update,					
		  FALSE AS ind_document, 
		  (select ind_hard_bounce_sms 
			 from `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_propz_client` b
			 inner join `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email` d 
			  on UPPER(TRIM(b.ds_email)) = UPPER(TRIM(d.ds_email)) and d.num_unique_client = c.num_unique_client
			where a.num_phone = b.num_mobile_phone 
			and UPPER(TRIM(b.ds_email)) is not null  
			) as ind_bounce_phone
		FROM
		  tab_selected_phone AS a
		INNER JOIN
		  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
		ON
		  a.num_unique_client = c.num_unique_client
		left join `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` 	 as oldgr 
			   ON  c.num_unique_client = oldgr.num_unique_client
		where c.num_document is null
		UNION ALL
		SELECT
		  c.num_unique_client,
		  a.num_phone,
		  SHA256(a.num_phone) AS hash_phone,
		  a.ds_phone_type,
		  False				  AS ind_main_phone,
		  CASE
			WHEN a.prioridade IN (1, 2) THEN 'VTEX'
			WHEN a.prioridade IN (3,
			4) THEN 'CSF'
			WHEN a.prioridade = 5 THEN 'RH'
			WHEN a.prioridade IN (6, 7) THEN 'SVA'
		--    WHEN a.prioridade = 8 THEN 'PROPZ'
			WHEN a.prioridade IN (9, 10) THEN 'NEOASSIST'
			WHEN a.prioridade = 11 THEN 'QUALIBEST'
		  END
		  AS ds_source_system,
		  coalesce(oldgr.dth_create,CURRENT_TIMESTAMP())						 as dth_create,						
		  case when oldgr.dth_create is not null then CURRENT_TIMESTAMP() end    as dth_update,					
		  FALSE 																 AS ind_document, 
		  (select ind_hard_bounce_sms 
			 from `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_propz_client` b
			 inner join `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email` d 
			  on UPPER(TRIM(b.ds_email)) = UPPER(TRIM(d.ds_email)) and d.num_unique_client = c.num_unique_client
			where a.num_phone = b.num_mobile_phone 
			and UPPER(TRIM(b.ds_email)) is not null  
			) as ind_bounce_phone
		FROM (
		  SELECT num_unique_client, num_phone, ds_phone_type, prioridade
		   from tab_aux_phone
		  except distinct
		  SELECT num_unique_client, num_phone, ds_phone_type, prioridade
		   from tab_selected_phone
		) a 
		INNER JOIN
		  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
		ON
		  c.num_unique_client = a.num_unique_client
		left join `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` 	 as oldgr 
			   ON  c.num_unique_client = oldgr.num_unique_client
		where c.num_document is not null
	)
;