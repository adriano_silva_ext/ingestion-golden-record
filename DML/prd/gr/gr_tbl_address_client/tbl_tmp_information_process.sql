Select a.*
      ,row_number() over(partition by num_unique_client) as num_row
from (
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  ds_country_name,
  a.num_house,
  a.ds_complement_street,
  CASE
    WHEN TRIM(a.ds_address_type) = 'RESIDENTIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'INVOICE'     THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) = 'RESIDENCIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'COBRANÇA'    THEN 'COBRANÇA'
/*    WHEN TRIM(a.ds_address_type) IN ('RESIDENTIAL', 'APARTAMENTO', 'CASA') THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) IN ('BILLING','INVOICE') THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) IN ('SHIPPING') THEN 'ENTREGA'
    WHEN TRIM(a.ds_address_type) IN ('COMERCIAL') THEN 'COMERCIAL'
*/	
  ELSE
  'OUTROS' 
END
  AS ds_address_type,
  a.ds_reference,
  'VTEX' AS ds_source_system,
  '1' AS prioridade,
  coalesce(a.dth_create_system_origin,a.dth_partition) as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_address` AS a
INNER JOIN (
  SELECT
    num_cpf,
    uuid_client
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_client`) AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  b.num_cpf = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  b.num_cpf IS NOT NULL
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  a.ini_state,
  d.ds_state AS ds_state_name,
  ds_country_name,
  a.num_house,
  a.ds_complement_street,
  CASE
    WHEN TRIM(a.ds_address_type) = 'RESIDENTIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'INVOICE'     THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) = 'RESIDENCIAL' THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) = 'COBRANÇA'    THEN 'COBRANÇA'
/*    WHEN TRIM(a.ds_address_type) IN ('RESIDENTIAL', 'APARTAMENTO', 'CASA') THEN 'RESIDENCIAL'
    WHEN TRIM(a.ds_address_type) IN ('BILLING','INVOICE') THEN 'COBRANÇA'
    WHEN TRIM(a.ds_address_type) IN ('SHIPPING') THEN 'ENTREGA'
    WHEN TRIM(a.ds_address_type) IN ('COMERCIAL') THEN 'COMERCIAL'
*/	
  ELSE
  'OUTROS' 
END
  AS ds_address_type,
  a.ds_reference,
  'SVA' AS ds_source_system,
  '3' AS prioridade,
  coalesce(a.dth_create,a.dth_partition) as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_address` AS a
INNER JOIN (
  SELECT
    num_document,
    uuid_client,
    ds_document_type
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_client`) AS b
ON
  a.uuid_client = b.uuid_client
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  b.num_document = c.num_document
  AND b.ds_document_type = 'CPF'
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ini_state)) = UPPER(TRIM(d.ini_state))
WHERE
  b.num_document IS NOT NULL
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  'BRA' AS ds_country_name,
  num_house,
  ds_complement_street,
  'RESIDENCIAL' AS ds_address_type,
  CAST(NULL AS STRING) AS ds_reference,
  'QUALIBEST' AS ds_source_system,
  '4' AS prioridade,
  a.dth_partition as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_qualibest_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  a.num_cpf IS NOT NULL
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep,
  a.ds_street,
  a.ds_neighborhood_name,
  a.ds_city_name,
  d.ini_state,
  a.ds_state AS ds_state_name,
  'BRA' AS ds_country_name,
  num_house,
  ds_complement_street,
  'RESIDENCIAL' AS ds_address_type,
  CAST(NULL AS STRING) AS ds_reference,
  'CSF' AS ds_source_system,
  '2' AS prioridade,
  a.dth_partition as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_csf_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  a.num_cpf IS NOT NULL
UNION ALL
SELECT
  c.num_unique_client,
  a.num_cep  			as num_cep,
  a.ds_street,
  null 					as ds_neighborhood_name,
  a.ds_city_name   		as ds_city_name,
  d.ini_state,
  d.ds_state 			AS ds_state_name,
  a.ds_country_name		AS ds_country_name,
  a.num_house			AS num_house,
  null 					as ds_complement_street,
  'RESIDENCIAL' 		AS ds_address_type,
  CAST(NULL AS STRING) 	AS ds_reference,
  a.ds_channel_create_client AS ds_source_system,
  '5' AS prioridade,
  a.dth_create as dt_create_ori,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_history_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_document = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_state` AS d
ON
  UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state))
  OR UPPER(TRIM(a.ds_state)) = UPPER(TRIM(d.ds_state_2))
WHERE
  a.num_document IS NOT NULL  
) a