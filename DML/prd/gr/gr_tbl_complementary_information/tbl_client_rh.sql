CREATE OR REPLACE TABLE `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_rh`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  cod_company STRING OPTIONS(description="Company Code / Código Da Empresa"),
  cod_registration STRING OPTIONS(description="Employee Register Number / Número Da Matrícula Funcionário"),
  cod_status STRING OPTIONS(description="Employee Status Code / Código Situação Funcionário"),
  cod_stocking STRING OPTIONS(description="Employee Staffing Code / Código Lotação Do Funcionário"),
  ds_situation_type STRING OPTIONS(description="Description Of The Employee's Type Of Situation / Decrição Do Tipo De Situação Do Funcionário"),
  ds_status_employee STRING OPTIONS(description="Employee Status / Situação Funcionário"),
  dth_admission TIMESTAMP OPTIONS(description="Admission Date / Data Admissão"),
  ind_situation_type STRING OPTIONS(description="Metadata Not Provided / Column name Not Standardized Only Adapted / Metadado Não Fornecido / Nome da Coluna Não Padronizado Apenas Adaptado"),
  ds_company_name STRING OPTIONS(description="Name Of Company / Nome Da Empresa"),
  ds_stocking_name STRING OPTIONS(description="Name Employee Capacity / Nome Lotação Funcionário"),
  ds_workplace_name STRING OPTIONS(description="Workplace Employee / Local Trabalho Funcionário"),
  num_employe_drt NUMERIC OPTIONS(description="Drt Of Employee / Drt Do Funcionário (Ctps)"),
  dth_partition TIMESTAMP OPTIONS(description="Partição da tabela Origem / Origin Table partition"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Table HR people information data / Tabela Dados De Informações De Pessoas Do Rh"
)
AS
SELECT
  c.num_unique_client,
  a.cod_company,
  a.cod_registration,
  a.cod_status,
  a.cod_stocking,
  a.ds_situation_type,
  a.ds_status_employee,
  a.dth_admission,
  a.ind_situation_type,
  a.ds_company_name,
  a.ds_stocking_name,
  a.ds_workplace_name,
  a.num_employe_drt,
  a.dth_partition,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_rh_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
  AND c.ds_document_type = 'CPF'
LEFT JOIN `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NOT NULL;