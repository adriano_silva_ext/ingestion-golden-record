SELECT DISTINCT 
  b.num_unique_client,
  a.cod_consent_collection_point_website,
  a.dth_consent_collection_point_website, 
  a.cod_consent_collection_point_chatbox,
  a.dth_consent_collection_point_chatbox,  
  a.cod_consent_collection_point_csf,
  a.dth_consent_collection_point_csf,  
  a.cod_source,
  a.dth_create,
  a.dth_update 
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client` a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
ON
    upper(TRIM(a.ds_email)) = upper(TRIM(b.ds_email))
WHERE a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL
  AND b.ind_document = FALSE
  and b.ind_main_email