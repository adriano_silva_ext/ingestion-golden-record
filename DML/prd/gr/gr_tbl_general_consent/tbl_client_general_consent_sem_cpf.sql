SELECT DISTINCT 
  b.num_unique_client,
  a.cod_consent_privacy_policy,
  a.dth_consent_privacy_policy,
  a.cod_consent_purchase_status,
  a.dth_consent_purchase_status,
  a.cod_consent_sharing_third,
  a.dth_consent_sharing_third,
  a.cod_source,
  a.dth_create,
  a.dth_update  
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client` a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` b
ON
  upper(TRIM(a.ds_email)) = upper(TRIM(b.ds_email))
WHERE a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL
  and b.ind_main_email  
  AND b.ind_document = FALSE