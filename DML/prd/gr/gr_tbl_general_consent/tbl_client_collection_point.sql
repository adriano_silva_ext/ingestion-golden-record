CREATE OR REPLACE TABLE `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_collection_point`
(
  num_unique_client STRING OPTIONS(description=" NUC - Unique Customer Number / NUC - Número Único Cliente"),
  cod_consent_collection_point_website STRING OPTIONS(description="Consent Code Collection Point Where The Email Will Be Transacted On The Website / Código Do Consentimento Do Ponto De Coleta Onde Será Transacionado E-mail No Site"),
  dth_consent_collection_point_website TIMESTAMP OPTIONS(description="Consent Date Collection Point Where The Email Will Be Transacted On The Website / Data Do Consentimento Do Ponto De Coleta Onde Será Transacionado E-mail No Site"),
  cod_consent_collection_point_chatbox STRING OPTIONS(description="Consent Code Colliection Point Carina Chatbox for Flyer Shipping That Sent Over The Phone / Código Do Consentimento Do Ponto De Coleta Do Chatbox Carina Para Envio De Folhetos que é Enviado o Telefone"),
  dth_consent_collection_point_chatbox TIMESTAMP OPTIONS(description="Consent Date Colliection Point Carina Chatbox for Flyer Shipping That Sent Over The Phone / Data Do Consentimento Do Ponto De Coleta Do Chatbox Carina Para Envio De Folhetos que é Enviado o Telefone"),
  cod_consent_collection_point_csf STRING OPTIONS(description="Consent Code Collection Point CSF / Código Do Consentimento Do Ponto de Coleta CSF"),
  dth_consent_collection_point_csf TIMESTAMP OPTIONS(description="Consent Date Collection Point CSF / Data Do Consentimento Do Ponto de Coleta CSF"),
  cod_source STRING OPTIONS(description="Source Code / Código De Origem"),
  dth_create TIMESTAMP OPTIONS(description=" Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Consent table Data from the collection points of people / Tabela De Consentimento Dados Dos Pontos De Coletas De Pessoas"
)
AS
SELECT DISTINCT 
  b.num_unique_client,
  a.cod_consent_collection_point_website,
  a.dth_consent_collection_point_website, 
  a.cod_consent_collection_point_chatbox,
  a.dth_consent_collection_point_chatbox,  
  a.cod_consent_collection_point_csf,
  a.dth_consent_collection_point_csf,  
  a.cod_source,
  a.dth_create,
  a.dth_update 
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client` a
INNER JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` b
ON
  a.num_cpf = b.num_document
WHERE a.num_cpf IS NOT NULL
  AND b.num_document IS NOT NULL 