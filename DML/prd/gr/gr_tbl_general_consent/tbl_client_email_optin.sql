CREATE OR REPLACE TABLE `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email_optin`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  ds_email STRING OPTIONS(description="Email / E-mail"),
  cod_consent_offer_email STRING OPTIONS(description="Consent Code Offer Email / Código Do Consentimento Oferta E-mail"),
  dth_consent_offer_email TIMESTAMP OPTIONS(description="Consent Date Offer Email / Data Do Consentimento Das Ofertas E-mail"),
  cod_source STRING OPTIONS(description="Source Code / Código De Origem"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client, ds_email
OPTIONS(
  description="Consent table Data from people emails / Tabela De Consentimento Dados De Emails De Pessoas"
)
AS
SELECT DISTINCT
    b.num_unique_client,
    a.ds_email,
    a.cod_consent_offer_email,
    a.dth_consent_offer_email,
    a.cod_source,
    a.dth_create,
    a.dth_update,
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client` a
  INNER JOIN ( select distinct a.num_unique_client, b.num_document, a.ds_email, a.ind_main_email 
                 from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email` a
                 inner join `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information`  b
                 on a.num_unique_client = b.num_unique_client and a.ind_main_email
             ) b
  ON
    TRIM(a.ds_email) = TRIM(b.ds_email)
  AND a.num_cpf = b.num_document  
  WHERE
    a.ds_email IS NOT NULL 