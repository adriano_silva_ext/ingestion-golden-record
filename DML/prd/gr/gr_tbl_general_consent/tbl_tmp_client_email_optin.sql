SELECT  
  a.num_unique_client,
  a.ds_email,
  a.cod_consent_offer_email,
  a.dth_consent_offer_email,
  a.cod_source,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email_optin` a; 