with last_version as (SELECT 
  COALESCE(hist.num_unique_client,oldgr.num_unique_client) AS num_unique_client,
  COALESCE(oldgr.num_version_golden_record,1) AS num_version_golden_record,
  COALESCE(
    oldgr.ds_full_name,
	hist.ds_full_name,CONCAT(hist.ds_first_name,' ',hist.ds_last_name)) AS ds_full_name,
  CASE
    WHEN oldgr.ds_full_name IS NOT NULL THEN substr(oldgr.ds_full_name,1,case when STRPOS(oldgr.ds_full_name, ' ') > 0 then STRPOS(oldgr.ds_full_name, ' ') else length(oldgr.ds_full_name)+1 end - 1)
    WHEN hist.ds_first_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_first_name),substr(hist.ds_full_name,1,case when STRPOS(hist.ds_full_name, ' ') > 0 then STRPOS(hist.ds_full_name, ' ') else length(hist.ds_full_name)+1 end - 1))	
END
  AS ds_first_name,
  CASE
    WHEN oldgr.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(oldgr.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
    WHEN hist.ds_last_name IS NOT NULL or hist.ds_full_name IS NOT NULL THEN coalesce(trim(hist.ds_last_name),ARRAY_REVERSE(SPLIT(replace(hist.ds_full_name,' ',',')))[SAFE_OFFSET(0)])
END
  AS ds_last_name,
  CASE
    WHEN oldgr.ds_full_name IS NOT NULL THEN oldgr.ds_name_source_system
    WHEN hist.ds_full_name IS NOT NULL or hist.ds_first_name IS NOT NULL or hist.ds_last_name IS NOT NULL THEN 'HIST'
	ELSE 'HIST'
END
  AS ds_name_source_system,
  COALESCE(oldgr.num_document,
	hist.num_document) AS num_document,
  SHA256(COALESCE(CAST(oldgr.num_document AS STRING),
	  CAST(hist.num_document AS STRING))) AS hash_document,
  'CPF' AS ds_document_type,
  CASE
    WHEN oldgr.num_document IS NOT NULL THEN oldgr.ds_document_source_system
	WHEN hist.num_document is not null then 'HIST'
END
  AS ds_document_source_system,
  COALESCE(oldgr.dth_birth,
	hist.dth_birth) AS dth_birth,
  CASE
    WHEN oldgr.dth_birth IS NOT NULL THEN oldgr.ds_birth_source_system
    WHEN hist.dth_birth IS NOT NULL THEN 'HIST'
END
  AS ds_birth_source_system,
  CASE
    when oldgr.ini_gender is not null then oldgr.ini_gender
    WHEN hist.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
END
  AS ini_gender,
  CASE
    when oldgr.ds_gender is not null then oldgr.ds_gender
    WHEN hist.ds_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = hist.ini_gender AND ds_source_system = 'HIST')
END
  AS ds_gender,
  CASE
    when oldgr.ini_gender is not null or oldgr.ds_gender is not null then oldgr.ds_gender_source_system
    WHEN hist.ini_gender IS NOT NULL THEN 'HIST'
END
  AS ds_gender_source_system,
  oldgr.ini_marital_status,
  oldgr.ds_marital_status,
  oldgr.ds_marital_status_source_system,
  oldgr.ds_nationality,
  oldgr.ds_country_birth_name,
  oldgr.ds_nationality_source_system,
  coalesce(oldgr.ind_status_client_csf_carrefour,hist.ind_status_client_csf_carrefour) AS ind_status_client_csf_carrefour,	
  coalesce(trim(oldgr.ds_status_client_csf_carrefour),trim(hist.nm_status_client_csf_carrefour)) AS ds_status_client_csf_carrefour,
  coalesce(loyat.dth_join_loyalty,
  oldgr.dt_admission_minhas_recompensas) AS dt_admission_minhas_recompensas,
  case
    WHEN (
	      Select true
		    from `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_rh_client` rh
		   where oldgr.num_document = rh.num_cpf
	        and STARTS_WITH(ds_status_employee, 'RESC') = FALSE 
			and ds_status_employee is not null 
			and CONTAINS_SUBSTR(ds_status_employee, 'ESTAGI') = False
         ) = True 			 
	THEN TRUE
    ELSE FALSE
  end AS ind_employee,
  coalesce(oldgr.ind_has_atacadao,
    FALSE) AS ind_has_atacadao,
  case when oldgr.ind_has_carrefour is not null then oldgr.ind_has_carrefour
       when hist.ind_has_carrefour = 1 then TRUE when hist.ind_has_carrefour = 0 then false else null  
  end AS ind_has_carrefour,
  coalesce(oldgr.ind_client_blocked,
    FALSE) AS ind_client_blocked,
  coalesce(oldgr.ind_portfolio,hist.ind_portfolio,'0') as ind_portfolio,
  FALSE AS ind_bad_golden_record,
  coalesce(oldgr.ind_deleted,case when hist.ind_deleted = 1 then true when hist.ind_deleted = 0 then false else null end,
    FALSE) AS ind_deleted,
  CASE
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.ds_channel_create_client
    WHEN hist.ds_channel_create_client is not null then hist.ds_channel_create_client    
END
  AS ds_channel_create_client,
    coalesce(oldgr.dth_create,
    CURRENT_TIMESTAMP()) AS dth_create,
--   CASE
--     WHEN oldgr.dth_create IS NOT NULL THEN CURRENT_TIMESTAMP()
-- END
--   AS dth_update,
  oldgr.dth_update,
  coalesce(devid.uuid_token_firebase,oldgr.uuid_token_firebase,
    hist.uuid_token_firebase) AS uuid_token_firebase,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_create_origin
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_create_system_origin
END
  AS dth_create_system_origin,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_last_change
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_update_system_origin
END
  AS dth_update_system_origin,
  oldgr.lst_client_origins,
  ROW_NUMBER() OVER (PARTITION BY oldgr.num_unique_client ORDER BY oldgr.dth_update DESC) RANK
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS oldgr
LEFT JOIN
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_history_client` AS hist
ON
  oldgr.num_document = hist.num_document
  AND oldgr.ds_document_type = 'CPF'
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_device_id_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'DEVICE_ID')      
          ) AS devid
ON
  oldgr.num_document = devid.num_cpf
  AND oldgr.ds_document_type = 'CPF'
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_loyalty_person`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'LOYALTY')   
          ) AS loyat
ON
  oldgr.num_document = loyat.num_cpf
  AND oldgr.ds_document_type = 'CPF'
LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'ONETRUST')    
          ) AS one
ON
  oldgr.num_document = one.num_cpf
  AND oldgr.ds_document_type = 'CPF'
WHERE oldgr.num_document is not null and
  NOT EXISTS (
  SELECT
    *
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_information_process` AS gr
  WHERE
    oldgr.num_document = gr.num_document
    AND 'CPF' = gr.ds_document_type
))
SELECT * EXCEPT(RANK) FROM last_version
WHERE RANK = 1