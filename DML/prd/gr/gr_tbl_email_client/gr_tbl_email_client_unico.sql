CREATE OR REPLACE TABLE `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_email`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  num_version_golden_record INT64 OPTIONS(description="Golden Record Version Number / Número Versão Do Golden Record"),
  ds_email STRING OPTIONS(description="Email / E-mail"),
  hash_email BYTES OPTIONS(description="Email Anonymization / Anonimização Email"),
  ds_email_type STRING OPTIONS(description="Email Type / Tipo De E-mail"),
  ind_main_email BOOL OPTIONS(description="Main Email Indicator / Indicador Email Principal"),
  ds_source_system STRING OPTIONS(description="Data Source System Indicator / Indicador De Sistema De Origem Do Dado"),
  ind_bounce_email BOOLEAN OPTIONS(description="Email Validation Indicator / Indicador de  Validação de Email"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente")
)
CLUSTER BY num_unique_client, ds_email
OPTIONS(
  description="Table Email data from people / Tabela Dados De Emails De Pessoas"
)
AS
With tb_emails_cliunico as ( 
   Select ds_email_type, ds_email	
	from (  Select ds_email_type, ds_email, count(0) as total
			  from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process`
			group by ds_email_type, ds_email
			having count(0)=1
		 ) a
)
Select a.num_unique_client	 ,
       a.num_version_golden_record,
	   a.ds_email			,
	   a.hash_email		 ,
	   a.ds_email_type	,
	   a.ind_main_email	,
	   a.ds_source_system	,
	   	a.ind_bounce_email,
	   a.dth_create,
	   a.dth_update,
  from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` a 
 inner join tb_emails_cliunico b
			on a.ds_email_type = b.ds_email_type 
		   and a.ds_email       = b.ds_email; 