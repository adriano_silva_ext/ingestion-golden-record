WITH
  tb_emails_multicli AS (
	select *
	from (
	  SELECT
		a.ds_email_type,
		a.ds_email,
		MIN(a.num_unique_client) num_unique_client
	  FROM
		`br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
	  inner join (
	   Select ds_email_type, ds_email
		from (  Select ds_email_type, ds_email, count(0) as total
				  from `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process`
				group by ds_email_type, ds_email
				having count(0)>1
			 ) a
	  ) b on a.ds_email = b.ds_email
		 and a.ds_email_type = b.ds_email_type
	  group by a.ds_email_type, a.ds_email    
	) a
 )
SELECT
  a.num_unique_client,
  a.num_version_golden_record,
  a.ds_email,
  a.hash_email,
  a.ds_email_type,
  a.ind_main_email,
  a.ds_source_system,
  a.dth_create,
  a.dth_update,
  a.ind_bounce_email
FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` a
INNER JOIN
  tb_emails_multicli b
ON
  a.ds_email = b.ds_email
  AND a.ds_email_type = b.ds_email_type
  AND a.num_unique_client = b.num_unique_client;