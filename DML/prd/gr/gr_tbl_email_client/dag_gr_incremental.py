import xml.etree.ElementTree as ET
from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.models import Variable
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from google.cloud import pubsub_v1, storage
from requests.api import get
from requests_oauthlib import OAuth1

environment = Variable.get("environment")
dag_docs = """
## Base Única Golden Record - Camada Golden Record
#### Objetivo
    Executar a partir de uma query a construção da camada Golden Record a partir da camada Target
"""
local_tz = pendulum.timezone("America/Sao_Paulo")
default_args = {
    "owner": "[Engenheiro: Adriano Martins da Silva]",
    "retries": 0,
    "depends_on_past": False,
    "start_date": datetime(2022, 1, 21, 18, 0, 0, tzinfo=local_tz),
}

dag = DAG(
    "dag_gr",
    default_args=default_args,
    description="Ingestão camada Golden Record a partir da Target",
    schedule_interval=None,
)

dag.doc_md = dag_docs

tbl_tmp_last_update_source = BigQueryOperator(
    task_id="tbl_tmp_last_update_source",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_last_update_source",
    sql="/queries/gr/tbl_tmp_last_update_source.sql",
    dag=dag,
)

tbl_last_update_source = BigQueryOperator(
    task_id="tbl_last_update_source",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_last_update_source.sql",
    dag=dag,
)

tbl_gr_tmp_client_personal_information = BigQueryOperator(
    task_id="gr_tbm_client_personal_information",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information",
    sql="/queries/gr/tbl_gr_tmp_client_personal_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_vtex_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_vtex_information",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_vtex_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_csf_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_csf_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_csf_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_rh_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_rh_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_rh_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_sva_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_sva_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_sva_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_neoassist_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_neoassist_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_neoassist_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_qualibest_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_qualibest_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_gr_tmp_client_qualibest_information.sql",
    dag=dag,
)

# tbl_gr_tmp_client_history_information = BigQueryOperator(
#     task_id="tbl_gr_tmp_client_history_information",
#     use_legacy_sql=False,
#     write_disposition="WRITE_APPEND",
#     create_disposition="CREATE_IF_NEEDED",
#     allow_large_results=True,
#     destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
#     sql="/queries/gr/tbl_gr_tmp_client_history_information.sql",
#     dag=dag,
# )

cpf_unico_gr_tbl_client_personal_information = BigQueryOperator(
    task_id="cpf_unico_gr_tbl_client_personal_information",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/cpf_unico_gr_tbl_client_personal_information.sql",
    dag=dag,
)

cpf_multicli_gr_tbl_client_personal_information = BigQueryOperator(
    task_id="cpf_multicli_gr_tbl_client_personal_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_personal_information",
    sql="/queries/gr/cpf_multicli_gr_tbl_client_personal_information.sql",
    dag=dag,
)

tbl_gr_tmp_client_oldgr_information = BigQueryOperator(
    task_id="tbl_gr_tmp_client_oldgr_information",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_personal_information",
    sql="/queries/gr/tbl_gr_tmp_client_oldgr_information.sql",
    dag=dag,
)

lst_reasons_bad_gr_client_personal_information = BigQueryOperator(
    task_id="lst_reasons_bad_gr_client_personal_information",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/lst_reasons_bad_gr_client_personal_information.sql",
    dag=dag,
)

gr_tbl_tmp_email_client = BigQueryOperator(
    task_id="gr_tbl_tmp_email_client",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_email",
    sql="/queries/gr/gr_tbl_tmp_email_client.sql",
    dag=dag,
)

gr_tbl_aux_email_client_cpf = BigQueryOperator(
    task_id="gr_tbl_aux_email_client_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_email_process",
    sql="/queries/gr/gr_tbl_aux_email_client_cpf.sql",
    dag=dag,
)

gr_tbl_aux_email_client_cpf_comercial = BigQueryOperator(
    task_id="gr_tbl_aux_email_client_cpf_comercial",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_email_process",
    sql="/queries/gr/gr_tbl_aux_email_client_cpf_comercial.sql",
    dag=dag,
)

gr_tbl_aux_email_client_sem_cpf = BigQueryOperator(
    task_id="gr_tbl_aux_email_client_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_email_process",
    sql="/queries/gr/gr_tbl_aux_email_client_sem_cpf.sql",
    dag=dag,
)

gr_tbl_aux_email_client_sem_cpf_comercial = BigQueryOperator(
    task_id="gr_tbl_aux_email_client_sem_cpf_comercial",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_email_process",
    sql="/queries/gr/gr_tbl_aux_email_client_sem_cpf_comercial.sql",
    dag=dag,
)

gr_tbl_email_client_unico = BigQueryOperator(
    task_id="gr_tbl_email_client_unico",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/gr_tbl_email_client_unico.sql",
    dag=dag,
)

# gr_tbl_email_cligr_tbl_client_email_cpl = BigQueryOperator(
#     task_id="gr_tbl_email_cligr_tbl_client_email_cpl",
#     use_legacy_sql=False,
#     write_disposition="WRITE_APPEND",
#     create_disposition="CREATE_IF_NEEDED",
#     allow_large_results=True,
#     destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_email",
#     sql="/queries/gr/gr_tbl_client_email_cpl.sql",
#     dag=dag,
# )

gr_tbl_email_client_deduplica = BigQueryOperator(
    task_id="gr_tbl_email_client_deduplica",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_email",
    sql="/queries/gr/gr_tbl_email_client_deduplica.sql",
    dag=dag,
)

tbl_client_personal_information_bad_gr = BigQueryOperator(
    task_id="tbl_client_personal_information_bad_gr",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_personal_information_bad_gr.sql",
    dag=dag,
)

tbl_client_personal_information_via_email = BigQueryOperator(
    task_id="tbl_client_personal_information_via_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_personal_information",
    sql="/queries/gr/tbl_client_personal_information_via_email.sql",
    dag=dag,
)

tbl_tmp_client_phone = BigQueryOperator(
    task_id="tbl_tmp_client_phone",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_phone",
    sql="/queries/gr/tbl_tmp_client_phone.sql",
    dag=dag,
)

tbl_tmp_information_process_phone_cpf = BigQueryOperator(
    task_id="tbl_tmp_information_process_phone_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_tmp_information_process_phone_cpf.sql",
    dag=dag,
)

tbl_tmp_information_process_phone_sem_cpf = BigQueryOperator(
    task_id="tbl_tmp_information_process_phone_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_tmp_information_process_phone_sem_cpf.sql",
    dag=dag,
)

tbl_client_phone = BigQueryOperator(
    task_id="tbl_client_phone",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_phone.sql",
    dag=dag,
)

tbl_tmp_client_phone_optin = BigQueryOperator(
    task_id="tbl_tmp_client_phone_optin",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_phone_optin",
    sql="/queries/gr/tbl_tmp_client_phone_optin.sql",
    dag=dag,
)

tbl_client_phone_optin = BigQueryOperator(
    task_id="tbl_client_phone_optin",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_phone_optin.sql",
    dag=dag,
)

tbl_client_phone_optin_sem_cpf = BigQueryOperator(
    task_id="tbl_client_phone_optin_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_phone_optin",
    sql="/queries/gr/tbl_client_phone_optin_sem_cpf.sql",
    dag=dag,
)

tbl_client_phone_optin_history = BigQueryOperator(
    task_id="tbl_client_phone_optin_history",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_phone_optin",
    sql="/queries/gr/tbl_client_phone_optin_history.sql",
    dag=dag,
)

tbl_tmp_client_general_consent = BigQueryOperator(
    task_id="tbl_tmp_client_general_consent",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_general_consent",
    sql="/queries/gr/tbl_tmp_client_general_consent.sql",
    dag=dag,
)

tbl_client_general_consent = BigQueryOperator(
    task_id="tbl_client_general_consent",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_general_consent.sql",
    dag=dag,
)

tbl_tmp_client_collection_point = BigQueryOperator(
    task_id="tbl_tmp_client_collection_point",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_collection_point",
    sql="/queries/gr/tbl_tmp_client_collection_point.sql",
    dag=dag,
)

tbl_client_collection_point = BigQueryOperator(
    task_id="tbl_client_collection_point",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_collection_point.sql",
    dag=dag,
)

tbl_client_general_consent_sem_cpf = BigQueryOperator(
    task_id="tbl_client_general_consent_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_general_consent",
    sql="/queries/gr/tbl_client_general_consent_sem_cpf.sql",
    dag=dag,
)

tbl_client_collection_point_sem_cpf = BigQueryOperator(
    task_id="tbl_client_collection_point_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_collection_point",
    sql="/queries/gr/tbl_client_collection_point_sem_cpf.sql",
    dag=dag,
)

tbl_tmp_client_email_optin = BigQueryOperator(
    task_id="tbl_tmp_client_email_optin",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_email_optin",
    sql="/queries/gr/tbl_tmp_client_email_optin.sql",
    dag=dag,
)

tbl_client_email_optin = BigQueryOperator(
    task_id="tbl_client_email_optin",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_email_optin.sql",
    dag=dag,
)

tbl_client_email_optin_history = BigQueryOperator(
    task_id="tbl_client_email_optin_history",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_email_optin",
    sql="/queries/gr/tbl_client_email_optin_history.sql",
    dag=dag,
)

tbl_tmp_client_vtex = BigQueryOperator(
    task_id="tbl_tmp_client_vtex",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_vtex",
    sql="/queries/gr/tbl_tmp_client_vtex.sql",
    dag=dag,
)

tbl_client_vtex = BigQueryOperator(
    task_id="tbl_client_vtex",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_vtex.sql",
    dag=dag,
)

tbl_client_vtex_email = BigQueryOperator(
    task_id="tbl_client_vtex_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_vtex",
    sql="/queries/gr/tbl_client_vtex_email.sql",
    dag=dag,
)

tbl_tmp_client_rh = BigQueryOperator(
    task_id="tbl_tmp_client_rh",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_rh",
    sql="/queries/gr/tbl_tmp_client_rh.sql",
    dag=dag,
)

tbl_client_rh = BigQueryOperator(
    task_id="tbl_client_rh",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_rh.sql",
    dag=dag,
)

tbl_client_rh_email = BigQueryOperator(
    task_id="tbl_client_rh_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_rh",
    sql="/queries/gr/tbl_client_rh_email.sql",
    dag=dag,
)

tbl_tmp_client_sva = BigQueryOperator(
    task_id="tbl_tmp_client_sva",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_sva",
    sql="/queries/gr/tbl_tmp_client_sva.sql",
    dag=dag,
)

tbl_client_sva = BigQueryOperator(
    task_id="tbl_client_sva",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_sva.sql",
    dag=dag,
)

tbl_client_sva_email = BigQueryOperator(
    task_id="tbl_client_sva_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_sva",
    sql="/queries/gr/tbl_client_sva_email.sql",
    dag=dag,
)

tbl_tmp_client_neoassist = BigQueryOperator(
    task_id="tbl_tmp_client_neoassist",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_neoassist",
    sql="/queries/gr/tbl_tmp_client_neoassist.sql",
    dag=dag,
)

tbl_client_neoassist = BigQueryOperator(
    task_id="tbl_client_neoassist",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_neoassist.sql",
    dag=dag,
)

tbl_client_neoassist_email = BigQueryOperator(
    task_id="tbl_client_neoassist_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_neoassist",
    sql="/queries/gr/tbl_client_neoassist_email.sql",
    dag=dag,
)

tbl_tmp_client_propz = BigQueryOperator(
    task_id="tbl_tmp_client_propz",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_propz",
    sql="/queries/gr/tbl_tmp_client_propz.sql",
    dag=dag,
)

tbl_client_propz = BigQueryOperator(
    task_id="tbl_client_propz",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_propz.sql",
    dag=dag,
)

tbl_client_propz_email = BigQueryOperator(
    task_id="tbl_client_propz_email",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_propz",
    sql="/queries/gr/tbl_client_propz_email.sql",
    dag=dag,
)

tbl_tmp_client_address = BigQueryOperator(
    task_id="tbl_tmp_client_address",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_client_address",
    sql="/queries/gr/tbl_tmp_client_address.sql",
    dag=dag,
)

tbl_tmp_information_process = BigQueryOperator(
    task_id="tbl_tmp_information_process",
    use_legacy_sql=False,
    write_disposition="WRITE_TRUNCATE",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
    sql="/queries/gr/tbl_tmp_information_process.sql",
    dag=dag,
)

tbl_client_address = BigQueryOperator(
    task_id="tbl_client_address",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_address.sql",
    dag=dag,
)

# tbl_tmp_information_process_sem_cpf = BigQueryOperator(
#     task_id="tbl_tmp_information_process_sem_cpf",
#     use_legacy_sql=False,
#     write_disposition="WRITE_TRUNCATE",
#     create_disposition="CREATE_IF_NEEDED",
#     allow_large_results=True,
#     destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_tmp_information_process",
#     sql="/queries/gr/tbl_tmp_information_process_sem_cpf.sql",
#     dag=dag,
# )

tbl_client_address_sem_cpf = BigQueryOperator(
    task_id="tbl_client_address_sem_cpf",
    use_legacy_sql=False,
    write_disposition="WRITE_APPEND",
    create_disposition="CREATE_IF_NEEDED",
    allow_large_results=True,
    destination_dataset_table=f"br-apps-bi-customermdm-{environment}.db_dolphin_target_clientmdm.tbl_client_address",
    sql="/queries/gr/tbl_client_address_sem_cpf.sql",
    dag=dag,
)

tbl_client = BigQueryOperator(
    task_id="tbl_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client.sql",
    dag=dag,
)

tbl_client_metrics = BigQueryOperator(
    task_id="tbl_client_metrics",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/gr/tbl_client_metrics.sql",
    dag=dag,
)

tk_start_gr = TriggerDagRunOperator(
    task_id="tk_start_gr", trigger_dag_id="dag_gr",
    dag=dag,
)


def print_configuration_function(**context):
    last_execution = str(context['prev_execution_date'])
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(
        context["project_id"], context["topic_name"])
    message_bytes = last_execution.encode('utf-8')
    try:
        publish_future = publisher.publish(topic_path, data=message_bytes)
        publish_future.result()  # Verify the publish succeeded
        return True
    except Exception:
        return False


load_bt = PythonOperator(
    task_id="load_bt",
    python_callable=print_configuration_function,
    provide_context=True,
    op_kwargs={'project_id': f'br-apps-bi-customermdm-{environment}',
               'topic_name': f'br-apps-bi-customermdm-{environment}-cliente-trigger-process'},
    dag=dag)

tk_run_optin = DummyOperator(task_id="tk_run_optin", dag=dag,)

tk_run_complementary = DummyOperator(task_id="tk_run_complementary", dag=dag,)

tbl_tmp_last_update_source >> tbl_last_update_source >> [
    tbl_gr_tmp_client_personal_information,
    gr_tbl_tmp_email_client,
    tbl_tmp_client_phone,
    tbl_tmp_client_phone_optin,
    tbl_tmp_client_address,
    tbl_tmp_client_general_consent,
    tbl_tmp_client_collection_point,
    tbl_tmp_client_email_optin,
    tbl_tmp_client_vtex,
    tbl_tmp_client_rh,
    tbl_tmp_client_sva,
    tbl_tmp_client_neoassist,
    tbl_tmp_client_propz,
    # ] >> tbl_gr_tmp_client_vtex_information >> tbl_gr_tmp_client_csf_information >> tbl_gr_tmp_client_rh_information >> tbl_gr_tmp_client_sva_information >> tbl_gr_tmp_client_neoassist_information >> tbl_gr_tmp_client_qualibest_information >> tbl_gr_tmp_client_history_information >> cpf_unico_gr_tbl_client_personal_information >> cpf_multicli_gr_tbl_client_personal_information >> tbl_gr_tmp_client_oldgr_information >> gr_tbl_aux_email_client_cpf >> gr_tbl_aux_email_client_cpf_comercial >> gr_tbl_aux_email_client_sem_cpf >> gr_tbl_aux_email_client_sem_cpf_comercial >> gr_tbl_email_client_unico >> gr_tbl_email_client_deduplica >> tbl_client_personal_information_bad_gr >> tbl_client_personal_information_via_email >> lst_reasons_bad_gr_client_personal_information >> tbl_tmp_information_process_phone_cpf >> tbl_tmp_information_process_phone_sem_cpf >> tbl_client_phone >> [
    # ] >> tbl_gr_tmp_client_vtex_information >> tbl_gr_tmp_client_csf_information >> tbl_gr_tmp_client_rh_information >> tbl_gr_tmp_client_sva_information >> tbl_gr_tmp_client_neoassist_information >> tbl_gr_tmp_client_qualibest_information >> cpf_unico_gr_tbl_client_personal_information >> cpf_multicli_gr_tbl_client_personal_information >> tbl_gr_tmp_client_oldgr_information >> gr_tbl_aux_email_client_cpf >> gr_tbl_aux_email_client_cpf_comercial >> gr_tbl_aux_email_client_sem_cpf >> gr_tbl_aux_email_client_sem_cpf_comercial >> gr_tbl_email_client_unico >> gr_tbl_email_client_deduplica >> gr_tbl_email_cligr_tbl_client_email_cpl >> tbl_client_personal_information_bad_gr >> tbl_client_personal_information_via_email >> lst_reasons_bad_gr_client_personal_information >> tbl_tmp_information_process_phone_cpf >> tbl_tmp_information_process_phone_sem_cpf >> tbl_client_phone >> [
] >> tbl_gr_tmp_client_vtex_information >> tbl_gr_tmp_client_csf_information >> tbl_gr_tmp_client_rh_information >> tbl_gr_tmp_client_sva_information >> tbl_gr_tmp_client_neoassist_information >> tbl_gr_tmp_client_qualibest_information >> cpf_unico_gr_tbl_client_personal_information >> cpf_multicli_gr_tbl_client_personal_information >> tbl_gr_tmp_client_oldgr_information >> gr_tbl_aux_email_client_cpf >> gr_tbl_aux_email_client_cpf_comercial >> gr_tbl_aux_email_client_sem_cpf >> gr_tbl_aux_email_client_sem_cpf_comercial >> gr_tbl_email_client_unico >> gr_tbl_email_client_deduplica >> tbl_client_personal_information_bad_gr >> tbl_client_personal_information_via_email >> lst_reasons_bad_gr_client_personal_information >> tbl_tmp_information_process_phone_cpf >> tbl_tmp_information_process_phone_sem_cpf >> tbl_client_phone >> [
    tbl_client_general_consent,
    tbl_client_collection_point,
    tbl_client_email_optin,
    tbl_client_phone_optin,
] >> tk_run_optin >> [
    tbl_client_general_consent_sem_cpf,
    tbl_client_collection_point_sem_cpf,
    tbl_client_email_optin_history,
    tbl_client_phone_optin_sem_cpf,
] >> tbl_client_phone_optin_history >> [
    tbl_client_vtex,
    tbl_client_rh,
    tbl_client_sva,
    tbl_client_neoassist,
    tbl_client_propz,
] >> tk_run_complementary >> [
    tbl_client_vtex_email,
    tbl_client_rh_email,
    tbl_client_sva_email,
    tbl_client_neoassist_email,
    tbl_client_propz_email,
    # ] >> tbl_tmp_information_process >> tbl_client_address >> tbl_tmp_information_process_sem_cpf >> tbl_client_address_sem_cpf >> [tbl_client, load_bt] >> tbl_client_metrics >> tk_start_gr
] >> tbl_tmp_information_process >> tbl_client_address >> tbl_client_address_sem_cpf >> [tbl_client, load_bt] >> tbl_client_metrics >> tk_start_gr
