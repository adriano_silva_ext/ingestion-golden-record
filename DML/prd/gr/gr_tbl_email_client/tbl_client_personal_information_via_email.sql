WITH
  tab_cli_aux AS (
  SELECT
    a.num_unique_client,
    a.num_version_golden_record AS num_version_golden_record,
  COALESCE(vtex.ds_full_name,CONCAT(vtex.ds_first_name,' ',vtex.ds_last_name),
    csf.ds_full_name,
    rh.ds_full_name,
    sva.ds_full_name,
--    CONCAT(propz.ds_first_name,' ',propz.ds_last_name),
    neo.ds_full_name,
    quali.ds_full_name) AS ds_full_name,
  CASE
		WHEN vtex.ds_first_name IS NOT NULL or vtex.ds_full_name IS NOT NULL THEN coalesce(trim(vtex.ds_first_name),substr(vtex.ds_full_name,1,case when STRPOS(vtex.ds_full_name, ' ') > 0 then STRPOS(vtex.ds_full_name, ' ') else length(vtex.ds_full_name)+1 end - 1))
		WHEN csf.ds_first_name IS NOT NULL or csf.ds_full_name IS NOT NULL THEN coalesce(trim(csf.ds_first_name),substr(csf.ds_full_name,1,case when STRPOS(csf.ds_full_name, ' ') > 0 then STRPOS(csf.ds_full_name, ' ') else length(csf.ds_full_name)+1 end - 1))
		WHEN rh.ds_full_name IS NOT NULL THEN substr(rh.ds_full_name,1,case when STRPOS(rh.ds_full_name, ' ') > 0 then STRPOS(rh.ds_full_name, ' ') else length(rh.ds_full_name)+1 end - 1)
		WHEN sva.ds_full_name IS NOT NULL THEN substr(sva.ds_full_name,1,case when STRPOS(sva.ds_full_name, ' ') > 0 then STRPOS(sva.ds_full_name, ' ') else length(sva.ds_full_name)+1 end - 1)
--		WHEN propz.ds_first_name IS NOT NULL THEN propz.ds_first_name
		WHEN neo.ds_full_name IS NOT NULL THEN substr(neo.ds_full_name,1,case when STRPOS(neo.ds_full_name, ' ') > 0 then STRPOS(neo.ds_full_name, ' ') else length(neo.ds_full_name)+1 end - 1)
		WHEN quali.ds_full_name IS NOT NULL THEN substr(quali.ds_full_name,1,case when STRPOS(quali.ds_full_name, ' ') > 0 then STRPOS(quali.ds_full_name, ' ') else length(quali.ds_full_name)+1 end - 1)
	END
    AS ds_first_name,
	CASE
		WHEN vtex.ds_last_name IS NOT NULL or vtex.ds_full_name IS NOT NULL THEN coalesce(trim(vtex.ds_last_name),ARRAY_REVERSE(SPLIT(replace(vtex.ds_full_name,' ',',')))[SAFE_OFFSET(0)])
		WHEN csf.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(csf.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
		WHEN rh.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(rh.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
		WHEN sva.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(sva.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
--		WHEN propz.ds_last_name IS NOT NULL THEN propz.ds_last_name
		WHEN neo.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(neo.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
		WHEN quali.ds_full_name IS NOT NULL THEN ARRAY_REVERSE(SPLIT(replace(quali.ds_full_name,' ',',')))[SAFE_OFFSET(0)]
	END
	AS ds_last_name,
	CASE
		WHEN vtex.ds_full_name IS NOT NULL THEN 'VTEX'
		WHEN csf.ds_full_name IS NOT NULL THEN 'CSF'
		WHEN rh.ds_full_name IS NOT NULL THEN 'RH'
		WHEN sva.ds_full_name IS NOT NULL THEN 'SVA'
--		WHEN CONCAT(propz.ds_first_name,' ',propz.ds_last_name) IS NOT NULL THEN 'PROPZ'
		WHEN neo.ds_full_name IS NOT NULL THEN 'NEOASSIST'
		WHEN quali.ds_full_name IS NOT NULL THEN 'QUALIBEST'
	END
	AS ds_name_source_system,
    CAST(NULL AS NUMERIC) AS num_document,
    CAST(NULL AS BYTES) AS hash_document,
    CAST(NULL AS STRING) AS ds_document_type,
    CAST(NULL AS STRING) AS ds_document_source_system,
    COALESCE(quali.dth_birth,
      vtex.dth_birth,
      rh.dth_birth,
      sva.dth_birth) AS dth_birth,
--      propz.dth_birth) AS dth_birth,
    CASE
      WHEN quali.dth_birth IS NOT NULL THEN 'QUALIBEST'
      WHEN vtex.dth_birth IS NOT NULL THEN 'VTEX'
      WHEN rh.dth_birth IS NOT NULL THEN 'RH'
      WHEN sva.dth_birth IS NOT NULL THEN 'SVA'
--      WHEN propz.dth_birth IS NOT NULL THEN 'PROPZ'
  END AS ds_birth_source_system,
    CASE
      WHEN vtex.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = vtex.ini_gender AND ds_source_system = 'VTEX')
      WHEN sva.ini_gender IS NOT NULL THEN (
    SELECT
      ini_gender
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender`
    WHERE
      ini_gender_origin = sva.ini_gender
      AND ds_source_system = 'SVA')
--      WHEN propz.ini_gender IS NOT NULL THEN ( SELECT ini_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = propz.ini_gender AND ds_source_system = 'PROPZ')
  END
    AS ini_gender,
    CASE
      WHEN vtex.ini_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = vtex.ini_gender AND ds_source_system = 'VTEX')
      WHEN sva.ini_gender IS NOT NULL THEN (
    SELECT
      ds_gender
    FROM
      `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender`
    WHERE
      ini_gender_origin = sva.ini_gender
      AND ds_source_system = 'SVA')
--      WHEN propz.ini_gender IS NOT NULL THEN ( SELECT ds_gender FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_gender` WHERE ini_gender_origin = propz.ini_gender AND ds_source_system = 'PROPZ')
  END
    AS ds_gender,
    CASE
      WHEN vtex.ini_gender IS NOT NULL THEN 'VTEX'
      WHEN sva.ini_gender IS NOT NULL THEN 'SVA'
--      WHEN propz.ini_gender IS NOT NULL THEN 'PROPZ'
  END
    AS ds_gender_source_system,
    CASE
      WHEN sva.ini_marital_status IS NOT NULL THEN ( SELECT ini_marital_status FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_marital` WHERE ini_marital_status_origin = sva.ini_marital_status AND ds_source_system = 'SVA')
  END
    AS ini_marital_status,
    CASE
      WHEN sva.ini_marital_status IS NOT NULL THEN ( SELECT ds_marital_status FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_aux_marital` WHERE ini_marital_status_origin = sva.ini_marital_status AND ds_source_system = 'SVA')
  END
    AS ds_marital_status,
    CASE
      WHEN sva.ini_marital_status IS NOT NULL THEN 'SVA'
  END
    AS ds_marital_status_source_system,
    sva.ds_nationality,
    sva.ds_country_birth_name,
    CASE
      WHEN sva.ds_nationality IS NOT NULL OR sva.ds_country_birth_name IS NOT NULL THEN 'SVA'
  END
    AS ds_nationality_source_system,
    TRIM(csf.cod_client_status) AS ind_status_client_csf_carrefour,
    CASE
      WHEN TRIM(csf.cod_client_status) = '1' THEN 'CLIENTE'
      WHEN TRIM(csf.cod_client_status) = '2' THEN 'NEGADOS'
      WHEN TRIM(csf.cod_client_status) = '3' THEN 'EX-CLIENTE'
  END
    AS ds_status_client_csf_carrefour,
    CAST(NULL AS TIMESTAMP) AS dt_admission_minhas_recompensas,
    CASE
      WHEN STARTS_WITH(ds_status_employee, 'RESC') = FALSE AND ds_status_employee IS NOT NULL AND CONTAINS_SUBSTR(ds_status_employee, 'ESTAGI') = FALSE THEN TRUE
    ELSE
    FALSE
  END
    AS ind_employee,
    coalesce(oldgr. ind_has_atacadao,
      NULL) AS ind_has_atacadao,
    CASE
      WHEN csf.cod_client_status = '1' THEN TRUE
    ELSE
    FALSE
  END
    AS ind_has_carrefour,
    coalesce(oldgr.ind_client_blocked,
      FALSE) AS ind_client_blocked,
    csf.ind_portfolio,
    TRUE AS ind_bad_golden_record /* pq neste caso o cliente não tem cpf preenchido */,
    coalesce(oldgr.ind_deleted,
      FALSE) AS ind_deleted,
    CASE
      WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.ds_channel_create_client
      WHEN vtex.num_cpf IS NOT NULL THEN 'VTEX'
      WHEN csf.num_cpf IS NOT NULL THEN 'CSF'
      WHEN rh.num_cpf IS NOT NULL THEN 'RH'
      WHEN sva.num_document IS NOT NULL THEN 'SVA'
--      WHEN propz.num_cpf IS NOT NULL THEN 'PROPZ'
      WHEN neo.num_cpf IS NOT NULL THEN 'NEOASSIST'
      WHEN quali.num_cpf IS NOT NULL THEN 'QUALIBEST'
  END
    AS ds_channel_create_client,
    coalesce(oldgr.dth_create,
      CURRENT_TIMESTAMP()) AS dth_create,
    CASE
      WHEN oldgr.dth_create IS NOT NULL THEN oldgr.dth_update
  END
    AS dth_update,
  coalesce(devid.uuid_token_firebase,oldgr.uuid_token_firebase,
    hist.uuid_token_firebase) AS uuid_token_firebase,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_create_origin
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_create_system_origin
    WHEN vtex.num_cpf IS NOT NULL THEN vtex.dth_create
    WHEN csf.num_cpf IS NOT NULL THEN csf.dth_timestamp
    WHEN rh.num_cpf IS NOT NULL THEN rh.dth_partition
    WHEN sva.num_document IS NOT NULL THEN sva.dth_create
--    WHEN propz.num_cpf IS NOT NULL THEN propz.dth_create
    WHEN neo.num_cpf IS NOT NULL THEN neo.dth_create
    WHEN quali.num_cpf IS NOT NULL THEN quali.dth_partition
END
  AS dth_create_system_origin,
  CASE
    WHEN hist.ds_channel_create_client is not null then hist.dth_last_change
    WHEN oldgr.ds_channel_create_client IS NOT NULL THEN oldgr.dth_update_system_origin
    WHEN vtex.num_cpf IS NOT NULL THEN vtex.dth_update
    WHEN csf.num_cpf IS NOT NULL THEN csf.dth_timestamp
    WHEN rh.num_cpf IS NOT NULL THEN rh.dth_partition
    WHEN sva.num_document IS NOT NULL THEN sva.dth_update
--    WHEN propz.num_cpf IS NOT NULL THEN propz.dth_update
    WHEN neo.num_cpf IS NOT NULL THEN neo.dth_update
    WHEN quali.num_cpf IS NOT NULL THEN quali.dth_partition
END
  AS dth_update_system_origin
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_email_process` AS a
  LEFT JOIN
    `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_client_personal_information` AS oldgr
  ON
    a.num_unique_client = oldgr.num_unique_client
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'VTEX')  
     )AS vtex
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(vtex.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_csf_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'CSF')  
          ) AS csf
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(csf.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_rh_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'RH')  
          ) AS rh
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(rh.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'SVA')  

          ) AS sva
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(sva.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_propz_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'PROPZ')  

          ) AS propz
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(propz.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_neoassist_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'NEOASSIST')  
          ) AS neo
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(neo.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_qualibest_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'QUALIBEST')  
          ) AS quali
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(quali.ds_email))
  LEFT JOIN (
  Select * FROM
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_device_id_client`
  where dth_partition > (SELECT dth_partition
                                       FROM `br-apps-bi-customermdm-prd.db_dolphin_target_clientmdm.tbl_tmp_last_update_source`
									  WHERE ds_source_name = 'DEVICE_ID')    
          ) AS devid
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(devid.ds_email))
  LEFT JOIN
    `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_history_client` AS hist
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(hist.ds_email)) 
  WHERE
    a.ind_document = FALSE )
SELECT
  a.num_unique_client,
  a.num_version_golden_record,
  a.ds_full_name,
  a.ds_first_name,
  a.ds_last_name,
  a.ds_name_source_system,
  a.num_document,
  a.hash_document,
  a.ds_document_type,
  a.ds_document_source_system,
  a.dth_birth,
  a.ds_birth_source_system,
  a.ini_gender,
  a.ds_gender,
  a.ds_gender_source_system,
  a.ini_marital_status,
  a.ds_marital_status,
  a.ds_marital_status_source_system,
  a.ds_nationality,
  a.ds_country_birth_name,
  a.ds_nationality_source_system,
  a.ind_status_client_csf_carrefour,
  a.ds_status_client_csf_carrefour,
  a.dt_admission_minhas_recompensas,
  a.ind_employee,
  a.ind_has_atacadao,
  a.ind_has_carrefour,
  a.ind_client_blocked,
  a.ind_portfolio,
  a.ind_bad_golden_record,
  a.ind_deleted,
  a.ds_channel_create_client,
  a.dth_create,
  a.dth_update,
  a.uuid_token_firebase,
  b.lst_client_origins
FROM (
  SELECT
    DISTINCT *
  FROM
    tab_cli_aux ) a
LEFT JOIN (
  SELECT
    num_unique_client,
    ARRAY_AGG(DISTINCT origem IGNORE NULLS) lst_client_origins
  FROM
    tab_cli_aux,
    UNNEST([CAST(ds_name_source_system AS STRING), CAST(ds_document_source_system AS STRING), CAST(ds_birth_source_system AS STRING), CAST(ds_gender_source_system AS STRING), CAST(ds_marital_status_source_system AS STRING), CAST(ds_status_client_csf_carrefour AS STRING)]) AS origem
  GROUP BY
    num_unique_client ) b
ON
  a.num_unique_client = b.num_unique_client;