MERGE
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_sva_client` tgt
USING
  (
WITH
  sva_client AS (
  SELECT
    UPPER(idclient) AS uuid_client,
    UPPER(maritalstatus) AS ini_marital_status,
    UPPER(sellerdevice) AS cod_seller_device,
    UPPER(email) AS ds_email,
    UPPER(nationality) AS ds_nationality,
    CASE
      WHEN CONTAINS_SUBSTR(birth, '/') THEN TIMESTAMP(CONCAT(SUBSTR(birth,7,4),'-',SUBSTR(birth,4,2),'-',SUBSTR(birth,1,2)), "America/Sao_Paulo")
    ELSE
    TIMESTAMP(SUBSTR(birth,0,10) , "America/Sao_Paulo")
  END
    AS dth_birth,
    TIMESTAMP(createorigin) AS dth_create_system_origin,
    TIMESTAMP(updateorigin) AS dth_update_system_origin,
    UPPER(emailinput) AS ind_email_input,
    UPPER(indloyalty) AS ind_loyalty,
    UPPER(optinemail) AS ind_optin_email,
    UPPER(optinsms) AS ind_optin_sms,
    UPPER(optinwhatsapp) AS ind_optin_whatsapp,
    UPPER(gender) AS ini_gender,
    UPPER(countrybirth) AS ds_country_birth_name,
    UPPER(namefull) AS ds_full_name,
    SAFE_CAST(REGEXP_REPLACE(numdocument, r'[.,-]', '') AS NUMERIC) AS num_document,
    UPPER(phone) AS num_landline,
    CASE CHAR_LENGTH(REGEXP_REPLACE(mobilefone, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(mobilefone, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(mobilefone, r'[\s()-]', '') END as num_mobile_phone,
    UPPER(statesubscription) AS num_state_subscription,
    UPPER(documenttype) AS ds_document_type,
    UPPER(seller) AS cod_seller,
    dth_partition,
    ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(numdocument, r'[.,-]', '') AS NUMERIC)
    ORDER BY
      dth_partition DESC) RANK
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_stage_client_base.tbl_sva_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    sva_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_document = stg.num_document
  WHEN MATCHED THEN UPDATE 
  SET 
  uuid_client = stg.uuid_client,
  ini_marital_status = stg.ini_marital_status, 
  cod_seller_device = stg.cod_seller_device,
  ds_email = stg.ds_email,
  ds_nationality = stg.ds_nationality,
  dth_birth = stg.dth_birth,
  dth_create_system_origin = stg.dth_create_system_origin,
  dth_update_system_origin = stg.dth_update_system_origin,
  ind_email_input = stg.ind_email_input,
  ind_loyalty = stg.ind_loyalty,
  ind_optin_email = stg.ind_optin_email,
  ind_optin_sms = stg.ind_optin_sms,
  ind_optin_whatsapp = stg.ind_optin_whatsapp,
  ini_gender = stg.ini_gender,
  ds_country_birth_name = stg.ds_country_birth_name,
  ds_full_name = stg.ds_full_name,
  num_document = stg.num_document,
  num_landline = stg.num_landline,
  num_mobile_phone = stg.num_mobile_phone,
  num_state_subscription = stg.num_state_subscription,
  ds_document_type = stg.ds_document_type,
  cod_seller = stg.cod_seller,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  uuid_client,
  ini_marital_status,
  cod_seller_device,
  ds_email,
  ds_nationality,
  dth_birth,
  dth_create_system_origin,
  dth_update_system_origin,
  ind_email_input,
  ind_loyalty,
  ind_optin_email,
  ind_optin_sms,
  ind_optin_whatsapp,
  ini_gender,
  ds_country_birth_name,
  ds_full_name,
  num_document,
  num_landline,
  num_mobile_phone,
  num_state_subscription,
  ds_document_type,
  cod_seller,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.uuid_client,
  stg.ini_marital_status,
  stg.cod_seller_device,
  stg.ds_email,
  stg.ds_nationality,
  stg.dth_birth,
  stg.dth_create_system_origin,
  stg.dth_update_system_origin,
  stg.ind_email_input,
  stg.ind_loyalty,
  stg.ind_optin_email,
  stg.ind_optin_sms,
  stg.ind_optin_whatsapp,
  stg.ini_gender,
  stg.ds_country_birth_name,
  stg.ds_full_name,
  stg.num_document,
  stg.num_landline,
  stg.num_mobile_phone,
  stg.num_state_subscription,
  stg.ds_document_type,
  stg.cod_seller,
  stg.dth_partition,
  stg.dth_partition)