MERGE
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_vtex_client` tgt
USING
(
WITH
  vtex_client AS (
  SELECT
    CASE
      WHEN REGEXP_CONTAINS(TRIM(email), r'(.\W|^)([a-zA-Z0-9.!#$%&&#39;+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)') IS TRUE THEN UPPER(email)
    ELSE
    NULL
  END
    AS ds_email,
    TIMESTAMP(SUBSTR(birthdate,1,10) , "America/Sao_Paulo") AS dth_birth,
    TIMESTAMP(SUBSTR(createdin,1,19) , "America/Sao_Paulo") AS dth_create_system_origin,
    TIMESTAMP(SUBSTR(updatedin,1,19) , "America/Sao_Paulo") AS dth_update_system_origin,
    SAFE_CAST(isnewsletteroptin AS BOOLEAN) AS ind_optin_email_indicator,
    SAFE_CAST(issmsoptin AS BOOLEAN) AS ind_optin_sms,
    SAFE_CAST(iswhatsapppptin AS BOOLEAN) AS ind_optin_whatsapp,
    UPPER(gender) AS ini_gender,
    UPPER(channel) AS ds_channel,
    UPPER(firstName) AS ds_first_name,
    UPPER(nm_full_name) AS ds_full_name,
    UPPER(lastname) AS ds_last_name,
    UPPER(businessphone) AS num_business_phone,
    SAFE_CAST(REGEXP_REPLACE(document, r'[.,-]', '') AS NUMERIC) AS num_cpf,
    CASE CHAR_LENGTH(REGEXP_REPLACE(homephone, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(homephone, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(homephone, r'[\s()-]', '') END as num_mobile_phone,
    CASE CHAR_LENGTH(REGEXP_REPLACE(phone, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(phone, r'[\s()-]', ''))
    WHEN 10 THEN CONCAT('+55',REGEXP_REPLACE(phone, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(phone, r'[\s()-]', '') END as num_phone,
    UPPER(documenttype) AS ds_document_type,
    UPPER(id) AS uuid_client,
    dth_partition,
    ROW_NUMBER() OVER (PARTITION BY TRIM(id)
    ORDER BY
      dth_partition DESC) RANK
  FROM
    `br-apps-bi-customermdm-prd.db_dolphin_stage_client_base.tbl_vtex_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date),
  vtex_client_id AS (
  SELECT
    * EXCEPT (RANK)
    ,ROW_NUMBER() OVER (PARTITION BY num_cpf ORDER BY dth_partition DESC) RANK1
  FROM
    vtex_client
  WHERE
    RANK = 1)
  SELECT
    * EXCEPT (RANK1)
  FROM
    vtex_client_id
  WHERE
    RANK1 = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET 
  ds_email = stg.ds_email, 
  dth_birth = stg.dth_birth,
  ind_optin_email_indicator = stg.ind_optin_email_indicator,
  ind_optin_sms = stg.ind_optin_sms,
  ind_optin_whatsapp = stg.ind_optin_whatsapp,
  ini_gender = stg.ini_gender,
  ds_channel = stg.ds_channel,
  ds_first_name = stg.ds_first_name,
  ds_full_name = stg.ds_full_name,
  ds_last_name = stg.ds_last_name,
  num_business_phone = stg.num_business_phone,
  num_cpf = stg.num_cpf,
  num_mobile_phone = stg.num_mobile_phone,
  num_phone = stg.num_phone,
  ds_document_type = stg.ds_document_type,
  uuid_client = stg.uuid_client,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  ds_email,
  dth_birth,
  dth_create_system_origin,
  dth_update_system_origin,
  ind_optin_email_indicator,
  ind_optin_sms,
  ind_optin_whatsapp,
  ini_gender,
  ds_channel,
  ds_first_name,
  ds_full_name,
  ds_last_name,
  num_business_phone,
  num_cpf,
  num_mobile_phone,
  num_phone,
  ds_document_type,
  uuid_client,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.ds_email,
  stg.dth_birth,
  stg.dth_create_system_origin,
  stg.dth_update_system_origin,
  stg.ind_optin_email_indicator,
  stg.ind_optin_sms,
  stg.ind_optin_whatsapp,
  stg.ini_gender,
  stg.ds_channel,
  stg.ds_first_name,
  stg.ds_full_name,
  stg.ds_last_name,
  stg.num_business_phone,
  stg.num_cpf,
  stg.num_mobile_phone,
  stg.num_phone,
  stg.ds_document_type,
  stg.uuid_client,
  stg.dth_partition,
  stg.dth_partition)