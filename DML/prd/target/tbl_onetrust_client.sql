MERGE
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_onetrust_client` tgt
USING
  (
  WITH onetrust_client AS
(
SELECT
  SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) as num_cpf
  ,UPPER(email) as ds_email
  ,UPPER(nome) as ds_first_name
  ,UPPER(sobrenome) as ds_last_name
  ,UPPER(telefone) as num_phone
  ,UPPER(genero) as ini_gender
  ,TIMESTAMP(CONCAT(SUBSTR(data_de_nascimento,5,4),'-',SUBSTR(data_de_nascimento,3,2),'-',SUBSTR(data_de_nascimento,1,2)) , "America/Sao_Paulo") as dth_birth
  ,UPPER(origem) as cod_source
  ,UPPER(fin_polpriv_cn) as cod_consent_privacy_policy
  ,SAFE_CAST(SUBSTR(fin_polpriv_gmt,1,19) AS TIMESTAMP) as dth_consent_privacy_policy
  ,UPPER(fin_ofertasmail_cn) as cod_consent_offer_email
  ,SAFE_CAST(SUBSTR(fin_ofertasmail_gmt,1,19) AS TIMESTAMP) as dth_consent_offer_email
  ,UPPER(fin_ofertassms_cn) as cod_consent_offer_sms
  ,SAFE_CAST(SUBSTR(fin_ofertassms_gmt,1,19) AS TIMESTAMP) as dth_consent_offer_sms
  ,UPPER(fin_ofertaswhats_cn) as cod_consent_offer_whatsapp
  ,SAFE_CAST(SUBSTR(fin_ofertaswhats_gmt,1,19) AS TIMESTAMP) as dth_consent_offer_whatsapp
  ,UPPER(fin_statuspedwhats_cn) as cod_consent_purchase_status
  ,SAFE_CAST(SUBSTR(fin_statuspedwhats_gmt,1,19) AS TIMESTAMP) as dth_consent_purchase_status
  ,UPPER(fin_terceiros_cn) as cod_consent_sharing_third
  ,SAFE_CAST(SUBSTR(fin_terceiros_gmt,1,19) AS TIMESTAMP) as dth_consent_sharing_third
  ,UPPER(fin_newsletter_cn) as cod_consent_collection_point_website
  ,SAFE_CAST(SUBSTR(fin_newsletter_gmt,1,19) AS TIMESTAMP) as dth_consent_collection_point_website
  ,UPPER(fin_carina_cn) as cod_consent_collection_point_chatbox
  ,SAFE_CAST(SUBSTR(fin_carina_gmt,1,19) AS TIMESTAMP) as dth_consent_collection_point_chatbox
  ,UPPER(fin_compartilha_cn) as cod_consent_collection_point_csf
  ,SAFE_CAST(SUBSTR(fin_compartilha_gmt,1,19) AS TIMESTAMP) as dth_consent_collection_point_csf
  ,dth_partition
  ,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-prd.db_dolphin_stage_client_base.tbl_onetrust_client`,
  (
    SELECT
      TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    onetrust_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET num_cpf = CASE WHEN stg.num_cpf IS NOT NULL THEN stg.num_cpf ELSE tgt.num_cpf END,
  ds_email = CASE WHEN stg.ds_email IS NOT NULL THEN stg.ds_email ELSE tgt.ds_email END,
  ds_first_name = CASE WHEN stg.ds_first_name IS NOT NULL THEN stg.ds_first_name ELSE tgt.ds_first_name END,
  ds_last_name = CASE WHEN stg.ds_last_name IS NOT NULL THEN stg.ds_last_name ELSE tgt.ds_last_name END,
  num_phone = CASE WHEN stg.num_phone IS NOT NULL THEN stg.num_phone ELSE tgt.num_phone END,
  ini_gender = CASE WHEN stg.ini_gender IS NOT NULL THEN stg.ini_gender ELSE tgt.ini_gender END,
  dth_birth = CASE WHEN stg.dth_birth IS NOT NULL THEN stg.dth_birth ELSE tgt.dth_birth END,
  cod_source = CASE WHEN stg.cod_source IS NOT NULL THEN stg.cod_source ELSE tgt.cod_source END,
  cod_consent_privacy_policy = CASE WHEN stg.cod_consent_privacy_policy IS NOT NULL THEN CASE WHEN tgt.cod_consent_privacy_policy is null THEN stg.cod_consent_privacy_policy WHEN stg.cod_consent_privacy_policy IN ('CON', 'REV') THEN stg.cod_consent_privacy_policy  ELSE tgt.cod_consent_privacy_policy END ELSE tgt.cod_consent_privacy_policy END,
  dth_consent_privacy_policy = CASE WHEN stg.cod_consent_privacy_policy IS NOT NULL THEN CASE WHEN tgt.cod_consent_privacy_policy is null THEN stg.dth_consent_privacy_policy WHEN stg.cod_consent_privacy_policy IN ('CON', 'REV') THEN stg.dth_consent_privacy_policy ELSE tgt.dth_consent_privacy_policy END ELSE tgt.dth_consent_privacy_policy END,
  cod_consent_offer_email = CASE WHEN stg.cod_consent_offer_email IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_email is null THEN stg.cod_consent_offer_email WHEN stg.cod_consent_offer_email IN ('CON', 'REV') THEN stg.cod_consent_offer_email ELSE tgt.cod_consent_offer_email END ELSE tgt.cod_consent_offer_email END,
  dth_consent_offer_email = CASE WHEN stg.cod_consent_offer_email IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_email is null THEN stg.dth_consent_offer_email WHEN stg.cod_consent_offer_email IN ('CON', 'REV') THEN stg.dth_consent_offer_email ELSE tgt.dth_consent_offer_email END ELSE tgt.dth_consent_offer_email END,
  cod_consent_offer_sms = CASE WHEN stg.cod_consent_offer_sms IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_sms is null THEN stg.cod_consent_offer_sms WHEN stg.cod_consent_offer_sms IN ('CON', 'REV') THEN stg.cod_consent_offer_sms ELSE tgt.cod_consent_offer_sms END ELSE tgt.cod_consent_offer_sms END,
  dth_consent_offer_sms = CASE WHEN stg.cod_consent_offer_sms IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_sms is null THEN stg.dth_consent_offer_sms WHEN stg.cod_consent_offer_sms IN ('CON', 'REV') THEN stg.dth_consent_offer_sms ELSE tgt.dth_consent_offer_sms END ELSE tgt.dth_consent_offer_sms END,
  cod_consent_offer_whatsapp = CASE WHEN stg.cod_consent_offer_whatsapp IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_whatsapp is null THEN stg.cod_consent_offer_whatsapp WHEN stg.cod_consent_offer_whatsapp IN ('CON', 'REV') THEN stg.cod_consent_offer_whatsapp ELSE tgt.cod_consent_offer_whatsapp END ELSE tgt.cod_consent_offer_whatsapp END,
  dth_consent_offer_whatsapp = CASE WHEN stg.cod_consent_offer_whatsapp IS NOT NULL THEN CASE WHEN tgt.cod_consent_offer_whatsapp is null THEN stg.dth_consent_offer_whatsapp WHEN stg.cod_consent_offer_whatsapp IN ('CON', 'REV') THEN stg.dth_consent_offer_whatsapp ELSE tgt.dth_consent_offer_whatsapp END ELSE tgt.dth_consent_offer_whatsapp END,
  cod_consent_purchase_status = CASE WHEN stg.cod_consent_purchase_status IS NOT NULL THEN CASE WHEN tgt.cod_consent_purchase_status is null THEN stg.cod_consent_purchase_status WHEN stg.cod_consent_purchase_status IN ('CON', 'REV') THEN stg.cod_consent_purchase_status ELSE tgt.cod_consent_purchase_status END ELSE tgt.cod_consent_purchase_status END,
  dth_consent_purchase_status = CASE WHEN stg.cod_consent_purchase_status IS NOT NULL THEN CASE WHEN tgt.cod_consent_purchase_status is null THEN stg.dth_consent_purchase_status WHEN stg.cod_consent_purchase_status IN ('CON', 'REV') THEN stg.dth_consent_purchase_status ELSE tgt.dth_consent_purchase_status END ELSE tgt.dth_consent_purchase_status END,
  cod_consent_sharing_third = CASE WHEN stg.cod_consent_sharing_third IS NOT NULL THEN CASE WHEN tgt.cod_consent_sharing_third is null THEN stg.cod_consent_sharing_third WHEN stg.cod_consent_sharing_third IN ('CON', 'REV') THEN stg.cod_consent_sharing_third ELSE tgt.cod_consent_sharing_third END ELSE tgt.cod_consent_sharing_third END,
  dth_consent_sharing_third = CASE WHEN stg.cod_consent_sharing_third IS NOT NULL THEN CASE WHEN tgt.cod_consent_sharing_third is null THEN stg.dth_consent_sharing_third WHEN stg.cod_consent_sharing_third IN ('CON', 'REV') THEN stg.dth_consent_sharing_third ELSE tgt.dth_consent_sharing_third END ELSE tgt.dth_consent_sharing_third END,
  cod_consent_collection_point_website = CASE WHEN stg.cod_consent_collection_point_website IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_website is null THEN stg.cod_consent_collection_point_website WHEN stg.cod_consent_collection_point_website IN ('CON', 'REV') THEN stg.cod_consent_collection_point_website ELSE tgt.cod_consent_collection_point_website END ELSE tgt.cod_consent_collection_point_website END,
  dth_consent_collection_point_website = CASE WHEN stg.cod_consent_collection_point_website IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_website is null THEN stg.dth_consent_collection_point_website WHEN stg.cod_consent_collection_point_website IN ('CON', 'REV') THEN stg.dth_consent_collection_point_website ELSE tgt.dth_consent_collection_point_website END ELSE tgt.dth_consent_collection_point_website END,
  cod_consent_collection_point_chatbox = CASE WHEN stg.cod_consent_collection_point_chatbox IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_chatbox is null THEN stg.cod_consent_collection_point_chatbox WHEN stg.cod_consent_collection_point_chatbox IN ('CON', 'REV') THEN stg.cod_consent_collection_point_chatbox ELSE tgt.cod_consent_collection_point_chatbox END ELSE tgt.cod_consent_collection_point_chatbox END,
  dth_consent_collection_point_chatbox = CASE WHEN stg.cod_consent_collection_point_chatbox IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_chatbox is null THEN stg.dth_consent_collection_point_chatbox WHEN stg.cod_consent_collection_point_chatbox IN ('CON', 'REV') THEN stg.dth_consent_collection_point_chatbox ELSE tgt.dth_consent_collection_point_chatbox END ELSE tgt.dth_consent_collection_point_chatbox END,
  cod_consent_collection_point_csf = CASE WHEN stg.cod_consent_collection_point_csf IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_csf is null THEN stg.cod_consent_collection_point_csf WHEN stg.cod_consent_collection_point_csf IN ('CON', 'REV') THEN stg.cod_consent_collection_point_csf ELSE tgt.cod_consent_collection_point_csf END ELSE tgt.cod_consent_collection_point_csf END,
  dth_consent_collection_point_csf = CASE WHEN stg.cod_consent_collection_point_csf IS NOT NULL THEN CASE WHEN tgt.cod_consent_collection_point_csf is null THEN stg.dth_consent_collection_point_csf WHEN stg.cod_consent_collection_point_csf IN ('CON', 'REV') THEN stg.dth_consent_collection_point_csf ELSE tgt.dth_consent_collection_point_csf END ELSE tgt.dth_consent_collection_point_csf END,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (num_cpf
  ,ds_email
  ,ds_first_name
  ,ds_last_name
  ,num_phone
  ,ini_gender
  ,dth_birth
  ,cod_source
  ,cod_consent_privacy_policy
  ,dth_consent_privacy_policy
  ,cod_consent_offer_email
  ,dth_consent_offer_email
  ,cod_consent_offer_sms
  ,dth_consent_offer_sms
  ,cod_consent_offer_whatsapp
  ,dth_consent_offer_whatsapp
  ,cod_consent_purchase_status
  ,dth_consent_purchase_status
  ,cod_consent_sharing_third
  ,dth_consent_sharing_third
  ,cod_consent_collection_point_website
  ,dth_consent_collection_point_website
  ,cod_consent_collection_point_chatbox
  ,dth_consent_collection_point_chatbox
  ,cod_consent_collection_point_csf
  ,dth_consent_collection_point_csf
  ,dth_partition
  ,dth_create)
VALUES
  (stg.num_cpf
  ,stg.ds_email
  ,stg.ds_first_name
  ,stg.ds_last_name
  ,stg.num_phone
  ,stg.ini_gender
  ,stg.dth_birth
  ,stg.cod_source
  ,stg.cod_consent_privacy_policy
  ,stg.dth_consent_privacy_policy
  ,stg.cod_consent_offer_email
  ,stg.dth_consent_offer_email
  ,stg.cod_consent_offer_sms
  ,stg.dth_consent_offer_sms
  ,stg.cod_consent_offer_whatsapp
  ,stg.dth_consent_offer_whatsapp
  ,stg.cod_consent_purchase_status
  ,stg.dth_consent_purchase_status
  ,stg.cod_consent_sharing_third
  ,stg.dth_consent_sharing_third
  ,stg.cod_consent_collection_point_website
  ,stg.dth_consent_collection_point_website
  ,stg.cod_consent_collection_point_chatbox
  ,stg.dth_consent_collection_point_chatbox
  ,stg.cod_consent_collection_point_csf
  ,stg.dth_consent_collection_point_csf
  ,stg.dth_partition
  ,stg.dth_partition)