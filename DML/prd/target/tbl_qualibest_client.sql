MERGE
  `br-apps-bi-customermdm-prd.db_dolphin_target_client_replica.tbl_qualibest_client` tgt
USING
  (
  WITH qualibest_client AS
(
SELECT
  SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) as num_cpf
  ,UPPER(nome) as ds_full_name
  ,TIMESTAMP(data_de_nascimento , "America/Sao_Paulo") as dth_birth
  ,UPPER(email) as ds_email
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(celular, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(celular, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(celular, r'[\s()-]', '') END as num_mobile_phone
  ,UPPER(cep) as num_cep
  ,UPPER(estado) as ds_state
  ,UPPER(cidade) as ds_city_name
  ,UPPER(bairro) as ds_neighborhood_name
  ,UPPER(endereco) as ds_street
  ,UPPER(complemento) as ds_complement_street
  ,UPPER(numero) as num_house
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-prd.db_dolphin_stage_client_base.tbl_qualibest_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    qualibest_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET 
  num_cpf = stg.num_cpf,
  ds_full_name = stg.ds_full_name, 
  dth_birth = stg.dth_birth,
  ds_email = stg.ds_email,
  num_mobile_phone = stg.num_mobile_phone,
  num_cep = stg.num_cep,
  ds_state = stg.ds_state,
  ds_city_name = stg.ds_city_name,
  ds_neighborhood_name = stg.ds_neighborhood_name,
  ds_street = stg.ds_street,
  ds_complement_street = stg.ds_complement_street,
  num_house = stg.num_house,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  num_cpf,
  ds_full_name,
  dth_birth,
  ds_email,
  num_mobile_phone,
  num_cep,
  ds_state,
  ds_city_name,
  ds_neighborhood_name,
  ds_street,
  ds_complement_street,
  num_house,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.num_cpf,
  stg.ds_full_name,
  stg.dth_birth,
  stg.ds_email,
  stg.num_mobile_phone,
  stg.num_cep,
  stg.ds_state,
  stg.ds_city_name,
  stg.ds_neighborhood_name,
  stg.ds_street,
  stg.ds_complement_street,
  stg.num_house,
  stg.dth_partition,
  stg.dth_partition)