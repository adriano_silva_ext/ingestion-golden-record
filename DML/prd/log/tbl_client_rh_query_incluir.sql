SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
  n.cod_company,
	n.cod_registration,
	n.cod_status,
	n.cod_stocking,
  n.ds_situation_type,
	n.ds_status_employee,
	n.dth_admission,
	n.ind_situation_type,
	n.ds_company_name,
	n.ds_stocking_name,
	n.ds_workplace_name,
	n.num_employe_drt,
	n.dth_partition,
  n.dth_create,
  n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_rh` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_rh` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null