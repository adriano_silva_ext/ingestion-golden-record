SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
	n.num_phone,
  n.cod_consent_offer_sms,
	n.dth_consent_offer_sms,
	n.cod_consent_offer_whatsapp,
	n.dth_consent_offer_whatsapp,
  n.cod_source,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_phone_optin` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_phone_optin` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null