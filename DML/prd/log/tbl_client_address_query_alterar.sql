SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.num_cep != v.num_cep THEN CONCAT(n.num_cep, '|', v.num_cep) END AS num_cep,
  CASE WHEN n.ds_street != v.ds_street THEN CONCAT(n.ds_street, '|', v.ds_street) END AS ds_street,
  CASE WHEN n.ds_neighborhood_name != v.ds_neighborhood_name THEN CONCAT(n.ds_neighborhood_name, '|', v.ds_neighborhood_name) END AS ds_neighborhood_name,
  CASE WHEN n.ds_city_name != v.ds_city_name THEN CONCAT(n.ds_city_name, '|', v.ds_city_name) END AS ds_city_name,
  CASE WHEN n.ini_state != v.ini_state THEN CONCAT(n.ini_state, '|', v.ini_state) END AS ini_state,
  CASE WHEN n.ds_state_name != v.ds_state_name THEN CONCAT(n.ds_state_name, '|', v.ds_state_name) END AS ds_state_name,
  CASE WHEN n.ds_country_name != v.ds_country_name THEN CONCAT(n.ds_country_name, '|', v.ds_country_name) END AS ds_country_name,
  CASE WHEN n.ind_main_address != v.ind_main_address THEN CONCAT(n.ind_main_address, '|', v.ind_main_address) END AS ind_main_address,
  CASE WHEN n.num_house != v.num_house THEN CONCAT(n.num_house, '|', v.num_house) END AS num_house,
  CASE WHEN n.ds_complement_street != v.ds_complement_street THEN CONCAT(n.ds_complement_street, '|', v.ds_complement_street) END AS ds_complement_street,
  CASE WHEN n.ds_address_type != v.ds_address_type THEN CONCAT(n.ds_address_type, '|', v.ds_address_type) END AS ds_address_type,
  CASE WHEN n.ds_reference != v.ds_reference THEN CONCAT(n.ds_reference, '|', v.ds_reference) END AS ds_reference,
  CONCAT(n.ds_source_system, '|', v.ds_source_system)  AS ds_source_system,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_address` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_address` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
  WHERE n.num_unique_client !=v.num_unique_client or n.num_cep !=v.num_cep or n.ds_street !=v.ds_street or n.ds_neighborhood_name !=v.ds_neighborhood_name or n.ds_city_name !=v.ds_city_name or n.ini_state !=v.ini_state or n.ds_state_name !=v.ds_state_name or n.ds_country_name !=v.ds_country_name or n.num_house !=v.num_house or n.ds_complement_street !=v.ds_complement_street or n.ds_address_type !=v.ds_address_type or n.ds_complement_street !=v.ds_complement_street or n.ind_main_address !=v.ind_main_address or n.ds_reference !=v.ds_reference or n.ds_source_system !=v.ds_source_system