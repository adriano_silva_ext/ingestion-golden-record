SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.uuid_client != v.uuid_client THEN CONCAT(n.uuid_client, '|', v.uuid_client) END AS uuid_client,
  CASE WHEN n.num_state_subscription != v.num_state_subscription THEN CONCAT(n.num_state_subscription, '|', v.num_state_subscription) END AS num_state_subscription,
  CASE WHEN n.ind_loyalty != v.ind_loyalty THEN CONCAT(n.ind_loyalty, '|', v.ind_loyalty) END AS ind_loyalty,
  CASE WHEN n.dth_partition != v.dth_partition THEN CONCAT(n.dth_partition, '|', v.dth_partition) END AS dth_partition,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_sva` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_sva` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.uuid_client !=v.uuid_client or n.num_state_subscription !=v.num_state_subscription or n.ind_loyalty !=v.ind_loyalty or n.dth_partition !=v.dth_partition
