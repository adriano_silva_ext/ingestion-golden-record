SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.uuid_client != v.uuid_client THEN CONCAT(n.uuid_client, '|', v.uuid_client) END AS uuid_client,
  CASE WHEN n.uuid_address != v.uuid_address THEN CONCAT(n.uuid_address, '|', v.uuid_address) END AS uuid_address,
  CASE WHEN n.ds_channel != v.ds_channel THEN CONCAT(n.ds_channel, '|', v.ds_channel) END AS ds_channel,
  CASE WHEN n.dth_create_system_address != v.dth_create_system_address THEN CONCAT(n.dth_create_system_address, '|', v.dth_create_system_address) END AS dth_create_system_address,
  CASE WHEN n.dth_update_system_address != v.dth_update_system_address THEN CONCAT(n.dth_update_system_address, '|', v.dth_update_system_address) END AS dth_update_system_address,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_vtex` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_vtex` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.uuid_client !=v.uuid_client or n.uuid_address !=v.uuid_address or n.ds_channel !=v.ds_channel or n.dth_create_system_address !=v.dth_create_system_address or n.dth_update_system_address !=v.dth_update_system_address
