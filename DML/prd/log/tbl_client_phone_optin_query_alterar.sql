SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.num_phone != v.num_phone THEN CONCAT(n.num_phone, '|', v.num_phone) END AS num_phone,
  CASE WHEN n.cod_consent_offer_sms != v.cod_consent_offer_sms THEN CONCAT(n.cod_consent_offer_sms, '|', v.cod_consent_offer_sms) END AS cod_consent_offer_sms,
  CASE WHEN n.dth_consent_offer_sms != v.dth_consent_offer_sms THEN CONCAT(n.dth_consent_offer_sms, '|', v.dth_consent_offer_sms) END AS dth_consent_offer_sms,
  CASE WHEN n.cod_consent_offer_whatsapp != v.cod_consent_offer_whatsapp THEN CONCAT(n.cod_consent_offer_whatsapp, '|', v.cod_consent_offer_whatsapp) END AS cod_consent_offer_whatsapp,
  CASE WHEN n.dth_consent_offer_whatsapp != v.dth_consent_offer_whatsapp THEN CONCAT(n.dth_consent_offer_whatsapp, '|', v.dth_consent_offer_whatsapp) END AS dth_consent_offer_whatsapp,
  CONCAT(n.cod_source, '|', v.cod_source)  AS cod_source,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_phone_optin` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_phone_optin` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.num_phone !=v.num_phone or n.cod_consent_offer_sms !=v.cod_consent_offer_sms or n.dth_consent_offer_sms !=v.dth_consent_offer_sms or n.cod_consent_offer_whatsapp !=v.cod_consent_offer_whatsapp or n.dth_consent_offer_whatsapp !=v.dth_consent_offer_whatsapp or n.cod_source !=v.cod_source
