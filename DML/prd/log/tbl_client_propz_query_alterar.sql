SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.ds_buy_phusical_virtual != v.ds_buy_phusical_virtual THEN CONCAT(n.ds_buy_phusical_virtual, '|', v.ds_buy_phusical_virtual) END AS ds_buy_phusical_virtual,
  CASE WHEN n.num_children != v.num_children THEN CONCAT(n.num_children, '|', v.num_children) END AS num_children,
  CASE WHEN n.num_coupons_used != v.num_coupons_used THEN CONCAT(n.num_coupons_used, '|', v.num_coupons_used) END AS num_coupons_used,
  CASE WHEN n.num_advertisements_customer_interacted_email != v.num_advertisements_customer_interacted_email THEN CONCAT(n.num_advertisements_customer_interacted_email, '|', v.num_advertisements_customer_interacted_email) END AS num_advertisements_customer_interacted_email,
  CASE WHEN n.dth_first_buy != v.dth_first_buy THEN CONCAT(n.dth_first_buy, '|', v.dth_first_buy) END AS dth_first_buy,
  CASE WHEN n.dth_last_buy != v.dth_last_buy THEN CONCAT(n.dth_last_buy, '|', v.dth_last_buy) END AS dth_last_buy,
  CASE WHEN n.dth_received_last_email_advertisement != v.dth_received_last_email_advertisement THEN CONCAT(n.dth_received_last_email_advertisement, '|', v.dth_received_last_email_advertisement) END AS dth_received_last_email_advertisement,
  CASE WHEN n.dth_partition != v.dth_partition THEN CONCAT(n.dth_partition, '|', v.dth_partition) END AS dth_partition,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_propz` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_propz` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.ds_buy_phusical_virtual !=v.ds_buy_phusical_virtual or n.num_children !=v.num_children or n.num_coupons_used !=v.num_coupons_used or n.num_advertisements_customer_interacted_email !=v.num_advertisements_customer_interacted_email or n.dth_first_buy !=v.dth_first_buy or n.dth_last_buy !=v.dth_last_buy or n.dth_partition !=v.dth_partition or n.dth_partition !=v.dth_partition
