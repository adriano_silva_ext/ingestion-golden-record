SELECT
	n.num_unique_client,
	n.num_version_golden_record,
	n.ds_email,
	TO_BASE64(n.hash_email) AS hash_email,
	n.ds_email_type,
	n.ind_main_email,
	n.ds_source_system,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_email` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_email` AS v
ON
  n.num_unique_client = v.num_unique_client
WHERE v.num_unique_client is null