SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.cod_company != v.cod_company THEN CONCAT(n.cod_company, '|', v.cod_company) END AS cod_company,
  CASE WHEN n.cod_registration != v.cod_registration THEN CONCAT(n.cod_registration, '|', v.cod_registration) END AS cod_registration,
  CASE WHEN n.cod_status != v.cod_status THEN CONCAT(n.cod_status, '|', v.cod_status) END AS cod_status,
  CASE WHEN n.cod_stocking != v.cod_stocking THEN CONCAT(n.cod_stocking, '|', v.cod_stocking) END AS cod_stocking,
  CASE WHEN n.ds_situation_type != v.ds_situation_type THEN CONCAT(n.ds_situation_type, '|', v.ds_situation_type) END AS ds_situation_type,
  CASE WHEN n.ds_status_employee != v.ds_status_employee THEN CONCAT(n.ds_status_employee, '|', v.ds_status_employee) END AS ds_status_employee,
  CASE WHEN n.dth_admission != v.dth_admission THEN CONCAT(n.dth_admission, '|', v.dth_admission) END AS dth_admission,
  CASE WHEN n.ind_situation_type != v.ind_situation_type THEN CONCAT(n.ind_situation_type, '|', v.ind_situation_type) END AS ind_situation_type,
  CASE WHEN n.ds_company_name != v.ds_company_name THEN CONCAT(n.ds_company_name, '|', v.ds_company_name) END AS ds_company_name,
  CASE WHEN n.ds_stocking_name != v.ds_stocking_name THEN CONCAT(n.ds_stocking_name, '|', v.ds_stocking_name) END AS ds_stocking_name,
  CASE WHEN n.ds_workplace_name != v.ds_workplace_name THEN CONCAT(n.ds_workplace_name, '|', v.ds_workplace_name) END AS ds_workplace_name,
  CASE WHEN n.num_employe_drt != v.num_employe_drt THEN CONCAT(n.num_employe_drt, '|', v.num_employe_drt) END AS num_employe_drt,
  CASE WHEN n.dth_partition != v.dth_partition THEN CONCAT(n.dth_partition, '|', v.dth_partition) END AS dth_partition,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_rh` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_rh` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.cod_company !=v.cod_company or n.cod_registration !=v.cod_registration or n.cod_status !=v.cod_status or n.cod_stocking !=v.cod_stocking or n.ds_situation_type !=v.ds_situation_type or n.ds_status_employee !=v.ds_status_employee or n.dth_admission !=v.dth_admission or n.ind_situation_type !=v.ind_situation_type or n.ds_company_name !=v.ds_company_name or n.ds_stocking_name !=v.ds_stocking_name or n.ds_workplace_name !=v.ds_workplace_name or n.num_employe_drt !=v.num_employe_drt or n.dth_partition !=v.dth_partition
