CREATE OR REPLACE TABLE `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  num_version_golden_record INT64 OPTIONS(description="Golden Record Version Number / Número Versão Do Golden Record"),
  ds_full_name STRING OPTIONS(description="Full Name / Nome Completo"),
  ds_first_name STRING OPTIONS(description="First Name / Primeiro Nome"),
  ds_last_name STRING OPTIONS(description="Last Name / Último Nome"),
  ds_name_source_system STRING OPTIONS(description="Name Source System Indicator / Indicador Do Sistema De Origem Do Nome"),
  num_document NUMERIC OPTIONS(description="Number Document / Número Do Documento"),
  hash_document BYTES OPTIONS(description="Document Anonymization / Anonimização De Documento"),
  ds_document_type STRING OPTIONS(description="Document Type / Tipo De Documento"),
  ds_document_source_system STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  dth_birth TIMESTAMP OPTIONS(description="Birth Date / Data Nascimento"),
  ds_birth_source_system STRING OPTIONS(description="Birth Date Source System Indicator / Indicador De Sistema De Origem De Data De Nascimento"),
  ini_gender STRING OPTIONS(description="Acronym Gender / Sigla Gênero (Sexo)"),
  ds_gender STRING OPTIONS(description="Gender / Gênero (Sexo)"),
  ds_gender_source_system STRING OPTIONS(description="System Indicator Source Of Sexual Gender / Indicador De Sistema De Origem Do Gênero"),
  ini_marital_status STRING OPTIONS(description="Acronym Marital Status / Sigla Estado Civíl"),
  ds_marital_status STRING OPTIONS(description="Marital Status / Estado Civíl"),
  ds_marital_status_source_system STRING OPTIONS(description="Civil Status Source System Indicator / Indicador De Sistema De Origem Do Estado Civíl"),
  ds_nationality STRING OPTIONS(description="Nationality / Nacionalidade"),
  ds_country_birth_name STRING OPTIONS(description="Country Origin (Birth) / País De Origem (Nascimento)"),
  ds_nationality_source_system STRING OPTIONS(description="Nationality Source System Indicator / Indicador Do Sistema De Origem Da Nacionalidade"),
  ind_status_client_csf_carrefour STRING OPTIONS(description="Indicator Status Client CSF / Indicador Status Do Cliente CSF"),
  ds_status_client_csf_carrefour STRING OPTIONS(description="Status Client CSF / Status Do Cliente CSF"),
  dt_admission_minhas_recompensas TIMESTAMP OPTIONS(description="Join My Loyalty / Data Adesão Minhas Recompensas"),
  ind_employee BOOL OPTIONS(description="Employee Status / Situação Funcionário"),
  ind_has_atacadao BOOL OPTIONS(description="Indicator Client Atacadão Flag / Indicador Cliente Atacadão Flag"),
  ind_has_carrefour BOOL OPTIONS(description="Indicator Client Carrefour / Indicador Cliente Carrefour"),
  ind_client_blocked BOOL OPTIONS(description="Blocked Client Indicator / Indicador Cliente Bloqueado"),
  ind_portfolio STRING OPTIONS(description="Portfolio Indicator / Indicador Do Portifólio"),
  ind_bad_golden_record BOOL OPTIONS(description="Golden Record Identifier Is Low Quality / Identificador Do Golden Record É De Baixa Qualidade"),
  ind_deleted BOOL OPTIONS(description="Deleted Field Indicator / Indicador De Campo Deletado"),
  ds_channel_create_client STRING OPTIONS(description="Name Of The Channel Through Which The Customer Was Registered / Nome Do Canal Pelo Qual O Cliente Foi Cadastrado"),
  uuid_token_firebase STRING OPTIONS(description="Code Firebase Token / Código Token Firebase"),
  lst_client_origins ARRAY<STRING> OPTIONS(description="List Data Source System Indicator / Lista Indicador De Sistema De Origem Do Dado"),
  lst_reason_bad_golden_record ARRAY<STRING> OPTIONS(description="Bad Golden Record Field Indicator List / Lista Indicadora Do Campo Bad Golden Record"),
  cod_consent_privacy_policy STRING OPTIONS(description="Consent Code Privacy Policy / Código Do Consentimento Da Política De Privacidade"),
  dth_consent_privacy_policy TIMESTAMP OPTIONS(description="Consent Date Privacy Policy / Data Do Consentimento Da Política De Privacidade"),
  cod_consent_purchase_status STRING OPTIONS(description="Consent Code Purchase Status / Código Do Consentimento Do Status Compra"),
  dth_consent_purchase_status TIMESTAMP OPTIONS(description="Consent Date Purchase Status / Data Do Consentimento Do Status Compra"),
  cod_consent_sharing_third STRING OPTIONS(description="Consent Code Share Data with Third Parties / Código Do Consentimento De Compartilhamento De Dados Com Terceiros"),
  dth_consent_sharing_third TIMESTAMP OPTIONS(description="Consent Date Share Data with Third Parties / Data Do Consentimento De Compartilhamento De Dados Com Terceiros"),
  cod_source_general_consent STRING OPTIONS(description="Consent Code Source General / Código De Consentimento Geral"),
  dth_create_general_consent TIMESTAMP OPTIONS(description="Consent Date Create Source General / Data De Criação Consentimento Geral"),
  cod_consent_collection_point_chatbox STRING OPTIONS(description="Consent Code Colliection Point Carina Chatbox for Flyer Shipping That Sent Over The Phone / Código Do Consentimento Do Ponto De Coleta Do Chatbox Carina Para Envio De Folhetos que é Enviado o Telefone"),
  dth_consent_collection_point_chatbox TIMESTAMP OPTIONS(description="Consent Code Collection Point CSF / Código Do Consentimento Do Ponto de Coleta CSF"),
  cod_consent_collection_point_csf STRING OPTIONS(description="Consent Code Collection Point CSF / Código Do Consentimento Do Ponto de Coleta CSF"),
  dth_consent_collection_point_csf TIMESTAMP OPTIONS(description="Consent Date Collection Point CSF / Data Do Consentimento Do Ponto de Coleta CSF"),
  cod_consent_collection_point_website STRING OPTIONS(description="Consent Code Collection Point Where The Email Will Be Transacted On The Website / Código Do Consentimento Do Ponto De Coleta Onde Será Transacionado E-mail No Site"),
  dth_consent_collection_point_website TIMESTAMP OPTIONS(description="Consent Date Collection Point Where The Email Will Be Transacted On The Website / Data Do Consentimento Do Ponto De Coleta Onde Será Transacionado E-mail No Site"),
  cod_source_collection_point STRING OPTIONS(description="Source Code Of Collected Data / Código De Origem Do Dado Coletado"),
  dth_create_collection_point TIMESTAMP OPTIONS(description="Date Create Collection Point / Data Criação Do Ponto de Coleta"),
  ds_personal_email STRING OPTIONS(description="Description Email Personal / Descrição Campo Email Pessoal"),
  hash_personal_email BYTES OPTIONS(description="Email Personal Anonymization / Anonimização Email Pessoal"),
  ds_source_system_personal_email STRING OPTIONS(description="System Origin Personal Email / Sistema Origem Email Pessoal"),
  dth_create_personal_email TIMESTAMP OPTIONS(description="Date Create Email Personal / Data De Criação Email Pessoal"),
  ind_bounce_personal_email BOOLEAN OPTIONS(description="Personal Email Validation Indicator / Indicador de  Validação de Email pessoal"),
  cod_consent_offer_personal_email STRING OPTIONS(description="Consent Code Offer Personal Email / Código Consentimento Oferta Email Pessoal"),
  dth_consent_offer_personal_email TIMESTAMP OPTIONS(description="Consent Date Offer Personal Email / Data Consentimento Oferta Email Pessoal"),
  cod_source_offer_personal_email STRING OPTIONS(description="Consent Code Origin Offer Email Personal / Código Origem Consentimento Oferta Email Pessoal"),
  dth_create_offer_personal_email TIMESTAMP OPTIONS(description="Date Create Offer Email Personal / Data Criação Oferta Email Pessoal"),
  ds_business_email STRING OPTIONS(description="Email Business / E-Mail Profissional"),
  hash_business_email BYTES OPTIONS(description="Email Business Anonymization / Anonimização Email Profissional"),
  ds_source_system_business_email STRING OPTIONS(description="System Origin Email Business / Sistema Origem Email Profissional"),
  dth_create_business_email TIMESTAMP OPTIONS(description="Date Create Email Business / Data Criação Email Profissional"),
  ind_bounce_business_email BOOLEAN OPTIONS(description="Personal Email Validation Indicator / Indicador de  validação de Email comercial"),
  cod_consent_offer_business_email STRING OPTIONS(description="Consent Code Offer Email Business / Código Consentimento Oferta Email Profissional"),
  dth_consent_offer_business_email TIMESTAMP OPTIONS(description="Consent Date Offer Email Business / Data Consentimento Oferta Email Profissional"),
  cod_source_offer_business_email STRING OPTIONS(description="Consent Code Origin Offer Email Business / Código Origem Consentimento Oferta Email Profissional"),
  dth_create_offer_business_email TIMESTAMP OPTIONS(description="Date Create Offer Email Business / Data Criação Oferta Email Business"),
  num_mobile_phone STRING OPTIONS(description="Mobile Phone / Telefone Celular"),
  hash_mobile_phone BYTES OPTIONS(description="Number Phone Anonymization / Anonimização Numero Celular"),
  ds_source_system_mobile_phone STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  dth_create_mobile_phone TIMESTAMP OPTIONS(description="Date Create Mobile Phone / Data Criação Telefone Celular"),
  ind_bounce_mobile_phone BOOLEAN OPTIONS(description="Mobile Phone Validation Indicator / Indicador de  validação de celular"),
  cod_consent_offer_sms_mobile_phone STRING OPTIONS(description="Consent Code Offer Cell Phone (SMS) / Código Consentimento Oferta Telefone Celular (SMS)"),
  dth_consent_offer_sms_mobile_phone TIMESTAMP OPTIONS(description="Consent Date Offer Mobile Phone (SMS) / Data Consentimento Oferta Telefone Celular (SMS)"),
  cod_consent_offer_whatsapp_mobile_phone STRING OPTIONS(description="Consent Code Offer WhatsApp Mobile Phone / Código De Consentimento Oferta WhatsApp Telefone Celular"),
  dth_consent_offer_whatsapp_mobile_phone TIMESTAMP OPTIONS(description="Consent Date Offer WhatsApp Mobile Phone / Data De Consentimento Oferta WhatsApp Telefone Celular"),
  cod_source_mobile_phone_optin STRING OPTIONS(description="Mobile Phone Source Code / Código De Origem Do Celular"),
  dth_create_mobile_phone_optin TIMESTAMP OPTIONS(description="Date Create Optin Mobile Phone / Data Criação Optin Telefone Celular"),
  num_landline_phone STRING OPTIONS(description="Number Landline / Némero Telefone Fixo"),
  hash_landline_phone BYTES OPTIONS(description="Number Landline Anonymization / Anonimização Número Telefone Fixo"),
  ds_source_system_landline_phone STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  dth_create_landline_phone TIMESTAMP OPTIONS(description="Date Create Landline Phone / Data Criação Telefone Fixo"),
  ind_bounce_landline_phone BOOLEAN OPTIONS(description="landline Phone Validation Indicator / Indicador de  validação de Telefone Fixo"),
  cod_consent_offer_sms_landline_phone STRING OPTIONS(description="Consent Code Offer Landline Telephone / Código Consentimento Oferta Telefone Fixo"),
  dth_consent_offer_sms_landline_phone TIMESTAMP OPTIONS(description="Consent Date Offer Landline Telephone / Data Consentimento Oferta Telefone Fixo"),
  cod_consent_offer_whatsapp_landline_phone STRING OPTIONS(description="Consent Code Landline Telephone Offer / Código De Consentimento Oferta Telefone Fixo"),
  dth_consent_offer_whatsapp_landline_phone TIMESTAMP OPTIONS(description="Consent Date Landline Telephone Offer / Data De Consentimento Oferta Telefone Fixo"),
  cod_source_landline_phone_optin STRING OPTIONS(description="Landline Source Code / Código De Origem Do Telefone Fixo"),
  dth_create_landline_phone_optin TIMESTAMP OPTIONS(description="Date Create Optin Landline Phone / Data Criação Optin Telefone Fixo"),
  num_cep_residential STRING OPTIONS(description="Zip Code Residential / Cep Residencial"),
  ds_street_residential STRING OPTIONS(description="Address Residential (Street Name) / Endereço Residencial (Nome Da Rua)"),
  ds_neighborhood_name_residential STRING OPTIONS(description="Neighborhood Name Residential / Nome Do Bairro Residencial"),
  ds_city_name_residential STRING OPTIONS(description="Residential City / Cidade Residencial"),
  ini_state_residential STRING OPTIONS(description="Acronym State Residential / Sigla Estado Residencial (Uf)"),
  ds_state_name_residential STRING OPTIONS(description="State Residential (Uf) / Estado Residencial (Uf)"),
  ds_country_name_residential STRING OPTIONS(description="Name Country Residential / Nome Do País De Residencia"),
  num_house_residential STRING OPTIONS(description="Address Number Residential / Número Do Endereço Residencial"),
  ds_complement_street_residential STRING OPTIONS(description="Address Complement Residential / Complemento Do Endereço Residencial"),
  ds_reference_residential STRING OPTIONS(description="Address Reference Point Residential / Ponto De Referência Do Endereço Residencial"),
  ds_source_system_residential STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  num_cep_invoice STRING OPTIONS(description="Zip Code Invoice / Cep Cobrança"),
  ds_street_invoice STRING OPTIONS(description="Address Invoice (Street Name) / Endereço Cobrança (Nome Da Rua)"),
  ds_neighborhood_name_invoice STRING OPTIONS(description="Neighborhood Name / Nome Do Bairro"),
  ds_city_name_invoice STRING OPTIONS(description="Invoice City / Cidade Cobrança"),
  ini_state_invoice STRING OPTIONS(description="Acronym State Invoice / Sigla Estado Cobrança (Uf)"),
  ds_state_name_invoice STRING OPTIONS(description="State Invoice (Uf) / Estado Cobrança (Uf)"),
  ds_country_name_invoice STRING OPTIONS(description="Name Country Invoice / Nome Do País De Cobrança"),
  num_house_invoice STRING OPTIONS(description="Address Number Invoice / Número Do Endereço Cobrança"),
  ds_complement_street_invoice STRING OPTIONS(description="Address Complement / Complemento Do Endereço"),
  ds_reference_invoice STRING OPTIONS(description="Address Reference Point Invoice / Ponto De Referência Do Endereço Cobrança"),
  ds_source_system_invoice STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  dth_create TIMESTAMP OPTIONS(description="Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description="Update Registration Client / Data Alteração Cadastro Cliente"),
  dth_create_system_origin TIMESTAMP OPTIONS(description="Date Create System Source / Data Criação Sistema Origem"),
  dth_update_system_origin TIMESTAMP OPTIONS(description="Update Source System / Data Alteração Sistema Origem")
)
CLUSTER BY num_unique_client, num_document, ds_personal_email
OPTIONS(
  description="Table Consolidated People Data The Origins / Tabela Dados De Pessoas Consolidada As Origens"
)
AS
with client as (
SELECT 
  a.num_unique_client,
  a.num_version_golden_record,
  a.ds_full_name,
  a.ds_first_name,
  a.ds_last_name,
  a.ds_name_source_system,
  a.num_document,
  a.hash_document,
  a.ds_document_type,
  a.ds_document_source_system,
  a.dth_birth,
  a.ds_birth_source_system,
  a.ini_gender,
  a.ds_gender,
  a.ds_gender_source_system,
  a.ini_marital_status,
  a.ds_marital_status,
  a.ds_marital_status_source_system,
  a.ds_nationality,
  a.ds_country_birth_name,
  a.ds_nationality_source_system,
  a.ind_status_client_csf_carrefour,
  a.ds_status_client_csf_carrefour,
  a.dt_admission_minhas_recompensas,
  a.ind_employee,
  a.ind_has_atacadao,
  a.ind_has_carrefour,
  a.ind_client_blocked,
  a.ind_portfolio,
  a.ind_bad_golden_record,
  a.ind_deleted,
  a.ds_channel_create_client,
  a.uuid_token_firebase,
  a.lst_client_origins,
  a.lst_reason_bad_golden_record,
--  a.lst_client_origins,

  b.cod_consent_privacy_policy,
  b.dth_consent_privacy_policy,
  b.cod_consent_purchase_status,
  b.dth_consent_purchase_status,
  b.cod_consent_sharing_third,
  b.dth_consent_sharing_third,
  b.cod_source as cod_source_general_consent,
  b.dth_create as dth_create_general_consent, 

  c.cod_consent_collection_point_chatbox,
  c.dth_consent_collection_point_chatbox,
  c.cod_consent_collection_point_csf,
  c.dth_consent_collection_point_csf,
  c.cod_consent_collection_point_website,
  c.dth_consent_collection_point_website,
  c.cod_source as cod_source_collection_point,
  c.dth_create as dth_create_collection_point,

  d.ds_email as ds_personal_email,
  d.hash_email as hash_personal_email,
  d.ds_source_system as ds_source_system_personal_email,
  d.dth_create as dth_create_personal_email,
  d.ind_bounce_email as ind_bounce_personal_email,

  e.cod_consent_offer_email as cod_consent_offer_personal_email,
  e.dth_consent_offer_email as dth_consent_offer_personal_email,
  e.cod_source as cod_source_offer_personal_email,
  e.dth_create as dth_create_offer_personal_email,

  f.ds_email as ds_business_email,
  f.hash_email as hash_business_email,
  f.ds_source_system as ds_source_system_business_email,
  f.dth_create as dth_create_business_email,
  f.ind_bounce_email as ind_bounce_business_email,

  g.cod_consent_offer_email as cod_consent_offer_business_email,
  g.dth_consent_offer_email as dth_consent_offer_business_email,
  g.cod_source as cod_source_offer_business_email,
  g.dth_create as dth_create_offer_business_email,

  h.num_phone as num_mobile_phone,
  h.hash_phone as hash_mobile_phone,
  h.ds_source_system as ds_source_system_mobile_phone,
  h.dth_create as dth_create_mobile_phone,
  h.ind_bounce_phone as ind_bounce_mobile_phone,

  i.cod_consent_offer_sms as cod_consent_offer_sms_mobile_phone,
  i.dth_consent_offer_sms as dth_consent_offer_sms_mobile_phone,
  i.cod_consent_offer_whatsapp as cod_consent_offer_whatsapp_mobile_phone,
  i.dth_consent_offer_whatsapp as dth_consent_offer_whatsapp_mobile_phone,
  i.cod_source as cod_source_mobile_phone_optin,
  i.dth_create as dth_create_mobile_phone_optin,

  j.num_phone as num_landline_phone,
  j.hash_phone as hash_landline_phone,
  j.ds_source_system as ds_source_system_landline_phone,
  j.dth_create as dth_create_landline_phone,
  j.ind_bounce_phone as ind_bounce_landline_phone,

  k.cod_consent_offer_sms as cod_consent_offer_sms_landline_phone,
  k.dth_consent_offer_sms as dth_consent_offer_sms_landline_phone,
  k.cod_consent_offer_whatsapp as cod_consent_offer_whatsapp_landline_phone,
  k.dth_consent_offer_whatsapp as dth_consent_offer_whatsapp_landline_phone,
  k.cod_source as cod_source_landline_phone_optin,
  k.dth_create as dth_create_landline_phone_optin,

  l.num_cep as num_cep_residential,
  l.ds_street as ds_street_residential,
  l.ds_neighborhood_name as ds_neighborhood_name_residential,
  l.ds_city_name as ds_city_name_residential,
  l.ini_state as ini_state_residential,
  l.ds_state_name as ds_state_name_residential,
  l.ds_country_name as ds_country_name_residential,
  l.num_house as num_house_residential,
  l.ds_complement_street as ds_complement_street_residential,
  l.ds_reference as ds_reference_residential,
  l.ds_source_system as ds_source_system_residential,

  m.num_cep as num_cep_invoice,
  m.ds_street as ds_street_invoice,
  m.ds_neighborhood_name as ds_neighborhood_name_invoice,
  m.ds_city_name as ds_city_name_invoice,
  m.ini_state as ini_state_invoice,
  m.ds_state_name as ds_state_name_invoice,
  m.ds_country_name as ds_country_name_invoice,
  m.num_house as num_house_invoice,
  m.ds_complement_street as ds_complement_street_invoice,
  m.ds_reference as ds_reference_invoice,
  m.ds_source_system as ds_source_system_invoice,
  
  a.dth_create,
  a.dth_update,
  
  a.dth_create_system_origin,
  a.dth_update_system_origin,
  ROW_NUMBER() OVER (PARTITION BY a.num_unique_client ORDER BY a.dth_update DESC) RANK
 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_general_consent` b
ON a.num_unique_client = b.num_unique_client
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_collection_point` c
ON a.num_unique_client = c.num_unique_client
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email` d
ON a.num_unique_client = d.num_unique_client and d.ds_email_type = 'PESSOAL'
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email_optin` e
ON a.num_unique_client = e.num_unique_client and TRIM(d.ds_email) = TRIM(e.ds_email)
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email` f
ON a.num_unique_client = f.num_unique_client and f.ds_email_type = 'COMERCIAL'
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email_optin` g
ON a.num_unique_client = g.num_unique_client and TRIM(f.ds_email) = TRIM(g.ds_email)
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone` h
ON a.num_unique_client = h.num_unique_client and h.ds_phone_type = 'CELULAR PESSOAL' AND h.ind_main_phone = true
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone_optin` i
ON a.num_unique_client = i.num_unique_client and TRIM(h.num_phone) = TRIM(i.num_phone)
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone` j
ON a.num_unique_client = j.num_unique_client and j.ds_phone_type = 'FIXO PESSOAL' AND j.ind_main_phone = true
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone_optin` k
ON a.num_unique_client = k.num_unique_client and TRIM(j.num_phone) = TRIM(k.num_phone)
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_address` l
ON a.num_unique_client = l.num_unique_client and 
(case when l.ds_address_type = 'RESIDENCIAL' and l.ind_main_address then true 
      when l.ds_address_type <> 'RESIDENCIAL' and l.ind_main_address then true 
      else false
end)
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_address` m
ON a.num_unique_client = m.num_unique_client and m.ds_address_type = 'COBRANÇA' and not m.ind_main_address)
SELECT * EXCEPT (RANK) FROM client 
WHERE RANK = 1 