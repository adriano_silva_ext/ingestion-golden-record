With tab_aux_email as (
select distinct num_document, ds_email_business, ds_email_type, prioridade, ds_source_system, dth_create, dth_update
from (
select num_cpf as num_document, ds_email_business, 1 as prioridade, 'COMERCIAL' as ds_email_type, 'RH' as ds_source_system, dth_create, dth_update 
  from `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_rh_client` a	
 where num_cpf is not null and ds_email is not null		
union all
select num_document as num_document, ds_email, prioridade, ds_email_type, ds_source_system, a.dth_create, a.dth_update 
  from (
  select a.num_document, b.ds_email, 
        case when b.ds_source_system = 'VTEX'      then 3
             when b.ds_source_system = 'RH'        then 2
             when b.ds_source_system = 'CSF'       then 4
             when b.ds_source_system = 'RH'        then 5
             when b.ds_source_system = 'SVA'       then 6
--             when b.ds_source_system = 'PROPZ'     then 7
             when b.ds_source_system = 'NEOASSIST' then 8
             when b.ds_source_system = 'QUALIBEST' then 9  
             when b.ds_source_system = 'OLDGR'	   then 10
             when b.ds_source_system = 'HIST'	   then 11
        end as prioridade,
        b.ds_email_type, b.ds_source_system, b.dth_create, b.dth_update
  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_email` b 
  inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` a 
  on a.num_unique_client = b.num_unique_client
  where b.ds_email_type = 'COMERCIAL'
   	) a
 where num_document is not null and ds_email is not null and REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true  

) as tab_aux_1	
),
tab_aux_main_email as (
select num_document, ds_email_type, min(prioridade) as prioridade
from   tab_aux_email
where trim(ds_email_business) != 'NONE@NONE.COM'
Group by num_document, ds_email_type 		
),
tab_selected_emails as (
    Select a.*
      from tab_aux_email a
      inner join tab_aux_main_email b 
         on a.num_document  = b.num_document 
        and a.ds_email_type = b.ds_email_type
        and a.prioridade    = b.prioridade  
)
Select * except(ind_bounce_email),
       case when ind_bounce_email = '0' then false
	        when ind_bounce_email = '1' then true
			else null 
	    end as ind_bounce_email 
from (	
		select c.num_unique_client, COALESCE(c.num_version_golden_record,
			1) AS num_version_golden_record,
			   a.ds_email_business as ds_email, SHA256(a.ds_email_business) as hash_email, a.ds_email_type, 
			   True as ind_main_email,
			   a.ds_source_system,
      a.dth_create,
      a.dth_update,
			   True              as ind_document,
		  (select ind_hard_bounce_email 
			 from `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_propz_client` b 
			where a.num_document = b.num_cpf and a.ds_email_business = b.ds_email) as ind_bounce_email	   
		from tab_selected_emails as a 
		inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` as c 
				on c.num_document = a.num_document
	)
;
