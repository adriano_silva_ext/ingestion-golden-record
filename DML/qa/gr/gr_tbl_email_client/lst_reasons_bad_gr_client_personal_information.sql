CREATE OR REPLACE TABLE `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  num_version_golden_record INT64 OPTIONS(description="Golden Record Version Number / Número Versão Do Golden Record"),
  ds_full_name STRING OPTIONS(description="Full Name / Nome Completo"),
  ds_first_name STRING OPTIONS(description="First Name / Primeiro Nome"),
  ds_last_name STRING OPTIONS(description="Last Name / Último Nome"),
  ds_name_source_system STRING OPTIONS(description="Name Source System Indicator / Indicador Do Sistema De Origem Do Nome"),
  num_document NUMERIC OPTIONS(description="Number Document / Número Do Documento"),
  hash_document BYTES OPTIONS(description="Document Anonymization / Anonimização De Documento"),
  ds_document_type STRING OPTIONS(description="Document Type / Tipo De Documento"),
  ds_document_source_system STRING OPTIONS(description="Document Source System Indicator / Indicador De Sistema De Origem Do Documento"),
  dth_birth TIMESTAMP OPTIONS(description="Birth Date / Data Nascimento"),
  ds_birth_source_system STRING OPTIONS(description="Birth Date Source System Indicator / Indicador De Sistema De Origem De Data De Nascimento"),
  ini_gender STRING OPTIONS(description="Acronym Gender / Sigla Gênero (Sexo)"),
  ds_gender STRING OPTIONS(description="Gender / Gênero (Sexo)"),
  ds_gender_source_system STRING OPTIONS(description="System Indicator Source Of Sexual Gender / Indicador De Sistema De Origem Do Gênero"),
  ini_marital_status STRING OPTIONS(description="Acronym Marital Status / Sigla Estado Civíl"),
  ds_marital_status STRING OPTIONS(description="Marital Status / Estado Civíl"),
  ds_marital_status_source_system STRING OPTIONS(description="Civil Status Source System Indicator / Indicador De Sistema De Origem Do Estado Civíl"),
  ds_nationality STRING OPTIONS(description="Nationality / Nacionalidade"),
  ds_country_birth_name STRING OPTIONS(description="Country Origin (Birth) / País De Origem (Nascimento)"),
  ds_nationality_source_system STRING OPTIONS(description="Nationality Source System Indicator / Indicador Do Sistema De Origem Da Nacionalidade"),
  ind_status_client_csf_carrefour STRING OPTIONS(description="Indicator Status Client CSF / Indicador Status Do Cliente CSF"),
  ds_status_client_csf_carrefour STRING OPTIONS(description="Status Client CSF / Status Do Cliente CSF"),
  dt_admission_minhas_recompensas TIMESTAMP OPTIONS(description="Join My Loyalty / Data Adesão Minhas Recompensas"),
  ind_employee BOOL OPTIONS(description="Employee Status / Situação Funcionário"),
  ind_has_atacadao BOOL OPTIONS(description="Indicator Client Atacadão Flag / Indicador Cliente Atacadão Flag"),
  ind_has_carrefour BOOL OPTIONS(description="Indicator Client Carrefour / Indicador Cliente Carrefour"),
  ind_client_blocked BOOL OPTIONS(description="Blocked Client Indicator / Indicador Cliente Bloqueado"),
  ind_portfolio STRING OPTIONS(description="Portfolio Indicator / Indicador Do Portifólio"),
  ind_bad_golden_record BOOL OPTIONS(description="Golden Record Identifier Is Low Quality / Identificador Do Golden Record É De Baixa Qualidade"),
  ind_deleted BOOL OPTIONS(description="Deleted Field Indicator / Indicador De Campo Deletado"),
  ds_channel_create_client STRING OPTIONS(description="Name Of The Channel Through Which The Customer Was Registered / Nome Do Canal Pelo Qual O Cliente Foi Cadastrado"),
  dth_create TIMESTAMP OPTIONS(description=" Data Criação / Date Creation"),
  dth_update TIMESTAMP OPTIONS(description="Data Alteração / Change Date"),
  uuid_token_firebase STRING OPTIONS(description="Code Firebase Token / Código Token Firebase"),
  dth_create_system_origin TIMESTAMP OPTIONS(description="Date Create System Source / Data Criação Sistema Origem"),
  dth_update_system_origin TIMESTAMP OPTIONS(description="Update Source System / Data Alteração Sistema Origem"),
  lst_client_origins ARRAY<STRING> OPTIONS(description="List Data Source System Indicator / Lista Indicador De Sistema De Origem Do Dado"),
  lst_reason_bad_golden_record ARRAY<STRING> OPTIONS(description="Bad Golden Record Field Indicator List / Lista Indicadora Do Campo Bad Golden Record")
)
CLUSTER BY num_unique_client, num_document
OPTIONS(
  description="Table data information from data sources of data / Tabela Dados De Informações De Pessoas Das Origens De Dados"
)
AS
Select 
  a.num_unique_client,
  a.num_version_golden_record,
  a.ds_full_name,
  a.ds_first_name,
  a.ds_last_name,
  a.ds_name_source_system,
  a.num_document,
  a.hash_document,
  a.ds_document_type,
  a.ds_document_source_system,
  a.dth_birth,
  a.ds_birth_source_system,
  a.ini_gender,
  a.ds_gender,
  a.ds_gender_source_system,
  a.ini_marital_status,
  a.ds_marital_status,
  a.ds_marital_status_source_system,
  a.ds_nationality,
  a.ds_country_birth_name,
  a.ds_nationality_source_system,
  a.ind_status_client_csf_carrefour,
  a.ds_status_client_csf_carrefour,
  a.dt_admission_minhas_recompensas,
  a.ind_employee,
  a.ind_has_atacadao,
  a.ind_has_carrefour,
  a.ind_client_blocked,
  a.ind_portfolio,
  a.ind_bad_golden_record,				  
  a.ind_deleted,
  a.ds_channel_create_client,
  a.dth_create,
  a.dth_update,
  a.uuid_token_firebase,
  a.dth_create_system_origin,
  a.dth_update_system_origin,
  a.lst_client_origins,
  b.lst_reason_bad_golden_record
  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a 
 LEFT JOIN (
Select num_unique_client, array_agg(erro) as lst_reason_bad_golden_record
from (
SELECT
  a.num_unique_client,
  case when (a.num_document is null) then 'num_document com valor null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.ds_full_name is null) then 'ds_full_name com valor null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.ds_first_name is null) then 'ds_first_name com valor null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.ds_last_name is null) then 'ds_last_name com valor null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  'ds_full_name contém caracter especial ou números' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and not REGEXP_CONTAINS(trim(ds_full_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")
union all  
SELECT
  a.num_unique_client,
  'ds_first_name contém caracter especial ou números' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and not REGEXP_CONTAINS(trim(ds_first_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")
union all  
SELECT
  a.num_unique_client,
  'ds_last_name contém caracter especial ou números' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and not REGEXP_CONTAINS(trim(ds_last_name),r"^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÈÍÏÓÔÕÖÚÜÇÑ .']+$")
union all  
SELECT
  a.num_unique_client,
  'ds_full_name contém email' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and REGEXP_CONTAINS(trim(ds_full_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
union all  
SELECT
  a.num_unique_client,
  'ds_first_name contém email' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and REGEXP_CONTAINS(trim(ds_first_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
union all  
SELECT
  a.num_unique_client,
  'ds_last_name contém email' as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
and REGEXP_CONTAINS(trim(ds_last_name),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")
union all  
SELECT
  a.num_unique_client,
  case when (a.ind_deleted is null) then 'Ind_Deleted com valor Null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.num_version_golden_record is null) then 'num_version_golden_record com valor Null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.ds_channel_create_client is null) then 'ds_channel_create_client com valor Null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (a.dth_create is null) then 'dth_create com valor Null' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when (b.ds_email is null) then 'Cliente sem email' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
left join  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email` b  
on a.num_unique_client = b.num_unique_client
Where a.ind_bad_golden_record
union all  
SELECT
  a.num_unique_client,
  case when not c.cpf_valido then 'CPF inválido' else null end as erro 
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` a
 LEFT JOIN (
   Select num_unique_client
         ,case when dig1 = cast(substr(num_cpf,10,1) as integer) and
                  cast(substr(num_cpf,11,1) as integer) = 
                  mod(mod(
                     cast(SUBSTR(num_cpf,2,1) as integer)   + cast(SUBSTR(num_cpf,3,1)as integer) *2 + cast(SUBSTR(num_cpf,4,1) as integer)*3 + 
                     cast(SUBSTR(num_cpf,5,1) as integer)*4 + cast(SUBSTR(num_cpf,6,1)as integer) *5 + cast(SUBSTR(num_cpf,7,1) as integer)*6 + 
                     cast(SUBSTR(num_cpf,8,1) as integer)*7 + cast(SUBSTR(num_cpf,9,1)as integer) *8 + dig1          *9, 11) , 10)
             then true else false end as cpf_valido
 	From (Select num_unique_client, 
 			     num_cpf,
 				 mod(mod(
 				 cast(SUBSTR(num_cpf,1,1) as integer)   + cast(SUBSTR(num_cpf,2,1) as integer) *2 + cast(SUBSTR(num_cpf,3,1) as integer) *3+
 				 cast(SUBSTR(num_cpf,4,1) as integer)*4 + cast(SUBSTR(num_cpf,5,1) as integer)*5 + cast(SUBSTR(num_cpf,6,1) as integer)*6+
 				 cast(SUBSTR(num_cpf,7,1) as integer)*7 + cast(SUBSTR(num_cpf,8,1) as integer)*8 + cast(SUBSTR(num_cpf,9,1) as integer)*9, 11), 10)  as dig1
 		   From (Select  num_unique_client,
 					     lpad(cast(num_document as string),11,'0') as num_cpf  
 				   FROM `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information`
            where ind_bad_golden_record) x ) x 
 )  c
 ON
    a.num_unique_client = c.num_unique_client
where a.ind_bad_golden_record
)
where erro is not null 
group by num_unique_client
 )  b
 ON
    a.num_unique_client = b.num_unique_client;
