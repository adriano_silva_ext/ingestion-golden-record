WITH
  tab_aux_email AS (
  SELECT
    ds_email,
    ds_email_type,
    prioridade,
    dth_create, 
    dth_update
  FROM (
    SELECT
      ds_email,
      1 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_vtex_client` a
    WHERE
      num_cpf IS NULL
      AND ds_email IS NOT NULL
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      2 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_csf_client` a
    WHERE
      num_cpf IS NULL
      AND ds_email IS NOT NULL
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      3 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_rh_client` a
    WHERE
      num_cpf IS NULL
      AND ds_email IS NOT NULL
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      4 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_sva_client` a
    WHERE
      num_document IS NULL
      AND ds_document_type = 'CPF'
      AND ds_email IS NOT NULL
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      6 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_neoassist_client` a
    WHERE
      num_cpf IS NULL
      AND ds_email IS NOT NULL
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      7 AS prioridade,
      'PESSOAL' AS ds_email_type, dth_create, dth_update
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_qualibest_client` a
    WHERE
      num_cpf IS NULL
      AND ds_email IS NOT NULL 
	  and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
    UNION ALL
    SELECT
      ds_email,
      8 AS prioridade,
      ds_email_type, dth_create, dth_update
    FROM (Select b.ds_email, b.ds_email_type, b.dth_create, b.dth_update
		  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` a
		  inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_email` b
				on a.num_unique_client = b.num_unique_client
         WHERE
             a.num_document IS NULL
         AND b.ds_email IS NOT NULL 
		 and  REGEXP_CONTAINS(trim(ds_email),r"(.*\W|^)([a-zA-Z0-9.!#$%&&#39;*+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)")= true 
	      ) a
	  ) AS a
  WHERE
    /* Garante que um mesmo email não seja vinculado a um cliente com cpf e outro sem cpf 
	   por isso tem que usar a tabela temporária que recebeu a carga na query anterior.
	*/
    NOT EXISTS (
    SELECT
      *
    FROM
      `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_email` b
    WHERE
      upper(trim(a.ds_email)) = upper(trim(b.ds_email))
      AND a.ds_email_type = b.ds_email_type )
 ),
tab_aux_main_email as (
select ds_email, ds_email_type, min(prioridade) as prioridade
from   tab_aux_email
where trim(ds_email) != 'NONE@NONE.COM'
Group by ds_email, ds_email_type 		
),
tab_selected_emails as (
    Select a.*
      from tab_aux_email a
      inner join tab_aux_main_email b 
         on upper(trim(a.ds_email)) = upper(trim(b.ds_email)) 
        and a.ds_email_type = b.ds_email_type
        and a.prioridade    = b.prioridade  
)
Select * except(ind_bounce_email),
       case when ind_bounce_email = '0' then false
	        when ind_bounce_email = '1' then true
			else null 
	    end as ind_bounce_email 
from (	
		SELECT
		  coalesce(oldemailgr.num_unique_client,
			generate_uuid()) AS num_unique_client,
		  coalesce(oldemailgr.num_version_golden_record,
			1) AS num_version_golden_record,
		  a.ds_email,
		  SHA256(a.ds_email) AS hash_email,
		  a.ds_email_type,
		  TRUE AS ind_main_email,
		  CASE
			WHEN a.prioridade = 1 AND a.ds_email_type = 'PESSOAL' THEN 'VTEX'
			WHEN a.prioridade = 1
		  AND a.ds_email_type = 'COMERCIAL' THEN 'RH'
			WHEN a.prioridade = 2 THEN 'CSF'
			WHEN a.prioridade = 3 THEN 'RH'
			WHEN a.prioridade = 4 THEN 'SVA'
		--    WHEN a.prioridade = 5 THEN 'PROPZ'
			WHEN a.prioridade = 6 THEN 'NEOASSIST'
			WHEN a.prioridade = 7 THEN 'QUALIBEST'
			WHEN a.prioridade = 8 THEN 'OLDGR'
		END
		  AS ds_source_system,
		  a.dth_create,
      a.dth_update,
		  False                    as ind_document,
		  b.ind_bounce_email
		FROM
		  tab_selected_emails AS a
		LEFT JOIN (
					Select ds_email, max(ind_hard_bounce_email) as ind_bounce_email
					  From `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_propz_client`  
					group by ds_email
				  ) b
		ON 
		  upper(trim(a.ds_email)) = upper(trim(b.ds_email))
		LEFT JOIN
		  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_email` AS oldemailgr
		ON
		  upper(trim(a.ds_email)) = upper(trim(oldemailgr.ds_email))
		where trim(a.ds_email) != 'NONE@NONE.COM'
	)
;


 