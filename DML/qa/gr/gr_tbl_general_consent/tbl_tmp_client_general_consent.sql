SELECT  
  a.num_unique_client,
  a.cod_consent_privacy_policy,
  a.dth_consent_privacy_policy,
  a.cod_consent_purchase_status,
  a.dth_consent_purchase_status,
  a.cod_consent_sharing_third,
  a.dth_consent_sharing_third,
  a.cod_source,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_general_consent` a 