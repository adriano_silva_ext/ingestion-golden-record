SELECT  
  a.num_unique_client,
  a.cod_consent_collection_point_website,
  a.dth_consent_collection_point_website,
  a.cod_consent_collection_point_chatbox,
  a.dth_consent_collection_point_chatbox,
  a.cod_consent_collection_point_csf,
  a.dth_consent_collection_point_csf,
  a.cod_source,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_collection_point` a 