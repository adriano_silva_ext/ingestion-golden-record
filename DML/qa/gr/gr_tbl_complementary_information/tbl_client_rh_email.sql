SELECT
  c.num_unique_client,
  a.cod_company,
  a.cod_registration,
  a.cod_status,
  a.cod_stocking,
  a.ds_situation_type,
  a.ds_status_employee,
  a.dth_admission,
  a.ind_situation_type,
  a.ds_company_name,
  a.ds_stocking_name,
  a.ds_workplace_name,
  a.num_employe_drt,
  a.dth_partition,
  a.dth_create,
  a.dth_update
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_rh_client` AS a
INNER JOIN (
			  select distinct num_unique_client, ds_email, ds_source_system 
			  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email`
			  where ds_source_system = 'RH'	
			) c
ON
  UPPER(TRIM(a.ds_email)) = UPPER(TRIM(c.ds_email))
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL;  
