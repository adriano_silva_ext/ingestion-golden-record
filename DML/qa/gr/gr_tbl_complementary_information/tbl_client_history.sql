SELECT
  c.num_unique_client,
  a.ind_removed,			
  a.dth_last_change,     
  a.dth_create_origin,   
  a.ini_country,         
  a.cod_hybris_consumer, 
  a.ds_origin,           
  a.uuid_row_hash,       
  a.cod_consumer        
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_history_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_document = c.num_document
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_document IS NOT NULL;