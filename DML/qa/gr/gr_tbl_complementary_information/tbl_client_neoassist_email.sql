SELECT
  c.num_unique_client,
  a.uuid_consumer,
  a.lst_cpfs,
  a.lst_phone_number,
  a.lst_email,
  a.lst_personalization_field,
  a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_neoassist_client` AS a
INNER JOIN (
			  select distinct num_unique_client, ds_email, ds_source_system 
			  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email`
			  where ds_source_system = 'NEOASSIST'	
			) c
ON
  UPPER(TRIM(a.ds_email)) = UPPER(TRIM(c.ds_email))
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NULL
  AND a.ds_email IS NOT NULL;