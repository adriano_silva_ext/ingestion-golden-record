CREATE OR REPLACE TABLE `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_neoassist`
(
  num_unique_client STRING OPTIONS(description="NUC - Unique Customer Number / NUC - Número Único Cliente"),
  uuid_consumer STRING OPTIONS(description="Group Calls From More Than One Consumer That Can Handle The Same Service / Agrupar Os Chamados De Mais De Um Consumidor Que Podem Tratar Do Mesmo Atendimento"),
  lst_cpfs STRING OPTIONS(description="List With Several Cpfs From Several Customers In The Same Field Related To The Same Service / Lista Com Diversos Cpfs De Vários Clientes No Mesmo Campo Relacionado Ao Mesmo Atendimento"),
  lst_phone_number STRING OPTIONS(description="List With Several Phone Numbers Several Customers In The Same Field Related To The Same Service / Lista Com Diversos Telefones Vários Clientes No Mesmo Campo Relacionado Ao Mesmo Atendimento"),
  lst_email STRING OPTIONS(description="List With Several E-Mails From Several Customers In The Field Related To The Same Service / Lista Com Diversos E-Mails De Vários Clientes No Campo Relacionado Ao Mesmo Atendimento"),
  lst_personalization_field STRING OPTIONS(description="Customization Field For Creating Other Fields To The Customer'S Taste / Campo De Personalização Para Criação De Outros Campos A Gosto Do Cliente"),
  dth_create TIMESTAMP OPTIONS(description=" Create Registration Client / Criação Cadastro Cliente"),
  dth_update TIMESTAMP OPTIONS(description=" Update Registration Client / Data Alteração Cadastro Cliente"),
  dth_partition TIMESTAMP OPTIONS(description="Partição da tabela Origem / Origin Table partition")
)
CLUSTER BY num_unique_client
OPTIONS(
  description="Table Information data from Neoassist people / Tabela Dados De Informações De Pessoas Da Neoassist"
)
AS
SELECT
  c.num_unique_client,
  a.uuid_consumer,
  a.lst_cpfs,
  a.lst_phone_number,
  a.lst_email,
  a.lst_personalization_field,
  a.dth_create,
  a.dth_update,
  a.dth_partition
FROM
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_neoassist_client` AS a
INNER JOIN
  `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` AS c
ON
  a.num_cpf = c.num_document
LEFT JOIN `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_personal_information` AS d
	   ON c.num_unique_client = d.num_unique_client
WHERE
  a.num_cpf IS NOT NULL;