WITH
  tab_phone_optin AS (
  SELECT
    b.num_unique_client,
    a.num_phone,
    CASE
      WHEN d.cod_consent_offer_sms IS NULL THEN a.cod_consent_offer_sms
      WHEN a.dth_consent_offer_sms > d.dth_consent_offer_sms THEN a.cod_consent_offer_sms
    ELSE
    d.cod_consent_offer_sms
  END
    AS cod_consent_offer_sms,
    CASE
      WHEN d.cod_consent_offer_sms IS NULL THEN a.dth_consent_offer_sms
      WHEN a.dth_consent_offer_sms > d.dth_consent_offer_sms THEN a.dth_consent_offer_sms
    ELSE
    d.dth_consent_offer_sms
  END
    AS dth_consent_offer_sms,
    CASE
      WHEN d.cod_consent_offer_whatsapp IS NULL THEN a.cod_consent_offer_whatsapp
      WHEN a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp THEN a.cod_consent_offer_whatsapp
    ELSE
    d.cod_consent_offer_whatsapp
  END
    AS cod_consent_offer_whatsapp,
    CASE
      WHEN d.cod_consent_offer_whatsapp IS NULL THEN a.dth_consent_offer_whatsapp
      WHEN a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp THEN a.dth_consent_offer_whatsapp
    ELSE
    d.dth_consent_offer_whatsapp
  END
    AS dth_consent_offer_whatsapp,
    CASE
      WHEN d.cod_source IS NULL THEN a.cod_source
      WHEN (a.dth_consent_offer_sms > d.dth_consent_offer_sms)
    OR (a.dth_consent_offer_whatsapp > d.dth_consent_offer_whatsapp) THEN a.cod_source
    ELSE
    d.cod_source
  END
    AS cod_source,
    a.dth_create,
    a.dth_update,
  FROM
    `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_onetrust_client` a
  INNER JOIN ( select distinct a.num_unique_client, b.num_document, a.ds_email, b.dth_update
                 from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_email` a
                 inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information`  b
                 on a.num_unique_client = b.num_unique_client 
             ) b
  ON
    upper(TRIM(a.ds_email)) = upper(TRIM(b.ds_email))
  AND a.num_cpf = b.num_document  
  INNER JOIN
    `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone` c
  ON
    b.num_unique_client = c.num_unique_client
    AND a.num_phone = c.num_phone
  LEFT JOIN
    `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_tmp_client_phone_optin` d
  ON
    b.num_unique_client = d.num_unique_client
    AND a.num_phone = d.num_phone
  WHERE
    a.num_cpf IS NULL
    AND a.ds_email IS NOT NULL
    AND a.num_phone IS NOT NULL ),
  tab_max_optin_sms AS (
  SELECT
    a.num_unique_client,
    a.num_phone,
    a.cod_consent_offer_sms,
    MAX(a.dth_consent_offer_sms) AS dth_consent_offer_sms
  FROM
    tab_phone_optin a
  GROUP BY
    1,
    2,
    3 ),
  tab_max_optin_sms_zap AS (
  SELECT
    a.num_unique_client,
    a.num_phone,
    a.cod_consent_offer_sms,
    a.dth_consent_offer_sms,
    a.cod_consent_offer_whatsapp,
    MAX(a.dth_consent_offer_whatsapp) AS dth_consent_offer_whatsapp
  FROM
    tab_phone_optin a
  INNER JOIN
    tab_max_optin_sms b
  ON
    a.num_unique_client = b.num_unique_client
    AND a.num_phone = b.num_phone
    AND TRIM(a.cod_consent_offer_sms) = TRIM(b.cod_consent_offer_sms)
    AND a.dth_consent_offer_sms = b.dth_consent_offer_sms
  GROUP BY
    1,
    2,
    3,
    4,
    5 )
SELECT distinct
  a.*
FROM
  tab_phone_optin a
INNER JOIN
  tab_max_optin_sms_zap b
ON
  a.num_unique_client = b.num_unique_client
  AND a.num_phone = b.num_phone
  AND TRIM(a.cod_consent_offer_sms) = TRIM(b.cod_consent_offer_sms)
  AND a.dth_consent_offer_sms = b.dth_consent_offer_sms
  AND TRIM(a.cod_consent_offer_whatsapp) = TRIM(b.cod_consent_offer_whatsapp)
  AND a.dth_consent_offer_whatsapp = b.dth_consent_offer_whatsapp;