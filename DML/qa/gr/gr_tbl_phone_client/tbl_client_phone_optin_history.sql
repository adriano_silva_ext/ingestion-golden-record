With tb_aux_numphonemob as (
select 
      a.num_unique_client, 
      a.num_mobile_phone  as num_phone,
      case when a.cod_consent_offer_sms = 0 then 'REV'
           when a.cod_consent_offer_sms = 1 then 'CON'
      end                   as cod_consent_offer_sms,     
      a.dth_create_origin   as dth_consent_offer_sms,
      case when a.cod_consent_offer_whatsapp = 0 then 'REV'
           when a.cod_consent_offer_whatsapp = 1 then 'CON'
      end                   as cod_consent_offer_whatsapp,     
      a.dth_create_origin   as dth_consent_offer_whatsapp,
      'CSF'                 as cod_source,
      a.dth_create,
      a.dth_update
from `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_history_client` a
inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` b
        on a.num_unique_client = b.num_unique_client and a.num_document = b.num_document
where (a.cod_consent_offer_sms is not null or cod_consent_offer_whatsapp is not null) and num_mobile_phone is not null 
  and not exists (
    Select *
	  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone_optin` c 
	 where a.num_unique_client = c.num_unique_client
       and a.num_mobile_phone = c.num_phone 	 
  ) 
),
tb_aux_numphone as (
select 
      a.num_unique_client, 
      a.num_phone 		as num_phone,
      case when a.cod_consent_offer_sms = 0 then 'REV'
           when a.cod_consent_offer_sms = 1 then 'CON'
           else 'REV'
      end                   as cod_consent_offer_sms,     
      a.dth_create_origin   as dth_consent_offer_sms,
      case when a.cod_consent_offer_whatsapp = 0 then 'REV'
           when a.cod_consent_offer_whatsapp = 1 then 'CON'
           else 'REV'
      end                   as cod_consent_offer_whatsapp,     
      a.dth_create_origin   as dth_consent_offer_whatsapp,
      'CSF'                 as cod_source,
      a.dth_create,
      a.dth_update
from `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_history_client` a
inner join `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_personal_information` b
        on a.num_unique_client = b.num_unique_client and a.num_document = b.num_document
where (a.cod_consent_offer_sms is not null or cod_consent_offer_whatsapp is not null) and  num_phone is not null 
  and not exists (
    Select *
	  from `br-apps-bi-customermdm-qa.db_dolphin_target_clientmdm.tbl_client_phone_optin` c 
	 where a.num_unique_client = c.num_unique_client
       and a.num_phone = c.num_phone 	 
  )
and not exists (
    Select *
	  from tb_aux_numphonemob c 
	 where a.num_unique_client = c.num_unique_client
       and a.num_phone = c.num_phone 	 
)
)
Select distinct a.* from (
select * from tb_aux_numphonemob
UNION ALL 
select * from tb_aux_numphone
  ) a;
  