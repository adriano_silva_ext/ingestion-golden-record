SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.ds_email != v.ds_email THEN CONCAT(n.ds_email, '|', v.ds_email) END AS ds_email,
  CASE WHEN n.cod_consent_offer_email != v.cod_consent_offer_email THEN CONCAT(n.cod_consent_offer_email, '|', v.cod_consent_offer_email) END AS cod_consent_offer_email,
  CASE WHEN n.dth_consent_offer_email != v.dth_consent_offer_email THEN CONCAT(n.dth_consent_offer_email, '|', v.dth_consent_offer_email) END AS dth_consent_offer_email,
  CONCAT(n.cod_source, '|', v.cod_source)  AS cod_source,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_email_optin` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_email_optin` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.ds_email !=v.ds_email or n.cod_consent_offer_email !=v.cod_consent_offer_email or n.dth_consent_offer_email !=v.dth_consent_offer_email or n.cod_source !=v.cod_source
