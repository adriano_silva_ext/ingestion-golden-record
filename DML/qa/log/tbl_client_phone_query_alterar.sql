SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.num_phone != v.num_phone THEN CONCAT(n.num_phone, '|', v.num_phone) END AS num_phone,
  CASE WHEN n.hash_phone != v.hash_phone THEN CONCAT(TO_BASE64(n.hash_phone), '|', TO_BASE64(v.hash_phone)) END AS hash_phone,
  CASE WHEN n.ind_main_phone != v.ind_main_phone THEN CONCAT(n.ind_main_phone, '|', v.ind_main_phone) END AS ind_main_phone,
  CASE WHEN n.ds_phone_type != v.ds_phone_type THEN CONCAT(n.ds_phone_type, '|', v.ds_phone_type) END AS ds_phone_type,
  CONCAT(n.ds_source_system, '|', v.ds_source_system)  AS ds_source_system,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_phone` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_phone` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.num_phone !=v.num_phone or n.hash_phone !=v.hash_phone or n.ind_main_phone !=v.ind_main_phone or n.ds_phone_type !=v.ds_phone_type or n.ds_source_system !=v.ds_source_system
