SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
  n.uuid_client,
	n.uuid_address,
	n.ds_channel,
	n.dth_create_system_address,
  n.dth_update_system_address,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_vtex` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_vtex` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null