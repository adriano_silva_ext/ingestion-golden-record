SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.uuid_consumer != v.uuid_consumer THEN CONCAT(n.uuid_consumer, '|', v.uuid_consumer) END AS uuid_consumer,
  CASE WHEN n.lst_cpfs != v.lst_cpfs THEN CONCAT(n.lst_cpfs, '|', v.lst_cpfs) END AS lst_cpfs,
  CASE WHEN n.lst_phone_number != v.lst_phone_number THEN CONCAT(n.lst_phone_number, '|', v.lst_phone_number) END AS lst_phone_number,
  CASE WHEN n.lst_email != v.lst_email THEN CONCAT(n.lst_email, '|', v.lst_email) END AS lst_email,
  CASE WHEN n.lst_personalization_field != v.lst_personalization_field THEN CONCAT(n.lst_personalization_field, '|', v.lst_personalization_field) END AS lst_personalization_field,
  CASE WHEN n.dth_partition != v.dth_partition THEN CONCAT(n.dth_partition, '|', v.dth_partition) END AS dth_partition,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_neoassist` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_neoassist` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.uuid_consumer !=v.uuid_consumer or n.lst_cpfs !=v.lst_cpfs or n.lst_phone_number !=v.lst_phone_number or n.lst_email !=v.lst_email or n.lst_personalization_field !=v.lst_personalization_field or n.dth_partition !=v.dth_partition
