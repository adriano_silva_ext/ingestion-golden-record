SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
	n.num_phone,
	TO_BASE64(n.hash_phone) AS hash_phone,
	n.ind_main_phone,
	n.ds_phone_type,
	n.ds_source_system,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_phone` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_phone` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null