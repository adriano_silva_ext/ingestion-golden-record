SELECT
	n.num_unique_client,
	gr.num_version_golden_record,
  n.cod_consent_collection_point_chatbox,
	n.dth_consent_collection_point_chatbox,
	n.cod_consent_collection_point_csf,
	n.dth_consent_collection_point_csf,
  n.cod_consent_collection_point_website,
	n.dth_consent_collection_point_website,
  n.cod_source,
	n.dth_create,
	n.dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_collection_point` AS n
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_collection_point` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE v.num_unique_client is null