SELECT
  n.num_unique_client,
  gr.num_version_golden_record,
  CASE WHEN n.cod_consent_collection_point_chatbox != v.cod_consent_collection_point_chatbox THEN CONCAT(n.cod_consent_collection_point_chatbox, '|', v.cod_consent_collection_point_chatbox) END AS cod_consent_collection_point_chatbox,
  CASE WHEN n.dth_consent_collection_point_chatbox != v.dth_consent_collection_point_chatbox THEN CONCAT(n.dth_consent_collection_point_chatbox, '|', v.dth_consent_collection_point_chatbox) END AS dth_consent_collection_point_chatbox,
  CASE WHEN n.cod_consent_collection_point_csf != v.cod_consent_collection_point_csf THEN CONCAT(n.cod_consent_collection_point_csf, '|', v.cod_consent_collection_point_csf) END AS cod_consent_collection_point_csf,
  CASE WHEN n.dth_consent_collection_point_csf != v.dth_consent_collection_point_csf THEN CONCAT(n.dth_consent_collection_point_csf, '|', v.dth_consent_collection_point_csf) END AS dth_consent_collection_point_csf,
  CASE WHEN n.cod_consent_collection_point_website != v.cod_consent_collection_point_website THEN CONCAT(n.cod_consent_collection_point_website, '|', v.cod_consent_collection_point_website) END AS cod_consent_collection_point_website,
  CASE WHEN n.dth_consent_collection_point_website != v.dth_consent_collection_point_website THEN CONCAT(n.dth_consent_collection_point_website, '|', v.dth_consent_collection_point_website) END AS dth_consent_collection_point_website,
  CONCAT(n.cod_source, '|', v.cod_source)  AS cod_source,
  CONCAT(n.dth_create, '|', v.dth_create) AS dth_create,
  CONCAT(n.dth_update, '|', v.dth_update) AS dth_update
FROM
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_collection_point` AS n
INNER JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_tmp_client_collection_point` AS v
ON
  n.num_unique_client = v.num_unique_client
LEFT JOIN
  `br-apps-bi-customermdm-ENVIRONMENT.db_dolphin_target_clientmdm.tbl_client_personal_information` AS gr
ON
  n.num_unique_client = gr.num_unique_client
WHERE n.num_unique_client !=v.num_unique_client or n.cod_consent_collection_point_chatbox !=v.cod_consent_collection_point_chatbox or n.dth_consent_collection_point_chatbox !=v.dth_consent_collection_point_chatbox or n.cod_consent_collection_point_csf !=v.cod_consent_collection_point_csf or n.dth_consent_collection_point_csf !=v.dth_consent_collection_point_csf or n.cod_consent_collection_point_website !=v.cod_consent_collection_point_website or n.dth_consent_collection_point_website !=v.dth_consent_collection_point_website or n.cod_source !=v.cod_source
