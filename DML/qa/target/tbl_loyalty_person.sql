MERGE
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_loyalty_person` tgt
USING
  (
  WITH loyalty_person AS
(
SELECT 
  SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) AS num_cpf
  ,TIMESTAMP(SUBSTR(data_adesao_mr,0,18), "America/Sao_Paulo") AS dth_join_loyalty
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(cpf, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-qa.db_dolphin_stage_client_base.tbl_loyalty_person`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    loyalty_person
  WHERE
    RANK = 1) stg
ON 
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET num_cpf = stg.num_cpf,
  dth_join_loyalty = stg.dth_join_loyalty,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (num_cpf,dth_join_loyalty,dth_partition,dth_create)
VALUES
  (stg.num_cpf, stg.dth_join_loyalty, stg.dth_partition,stg.dth_partition)