MERGE
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_vtex_address` tgt
USING
  (
  WITH vtex_address AS
(
SELECT 
  UPPER(complement) as ds_complement_street
  ,CASE
      WHEN REGEXP_CONTAINS(TRIM(email), r'(.\W|^)([a-zA-Z0-9.!#$%&&#39;+\=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?))(\W.*|$)') IS TRUE THEN UPPER(email)
    ELSE
    NULL
  END
    AS ds_email
  ,UPPER(reference) as ds_reference
  ,UPPER(state) as ds_state
  ,UPPER(street) as ds_street
  ,TIMESTAMP(SUBSTR(createdin,1,19) , "America/Sao_Paulo") as dth_create_system_origin
  ,TIMESTAMP(SUBSTR(updatedin,1,19) , "America/Sao_Paulo") as dth_update_system_origin
  ,UPPER(addressname) as ds_address_name
  ,UPPER(city) as ds_city_name
  ,UPPER(country) as ds_country_name
  ,UPPER(countryfake) as ds_country_fake_name
  ,UPPER(neighborhood) as ds_neighborhood_name
  ,UPPER(postalcode) as num_cep
  ,UPPER(number) as num_house
  ,UPPER(addresstype) as ds_address_type
  ,UPPER(id) as uuid_address
  ,UPPER(userid) as uuid_client
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY TRIM(email), UPPER(addresstype) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-qa.db_dolphin_stage_client_base.tbl_vtex_address`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    vtex_address
  WHERE
    RANK = 1) stg
ON
  tgt.ds_email = stg.ds_email and tgt.ds_address_type = stg.ds_address_type
  WHEN MATCHED THEN UPDATE 
  SET 
  ds_complement_street = stg.ds_complement_street,
  ds_email = stg.ds_email, 
  ds_reference = stg.ds_reference,
  ds_state = stg.ds_state,
  ds_street = stg.ds_street,
  dth_create_system_origin = stg.dth_create_system_origin,
  dth_update_system_origin = stg.dth_update_system_origin,
  ds_address_name = stg.ds_address_name,
  ds_city_name = stg.ds_city_name,
  ds_country_name = stg.ds_country_name,
  ds_country_fake_name = stg.ds_country_fake_name,
  ds_neighborhood_name = stg.ds_neighborhood_name,
  num_cep = stg.num_cep,
  num_house = stg.num_house,
  ds_address_type = stg.ds_address_type,
  uuid_address = stg.uuid_address,
  uuid_client = stg.uuid_client,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  ds_complement_street,
  ds_email,
  ds_reference,
  ds_state,
  ds_street,
  dth_create_system_origin,
  dth_update_system_origin,
  ds_address_name,
  ds_city_name,
  ds_country_name,
  ds_country_fake_name,
  ds_neighborhood_name,
  num_cep,
  num_house,
  ds_address_type,
  uuid_address,
  uuid_client,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.ds_complement_street,
  stg.ds_email,
  stg.ds_reference,
  stg.ds_state,
  stg.ds_email,
  stg.dth_create_system_origin,
  stg.dth_update_system_origin,
  stg.ds_address_name,
  stg.ds_city_name,
  stg.ds_country_name,
  stg.ds_country_fake_name,
  stg.ds_neighborhood_name,
  stg.num_cep,
  stg.num_house,
  stg.ds_address_type,
  stg.uuid_address,
  stg.uuid_client,
  stg.dth_partition,
  stg.dth_partition)