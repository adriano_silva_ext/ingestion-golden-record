MERGE
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_propz_client` tgt
USING
  (
  WITH propz_client AS
(
SELECT 
  UPPER(status) as cod_client_status
  ,UPPER(flagcustomer) as ds_buy_phusical_virtual
  ,UPPER(homeemail) as ds_email
  ,UPPER(crforigins) as ds_origin_system
  ,UPPER(homestreet) as ds_street
  ,TIMESTAMP(SUBSTR(dateofbirth,0,10) , "America/Sao_Paulo") AS dth_birth
  ,TIMESTAMP(creationdate) as dth_create_system_origin
  ,TIMESTAMP(dtflagcustomer) as dth_first_buy
  ,TIMESTAMP(lastpurchase) as dth_last_buy
  ,TIMESTAMP(lastdatecampaignemailreceived) as dth_received_last_email_advertisement
  ,TIMESTAMP(dateupdated) AS dth_update_system_origin
  ,UPPER(pmbounceemail) as ind_hard_bounce_email
  ,UPPER(pmbouncesms) as ind_hard_bounce_sms
  ,UPPER(optinall) as ind_optin_all
  ,UPPER(emailcontactflag) as ind_optin_email_client
  ,UPPER(smscontactflag) as ind_optin_sms_client
  ,UPPER(pmunsubscribedemail) as ind_optout_email_client
  ,UPPER(pmunsubscribedsms) as ind_optout_sms_client
  ,UPPER(homecountry) as ini_country
  ,UPPER(gender) as ini_gender
  ,UPPER(homestate) as ini_state
  ,UPPER(homecity) as ds_city_name
  ,UPPER(crfchannel) as ds_client_registration_channel
  ,UPPER(firstname) as ds_first_name
  ,UPPER(lastname) as ds_last_name
  ,SAFE_CAST(childrennum as NUMERIC) as num_children
  ,UPPER(numbercupons) as num_coupons_used
  ,CASE CHAR_LENGTH(REGEXP_REPLACE(mobilephone, r'[\s()-]', ''))
    WHEN 11 THEN CONCAT('+55',REGEXP_REPLACE(mobilephone, r'[\s()-]', ''))
    ELSE REGEXP_REPLACE(mobilephone, r'[\s()-]', '') END as num_mobile_phone
  ,SAFE_CAST(engagementscoreemail AS NUMERIC) as num_advertisements_customer_interacted_email
  ,SAFE_CAST(REGEXP_REPLACE(customerid, r'[.,-]', '') AS NUMERIC) as num_cpf
  ,dth_partition
 	,ROW_NUMBER() OVER (PARTITION BY SAFE_CAST(REGEXP_REPLACE(customerid, r'[.,-]', '') AS NUMERIC) ORDER BY dth_partition DESC) RANK
FROM  `br-apps-bi-customermdm-qa.db_dolphin_stage_client_base.tbl_propz_client`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      (CONTAINS_SUBSTR(customerid, '@') = False) AND (dth_partition > last_date))
SELECT
    * EXCEPT (RANK)
  FROM
    propz_client
  WHERE
    RANK = 1) stg
ON
  tgt.num_cpf = stg.num_cpf
  WHEN MATCHED THEN UPDATE 
  SET cod_client_status = stg.cod_client_status,
  ds_buy_phusical_virtual = stg.ds_buy_phusical_virtual, 
  ds_email = stg.ds_email,
  ds_origin_system = stg.ds_origin_system,
  dth_birth = stg.dth_birth,
  dth_create_system_origin = stg.dth_create_system_origin,
  dth_first_buy = stg.dth_first_buy,
  dth_last_buy = stg.dth_last_buy,
  dth_received_last_email_advertisement = stg.dth_received_last_email_advertisement,
  dth_update_system_origin = stg.dth_update_system_origin,
  ind_hard_bounce_email = stg.ind_hard_bounce_email,
  ind_hard_bounce_sms = stg.ind_hard_bounce_sms,
  ind_optin_all = stg.ind_optin_all,
  ind_optin_email_client = stg.ind_optin_email_client,
  ind_optin_sms_client = stg.ind_optin_sms_client,
  ind_optout_email_client = stg.ind_optout_email_client,
  ind_optout_sms_client = stg.ind_optout_sms_client,
  ini_country = stg.ini_country,
  ini_gender = stg.ini_gender,
  ini_state = stg.ini_state,
  ds_city_name = stg.ds_city_name,
  ds_client_registration_channel = stg.ds_client_registration_channel,
  ds_first_name = stg.ds_first_name,
  ds_last_name = stg.ds_last_name,
  num_children = stg.num_children,
  num_coupons_used = stg.num_coupons_used,
  num_mobile_phone = stg.num_mobile_phone,
  num_advertisements_customer_interacted_email = stg.num_advertisements_customer_interacted_email,
  num_cpf = stg.num_cpf,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  cod_client_status,
  ds_buy_phusical_virtual,
  ds_email,
  ds_origin_system,
  dth_birth,
  dth_create_system_origin,
  dth_first_buy,
  dth_last_buy,
  dth_received_last_email_advertisement,
  dth_update_system_origin,
  ind_hard_bounce_email,
  ind_hard_bounce_sms,
  ind_optin_all,
  ind_optin_email_client,
  ind_optin_sms_client,
  ind_optout_email_client,
  ind_optout_sms_client,
  ini_country,
  ini_gender,
  ini_state,
  ds_city_name,
  ds_client_registration_channel,
  ds_first_name,
  ds_last_name,
  num_children,
  num_coupons_used,
  num_mobile_phone,
  num_advertisements_customer_interacted_email,
  num_cpf,
  dth_partition,
  dth_create)
VALUES
  (  
  stg.cod_client_status,
  stg.ds_buy_phusical_virtual,
  stg.ds_email,
  stg.ds_origin_system,
  stg.dth_birth,
  stg.dth_create_system_origin,
  stg.dth_first_buy,
  stg.dth_last_buy,
  stg.dth_received_last_email_advertisement,
  stg.dth_update_system_origin,
  stg.ind_hard_bounce_email,
  stg.ind_hard_bounce_sms,
  stg.ind_optin_all,
  stg.ind_optin_email_client,
  stg.ind_optin_sms_client,
  stg.ind_optout_email_client,
  stg.ind_optout_sms_client,
  stg.ini_country,
  stg.ini_gender,
  stg.ini_state,
  stg.ds_city_name,
  stg.ds_client_registration_channel,
  stg.ds_first_name,
  stg.ds_last_name,
  stg.num_children,
  stg.num_coupons_used,
  stg.num_mobile_phone,
  stg.num_advertisements_customer_interacted_email,
  stg.num_cpf,
  stg.dth_partition,
  stg.dth_partition)