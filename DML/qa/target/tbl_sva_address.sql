MERGE
  `br-apps-bi-customermdm-qa.db_dolphin_target_client_replica.tbl_sva_address` tgt
USING
  (
WITH
  sva_address AS (
  SELECT
    UPPER(postalcode) AS num_cep
    ,UPPER(city) AS ds_city_name
    ,UPPER(idclient) AS uuid_client
    ,UPPER(complement) AS ds_complement_street
    ,UPPER(street) AS ds_street
    ,UPPER(inddeliveryaddress) AS ds_delivery_address
    ,UPPER(addresstype) AS ds_address_type
    ,UPPER(neighborhood) AS ds_neighborhood_name
    ,UPPER(addressnumber) AS num_house
    ,UPPER(country) AS ds_country_name
    ,UPPER(reference) AS ds_reference
    ,UPPER(state) AS ini_state
    ,dth_partition
    ,ROW_NUMBER() OVER (PARTITION BY idclient, addresstype ORDER BY dth_partition DESC) RANK
  FROM
    `br-apps-bi-customermdm-qa.db_dolphin_stage_client_base.tbl_sva_address`,
    (
      SELECT
        TIMESTAMP(@LAST_DATE) as last_date)
    WHERE
      dth_partition > last_date)
  SELECT
    * EXCEPT (RANK)
  FROM
    sva_address
  WHERE
    RANK = 1) stg
ON
  tgt.uuid_client = stg.uuid_client AND tgt.ds_address_type = stg.ds_address_type
  WHEN MATCHED THEN UPDATE 
  SET 
  num_cep = stg.num_cep,
  ds_city_name = stg.ds_city_name, 
  uuid_client = stg.uuid_client,
  ds_complement_street = stg.ds_complement_street,
  ds_street = stg.ds_street,
  ds_delivery_address = stg.ds_delivery_address,
  ds_address_type = stg.ds_address_type,
  ds_neighborhood_name = stg.ds_neighborhood_name,
  num_house = stg.num_house,
  ds_country_name = stg.ds_country_name,
  ds_reference = stg.ds_reference,
  ini_state = stg.ini_state,
  dth_partition = stg.dth_partition,
  dth_update = stg.dth_partition
  WHEN NOT MATCHED
  THEN
INSERT
  (
  num_cep,
  ds_city_name,
  uuid_client,
  ds_complement_street,
  ds_street,
  ds_delivery_address,
  ds_address_type,
  ds_neighborhood_name,
  num_house,
  ds_country_name,
  ds_reference,
  ini_state,
  dth_partition,
  dth_create)
VALUES
  ( 
  stg.num_cep,
  stg.ds_city_name,
  stg.uuid_client,
  stg.ds_complement_street,
  stg.ds_street,
  stg.ds_delivery_address,
  stg.ds_address_type,
  stg.ds_neighborhood_name,
  stg.num_house,
  stg.ds_country_name,
  stg.ds_reference,
  stg.ini_state,
  stg.dth_partition,
  stg.dth_partition)