import argparse
import datetime as dt
import hashlib
import json
import logging
import os

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from google.cloud import bigquery
from pytz import timezone


class ParseJson(beam.DoFn):
    def encrypt_string(self, hash_string):
        sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
        return sha_signature
    def process(self, row):
        zone = timezone('America/Sao_Paulo')
        row['cpf'] = self.encrypt_string(row['cpf'])
        row['email'] = row['email'].lower()
        row['email_hash'] = self.encrypt_string(row['email'])
        yield {
            "num_document_hash": row["cpf"] if row["cpf"] not in ['','null'] else None,
            "ds_email": row["email"] if row["email"] not in ['','null'] else None,
            "dth_start_period": dt.datetime.now(zone),
            "dth_final_period": '2200-12-31 23:59:59.999',
            "ind_status": 1,
            "dth_request": row["dateRequest"] if row["dateRequest"] not in ['','null'] else None,
            "id_ticket": row["idTicket"] if row["idTicket"] not in ['','null'] else None,
            "ds_channel_request": row["channelRequest"] if row["channelRequest"] not in ['','null'] else None,
            "ds_description": row["description"] if row["description"] not in ['','null'] else None,
            "dth_create": dt.datetime.now(zone),
            "dth_update": dt.datetime.now(zone),
            "ds_email_hash": row["email_hash"] if row["email_hash"] not in ['','null'] else None
        }

class ParseJsonVtex(beam.DoFn):
    def encrypt_string(self, hash_string):
        sha_signature = hashlib.sha256(hash_string.encode()).hexdigest()
        return sha_signature
    def process(self, row):
        zone = timezone('America/Sao_Paulo')
        if 'userId' in row:
            row['cpf'] = None
            vtex = 'VTEX_ADDRES'
        else:
            row['cpf'] = self.encrypt_string(row['document'])
            vtex = 'VTEX_CLIENT'
        row['email'] = row['email'].lower()
        row['email_hash'] = self.encrypt_string(row['email'])
        yield {
            "num_document_hash": row["cpf"] if row["cpf"] not in ['','null'] else None,
            "ds_email": row["email"] if row["email"] not in ['','null'] else None,
            "dth_start_period": dt.datetime.now(zone),
            "dth_final_period": '2200-12-31 23:59:59.999',
            "ind_status": 1,
            "dth_request": row["createdIn"]  if row["createdIn"] not in ['','null'] else None,
            "id_ticket": None,
            "ds_channel_request": vtex,
            "ds_description": None,
            "dth_create": dt.datetime.now(zone),
            "dth_update": dt.datetime.now(zone),
            "ds_email_hash": row["email_hash"] if row["email_hash"] not in ['','null'] else None
        }

class ParsePayload(beam.DoFn):
    def process(self, message):
        try:
            row = json.loads(message)
            if 'BLACK_LIST' in row.keys():
                yield beam.pvalue.TaggedOutput('ticket', row["BLACK_LIST"])
            else:
                yield beam.pvalue.TaggedOutput('vtex', row)
        except:
            logging.info(f'Error while parsing the message: {message}')

class DeleteProcess(beam.DoFn):
    def __init__(self, env):
        self.env=env
        
    def exclui(self, delete):
        try:
            client = bigquery.Client()
            client.query(delete).result()
            print('deletado')
            return
        except:
            print('nao_deletado')
            logging.info(f'Error while deleting message: {delete}')
            
    def string_query(self, cpf):
        query = f"""
        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_csf_client`
        WHERE id = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_device_id_client`
        WHERE customerId = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_loyalty_person`
        WHERE cpf = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_neoassist_client`
        WHERE customerid = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_onetrust_client`
        WHERE cpf = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_propz_client`
        WHERE customerid = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_qualibest_client`
        WHERE cpf = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_rh_client`
        WHERE ns0cpf = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_sva_client`
        WHERE numdocument = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_sva_address` a
        WHERE a.idclient IN 
            (SELECT c.idclient FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_sva_client` c 
            WHERE c.numdocument = '{cpf}');

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_vtex_client`
        WHERE document = '{cpf}';

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_vtex_address` a
        WHERE a.userid IN 
            (SELECT c.id FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_stage_client_base.tbl_vtex_client` c 
            WHERE c.document = '{cpf}');

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_csf_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_device_id_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_loyalty_person`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_neoassist_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_onetrust_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_propz_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_qualibest_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_rh_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_sva_client`
        WHERE num_document = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_sva_address` a
        WHERE a.idclient IN 
            (SELECT c.idclient FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_sva_client` c 
            WHERE c.num_document = {cpf});

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_vtex_client`
        WHERE num_cpf = {cpf};

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_vtex_address` a
        WHERE a.userid IN 
            (SELECT c.id FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_vtex_client` c 
            WHERE c.num_cpf = {cpf});

        DELETE FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_client_replica.tbl_client_personal_information`
        WHERE num_document = {cpf}
        """
        return query

    def process(self, message):
        logging.info(f'Iniciando processo de exclusão')
        if 'cpf' in message.keys():
            query = self.string_query(message['cpf'])
        elif 'document' in message.keys():
            query = self.string_query(message['document'])
        self.exclui(query)
        logging.info(f'Excluído')
        yield
        

def get_schema_raw():
    schema = json.dumps([
        {"name": "num_document_hash", "type": "STRING"},
        {"name": "ds_email", "type": "STRING"},
        {"name": "dth_start_period", "type": "TIMESTAMP"},
        {"name": "dth_final_period", "type": "TIMESTAMP"},
        {"name": "ind_status", "type": "INTEGER"},
        {"name": "dth_request", "type": "TIMESTAMP"},
        {"name": "id_ticket", "type": "STRING"},
        {"name": "ds_channel_request", "type": "STRING"},
        {"name": "ds_description", "type": "STRING"},
        {"name": "dth_create", "type": "TIMESTAMP"},
        {"name": "dth_update", "type": "TIMESTAMP"},
        {"name": "ds_email_hash", "type": "STRING"}
    ])
  
    return schema

def run(options, input_subscription, output_table, env):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        parsed = (pipeline 
            | 'Read from PubSub' >> beam.io.ReadFromPubSub(subscription=input_subscription).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
            | "Parse Json" >> beam.ParDo(ParsePayload()).with_outputs())

        (parsed.ticket
            | "FlatMap" >>beam.FlatMap(lambda elements: elements)
            | 'Parse JSON messages black list' >> beam.ParDo(ParseJson())
            | 'Write to BigQuery black list ' >> beam.io.WriteToBigQuery(output_table,
                                        create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                                        schema=get_schema_raw()))

        (parsed.vtex
            | 'Parse JSON messages Vtex' >> beam.ParDo(ParseJsonVtex())
            | 'Write to BigQuery vtex' >> beam.io.WriteToBigQuery(output_table,
                                        create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                                        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                                        schema=get_schema_raw()))

        (parsed.ticket 
            | 'Delete Raws via ticket' >> beam.ParDo(DeleteProcess(env=env)))

        (parsed.vtex 
            | 'Delete Raws via vtex' >> beam.ParDo(DeleteProcess(env=env)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_subscription', required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".')
    parser.add_argument(
        '--output_table', required=True,
        help='Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.')
    parser.add_argument(
        '--env', required=True,
        help='Environment (dev, qa, prd)')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.input_subscription, known_args.output_table, known_args.env)
