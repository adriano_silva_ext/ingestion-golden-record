import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
import argparse
import json
from datetime import datetime as dt
import requests
import json
import warnings
from google.cloud import bigquery
from google.cloud import pubsub_v1

class Propz(beam.DoFn):
    def __init__(self, env):
        self.env = env
        self.project_id = f"br-apps-bi-customermdm-{env}"
        self.topic_id = f"br-apps-bi-customermdm-{env}-client-propz"
    def call_propz(self, limit, offset, last_update, environment):
        url = f"https://propzcloud.crrf.vc/v1/databases/c06fb85a-2598-443a-8595-6573196881de/segment-memberships?" \
            f"segment=c8a4c5af-1957-47dc-bc3a-6ed649d9ad56&expand=user.properties&limit={limit}&offset={offset}"
        payload = ""
        headers = {
            'Authorization': 'Basic YTQxMjY4YzMtNjAxNy00YjNlLWE1ODMtOWYwYjc0ODZhMjEwOmExYzFlYmE3LTJkYWItNGI2OS05ZTE3LTljYjYyMGY2YmI3MQ=='
        }
        response = requests.request("GET", url, headers=headers, data=payload)

        parsed = json.loads(response.text)

        # verifying is list is not empty
        if parsed['items']:
            self.transform_payload(parsed, last_update, environment)
            offset += limit
            self.call_propz(limit=limit, offset=offset, last_update=last_update, environment=environment)


    def transform_payload(self, complete_payload, last_update, environment):
        messages = []

        for i in complete_payload['items']:
            registro = {}

            registro['dateUpdated'] = i['user']['dateUpdated']
            registro['creationDate'] = i['user']['dateCreated']

            for j in i['user']['properties']['items']:

                # to key status
                if j['name'] == 'status':
                    registro['status'] = j['stringValue']

                # to key flagCustomer
                elif j['name'] == 'flagCustomer':
                    registro['flagCustomer'] = j['stringValue']

                # to key homeEmail
                elif j['name'] == 'homeEmail':
                    registro['homeEmail'] = j['stringValue']

                # to key crfOrigins
                elif j['name'] == 'crfOrigins':
                    registro['crfOrigins'] = j['stringValue']

                # to key homeStreet
                elif j['name'] == 'homeStreet':
                    registro['homeStreet'] = j['stringValue']

                # to key dateOfBirth
                elif j['name'] == 'dateOfBirth':
                    registro['dateOfBirth'] = j['dateValue']

                # to key dtFlagCustomer
                elif j['name'] == 'dtFlagCustomer':
                    registro['dtFlagCustomer'] = j['dateValue']

                # to key lastPurchase
                elif j['name'] == 'lastPurchase':
                    registro['lastPurchase'] = j['dateValue']

                # to key lastDateCampaignEMAILReceived
                elif j['name'] == 'lastDateCampaignEMAILReceived':
                    registro['lastDateCampaignEMAILReceived'] = j['dateValue']

                # to key dateUpdate
                elif j['name'] == 'dateUpdate':
                    registro['dateUpdate'] = j['dateValue']

                # to key optinAll
                elif j['name'] == 'optinAll':
                    registro['optinAll'] = j['integerValue']

                # to key emailContactFlag
                elif j['name'] == 'emailContactFlag':
                    registro['emailContactFlag'] = j['integerValue']

                # to key smsContactFlag
                elif j['name'] == 'smsContactFlag':
                    registro['smsContactFlag'] = j['integerValue']

                # to key pmUnsubscribedEmail
                elif j['name'] == 'pmUnsubscribedEmail':
                    registro['pmUnsubscribedEmail'] = j['integerValue']

                # to key pmUnsubscribedSms
                elif j['name'] == 'pmUnsubscribedSms':
                    registro['pmUnsubscribedSms'] = j['stringValue']

                # to key homeCountry
                elif j['name'] == 'homeCountry':
                    registro['homeCountry'] = j['stringValue']

                # to key gender
                elif j['name'] == 'gender':
                    registro['gender'] = j['stringValue']

                # to key homeState
                elif j['name'] == 'homeState':
                    registro['homeState'] = j['stringValue']

                # to key homeCity
                elif j['name'] == 'homeCity':
                    registro['homeCity'] = j['stringValue']

                # to key crfChannel
                elif j['name'] == 'crfChannel':
                    registro['crfChannel'] = j['stringValue']

                # to key firstName
                elif j['name'] == 'firstName':
                    registro['firstName'] = j['stringValue']

                # to key lastName
                elif j['name'] == 'lastName':
                    registro['lastName'] = j['stringValue']

                # to key childrenNum
                elif j['name'] == 'childrenNum':
                    registro['childrenNum'] = j['integerValue']

                # to key numberCupons
                elif j['name'] == 'numberCupons':
                    registro['numberCupons'] = j['integerValue']

                # to key mobilePhone
                elif j['name'] == 'mobilePhone':
                    registro['mobilePhone'] = j['stringValue']

                # to key engagementScoreEMAIL
                elif j['name'] == 'engagementScoreEMAIL':
                    registro['engagementScoreEMAIL'] = j['stringValue']

                # to key customerId
                elif j['name'] == 'customerId':
                    registro['customerId'] = j['stringValue']

                # to key pmBounceEmail
                elif j['name'] == 'pmBounceEmail':
                    registro['pmBounceEmail'] = j['integerValue']

                # to key pmBounceSms
                elif j['name'] == 'pmBounceSms':
                    registro['pmBounceSms'] = j['integerValue']

                # to key emailContactFlag
                elif j['name'] == 'emailContactFlag':
                    registro['emailContactFlag'] = j['integerValue']

            if 'status' not in registro.keys():
                registro['status'] = "Null"
            if 'flagCustomer' not in registro.keys():
                registro['flagCustomer'] = ""
            if 'homeEmail' not in registro.keys():
                registro['homeEmail'] = ""
            if 'crfOrigins' not in registro.keys():
                registro['crfOrigins'] = ""
            if 'homeStreet' not in registro.keys():
                registro['homeStreet'] = ""
            if 'dateOfBirth' not in registro.keys():
                registro['dateOfBirth'] = ""
            if 'dateUpdated' not in registro.keys():
                registro['dateUpdated'] = ""
            if 'creationDate' not in registro.keys():
                registro['creationDate'] = registro['dateUpdated']
            if 'dtFlagCustomer' not in registro.keys():
                registro['dtFlagCustomer'] = ""
            if 'lastPurchase' not in registro.keys():
                registro['lastPurchase'] = ""
            if 'lastDateCampaignEMAILReceived' not in registro.keys():
                registro['lastDateCampaignEMAILReceived'] = ""
            if 'optinAll' not in registro.keys():
                registro['optinAll'] = ""
            if 'emailContactFlag' not in registro.keys():
                registro['emailContactFlag'] = ""
            if 'smsContactFlag' not in registro.keys():
                registro['smsContactFlag'] = ""
            if 'pmUnsubscribedEmail' not in registro.keys():
                registro['pmUnsubscribedEmail'] = ""
            if 'pmUnsubscribedSms' not in registro.keys():
                registro['pmUnsubscribedSms'] = ""
            if 'homeCountry' not in registro.keys():
                registro['homeCountry'] = ""
            if 'gender' not in registro.keys():
                registro['gender'] = ""
            if 'homeState' not in registro.keys():
                registro['homeState'] = ""
            if 'homeCity' not in registro.keys():
                registro['homeCity'] = ""
            if 'crfChannel' not in registro.keys():
                registro['crfChannel'] = ""
            if 'firstName' not in registro.keys():
                registro['firstName'] = ""
            if 'lastName' not in registro.keys():
                registro['lastName'] = ""
            if 'childrenNum' not in registro.keys():
                registro['childrenNum'] = ""
            if 'numberCupons' not in registro.keys():
                registro['numberCupons'] = ""
            if 'mobilePhone' not in registro.keys():
                registro['mobilePhone'] = ""
            if 'engagementScoreEMAIL' not in registro.keys():
                registro['engagementScoreEMAIL'] = ""
            if 'customerId' not in registro.keys():
                registro['customerId'] = ""
            if 'pmBounceEmail' not in registro.keys():
                registro['pmBounceEmail'] = ""
            if 'pmBounceSms' not in registro.keys():
                registro['pmBounceSms'] = ""
            if 'emailContactFlag' not in registro.keys():
                registro['emailContactFlag'] = ""

            messages.append(registro)

        for n in messages:
            self.send_pubsub(n, last_update)


    def send_pubsub(self, message, last_update):

        dt_format = "%Y-%m-%d %H:%M:%S"

        if dt.strptime(message['dateUpdated'][:18].replace('T', ' '), dt_format) > dt.strptime(last_update, dt_format):

            data = json.dumps(message)
            publisher = pubsub_v1.PublisherClient()
            topic_path = publisher.topic_path(self.project_id, self.topic_id)

            future = publisher.publish(topic_path, data.encode("utf-8"))

            print(f"Published messages response: {future.result()}.")
        return 0


    def last_dt_updated(self, environment):
        query = f"""SELECT MAX(PARSE_DATETIME("%Y-%m-%dT%H:%M:%S", left(dateUpdated,19))) as maior_data 
                    FROM `br-apps-bi-customermdm-{environment}.db_dolphin_stage_client_base.tbl_propz_client`"""
        warnings.filterwarnings("ignore")
        client = bigquery.Client(project=self.project_id)
        result = client.query(query)

        for resultado in result:
            if resultado.maior_data is not None:
                horario = resultado.maior_data.strftime("%Y-%m-%d %H:%M:%S")
            else:
                horario = "1960-01-01 00:00:00"

        return horario

    def process(self, env):
        last_updated_bq = self.last_dt_updated(self.env)
        self.call_propz(limit=1000, offset=0, last_update=last_updated_bq, environment=self.env)

def run(options, env):
    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline 
            | 'Create' >> beam.Create([env])  
            | 'Grava arquivos' >> beam.ParDo(Propz(env=env)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--env', required=True,
        help='environemnt (dev, qa, prd)')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.env)
