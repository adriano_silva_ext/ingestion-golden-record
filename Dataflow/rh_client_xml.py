import argparse
import datetime as dt
import json
import logging
import os
from typing import Any, Dict, List

import apache_beam as beam
import xmltodict
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJson(beam.DoFn):
    def process(self, row):
        zone = timezone("America/Sao_Paulo")
        yield {
            "ns0cod_empresa": row["ns0:cod_empresa"]
            if "ns0:cod_empresa" in row.keys()
            else None,
            "ns0matricula_func": row["ns0:matricula_func"]
            if "ns0:matricula_func" in row.keys()
            else None,
            "ns0cod_sit_func": row["ns0:cod_sit_func"]
            if "ns0:cod_sit_func" in row.keys()
            else None,
            "ns0cod_lotacao": row["ns0:cod_lotacao"]
            if "ns0:cod_lotacao" in row.keys()
            else None,
            "ns0email_pessoal": row["ns0:email_pessoal"]
            if "ns0:email_pessoal" in row.keys()
            else None,
            "ns0email_corp": row["ns0:email_corp"]
            if "ns0:email_corp" in row.keys()
            else None,
            "ns0desc_tipo_situacao": row["ns0:desc_tipo_situacao"]
            if "ns0:desc_tipo_situacao" in row.keys()
            else None,
            "ns0desc_situacao": row["ns0:desc_situacao"]
            if "ns0:desc_situacao" in row.keys()
            else None,
            "ns0data_contratacao": row["ns0:data_contratacao"]
            if "ns0:data_contratacao" in row.keys()
            else None,
            "ns0data_nasc": row["ns0:data_nasc"]
            if "ns0:data_nasc" in row.keys()
            else None,
            "ns0tipo_situacao": row["ns0:tipo_situacao"]
            if "ns0:tipo_situacao" in row.keys()
            else None,
            "ns0nome": row["ns0:nome"] if "ns0:nome" in row.keys() else None,
            "ns0desc_empresa": row["ns0:desc_empresa"]
            if "ns0:desc_empresa" in row.keys()
            else None,
            "ns0name_lotacao": row["ns0:name_lotacao"]
            if "ns0:name_lotacao" in row.keys()
            else None,
            "ns0local_trabalho": row["ns0:local_trabalho"]
            if "ns0:local_trabalho" in row.keys()
            else None,
            "ns0cpf": row["ns0:cpf"] if "ns0:cpf" in row.keys() else None,
            "ns0drt": row["ns0:drt"] if "ns0:drt" in row.keys() else None,
            "ns0cellphone": row["ns0:cellphone"]
            if "ns0:cellphone" in row.keys()
            else None,
            "dth_inclusion": dt.datetime.now(zone),
            "dth_partition": dt.datetime.now(zone)
        }


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        try:
            doc = json.dumps(xmltodict.parse(message))
            doc = json.loads(doc)
            row = doc["ns0:SchemaFPW"]["ns0:LayoutFPW"]
            # row = json.loads(message)
            yield beam.pvalue.TaggedOutput("process", row)
        except:
            logging.info(f'Error while parsing the message: {message}')


def get_schema_raw():
    schema = json.dumps(
        [
            {"name": "ns0cod_empresa", "type": "STRING"},
            {"name": "ns0matricula_func", "type": "STRING"},
            {"name": "ns0cod_sit_func", "type": "STRING"},
            {"name": "ns0cod_lotacao", "type": "STRING"},
            {"name": "ns0email_pessoal", "type": "STRING"},
            {"name": "ns0email_corp", "type": "STRING"},
            {"name": "ns0desc_tipo_situacao", "type": "STRING"},
            {"name": "ns0desc_situacao", "type": "STRING"},
            {"name": "ns0data_contratacao", "type": "STRING"},
            {"name": "ns0data_nasc", "type": "STRING"},
            {"name": "ns0tipo_situacao", "type": "STRING"},
            {"name": "ns0nome", "type": "STRING"},
            {"name": "ns0desc_empresa", "type": "STRING"},
            {"name": "ns0name_lotacao", "type": "STRING"},
            {"name": "ns0local_trabalho", "type": "STRING"},
            {"name": "ns0cpf", "type": "STRING"},
            {"name": "ns0drt", "type": "STRING"},
            {"name": "ns0cellphone", "type": "STRING"},
            {"name": "dth_inclusion", "type": "TIMESTAMP"},
            {"name": "dth_partition", "type": "TIMESTAMP"}
        ]
    )

    return schema


def run(options, input_subscription, output_table_raw, output_path):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | "Read from PubSub"
            >> beam.io.ReadFromPubSub(
                subscription=input_subscription
            ).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (
            rows | "Data Guard verify" >> beam.ParDo(
                DataGuardVerify()).with_outputs()
        )

        (
            verify.process
            | "Parse JSON messages RAW" >> beam.ParDo(ParseJson())
            | "Write to BigQuery RAW"
            >> beam.io.WriteToBigQuery(
                output_table_raw,
                create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                schema=get_schema_raw(),
            )
        )

        (
            verify.process
            | "Writing .json to bucket"
            >> beam.ParDo(
                WriteElementsToGCS(output_path=output_path, origem="rh_client")
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_subscription",
        required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".',
    )
    parser.add_argument(
        "--output_table_raw",
        required=True,
        help="Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.",
    )
    parser.add_argument(
        "--output_path",
        required=True,
        help="Output Google Cloud Storage specified as: gs//<BUCKET>.",
    )
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(
        pipeline_options,
        known_args.input_subscription,
        known_args.output_table_raw,
        known_args.output_path,
    )
