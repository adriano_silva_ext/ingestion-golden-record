import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions
import argparse
import json
from google.cloud import storage
import datetime as dt
import logging
from datetime import datetime
from datetime import timedelta
import os

# class GravaArquivos(beam.DoFn):
#     def __init__(self, env):
#         self.env = env

#     def remove_ultima_linha(self, nome_arquivo):
#         with open(nome_arquivo, 'r+', encoding = "utf-8") as arquivo:
#             arquivo.seek(0, os.SEEK_END)
#             pos = arquivo.tell() - 1
#             while pos > 0 and arquivo.read(1) != "\n":
#                 pos -= 1
#                 arquivo.seek(pos, os.SEEK_SET)

#             # Enquanto não estivermos no começo do arquivo, deleta todos os caracteres para frente desta posição
#             if pos > 0:
#                 arquivo.seek(pos, os.SEEK_SET)
#                 arquivo.truncate()
#         arquivo.close()
#         with open(f'{self.origem}-{self.contador}-{self.data_anterior}.json', 'a') as arquivo:
#             arquivo.write(']')
#             arquivo.close()
#         return
#     def grava_arquivos(self, bucket_name, prefix, delimiter=None):
#         storage_client = storage.Client()

#         storage_client = storage.Client()
#         blobs = storage_client.list_blobs(bucket_name, prefix=prefix, delimiter=delimiter)

#         self.contador = 1
#         with open(f'{self.origem}-{self.contador}-{self.data_anterior}.json', "w") as f:
#             f.write('[')
#             f.close()
#         lista_arquivos = []
#         lista_arquivos.append(f'{self.origem}-{self.contador}-{self.data_anterior}.json')
#         for blob in blobs:
#             if os.path.getsize(f'{self.origem}-{self.contador}-{self.data_anterior}.json') / (1024 * 1024) < 5:
#                 with open(f'{self.origem}-{self.contador}-{self.data_anterior}.json', 'a') as f:
#                     blob.download_to_filename(f'{self.origem}_{self.data_anterior}.json')
#                     blob.delete()
#                     with open(f'{self.origem}_{self.data_anterior}.json') as infile:
#                         f.write(infile.read() + '\n,' )
#                     infile.close()
#                     f.close()
#             else:
#                 self.remove_ultima_linha(f'{self.origem}-{self.contador}-{self.data_anterior}.json')
#                 self.contador += 1
#                 with open(f'{self.origem}-{self.contador}-{self.data_anterior}.json', 'a') as f:
#                     blob.download_to_filename(f'{self.origem}_{self.data_anterior}.json')
#                     blob.delete()
#                     with open(f'{self.origem}_{self.data_anterior}.json') as infile:
#                         f.write(infile.read() + '\n,' )
#                     infile.close()
#                     f.close()
#                     lista_arquivos.append(f'{self.origem}-{self.contador}-{self.data_anterior}.json')
#         self.remove_ultima_linha(f'{self.origem}-{self.contador}-{self.data_anterior}.json')
#         return lista_arquivos

#     def process(self, origem):
#         self.origem = origem
#         self.data_anterior = (datetime.now()-timedelta(days=1)).strftime('%Y%m%d')
#         # self.data_anterior = '20211125'
#         lista_arquivos = self.grava_arquivos(f"br-apps-bi-customermdm-{self.env}-client-dataguard", f"{origem}/{self.data_anterior}")
#         return lista_arquivos
from subprocess import call
from subprocess import getoutput

class GravaArquivos(beam.DoFn):
    def __init__(self, env):
        self.env = env

    def process(self, origem):
        logging.info(origem)
        print(origem)
        self.origem = origem
        self.data_anterior = (datetime.now()-timedelta(days=1)).strftime('%Y%m%d')
        arquivao = f"gsutil cat gs://br-apps-bi-customermdm-{self.env}-client-dataguard/{self.origem}/{self.data_anterior}/*.json > merged_{self.origem}.json"
        split_varias_linhas = f"sed -i -e 's/}}{{/}},\\n{{/g' merged_{self.origem}.json"
        split_quantidade_linhas = f"split -dl 10000 --additional-suffix=.json merged_{self.origem}.json {self.origem}-{self.data_anterior}-"
        call(arquivao, shell=True)
        call(split_varias_linhas, shell=True)
        call(split_quantidade_linhas, shell=True)
        listar_splits = f"ls {self.origem}-*"
        lista_splits = getoutput(listar_splits)
        contador = 0
        lista = lista_splits.split('\n')
        for arquivo in reversed(lista):
            colchete_primeira_posicao = f"sed -i '1i[' {arquivo}"
            call(colchete_primeira_posicao, shell=True)
            if contador == 0:
                colchete_ultima_posicao = f"echo ']' >> {arquivo}"
                call(colchete_ultima_posicao, shell=True)
            else:
                apaga_ultima_posicao = f"sed -i '$! {{ P; D; }}; s/.$//' {arquivo}"
                call(apaga_ultima_posicao, shell=True)
                colchete_ultima_posicao = f"echo ']' >> {arquivo}"
                call(colchete_ultima_posicao, shell=True)
            contador += 1
        return lista

class SobeArquivos(beam.DoFn):
    def __init__(self, env):
        self.env = env

    def apaga_arquivo_local(self):
        if os.path.exists(self.arquivo):
            os.remove(self.arquivo)
        if os.path.exists(f'merged_{self.origem}.json'):
            os.remove(f'merged_{self.origem}.json')

    def process(self, arquivo):
        logging.info(arquivo)
        print(arquivo)
        self.arquivo = arquivo
        lista = self.arquivo.split('-')
        self.origem = lista[0]
        self.contador = lista[2]
        self.dia_anterior = lista[1]
        client = storage.Client()
        bucket = client.bucket(f"br-apps-bi-customermdm-{self.env}-client-dataguard")
        blob = bucket.blob(f'{self.origem}/{self.dia_anterior}/{self.origem}{self.contador}')
        blob.upload_from_filename(self.arquivo)
        self.apaga_arquivo_local()


def run(options, env):
    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline 
            | "Lista de origens" >> beam.Create(['csf_client','sva','rh_client', 'neoassist_client', 'onetrust_client', 'qualibest_client', 'propz_client', 'loyalty_person_client', 'vtex_client', 'vtex_address', 'device_id_client'])   
            | 'Grava arquivos' >> beam.ParDo(GravaArquivos(env=env))
            | 'Sobe arquivos' >> beam.ParDo(SobeArquivos(env=env)))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--env', required=True,
        help='environemnt (dev, qa, prd)')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.env)