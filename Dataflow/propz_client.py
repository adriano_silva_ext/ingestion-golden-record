import argparse
import datetime as dt
import json
import logging
import os
from typing import Any, Dict, List

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJson(beam.DoFn):
    def process(self, row):
        zone = timezone('America/Sao_Paulo')
        yield {
            "status": row["status"] if row["status"] not in ['', 'null'] else None,
            "flagcustomer": row["flagCustomer"] if row["flagCustomer"] not in ['', 'null'] else None,
            "homeemail": row["homeEmail"] if row["homeEmail"] not in ['', 'null'] else None,
            "crforigins": row["crfOrigins"] if row["crfOrigins"] not in ['', 'null'] else None,
            "homestreet": row["homeStreet"] if row["homeStreet"] not in ['', 'null'] else None,
            "dateofbirth": row["dateOfBirth"] if row["dateOfBirth"] not in ['', 'null'] else None,
            "creationdate": row["creationDate"] if row["creationDate"] not in ['', 'null'] else None,
            "dtflagcustomer": row["dtFlagCustomer"] if row["dtFlagCustomer"] not in ['', 'null'] else None,
            "lastpurchase": row["lastPurchase"] if row["lastPurchase"] not in ['', 'null'] else None,
            "lastdatecampaignemailreceived": row["lastDateCampaignEMAILReceived"] if row["lastDateCampaignEMAILReceived"] not in ['', 'null'] else None,
            "pmbounceemail": row["pmBounceEmail"] if row["pmBounceEmail"] not in ['', 'null'] else None,
            "pmbouncesms": row["pmBounceSms"] if row["pmBounceSms"] not in ['', 'null'] else None,
            "dateupdated": row["dateUpdated"] if row["dateUpdated"] not in ['', 'null'] else None,
            "optinall": row["optinAll"] if row["optinAll"] not in ['', 'null'] else None,
            "emailcontactflag": row["emailContactFlag"] if row["emailContactFlag"] not in ['', 'null'] else None,
            "smscontactflag": row["smsContactFlag"] if row["smsContactFlag"] not in ['', 'null'] else None,
            "pmunsubscribedemail": row["pmUnsubscribedEmail"] if row["pmUnsubscribedEmail"] not in ['', 'null'] else None,
            "pmunsubscribedsms": row["pmUnsubscribedSms"] if row["pmUnsubscribedSms"] not in ['', 'null'] else None,
            "homecountry": row["homeCountry"] if row["homeCountry"] not in ['', 'null'] else None,
            "gender": row["gender"] if row["gender"] not in ['', 'null'] else None,
            "homestate": row["homeState"] if row["homeState"] not in ['', 'null'] else None,
            "homecity": row["homeCity"] if row["homeCity"] not in ['', 'null'] else None,
            "crfchannel": row["crfChannel"] if row["crfChannel"] not in ['', 'null'] else None,
            "firstname": row["firstName"] if row["firstName"] not in ['', 'null'] else None,
            "lastname": row["lastName"] if row["lastName"] not in ['', 'null'] else None,
            "childrennum": row["childrenNum"] if row["childrenNum"] not in ['', 'null'] else None,
            "numbercupons": row["numberCupons"] if row["numberCupons"] not in ['', 'null'] else None,
            "mobilephone": row["mobilePhone"] if row["mobilePhone"] not in ['', 'null'] else None,
            "engagementscoreemail": row["engagementScoreEMAIL"] if row["engagementScoreEMAIL"] not in ['', 'null'] else None,
            "customerid": row["customerId"] if row["customerId"] not in ['', 'null'] else None,
            "dth_inclusion": dt.datetime.now(zone),
            "dth_partition": dt.datetime.now(zone)
        }


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        try:
            row = json.loads(message)
            yield beam.pvalue.TaggedOutput('process', row)
        except:
            logging.info(f'Error while parsing the message: {message}')


def get_schema_raw():
    schema = json.dumps([
        {"name": "status", "type": "STRING"},
        {"name": "flagcustomer", "type": "STRING"},
        {"name": "homeemail", "type": "STRING"},
        {"name": "crforigins", "type": "STRING"},
        {"name": "homestreet", "type": "STRING"},
        {"name": "dateofbirth", "type": "STRING"},
        {"name": "creationdate", "type": "STRING"},
        {"name": "dtflagcustomer", "type": "STRING"},
        {"name": "lastpurchase", "type": "STRING"},
        {"name": "lastdatecampaignemailreceived", "type": "STRING"},
        {"name": "dateupdated", "type": "STRING"},
        {"name": "optinall", "type": "STRING"},
        {"name": "emailcontactflag", "type": "STRING"},
        {"name": "smscontactflag", "type": "STRING"},
        {"name": "pmunsubscribedemail", "type": "STRING"},
        {"name": "pmunsubscribedsms", "type": "STRING"},
        {"name": "homecountry", "type": "STRING"},
        {"name": "gender", "type": "STRING"},
        {"name": "homestate", "type": "STRING"},
        {"name": "homecity", "type": "STRING"},
        {"name": "crfchannel", "type": "STRING"},
        {"name": "firstname", "type": "STRING"},
        {"name": "lastname", "type": "STRING"},
        {"name": "childrennum", "type": "STRING"},
        {"name": "numbercupons", "type": "STRING"},
        {"name": "mobilephone", "type": "STRING"},
        {"name": "engagementscoreemail", "type": "STRING"},
        {"name": "customerid", "type": "STRING"},
        {"name": "dth_inclusion", "type": "TIMESTAMP"},
        {"name": "dth_partition", "type": "TIMESTAMP"}])

    return schema


def run(options, input_subscription, output_table_raw, output_path):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | 'Read from PubSub' >> beam.io.ReadFromPubSub(subscription=input_subscription).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (rows | 'Data Guard verify' >> beam.ParDo(
            DataGuardVerify()).with_outputs())

        (verify.process
         | 'Parse JSON messages RAW' >> beam.ParDo(ParseJson())
         | 'Write to BigQuery RAW' >> beam.io.WriteToBigQuery(output_table_raw,
                                                              create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                                                              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                                                              schema=get_schema_raw()))

        (verify.process | 'Writing .json to bucket' >> beam.ParDo(
            WriteElementsToGCS(output_path=output_path, origem='propz_client')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_subscription', required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".')
    parser.add_argument(
        '--output_table_raw', required=True,
        help='Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.')
    parser.add_argument(
        '--output_path', required=True,
        help='Output Google Cloud Storage specified as: gs//<BUCKET>.')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.input_subscription,
        known_args.output_table_raw, known_args.output_path)
