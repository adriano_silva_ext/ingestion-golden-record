import argparse
import datetime as dt
import json
import logging
import os
from typing import Any, Dict, List

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJson(beam.DoFn):
    def process(self, row):
        zone = timezone('America/Sao_Paulo')
        yield {
            "cpf": row["CPF"] if row["CPF"] not in ['', 'null'] else None,
            "email": row["Email"] if row["Email"] not in ['', 'null'] else None,
            "nome": row["Nome"] if row["Nome"] not in ['', 'null'] else None,
            "sobrenome": row["Sobrenome"] if row["Sobrenome"] not in ['', 'null'] else None,
            "telefone": row["Telefone"] if row["Telefone"] not in ['', 'null'] else None,
            "genero": row["Genero"] if row["Genero"] not in ['', 'null'] else None,
            "data_de_nascimento": row["data_de_nascimento"] if row["data_de_nascimento"] not in ['', 'null'] else None,
            "origem": row["Origem"] if row["Origem"] not in ['', 'null'] else None,
            "fin_polpriv_cn": row["FIN_POLPRIV_CN"] if row["FIN_POLPRIV_CN"] not in ['', 'null'] else None,
            "fin_polpriv_gmt": row["FIN_POLPRIV_GMT"] if row["FIN_POLPRIV_GMT"] not in ['', 'null'] else None,
            "fin_ofertasmail_cn": row["FIN_OFERTASMAIL_CN"] if row["FIN_OFERTASMAIL_CN"] not in ['', 'null'] else None,
            "fin_ofertasmail_gmt": row["FIN_OFERTASMAIL_GMT"] if row["FIN_OFERTASMAIL_GMT"] not in ['', 'null'] else None,
            "fin_ofertassms_cn": row["FIN_OFERTASSMS_CN"] if row["FIN_OFERTASSMS_CN"] not in ['', 'null'] else None,
            "fin_ofertassms_gmt": row["FIN_OFERTASSMS_GMT"] if row["FIN_OFERTASSMS_GMT"] not in ['', 'null'] else None,
            "fin_ofertaswhats_cn": row["FIN_OFERTASWHATS_CN"] if row["FIN_OFERTASWHATS_CN"] not in ['', 'null'] else None,
            "fin_ofertaswhats_gmt": row["FIN_OFERTASWHATS_GMT"] if row["FIN_OFERTASWHATS_GMT"] not in ['', 'null'] else None,
            "fin_statuspedwhats_cn": row["FIN_STATUSPEDWHATS_CN"] if row["FIN_STATUSPEDWHATS_CN"] not in ['', 'null'] else None,
            "fin_statuspedwhats_gmt": row["FIN_STATUSPEDWHATS_GMT"] if row["FIN_STATUSPEDWHATS_GMT"] not in ['', 'null'] else None,
            "fin_terceiros_cn": row["FIN_TERCEIROS_CN"] if row["FIN_TERCEIROS_CN"] not in ['', 'null'] else None,
            "fin_terceiros_gmt": row["FIN_TERCEIROS_GMT"] if row["FIN_TERCEIROS_GMT"] not in ['', 'null'] else None,
            "fin_newsletter_cn": row["FIN_NEWSLETTER_CN"] if row["FIN_NEWSLETTER_CN"] not in ['', 'null'] else None,
            "fin_newsletter_gmt": row["FIN_NEWSLETTER_GMT"] if row["FIN_NEWSLETTER_GMT"] not in ['', 'null'] else None,
            "fin_carina_cn": row["FIN_CARINA_CN"] if row["FIN_CARINA_CN"] not in ['', 'null'] else None,
            "fin_carina_gmt": row["FIN_CARINA_GMT"] if row["FIN_CARINA_GMT"] not in ['', 'null'] else None,
            "fin_compartilha_cn": row["FIN_COMPARTILHA_CN"] if row["FIN_COMPARTILHA_CN"] not in ['', 'null'] else None,
            "fin_compartilha_gmt": row["FIN_COMPARTILHA_GMT"] if row["FIN_COMPARTILHA_GMT"] not in ['', 'null'] else None,
            "dth_inclusion": dt.datetime.now(zone),
            "dth_partition": dt.datetime.now(zone)
        }


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        try:
            row = json.loads(message)
            yield beam.pvalue.TaggedOutput('process', row)
        except:
            logging.info(f'Error while parsing the message: {message}')


def get_schema_raw():
    schema = json.dumps([
        {"name": "cpf", "type": "STRING"},
        {"name": "email", "type": "STRING"},
        {"name": "nome", "type": "STRING"},
        {"name": "sobrenome", "type": "STRING"},
        {"name": "telefone", "type": "STRING"},
        {"name": "genero", "type": "STRING"},
        {"name": "data_de_nascimento", "type": "STRING"},
        {"name": "origem", "type": "STRING"},
        {"name": "fin_polpriv_cn", "type": "STRING"},
        {"name": "fin_polpriv_gmt", "type": "STRING"},
        {"name": "fin_ofertasmail_cn", "type": "STRING"},
        {"name": "fin_ofertasmail_gmt", "type": "STRING"},
        {"name": "fin_ofertassms_cn", "type": "STRING"},
        {"name": "fin_ofertassms_gmt", "type": "STRING"},
        {"name": "fin_ofertaswhats_cn", "type": "STRING"},
        {"name": "fin_ofertaswhats_gmt", "type": "STRING"},
        {"name": "fin_statuspedwhats_cn", "type": "STRING"},
        {"name": "fin_statuspedwhats_gmt", "type": "STRING"},
        {"name": "fin_terceiros_cn", "type": "STRING"},
        {"name": "fin_terceiros_gmt", "type": "STRING"},
        {"name": "fin_newsletter_cn", "type": "STRING"},
        {"name": "fin_newsletter_gmt", "type": "STRING"},
        {"name": "fin_carina_cn", "type": "STRING"},
        {"name": "fin_carina_gmt", "type": "STRING"},
        {"name": "fin_compartilha_cn", "type": "STRING"},
        {"name": "fin_compartilha_gmt", "type": "STRING"},
        {"name": "dth_inclusion", "type": "TIMESTAMP"},
        {"name": "dth_partition", "type": "TIMESTAMP"}
    ])

    return schema


def run(options, input_subscription, output_table_raw, output_path):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | 'Read from PubSub' >> beam.io.ReadFromPubSub(subscription=input_subscription).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (rows | 'Data Guard verify' >> beam.ParDo(
            DataGuardVerify()).with_outputs())

        (verify.process
         | 'Parse JSON messages RAW' >> beam.ParDo(ParseJson())
         | 'Write to BigQuery RAW' >> beam.io.WriteToBigQuery(output_table_raw,
                                                              create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                                                              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                                                              schema=get_schema_raw()))

        (verify.process | 'Writing .json to bucket' >> beam.ParDo(
            WriteElementsToGCS(output_path=output_path, origem='onetrust_client')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_subscription', required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".')
    parser.add_argument(
        '--output_table_raw', required=True,
        help='Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.')
    parser.add_argument(
        '--output_path', required=True,
        help='Output Google Cloud Storage specified as: gs//<BUCKET>.')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.input_subscription,
        known_args.output_table_raw, known_args.output_path)
