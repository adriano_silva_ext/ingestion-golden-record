import argparse
import datetime as dt
import json
import logging
from typing import Any, Dict, List

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJson(beam.DoFn):
    def process(self, row):
        zone = timezone('America/Sao_Paulo')
        yield {
            "cpf": row["cpf"] if "cpf" in row.keys() else None,
            "nome": row["nome"] if "nome" in row.keys() else None,
            "data_de_nascimento": row["data_de_nascimento"] if "data_de_nascimento" in row.keys() else None,
            "email": row["email"] if "email" in row.keys() else None,
            "celular": row["celular"] if "celular" in row.keys() else None,
            "cep": row["cep"] if "cep" in row.keys() else None,
            "estado": row["estado"] if "estado" in row.keys() else None,
            "cidade": row["cidade"] if "cidade" in row.keys() else None,
            "bairro": row["bairro"] if "bairro" in row.keys() else None,
            "endereco": row["endereco"] if "endereco" in row.keys() else None,
            "complemento": row["complemento"] if "complemento" in row.keys() else None,
            "numero": row["numero"] if "numero" in row.keys() else None,
            "dth_inclusion": dt.datetime.now(zone),
            "dth_partition": dt.datetime.now(zone)
        }


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        try:
            row = json.loads(message)
            if type(row) == list:
                yield beam.pvalue.TaggedOutput('data_guard', row)
            else:
                yield beam.pvalue.TaggedOutput('process', row)
        except:
            logging.info(f'Error while parsing the message: {message}')


def get_schema_raw():
    schema = json.dumps([
        {"name": "cpf", "type": "STRING"},
        {"name": "nome", "type": "STRING"},
        {"name": "data_de_nascimento", "type": "STRING"},
        {"name": "email", "type": "STRING"},
        {"name": "celular", "type": "STRING"},
        {"name": "cep", "type": "STRING"},
        {"name": "estado", "type": "STRING"},
        {"name": "cidade", "type": "STRING"},
        {"name": "bairro", "type": "STRING"},
        {"name": "endereco", "type": "STRING"},
        {"name": "complemento", "type": "STRING"},
        {"name": "numero", "type": "STRING"},
        {"name": "dth_inclusion", "type": "TIMESTAMP"},
        {"name": "dth_partition", "type": "TIMESTAMP"}
    ])

    return schema


def run(options, input_subscription, output_table_raw, output_path):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | 'Read from PubSub' >> beam.io.ReadFromPubSub(subscription=input_subscription).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (rows | 'Data Guard verify' >> beam.ParDo(
            DataGuardVerify()).with_outputs())

        (verify.process
         | 'Parse JSON messages RAW' >> beam.ParDo(ParseJson())
         | 'Write to BigQuery RAW' >> beam.io.WriteToBigQuery(output_table_raw,
                                                              create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                                                              write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                                                              schema=get_schema_raw()))

        (verify.process | 'Writing .json to bucket' >> beam.ParDo(
            WriteElementsToGCS(output_path=output_path, origem='qualibest_client')))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input_subscription', required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".')
    parser.add_argument(
        '--output_table_raw', required=True,
        help='Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.')
    parser.add_argument(
        '--output_path', required=True,
        help='Output Google Cloud Storage specified as: gs//<BUCKET>.')
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(pipeline_options, known_args.input_subscription,
        known_args.output_table_raw, known_args.output_path)
