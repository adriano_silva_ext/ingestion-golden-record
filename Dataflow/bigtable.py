import argparse
import datetime
import logging
from datetime import timezone

import apache_beam as beam
from apache_beam.options.pipeline_options import (
    GoogleCloudOptions,
    PipelineOptions,
    SetupOptions,
)
from google.cloud import bigquery, bigtable


class WriteBigTable(beam.DoFn):
    def __init__(self, project_id, instance_id, table_id, bucket_name):
        self.project_id = project_id
        self.instance_id = instance_id
        self.table_id = table_id
        self.bucket_name = bucket_name

    def process(self, message):
        last_runtime = message
        now = datetime.datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
        client = bigquery.Client()
        query = f"""
            SELECT
                num_unique_client			
                ,num_version_golden_record			
                ,ds_full_name	as ds_full_name			
                ,ds_first_name	as ds_first_name			
                ,ds_last_name	as ds_last_name			
                ,ds_name_source_system	as ds_name_source_system			
                ,num_document			
                ,TO_BASE64(hash_document) as hash_document		
                ,ds_document_type	as ds_document_type			
                ,ds_document_source_system	as ds_document_source_system			
                ,dth_birth			
                ,ds_birth_source_system	as ds_birth_source_system			
                ,ini_gender   		as ini_gender	
                ,ds_gender   			as ds_gender
                ,ds_gender_source_system   		as ds_gender_source_system	
                ,ini_marital_status   	as ini_marital_status		
                ,ds_marital_status   as ds_marital_status			
                ,ds_marital_status_source_system   	as ds_marital_status_source_system		
                ,ds_nationality   		as ds_nationality	
                ,ds_country_birth_name   		as ds_country_birth_name	
                ,ds_nationality_source_system   		as ds_nationality_source_system	
                ,ind_status_client_csf_carrefour   		as ind_status_client_csf_carrefour	
                ,ds_status_client_csf_carrefour   		as ds_status_client_csf_carrefour	
                ,dt_admission_minhas_recompensas			
                ,ind_employee			
                ,ind_has_atacadao			
                ,ind_has_carrefour			
                ,ind_client_blocked			
                ,ind_portfolio				as ind_portfolio
                ,ind_bad_golden_record			
                ,ind_deleted			
                ,ds_channel_create_client		as ds_channel_create_client		
                ,uuid_token_firebase				as uuid_token_firebase
                ,lst_client_origins	
                ,lst_reason_bad_golden_record		
                ,cod_consent_privacy_policy   	as cod_consent_privacy_policy		
                ,dth_consent_privacy_policy			
                ,cod_consent_purchase_status   		as cod_consent_purchase_status	
                ,dth_consent_purchase_status			
                ,cod_consent_sharing_third   		as cod_consent_sharing_third	
                ,dth_consent_sharing_third			
                ,cod_source_general_consent   			as cod_source_general_consent
                ,dth_create_general_consent			
                ,cod_consent_collection_point_chatbox   	as cod_consent_collection_point_chatbox		
                ,dth_consent_collection_point_chatbox			
                ,cod_consent_collection_point_csf   		as cod_consent_collection_point_csf	
                ,dth_consent_collection_point_csf			
                ,cod_consent_collection_point_website   	as cod_consent_collection_point_website		
                ,dth_consent_collection_point_website			
                ,dth_create_collection_point			
                ,cod_source_collection_point   			as cod_source_collection_point
                ,ds_personal_email		            as ds_personal_email
                ,TO_BASE64(hash_personal_email) as hash_personal_email					
                ,ds_source_system_personal_email			as ds_source_system_personal_email	
                ,dth_create_personal_email			
                ,cod_consent_offer_personal_email   		as cod_consent_offer_personal_email	
                ,dth_consent_offer_personal_email			
                ,cod_source_offer_personal_email   		as cod_source_offer_personal_email	
                ,dth_create_offer_personal_email		
                ,ind_bounce_personal_email	
                ,ds_business_email			as ds_business_email
                ,TO_BASE64(hash_business_email) as hash_business_email		
                ,ds_source_system_business_email				as ds_source_system_business_email
                ,dth_create_business_email			
                ,cod_consent_offer_business_email   			as cod_consent_offer_business_email
                ,dth_consent_offer_business_email			
                ,cod_source_offer_business_email   			as cod_source_offer_business_email
                ,dth_create_offer_business_email	
                ,ind_bounce_business_email		
                ,num_mobile_phone as num_mobile_phone	
                ,TO_BASE64(hash_mobile_phone) as hash_mobile_phone				
                ,ds_source_system_mobile_phone				as ds_source_system_mobile_phone
                ,dth_create_mobile_phone
                ,ind_bounce_mobile_phone			
                ,cod_consent_offer_sms_mobile_phone   			as cod_consent_offer_sms_mobile_phone
                ,dth_consent_offer_sms_mobile_phone			
                ,cod_consent_offer_whatsapp_mobile_phone   		as cod_consent_offer_whatsapp_mobile_phone	
                ,dth_consent_offer_whatsapp_mobile_phone			
                ,cod_source_mobile_phone_optin   			as cod_source_mobile_phone_optin
                ,dth_create_mobile_phone_optin			
                ,num_landline_phone as num_landline_phone	
                ,TO_BASE64(hash_landline_phone) as hash_landline_phone				
                ,ds_source_system_landline_phone				as ds_source_system_landline_phone
                ,dth_create_landline_phone	
                ,ind_bounce_landline_phone		
                ,cod_consent_offer_sms_landline_phone   		as cod_consent_offer_sms_landline_phone	
                ,dth_consent_offer_sms_landline_phone			
                ,cod_consent_offer_whatsapp_landline_phone   	as cod_consent_offer_whatsapp_landline_phone		
                ,dth_consent_offer_whatsapp_landline_phone			
                ,cod_source_landline_phone_optin   			as cod_source_landline_phone_optin
                ,dth_create_landline_phone_optin			
                ,num_cep_residential as num_cep_residential			
                ,ds_street_residential				as ds_street_residential
                ,ds_neighborhood_name_residential		as ds_neighborhood_name_residential		
                ,ds_city_name_residential				as ds_city_name_residential
                ,ini_state_residential as ini_state_residential			
                ,ds_state_name_residential				as ds_state_name_residential
                ,ds_country_name_residential				as ds_country_name_residential
                ,num_house_residential          as num_house_residential			
                ,ds_complement_street_residential			as ds_complement_street_residential	
                ,ds_reference_residential				as ds_reference_residential
                ,ds_source_system_residential			as ds_source_system_residential	
                ,num_cep_invoice as num_cep_invoice			
                ,ds_street_invoice				as ds_street_invoice
                ,ds_neighborhood_name_invoice		as ds_neighborhood_name_invoice		
                ,ds_city_name_invoice				as ds_city_name_invoice
                ,ini_state_invoice	 as ini_state_invoice			
                ,ds_state_name_invoice				as ds_state_name_invoice
                ,ds_country_name_invoice				as ds_country_name_invoice
                ,num_house_invoice as num_house_invoice			
                ,ds_complement_street_invoice			as ds_complement_street_invoice	
                ,ds_reference_invoice				as ds_reference_invoice
                ,ds_source_system_invoice			as ds_source_system_invoice		
                ,dth_create			
                ,dth_update	
                ,dth_create_system_origin
                ,dth_update_system_origin		
            FROM `db_dolphin_metric_clientmdm.tbl_client`
            WHERE dth_create BETWEEN '{last_runtime}' AND '{now}'
            OR dth_update BETWEEN '{last_runtime}' AND '{now}'
        """
        query_job = client.query(query)
        for result in query_job:
            try:
                client = bigtable.Client(project=self.project_id, admin=True)
                instance = client.instance(self.instance_id)
                table = instance.table(self.table_id)
                column_family_id = "goldenrecord"
                row_key = f"{message['num_document']}#{message['num_unique_client']}"
                row = table.direct_row(row_key)

                lista_campos = [
                    "num_unique_client",
                    "num_version_golden_record",
                    "ds_full_name",
                    "ds_first_name",
                    "ds_last_name",
                    "ds_name_source_system",
                    "num_document",
                    "hash_document",
                    "ds_document_type",
                    "ds_document_source_system",
                    "dth_birth",
                    "ds_birth_source_system",
                    "ini_gender",
                    "ds_gender",
                    "ds_gender_source_system",
                    "ini_marital_status",
                    "ds_marital_status",
                    "ds_marital_status_source_system",
                    "ds_nationality",
                    "ds_country_birth_name",
                    "ds_nationality_source_system",
                    "ind_status_client_csf_carrefour",
                    "ds_status_client_csf_carrefour",
                    "dt_admission_minhas_recompensas",
                    "ind_employee",
                    "ind_has_atacadao",
                    "ind_has_carrefour",
                    "ind_client_blocked",
                    "ind_portfolio",
                    "ind_bad_golden_record",
                    "ind_deleted",
                    "ds_channel_create_client",
                    "uuid_token_firebase",
                    "lst_client_origins",
                    "lst_reason_bad_golden_record",
                    "cod_consent_privacy_policy",
                    "dth_consent_privacy_policy",
                    "cod_consent_purchase_status",
                    "dth_consent_purchase_status",
                    "cod_consent_sharing_third",
                    "dth_consent_sharing_third",
                    "cod_source_general_consent",
                    "dth_create_general_consent",
                    "cod_consent_collection_point_chatbox",
                    "dth_consent_collection_point_chatbox",
                    "cod_consent_collection_point_csf",
                    "dth_consent_collection_point_csf",
                    "cod_consent_collection_point_website",
                    "dth_consent_collection_point_website",
                    "dth_create_collection_point",
                    "cod_source_collection_point",
                    "ds_personal_email",
                    "hash_personal_email",
                    "ds_source_system_personal_email",
                    "dth_create_personal_email",
                    "ind_bounce_personal_email",
                    "cod_consent_offer_personal_email",
                    "dth_consent_offer_personal_email",
                    "cod_source_offer_personal_email",
                    "dth_create_offer_personal_email",
                    "ds_business_email",
                    "hash_business_email",
                    "ds_source_system_business_email",
                    "dth_create_business_email",
                    "ind_bounce_business_email",
                    "cod_consent_offer_business_email",
                    "dth_consent_offer_business_email",
                    "cod_source_offer_business_email",
                    "dth_create_offer_business_email",
                    "num_mobile_phone",
                    "hash_mobile_phone",
                    "ds_source_system_mobile_phone",
                    "dth_create_mobile_phone",
                    "ind_bounce_mobile_phone",
                    "cod_consent_offer_sms_mobile_phone",
                    "dth_consent_offer_sms_mobile_phone",
                    "cod_consent_offer_whatsapp_mobile_phone",
                    "dth_consent_offer_whatsapp_mobile_phone",
                    "cod_source_mobile_phone_optin",
                    "dth_create_mobile_phone_optin",
                    "num_landline_phone",
                    "hash_landline_phone",
                    "ds_source_system_landline_phone",
                    "dth_create_landline_phone",
                    "ind_bounce_landline_phone",
                    "cod_consent_offer_sms_landline_phone",
                    "dth_consent_offer_sms_landline_phone",
                    "cod_consent_offer_whatsapp_landline_phone",
                    "dth_consent_offer_whatsapp_landline_phone",
                    "cod_source_landline_phone_optin",
                    "dth_create_landline_phone_optin",
                    "num_cep_residential",
                    "ds_street_residential",
                    "ds_neighborhood_name_residential",
                    "ds_city_name_residential",
                    "ini_state_residential",
                    "ds_state_name_residential",
                    "ds_country_name_residential",
                    "num_house_residential",
                    "ds_complement_street_residential",
                    "ds_reference_residential",
                    "ds_source_system_residential",
                    "num_cep_invoice",
                    "ds_street_invoice",
                    "ds_neighborhood_name_invoice",
                    "ds_city_name_invoice",
                    "ini_state_invoice",
                    "ds_state_name_invoice",
                    "ds_country_name_invoice",
                    "num_house_invoice",
                    "ds_complement_street_invoice",
                    "ds_reference_invoice",
                    "ds_source_system_invoice",
                    "dth_create",
                    "dth_update",
                    "dth_create_system_origin",
                    "dth_update_system_origin",
                ]
                for key in lista_campos:
                    if type(result[key]) in [int, bool]:
                        row.set_cell(column_family_id, key, result[key])
                    elif type(result[key]) in [list]:
                        row.set_cell(column_family_id, key, str(
                            result[key]).encode("utf-8"))
                    else:
                        row.set_cell(column_family_id, key,
                                     result[key].encode("utf-8"))

                row.commit()
                logging.info("Successfully wrote row {}.".format(row_key))
            except Exception as e:
                logging.info(
                    f"erro ao inserir registro: {result} e o erro foi: {e}")


def run(options, project_id, instance_id, table_id, bucket_name, input_subscription):
    with beam.Pipeline(options=options) as pipeline:
        result = (
            pipeline
            | "Read from PubSub"
            >> beam.io.ReadFromPubSub(
                subscription=input_subscription
            ).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
            | "Write to BigTable"
            >> beam.ParDo(
                WriteBigTable(
                    project_id=project_id,
                    instance_id=instance_id,
                    table_id=table_id,
                    bucket_name=bucket_name,
                )
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--project_id", required=True)
    parser.add_argument("--instance_id", required=True)
    parser.add_argument("--table_id", required=True)
    parser.add_argument("--bucket_name", required=True)
    parser.add_argument("--input_subscription", required=True)
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    pipeline_options.view_as(
        GoogleCloudOptions).project = known_args.project_id
    run(
        pipeline_options,
        known_args.project_id,
        known_args.instance_id,
        known_args.table_id,
        known_args.bucket_name,
        known_args.input_subscription,
    )
