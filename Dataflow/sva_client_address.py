import argparse
import datetime as dt
import json
import logging
from datetime import datetime
from typing import Any, Dict, List

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJsonClient(beam.DoFn):
    def process(self, row):
        try:
            zone = timezone("America/Sao_Paulo")
            if row["client"] != {} or row["client"] != "null":
                row_client = {
                    "idclient": row["client"]["clientid"]
                    if row["client"]["clientid"] not in ["", "null"]
                    else None,
                    "maritalstatus": row["client"]["maritalstatus"]
                    if row["client"]["maritalstatus"] not in ["", "null"]
                    else None,
                    "sellerdevice": row["client"]["sellerdevice"]
                    if row["client"]["sellerdevice"] not in ["", "null"]
                    else None,
                    "email": row["client"]["email"]
                    if row["client"]["email"] not in ["", "null"]
                    else None,
                    "nationality": row["client"]["nationality"]
                    if row["client"]["nationality"] not in ["", "null"]
                    else None,
                    "birth": row["client"]["birth"]
                    if row["client"]["birth"] not in ["", "null"]
                    else None,
                    "createorigin": row["client"]["createorigin"]
                    if row["client"]["createorigin"] not in ["", "null"]
                    else None,
                    "updateorigin": row["client"]["updateorigin"]
                    if row["client"]["updateorigin"] not in ["", "null"]
                    else None,
                    "emailinput": row["client"]["emailinput"]
                    if row["client"]["emailinput"] not in ["", "null"]
                    else None,
                    "indloyalty": row["client"]["indloyalty"]
                    if row["client"]["indloyalty"] not in ["", "null"]
                    else None,
                    "optinemail": row["client"]["optinemail"]
                    if row["client"]["optinemail"] not in ["", "null"]
                    else None,
                    "optinsms": row["client"]["optinsms"]
                    if row["client"]["optinsms"] not in ["", "null"]
                    else None,
                    "optinwhatsapp": row["client"]["optinwhatsapp"]
                    if row["client"]["optinwhatsapp"] not in ["", "null"]
                    else None,
                    "gender": row["client"]["gender"]
                    if row["client"]["gender"] not in ["", "null"]
                    else None,
                    "countrybirth": row["client"]["countrybirth"]
                    if row["client"]["countrybirth"] not in ["", "null"]
                    else None,
                    "namefull": row["client"]["namefull"]
                    if row["client"]["namefull"] not in ["", "null"]
                    else None,
                    "numdocument": row["client"]["numdocument"]
                    if row["client"]["numdocument"] not in ["", "null"]
                    else None,
                    "phone": row["client"]["phone"]
                    if row["client"]["phone"] not in ["", "null"]
                    else None,
                    "mobilefone": row["client"]["mobilefone"]
                    if row["client"]["mobilefone"] not in ["", "null"]
                    else None,
                    "statesubscription": row["client"]["statesubscription"]
                    if row["client"]["statesubscription"] not in ["", "null"]
                    else None,
                    "documenttype": row["client"]["documenttype"]
                    if row["client"]["documenttype"] not in ["", "null"]
                    else None,
                    "seller": row["client"]["seller"]
                    if row["client"]["seller"] not in ["", "null"]
                    else None,
                    "dth_inclusion": dt.datetime.now(zone),
                    "dth_partition": dt.datetime.now(zone)
                }

            yield row_client
        except:
            logging.info("Erro ao gravar payload address: {0}".format(row))


class ParseJsonAddress(beam.DoFn):
    def process(self, row):
        try:
            zone = timezone("America/Sao_Paulo")
            yield {
                "idclient": row["clientid"]
                if row["clientid"] not in ["", "null"]
                else None,
                "complement": row["complement"]
                if row["complement"] not in ["", "null"]
                else None,
                "reference": row["reference"]
                if row["reference"] not in ["", "null"]
                else None,
                "street": row["street"] if row["street"] not in ["", "null"] else None,
                "state": row["state"] if row["state"] not in ["", "null"] else None,
                "city": row["city"] if row["city"] not in ["", "null"] else None,
                "country": row["country"]
                if row["country"] not in ["", "null"]
                else None,
                "inddeliveryaddress": row["inddeliveryaddress"]
                if row["inddeliveryaddress"] not in ["", "null"]
                else None,
                "neighborhood": row["neighborhood"]
                if row["neighborhood"] not in ["", "null"]
                else None,
                "postalcode": row["postalcode"]
                if row["postalcode"] not in ["", "null"]
                else None,
                "addressnumber": row["addressnumber"]
                if row["addressnumber"] not in ["", "null"]
                else None,
                "addresstype": row["addresstype"]
                if row["addresstype"] not in ["", "null"]
                else None,
                "dth_inclusion": dt.datetime.now(zone),
                "dth_partition": dt.datetime.now(zone)
            }
        except:
            logging.info("Erro ao gravar payload address: {0}".format(row))


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        try:
            row = json.loads(message)
            yield beam.pvalue.TaggedOutput("process", row)
        except:
            logging.info(f"Error while parsing the message: {message}")


def get_schema_client():
    schema = json.dumps(
        [
            {"name": "idclient", "type": "STRING"},
            {"name": "maritalstatus", "type": "STRING"},
            {"name": "sellerdevice", "type": "STRING"},
            {"name": "email", "type": "STRING"},
            {"name": "nationality", "type": "STRING"},
            {"name": "birth", "type": "STRING"},
            {"name": "createorigin", "type": "STRING"},
            {"name": "updateorigin", "type": "STRING"},
            {"name": "emailinput", "type": "STRING"},
            {"name": "indloyalty", "type": "STRING"},
            {"name": "optinemail", "type": "STRING"},
            {"name": "optinsms", "type": "STRING"},
            {"name": "optinwhatsapp", "type": "STRING"},
            {"name": "gender", "type": "STRING"},
            {"name": "countrybirth", "type": "STRING"},
            {"name": "namefull", "type": "STRING"},
            {"name": "numdocument", "type": "STRING"},
            {"name": "phone", "type": "STRING"},
            {"name": "mobilefone", "type": "STRING"},
            {"name": "statesubscription", "type": "STRING"},
            {"name": "documenttype", "type": "STRING"},
            {"name": "seller", "type": "STRING"},
            {"name": "dth_inclusion", "type": "TIMESTAMP"},
            {"name": "dth_partition", "type": "TIMESTAMP"}
        ]
    )

    return schema


def get_schema_address():
    schema = json.dumps(
        [
            {"name": "idclient", "type": "STRING"},
            {"name": "complement", "type": "STRING"},
            {"name": "reference", "type": "STRING"},
            {"name": "street", "type": "STRING"},
            {"name": "createorigin", "type": "STRING"},
            {"name": "state", "type": "STRING"},
            {"name": "city", "type": "STRING"},
            {"name": "country", "type": "STRING"},
            {"name": "inddeliveryaddress", "type": "STRING"},
            {"name": "neighborhood", "type": "STRING"},
            {"name": "postalcode", "type": "STRING"},
            {"name": "addressnumber", "type": "STRING"},
            {"name": "addresstype", "type": "STRING"},
            {"name": "dth_inclusion", "type": "TIMESTAMP"},
            {"name": "dth_partition", "type": "TIMESTAMP"}
        ]
    )

    return schema


def parse_json(message):
    json_message = json.loads(message)
    return json_message


def run(
    options, input_subscription, output_table_client, output_table_address, output_path
):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | "Read from PubSub"
            >> beam.io.ReadFromPubSub(
                subscription=input_subscription
            ).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (
            rows | "Data Guard verify" >> beam.ParDo(
                DataGuardVerify()).with_outputs()
        )

        (
            verify.process
            | "Parse Client" >> beam.ParDo(ParseJsonClient())
            | "Write to BigQuery table Client"
            >> beam.io.WriteToBigQuery(
                output_table_client,
                create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                schema=get_schema_client(),
            )
        )

        (
            verify.process
            | "Address" >> beam.Map(lambda elements: elements["address"])
            | "FlatMap" >> beam.FlatMap(lambda elements: elements)
            | "Parse Address" >> beam.ParDo(ParseJsonAddress())
            | "Write to BigQuery table Address"
            >> beam.io.WriteToBigQuery(
                output_table_address,
                create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                schema=get_schema_address(),
            )
        )

        (
            verify.process
            | "Writing .json to bucket"
            >> beam.ParDo(WriteElementsToGCS(output_path=output_path, origem="sva"))
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_subscription",
        required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".',
    )
    parser.add_argument(
        "--output_table_client",
        required=True,
        help="Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.",
    )
    parser.add_argument(
        "--output_table_address",
        required=True,
        help="Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.",
    )
    parser.add_argument(
        "--output_path",
        required=True,
        help="Output Google Cloud Storage specified as: gs//<BUCKET>.",
    )
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(
        pipeline_options,
        known_args.input_subscription,
        known_args.output_table_client,
        known_args.output_table_address,
        known_args.output_path,
    )
