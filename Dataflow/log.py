import argparse
import json
import logging
import os
from datetime import datetime, timedelta, timezone
from subprocess import call, getoutput

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from google.cloud import bigquery, storage


class MontaAtributos(beam.DoFn):
    def __init__(self, env, input_queries_bucket):
        self.env = env
        self.input_queries_bucket = input_queries_bucket

    def process(self, origem):
        print('start')
        from google.cloud import bigquery, storage

        logging.info("Job started")
        self.origem = origem
        read_storage_client = storage.Client()
        bucket = read_storage_client.get_bucket(self.input_queries_bucket)
        query_atributos = bucket.get_blob(
            "dags/queries/log/query_atributos.sql")
        query_source_systems = bucket.get_blob(
            "dags/queries/log/query_source_systems.sql"
        )
        resultado_atributos_query = (
            query_atributos.download_as_string()
            .decode("utf-8")
            .replace("ENVIRONMENT", self.env)
        )
        source_systems_query = (
            query_source_systems.download_as_string()
            .decode("utf-8")
            .replace("ENVIRONMENT", self.env)
        )

        client_bq = bigquery.Client()
        resultado_atributos = client_bq.query(resultado_atributos_query)
        source_systems = client_bq.query(source_systems_query)

        atributos = {}
        lista_tbl_client_personal_information = []
        lista_tbl_client_email = []
        lista_tbl_client_address = []
        lista_tbl_client_phone = []
        lista_tbl_client_phone_optin = []
        lista_tbl_client_email_optin = []
        lista_tbl_client_general_consent = []
        lista_tbl_client_collection_point = []
        lista_tbl_client_vtex = []
        lista_tbl_client_rh = []
        lista_tbl_client_sva = []
        lista_tbl_client_neoassist = []
        lista_tbl_client_propz = []
        campos = {}
        campos_tbl_client_personal_information_tmp = {}
        campos_tbl_client_email_tmp = {}
        campos_tbl_client_address_tmp = {}
        campos_tbl_client_phone_tmp = {}
        campos_tbl_client_phone_optin_tmp = {}
        campos_tbl_client_email_optin_tmp = {}
        campos_tbl_client_general_consent_tmp = {}
        campos_tbl_client_collection_point_tmp = {}
        campos_tbl_client_vtex_tmp = {}
        campos_tbl_client_rh_tmp = {}
        campos_tbl_client_sva_tmp = {}
        campos_tbl_client_neoassist_tmp = {}
        campos_tbl_client_propz_tmp = {}
        origens = {}
        for atributo in resultado_atributos:
            if (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_personal_information`"
            ):
                lista_tbl_client_personal_information.append(
                    atributo.nm_attribute_name)
                campos_tbl_client_personal_information_tmp[
                    atributo.nm_attribute_name
                ] = atributo.id_attribute_golden_record
                campos[
                    "tbl_client_personal_information"
                ] = campos_tbl_client_personal_information_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_email`"
            ):
                lista_tbl_client_email.append(atributo.nm_attribute_name)
                campos_tbl_client_email_tmp[
                    atributo.nm_attribute_name
                ] = atributo.id_attribute_golden_record
                campos["tbl_client_email"] = campos_tbl_client_email_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_address`"
            ):
                if len(lista_tbl_client_address) == 1:
                    lista_tbl_client_address.append(
                        "num_version_golden_record")
                    lista_tbl_client_address.append(atributo.nm_attribute_name)
                    campos_tbl_client_address_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_address"] = campos_tbl_client_address_tmp
                else:
                    lista_tbl_client_address.append(atributo.nm_attribute_name)
                    campos_tbl_client_address_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_address"] = campos_tbl_client_address_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_phone`"
            ):
                if len(lista_tbl_client_phone) == 1:
                    lista_tbl_client_phone.append("num_version_golden_record")
                    lista_tbl_client_phone.append(atributo.nm_attribute_name)
                    campos_tbl_client_phone_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_phone"] = campos_tbl_client_phone_tmp
                else:
                    lista_tbl_client_phone.append(atributo.nm_attribute_name)
                    campos_tbl_client_phone_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_phone"] = campos_tbl_client_phone_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_phone_optin`"
            ):
                if len(lista_tbl_client_phone_optin) == 1:
                    lista_tbl_client_phone_optin.append(
                        "num_version_golden_record")
                    lista_tbl_client_phone_optin.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_phone_optin_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_phone_optin"] = campos_tbl_client_phone_optin_tmp
                else:
                    lista_tbl_client_phone_optin.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_phone_optin_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_phone_optin"] = campos_tbl_client_phone_optin_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_email_optin`"
            ):
                if len(lista_tbl_client_email_optin) == 1:
                    lista_tbl_client_email_optin.append(
                        "num_version_golden_record")
                    lista_tbl_client_email_optin.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_email_optin_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_email_optin"] = campos_tbl_client_email_optin_tmp
                else:
                    lista_tbl_client_email_optin.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_email_optin_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_email_optin"] = campos_tbl_client_email_optin_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_general_consent`"
            ):
                if len(lista_tbl_client_general_consent) == 1:
                    lista_tbl_client_general_consent.append(
                        "num_version_golden_record")
                    lista_tbl_client_general_consent.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_general_consent_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos[
                        "tbl_client_general_consent"
                    ] = campos_tbl_client_general_consent_tmp
                else:
                    lista_tbl_client_general_consent.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_general_consent_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos[
                        "tbl_client_general_consent"
                    ] = campos_tbl_client_general_consent_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_collection_point`"
            ):
                if len(lista_tbl_client_collection_point) == 1:
                    lista_tbl_client_collection_point.append(
                        "num_version_golden_record"
                    )
                    lista_tbl_client_collection_point.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_collection_point_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos[
                        "tbl_client_collection_point"
                    ] = campos_tbl_client_collection_point_tmp
                else:
                    lista_tbl_client_collection_point.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_collection_point_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos[
                        "tbl_client_collection_point"
                    ] = campos_tbl_client_collection_point_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_vtex`"
            ):
                if len(lista_tbl_client_vtex) == 1:
                    lista_tbl_client_vtex.append("num_version_golden_record")
                    lista_tbl_client_vtex.append(atributo.nm_attribute_name)
                    campos_tbl_client_vtex_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_vtex"] = campos_tbl_client_vtex_tmp
                else:
                    lista_tbl_client_vtex.append(atributo.nm_attribute_name)
                    campos_tbl_client_vtex_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_vtex"] = campos_tbl_client_vtex_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_rh`"
            ):
                if len(lista_tbl_client_rh) == 1:
                    lista_tbl_client_rh.append("num_version_golden_record")
                    lista_tbl_client_rh.append(atributo.nm_attribute_name)
                    campos_tbl_client_rh_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_rh"] = campos_tbl_client_rh_tmp
                else:
                    lista_tbl_client_rh.append(atributo.nm_attribute_name)
                    campos_tbl_client_rh_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_rh"] = campos_tbl_client_rh_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_sva`"
            ):
                if len(lista_tbl_client_sva) == 1:
                    lista_tbl_client_sva.append("num_version_golden_record")
                    lista_tbl_client_sva.append(atributo.nm_attribute_name)
                    campos_tbl_client_sva_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_sva"] = campos_tbl_client_sva_tmp
                else:
                    lista_tbl_client_sva.append(atributo.nm_attribute_name)
                    campos_tbl_client_sva_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_sva"] = campos_tbl_client_sva_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_neoassist`"
            ):
                if len(lista_tbl_client_neoassist) == 1:
                    lista_tbl_client_neoassist.append(
                        "num_version_golden_record")
                    lista_tbl_client_neoassist.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_neoassist_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_neoassist"] = campos_tbl_client_neoassist_tmp
                else:
                    lista_tbl_client_neoassist.append(
                        atributo.nm_attribute_name)
                    campos_tbl_client_neoassist_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_neoassist"] = campos_tbl_client_neoassist_tmp
            elif (
                atributo.nm_table_name
                == f"`br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_propz`"
            ):
                if len(lista_tbl_client_propz) == 1:
                    lista_tbl_client_propz.append("num_version_golden_record")
                    lista_tbl_client_propz.append(atributo.nm_attribute_name)
                    campos_tbl_client_propz_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_propz"] = campos_tbl_client_propz_tmp
                else:
                    lista_tbl_client_propz.append(atributo.nm_attribute_name)
                    campos_tbl_client_propz_tmp[
                        atributo.nm_attribute_name
                    ] = atributo.id_attribute_golden_record
                    campos["tbl_client_propz"] = campos_tbl_client_propz_tmp

        atributos[
            "tbl_client_personal_information"
        ] = lista_tbl_client_personal_information
        atributos["tbl_client_email"] = lista_tbl_client_email
        atributos["tbl_client_address"] = lista_tbl_client_address
        atributos["tbl_client_phone"] = lista_tbl_client_phone
        atributos["tbl_client_phone_optin"] = lista_tbl_client_phone_optin
        atributos["tbl_client_email_optin"] = lista_tbl_client_email_optin
        atributos["tbl_client_general_consent"] = lista_tbl_client_general_consent
        atributos["tbl_client_collection_point"] = lista_tbl_client_collection_point
        atributos["tbl_client_vtex"] = lista_tbl_client_vtex
        atributos["tbl_client_rh"] = lista_tbl_client_rh
        atributos["tbl_client_sva"] = lista_tbl_client_sva
        atributos["tbl_client_neoassist"] = lista_tbl_client_neoassist
        atributos["tbl_client_propz"] = lista_tbl_client_propz

        for origem in source_systems:
            origens[origem.ini_source_information] = origem.id_source_information
        origens["SVA"] = origens.pop("SVA+")
        origens["AUTOMATICO"] = 0

        lista_retorno = []
        for tabela in list(atributos):
            retorno = {}
            retorno[tabela] = {
                "atributos": atributos,
                "origens": origens,
                "campos": campos,
            }
            lista_retorno.append(retorno)
        yield lista_retorno


class MontaQuery(beam.DoFn):
    def __init__(self, env, input_queries_bucket):
        self.env = env
        self.input_queries_bucket = input_queries_bucket

    def process(self, atributos):
        from google.cloud import storage

        self.atributos = atributos
        read_storage_client = storage.Client()
        tabela = list(atributos)[0]
        bucket = read_storage_client.get_bucket(self.input_queries_bucket)
        query_incluir = bucket.get_blob(
            f"dags/queries/log/{tabela}_query_incluir.sql")
        query_incluir_data = (
            query_incluir.download_as_string()
            .decode("utf-8")
            .replace("ENVIRONMENT", self.env)
        )
        query_alterar = bucket.get_blob(
            f"dags/queries/log/{tabela}_query_alterar.sql")
        query_alterar_data = (
            query_alterar.download_as_string()
            .decode("utf-8")
            .replace("ENVIRONMENT", self.env)
        )
        atributos["incluir"] = query_incluir_data
        atributos["alterar"] = query_alterar_data

        yield atributos


class GravaLog(beam.DoFn):
    def __init__(self, env):
        self.env = env
        self.ds_name_source_system = [
            "ds_full_name", "ds_first_name", "ds_last_name"]
        self.ds_document_source_system = [
            "num_document",
            "hash_document",
            "ds_document_type",
        ]
        self.ds_birth_source_system = ["dth_birth"]
        self.ds_gender_source_system = ["ini_gender", "ds_gender"]
        self.ds_marital_status_source_system = [
            "ini_marital_status",
            "ds_marital_status",
        ]
        self.ds_nationality_source_system = [
            "ds_nationality", "ds_country_birth_name"]
        self.csf = [
            "ind_status_client_csf_carrefour",
            "ds_status_client_csf_carrefour",
            "ind_has_atacadao",
            "ind_has_carrefour",
            "ind_portfolio",
            "ds_channel_create_client",
        ]
        self.minhas_recompensas = ["dt_admission_minhas_recompensas"]
        self.rh = ["ind_employee"]
        self.device_id = ["uuid_token_firebase"]

    def monta_dict_alteracao(self):
        if self.tabela == "tbl_client_personal_information":
            contadores_invalidos = [0, 1, 36, 37]
        elif self.tabela == "tbl_client_email":
            contadores_invalidos = [0, 1, 7, 8]
        elif self.tabela == "tbl_client_address":
            contadores_invalidos = [0, 1, 14, 15]
        elif self.tabela == "tbl_client_phone":
            contadores_invalidos = [0, 1, 7, 8]
        elif self.tabela == "tbl_client_phone_optin":
            contadores_invalidos = [0, 1, 8, 9]
        elif self.tabela == "tbl_client_email_optin":
            contadores_invalidos = [0, 1, 6, 7]
        elif self.tabela == "tbl_client_general_consent":
            contadores_invalidos = [0, 1, 9, 10]
        elif self.tabela == "tbl_client_collection_point":
            contadores_invalidos = [0, 1, 9, 10]
        elif self.tabela == "tbl_client_vtex":
            contadores_invalidos = [0, 1, 7, 8]
        elif self.tabela == "tbl_client_rh":
            contadores_invalidos = [0, 1, 15, 16]
        elif self.tabela == "tbl_client_sva":
            contadores_invalidos = [0, 1, 6, 7]
        elif self.tabela == "tbl_client_neoassist":
            contadores_invalidos = [0, 1, 8, 9]
        elif self.tabela == "tbl_client_propz":
            contadores_invalidos = [0, 1, 10, 11]
        client = bigquery.Client()
        query_job = client.query(self.query_alterar)
        lista = []
        for row in query_job:
            contador = 0
            for resultado in row:
                if (resultado is not None) and (contador not in contadores_invalidos):
                    if type(resultado) == list:
                        resultado = str(resultado)
                    resultado_split = resultado.split("|")
                    if len(resultado_split) > 1:
                        if resultado_split[0] != resultado_split[1]:
                            inserir = {}
                            if self.tabela == "tbl_client_personal_information":
                                if (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_name_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_name_source_system.split("|")[0]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_document_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_document_source_system.split("|")[
                                            0]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_birth_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_birth_source_system.split("|")[
                                            0]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_gender_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_gender_source_system.split("|")[
                                            0]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_marital_status_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_marital_status_source_system.split("|")[
                                            0
                                        ]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_nationality_source_system
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[
                                        row.ds_nationality_source_system.split("|")[
                                            0]
                                    ]
                                elif self.atributos[self.tabela][contador] in self.csf:
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens["CSF"]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.minhas_recompensas
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens["LOYALTY"]
                                elif self.atributos[self.tabela][contador] in self.rh:
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens["RH"]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.device_id
                                ):
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens["DEVICE ID"]
                                else:
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens["AUTOMATICO"]
                            elif self.tabela in [
                                "tbl_client_email",
                                "tbl_client_address",
                                "tbl_client_phone",
                            ]:
                                if row.ds_source_system is not None:
                                    inserir[
                                        "id_current_source_information"
                                    ] = self.origens[row.ds_source_system.split("|")[0]]
                                else:
                                    inserir["id_current_source_information"] = 0
                            elif self.tabela in [
                                "tbl_client_phone_optin",
                                "tbl_client_email_optin",
                                "tbl_client_general_consent",
                                "tbl_client_collection_point",
                            ]:
                                inserir["id_current_source_information"] = self.origens[
                                    "ONETRUST"
                                ]
                            elif self.tabela == "tbl_client_vtex":
                                inserir["id_current_source_information"] = self.origens[
                                    "VTEX"
                                ]
                            elif self.tabela == "tbl_client_rh":
                                inserir["id_current_source_information"] = self.origens[
                                    "RH"
                                ]
                            elif self.tabela == "tbl_client_sva":
                                inserir["id_current_source_information"] = self.origens[
                                    "SVA"
                                ]
                            elif self.tabela == "tbl_client_neoassist":
                                inserir["id_current_source_information"] = self.origens[
                                    "NEOASSIST"
                                ]
                            elif self.tabela == "tbl_client_propz":
                                inserir["id_current_source_information"] = self.origens[
                                    "PROPZ"
                                ]

                            inserir["num_unique_client"] = row.num_unique_client

                            inserir[
                                "num_version_golden_record"
                            ] = row.num_version_golden_record

                            inserir["id_attribute_golden_record"] = self.campos[
                                self.tabela
                            ][self.atributos[self.tabela][contador]]

                            if self.tabela == "tbl_client_personal_information":
                                if (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_name_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_name_source_system.split("|")[1]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_document_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_document_source_system.split("|")[
                                            1]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_birth_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_birth_source_system.split("|")[
                                            1]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_gender_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_gender_source_system.split("|")[
                                            1]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_marital_status_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_marital_status_source_system.split("|")[
                                            1
                                        ]
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.ds_nationality_source_system
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_nationality_source_system.split("|")[
                                            1]
                                    ]
                                elif self.atributos[self.tabela][contador] in self.csf:
                                    inserir["id_old_source_information"] = self.origens[
                                        "CSF"
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.minhas_recompensas
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        "LOYALTY"
                                    ]
                                elif self.atributos[self.tabela][contador] in self.rh:
                                    inserir["id_old_source_information"] = self.origens[
                                        "RH"
                                    ]
                                elif (
                                    self.atributos[self.tabela][contador]
                                    in self.device_id
                                ):
                                    inserir["id_old_source_information"] = self.origens[
                                        "DEVICE ID"
                                    ]
                                else:
                                    inserir["id_old_source_information"] = self.origens[
                                        "AUTOMATICO"
                                    ]
                            elif self.tabela in [
                                "tbl_client_email",
                                "tbl_client_address",
                                "tbl_client_phone",
                            ]:
                                if row.ds_source_system is not None:
                                    inserir["id_old_source_information"] = self.origens[
                                        row.ds_source_system.split("|")[1]
                                    ]
                                else:
                                    inserir["id_old_source_information"] = 0
                            elif self.tabela in [
                                "tbl_client_phone_optin",
                                "tbl_client_email_optin",
                                "tbl_client_general_consent",
                                "tbl_client_collection_point",
                            ]:
                                inserir["id_old_source_information"] = self.origens[
                                    "ONETRUST"
                                ]
                            elif self.tabela == "tbl_client_vtex":
                                inserir["id_old_source_information"] = self.origens[
                                    "VTEX"
                                ]
                            elif self.tabela == "tbl_client_rh":
                                inserir["id_old_source_information"] = self.origens[
                                    "RH"
                                ]
                            elif self.tabela == "tbl_client_sva":
                                inserir["id_old_source_information"] = self.origens[
                                    "SVA"
                                ]
                            elif self.tabela == "tbl_client_neoassist":
                                inserir["id_old_source_information"] = self.origens[
                                    "NEOASSIST"
                                ]
                            elif self.tabela == "tbl_client_propz":
                                inserir["id_old_source_information"] = self.origens[
                                    "PROPZ"
                                ]

                            inserir["ds_operarion"] = "A"

                            if type(resultado_split[0]) == datetime:
                                inserir["ds_current_value"] = resultado_split[0][:-3]
                            elif type(resultado) == bytes:
                                inserir["ds_current_value"] = resultado_split[0].decode(
                                    "utf-8"
                                )
                            else:
                                inserir["ds_current_value"] = str(
                                    resultado_split[0])

                            if resultado_split[1] == "1900-01-01 00:00:00+00":
                                inserir["ds_old_value"] = None
                            else:
                                inserir["ds_old_value"] = resultado_split[1]

                            inserir["dth_create"] = row.dth_create.split("|")[
                                0][:-3]

                            inserir["dth_update"] = datetime.now(timezone.utc).strftime(
                                "%Y-%m-%d %H:%M:%S"
                            )

                            lista.append(inserir)
                if len(lista) > 10000:
                    client = bigquery.Client()
                    errors = client.insert_rows_json(
                        f"br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_log_golden_record",
                        lista,
                    )
                    if errors == []:
                        print("New rows have been added.")
                    else:
                        print(
                            "Encountered errors while inserting rows: {}".format(
                                errors)
                        )
                    lista = []

                contador += 1
        if len(lista) > 0:
            client = bigquery.Client()
            errors = client.insert_rows_json(
                f"br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_log_golden_record",
                lista,
            )
            if errors == []:
                print("New rows have been added.")
            else:
                print("Encountered errors while inserting rows: {}".format(errors))
        return lista

    def monta_dict_inclusao(self):
        client = bigquery.Client()
        query_job = client.query(self.query_incluir)
        lista = []
        for row in query_job:
            contador = 0
            for resultado in row:
                inserir = {}
                if (
                    self.tabela
                    in [
                        "tbl_client_address",
                        "tbl_client_phone",
                        "tbl_client_phone_optin",
                        "tbl_client_email_optin",
                        "tbl_client_general_consent",
                        "tbl_client_collection_point",
                        "tbl_client_vtex",
                        "tbl_client_sva",
                        "tbl_client_neoassist",
                        "tbl_client_propz",
                    ]
                    and self.atributos[self.tabela][contador]
                    == "num_version_golden_record"
                ):
                    contador += 1
                else:
                    if self.tabela == "tbl_client_personal_information":
                        if (
                            self.atributos[self.tabela][contador]
                            in self.ds_name_source_system
                        ):
                            if row.ds_name_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_name_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.ds_document_source_system
                        ):
                            if row.ds_document_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_document_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.ds_birth_source_system
                        ):
                            if row.ds_birth_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_birth_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.ds_gender_source_system
                        ):
                            if row.ds_gender_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_gender_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.ds_marital_status_source_system
                        ):
                            if row.ds_marital_status_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_marital_status_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.ds_nationality_source_system
                        ):
                            if row.ds_nationality_source_system is not None:
                                inserir["id_current_source_information"] = self.origens[
                                    row.ds_nationality_source_system
                                ]
                            else:
                                inserir["id_current_source_information"] = 11
                        elif self.atributos[self.tabela][contador] in self.csf:
                            if resultado is None:
                                inserir["id_current_source_information"] = 11
                            else:
                                inserir["id_current_source_information"] = self.origens[
                                    "CSF"
                                ]
                        elif (
                            self.atributos[self.tabela][contador]
                            in self.minhas_recompensas
                        ):
                            if resultado is None:
                                inserir["id_current_source_information"] = self.origens[
                                    "LOYALTY"
                                ]
                            else:
                                inserir["id_current_source_information"] = 10
                        elif self.atributos[self.tabela][contador] in self.rh:
                            if resultado is None:
                                inserir["id_current_source_information"] = 11
                            else:
                                inserir["id_current_source_information"] = self.origens[
                                    "RH"
                                ]
                        elif self.atributos[self.tabela][contador] in self.device_id:
                            if resultado is None:
                                inserir["id_current_source_information"] = 11
                            else:
                                inserir["id_current_source_information"] = self.origens[
                                    "DEVICE ID"
                                ]
                        else:
                            if resultado is None:
                                inserir["id_current_source_information"] = 11
                            else:
                                inserir["id_current_source_information"] = self.origens[
                                    "AUTOMATICO"
                                ]

                    elif self.tabela in [
                        "tbl_client_email",
                        "tbl_client_address",
                        "tbl_client_phone",
                    ]:
                        if row.ds_source_system is not None:
                            inserir["id_current_source_information"] = self.origens[
                                row.ds_source_system
                            ]
                        else:
                            inserir["id_current_source_information"] = 0
                    elif self.tabela in [
                        "tbl_client_phone_optin",
                        "tbl_client_email_optin",
                        "tbl_client_general_consent",
                        "tbl_client_collection_point",
                    ]:
                        inserir["id_current_source_information"] = self.origens[
                            "ONETRUST"
                        ]
                    elif self.tabela == "tbl_client_vtex":
                        inserir["id_current_source_information"] = self.origens["VTEX"]
                    elif self.tabela == "tbl_client_rh":
                        inserir["id_current_source_information"] = self.origens["RH"]
                    elif self.tabela == "tbl_client_sva":
                        inserir["id_current_source_information"] = self.origens["SVA"]
                    elif self.tabela == "tbl_client_neoassist":
                        inserir["id_current_source_information"] = self.origens[
                            "NEOASSIST"
                        ]
                    elif self.tabela == "tbl_client_propz":
                        inserir["id_current_source_information"] = self.origens["PROPZ"]

                    inserir["num_unique_client"] = row.num_unique_client

                    inserir["num_version_golden_record"] = row.num_version_golden_record

                    inserir["id_attribute_golden_record"] = self.campos[self.tabela][
                        self.atributos[self.tabela][contador]
                    ]

                    inserir["id_old_source_information"] = 11

                    inserir["ds_operarion"] = "I"

                    if type(resultado) == datetime:
                        inserir["ds_current_value"] = resultado.strftime(
                            "%Y-%m-%d %H:%M:%S"
                        )
                    elif type(resultado) == bytes:
                        inserir["ds_current_value"] = resultado.decode("utf-8")
                    elif resultado is None:
                        inserir["ds_current_value"] = None
                    else:
                        inserir["ds_current_value"] = str(resultado)

                    inserir["ds_old_value"] = None

                    inserir["dth_create"] = row.dth_create.strftime(
                        "%Y-%m-%d %H:%M:%S")

                    if row.dth_update is not None:
                        inserir["dth_update"] = row.dth_update.strftime(
                            "%Y-%m-%d %H:%M:%S"
                        )
                    else:
                        inserir["dth_update"] = None

                    lista.append(inserir)

                    contador += 1
            if len(lista) > 10000:
                client = bigquery.Client()
                errors = client.insert_rows_json(
                    f"br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_log_golden_record",
                    lista,
                )
                if errors == []:
                    print("New rows have been added.")
                else:
                    print("Encountered errors while inserting rows: {}".format(errors))
                lista = []
        if len(lista) > 0:
            client = bigquery.Client()
            errors = client.insert_rows_json(
                f"br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_log_golden_record",
                lista,
            )
            if errors == []:
                print("New rows have been added.")
            else:
                print("Encountered errors while inserting rows: {}".format(errors))
        return lista

    # def get_updated_nuc(self):
    #     now = datetime.now(timezone.utc).strftime("%Y-%m-%d %H:%M:%S")
    #     get_updated = f"""
    #         SELECT
    #             DISTINCT num_unique_client
    #         FROM `br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_log_golden_record`
    #         WHERE dth_update BETWEEN '{self.last_execution_date}' and '{now}'
    #         """
    #     client = bigquery.Client()
    #     resultado = client.query(get_updated).result()
    #     lista_nuc = []
    #     for nuc in resultado:
    #         lista_nuc.append(nuc)
    #     if len(lista_nuc) == 0:
    #         nuc_consulta = ''
    #     else:
    #         nuc_consulta = str(lista_nuc)[1:-1]
    #     return nuc_consulta

    # def altera_versao(self):
    #     nuc = self.get_updated_nuc()
    #     try:
    #         if nuc != '':
    #             query_update = f"""
    #             UPDATE `br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_personal_information`
    #             SET num_version_golden_record = num_version_golden_record +1
    #             WHERE num_unique_client in ({nuc});

    #             UPDATE `br-apps-bi-customermdm-{self.env}.db_dolphin_target_clientmdm.tbl_client_email`
    #             SET num_version_golden_record = num_version_golden_record +1
    #             WHERE num_unique_client in ({nuc});"""
    #             print(query_update)
    #             client = bigquery.Client()
    #             client.query(query_update).result()
    #         return
    #     except:
    #         logging.info(f'Error while updating num version golden record')

    def process(self, payload):
        self.tabela = list(payload)[0]
        self.query_alterar = payload["alterar"]
        self.query_incluir = payload["incluir"]
        payload_json = payload[self.tabela]
        self.origens = payload_json["origens"]
        self.atributos = payload_json["atributos"]
        self.campos = payload_json["campos"]
        print(self.tabela)
        # self.monta_dict_alteracao()
        self.monta_dict_inclusao()

        # if self.tabela == 'tbl_client_propz':
        #     self.altera_versao()
        return self.tabela


def run(options, env, input_subscription, input_queries_bucket):
    with beam.Pipeline(options=options) as pipeline:
        rows = (
            pipeline
            | "Lista de origens" >> beam.Create(['0'])
            # | "Read from PubSub"
            # >> beam.io.ReadFromPubSub(
            #     subscription=input_subscription
            # ).with_output_types(bytes)
            # | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
            | "Monta atributos"
            >> beam.ParDo(
                MontaAtributos(
                    env=env, input_queries_bucket=input_queries_bucket)
            )
            | "FlatMap" >> beam.FlatMap(lambda elements: elements)
            | "Monta queries"
            >> beam.ParDo(
                MontaQuery(env=env, input_queries_bucket=input_queries_bucket)
            )
            | "Grava log" >> beam.ParDo(GravaLog(env=env))
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--env", required=True,
                        help="environemnt (dev, qa, prd)")
    # parser.add_argument(
    #     '--last_execution_date', required=False,
    #     help='DAG last run time')
    parser.add_argument("--input_subscription", required=False)
    parser.add_argument("--input_queries_bucket", required=False)
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(
        pipeline_options,
        known_args.env,
        known_args.input_subscription,
        known_args.input_queries_bucket,
    )
