import argparse
import logging

import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions


class TopicVerify(beam.DoFn):
    def process(self, message):
        import apache_beam as beam

        try:
            atributo = message.attributes["eventType"]
            row = message.data
            if atributo == "DELETE":
                yield beam.pvalue.TaggedOutput("exclusao", row)
            else:
                yield beam.pvalue.TaggedOutput("process", row)
        except Exception as e:
            logging.info(f"Error while parsing the message: {message} and error: {e}")


def run(options, input_subscription, output_topic_exclusao, output_topic_vtex):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | "Read from PubSub"
            >> beam.io.ReadFromPubSub(
                subscription=input_subscription, with_attributes=True
            )
            | "Topic verify" >> beam.ParDo(TopicVerify()).with_outputs()
        )

        (
            rows.exclusao
            | "Publica no topico do pubsub de exclusão"
            >> beam.io.WriteToPubSub(topic=output_topic_exclusao)
        )

        (
            rows.process
            | "Publica no topico do pubsub do vtex"
            >> beam.io.WriteToPubSub(topic=output_topic_vtex)
        )


if __name__ == "__main__":
    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="C:\\Users\\adriano.silva\\Downloads\\br-apps-bi-customermdm-prd-55e09f553eab.json"
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_subscription",
        required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".',
    )
    parser.add_argument(
        "--output_topic_exclusao",
        required=True,
        help='Input PubSub topic of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>',
    )
    parser.add_argument(
        "--output_topic_vtex",
        required=True,
        help='Input PubSub topic of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>',
    )
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(
        pipeline_options,
        known_args.input_subscription,
        known_args.output_topic_exclusao,
        known_args.output_topic_vtex,
    )
