import argparse
import datetime as dt
import json
import logging
import os
from typing import Any, Dict, List

import apache_beam as beam
import xmltodict
from apache_beam.options.pipeline_options import PipelineOptions, SetupOptions
from apitools.base.py.exceptions import HttpForbiddenError
from pytz import timezone


class WriteElementsToGCS(beam.DoFn):
    def __init__(self, output_path, origem):
        self.output_path = output_path
        self.origem = origem

    def process(self, element):
        row = json.dumps(element)
        logging.info("Begin Writing to GCS")
        zone = timezone('America/Sao_Paulo')
        data_atual = dt.datetime.now(zone)
        milissegundos = int(data_atual.timestamp() * 1000)
        dia_atual = dt.datetime.now(zone).strftime('%Y%m%d')
        filename = f'gs://{self.output_path}/{self.origem}/{dia_atual}/{self.origem}_{milissegundos}.json'

        try:
            with beam.io.gcp.gcsio.GcsIO().open(filename=filename, mode="w") as f:
                f.write(row.encode('utf-8'))

            logging.info('Gravado com sucesso em: {0}'.format(filename))
        except HttpForbiddenError as e:
            logging.info(f'File name already exists: gs://{filename}')
        except Exception as e:
            logging.warn("Error Writing: " + str(e))

        return row


class ParseJson(beam.DoFn):
    def process(self, row):
        zone = timezone("America/Sao_Paulo")
        facets0 = {}
        facets1 = {}
        if "Facets" in row.keys():
            if len(row["Facets"]) > 1:
                facets0 = row["Facets"][0]
                facets1 = row["Facets"][1]
            else:
                facets0 = row["Facets"][0]

        market_permissions0 = {}
        market_permissions1 = {}
        if "MarketingPermissions" in row.keys():
            if len(row["MarketingPermissions"]) > 1:
                market_permissions0 = row["MarketingPermissions"][0]
                market_permissions1 = row["MarketingPermissions"][1]
            else:
                market_permissions0 = row["MarketingPermissions"][0]

        yield {
            "emailaddress": row["EMailAddress"] if "EMailAddress" in row.keys() else None,
            "emailoptin": row["EMailOptIn"] if "EMailOptIn" in row.keys() else None,
            "facets_0_id": facets0["Id"] if facets0 != {} else None,
            "facets_0_idorigin": facets0["IdOrigin"] if facets0 != {} else None,
            "facets_0_timestamp": facets0["Timestamp"] if facets0 != {} else None,
            "facets_1_id": facets1["Id"] if facets1 != {} else None,
            "facets_1_idorigin": facets1["IdOrigin"] if facets1 != {} else None,
            "facets_1_optin": facets1["OptIn"] if facets1 != {} else None,
            "facets_1_timestamp": facets1["Timestamp"] if facets1 != {} else None,
            "firstname": row["FirstName"] if "FirstName" in row.keys() else None,
            "fullname": row["FullName"] if "FullName" in row.keys() else None,
            "id": row["Id"] if "Id" in row.keys() else None,
            "idorigin": row["IdOrigin"] if "IdOrigin" in row.keys() else None,
            "isconsumer": str(row["IsConsumer"]) if "IsConsumer" in row.keys() else None,
            "iscontact": str(row["IsContact"]) if "IsContact" in row.keys() else None,
            "marketingpermissions_0_communicationdirection": market_permissions0["CommunicationDirection"] if market_permissions0 != {} else None,
            "marketingpermissions_0_id": market_permissions0["Id"] if market_permissions0 != {} else None,
            "marketingpermissions_0_idorigin": market_permissions0["IdOrigin"] if market_permissions0 != {} else None,
            "marketingpermissions_0_optin": market_permissions0["OptIn"] if market_permissions0 != {} else None,
            "marketingpermissions_0_outboundcommunicationmedium": market_permissions0["OutboundCommunicationMedium"] if market_permissions0 != {} else None,
            "marketingpermissions_0_timestamp": market_permissions0["Timestamp"] if market_permissions0 != {} else None,
            "mobilephonenumber": row["MobilePhoneNumber"] if "MobilePhoneNumber" in row.keys() else None,
            "phonenumber": row["PhoneNumber"] if "PhoneNumber" in row.keys() else None,
            "timestamp": row["Timestamp"] if "Timestamp" in row.keys() else None,
            "yy1_csf_city_enh": row["YY1_CSF_CITY_ENH"] if "YY1_CSF_CITY_ENH" in row.keys() else None,
            "yy1_csf_complement_enh": row["YY1_CSF_COMPLEMENT_ENH"] if "YY1_CSF_COMPLEMENT_ENH" in row.keys() else None,
            "yy1_csf_neighborhood_enh": row["YY1_CSF_NEIGHBORHOOD_ENH"] if "YY1_CSF_NEIGHBORHOOD_ENH" in row.keys() else None,
            "yy1_csf_number_house_enh": row["YY1_CSF_NUMBER_HOUSE_ENH"] if "YY1_CSF_NUMBER_HOUSE_ENH" in row.keys() else None,
            "yy1_csf_optin_enh": str(row["YY1_CSF_OPTIN_ENH"]) if "YY1_CSF_OPTIN_ENH" in row.keys() else None,
            "yy1_csf_postal_code1_enh": row["YY1_CSF_POSTAL_CODE1_ENH"] if "YY1_CSF_POSTAL_CODE1_ENH" in row.keys() else None,
            "yy1_csf_state_enh": row["YY1_CSF_STATE_ENH"] if "YY1_CSF_STATE_ENH" in row.keys() else None,
            "yy1_csf_status_enh": row["YY1_CSF_STATUS_ENH"] if "YY1_CSF_STATUS_ENH" in row.keys() else None,
            "yy1_csf_street_enh": row["YY1_CSF_STREET_ENH"] if "YY1_CSF_STREET_ENH" in row.keys() else None,
            "yy1_fg_grupo_controle_enh": row["YY1_FG_GRUPO_CONTROLE_ENH"] if "YY1_FG_GRUPO_CONTROLE_ENH" in row.keys() else None,
            "yy1_fg_portfolio_enh": row["YY1_FG_PORTFOLIO_ENH"] if "YY1_FG_PORTFOLIO_ENH" in row.keys() else None,
            "dth_inclusion": dt.datetime.now(zone),
            "dth_partition": dt.datetime.now(zone),
        }


class DataGuardVerify(beam.DoFn):
    def process(self, message):
        doc = json.dumps(xmltodict.parse(message))
        doc = json.loads(doc)
        doc = doc["ns0:General"]["ns0:Items"]["ns0:Item"]["ns0:AnyStructure"][0]
        doc1 = json.loads(doc)
        row = doc1["Contacts"]
        if type(row) == list:
            yield beam.pvalue.TaggedOutput("data_guard", row)
        else:
            yield beam.pvalue.TaggedOutput("process", row)


def get_schema_raw():
    schema = json.dumps(
        [
            {"name": "emailaddress", "type": "STRING"},
            {"name": "emailoptin", "type": "STRING"},
            {"name": "facets_0_id", "type": "STRING"},
            {"name": "facets_0_idorigin", "type": "STRING"},
            {"name": "facets_0_timestamp", "type": "STRING"},
            {"name": "facets_1_id", "type": "STRING"},
            {"name": "facets_1_idorigin", "type": "STRING"},
            {"name": "facets_1_optin", "type": "STRING"},
            {"name": "facets_1_timestamp", "type": "STRING"},
            {"name": "firstname", "type": "STRING"},
            {"name": "fullname", "type": "STRING"},
            {"name": "id", "type": "STRING"},
            {"name": "idorigin", "type": "STRING"},
            {"name": "isconsumer", "type": "STRING"},
            {"name": "iscontact", "type": "STRING"},
            {"name": "marketingpermissions_0_communicationdirection", "type": "STRING"},
            {"name": "marketingpermissions_0_id", "type": "STRING"},
            {"name": "marketingpermissions_0_idorigin", "type": "STRING"},
            {"name": "marketingpermissions_0_optin", "type": "STRING"},
            {
                "name": "marketingpermissions_0_outboundcommunicationmedium",
                "type": "STRING",
            },
            {"name": "marketingpermissions_0_timestamp", "type": "STRING"},
            {"name": "mobilephonenumber", "type": "STRING"},
            {"name": "phonenumber", "type": "STRING"},
            {"name": "timestamp", "type": "STRING"},
            {"name": "yy1_csf_city_enh", "type": "STRING"},
            {"name": "yy1_csf_complement_enh", "type": "STRING"},
            {"name": "yy1_csf_neighborhood_enh", "type": "STRING"},
            {"name": "yy1_csf_number_house_enh", "type": "STRING"},
            {"name": "yy1_csf_optin_enh", "type": "STRING"},
            {"name": "yy1_csf_postal_code1_enh", "type": "STRING"},
            {"name": "yy1_csf_state_enh", "type": "STRING"},
            {"name": "yy1_csf_status_enh", "type": "STRING"},
            {"name": "yy1_csf_street_enh", "type": "STRING"},
            {"name": "yy1_fg_grupo_controle_enh", "type": "STRING"},
            {"name": "yy1_fg_portfolio_enh", "type": "STRING"},
            {"name": "dth_inclusion", "type": "TIMESTAMP"},
            {"name": "dth_partition", "type": "TIMESTAMP"}
        ]
    )

    return schema


def run(options, input_subscription, output_table_raw, output_path):
    """
    Build and run Pipeline
    :param options: pipeline options
    :param input_subscription: input PubSub subscription
    :param output_table: id of an output BigQuery table
    :param output_error_table: id of an output BigQuery table for error messages
    """

    with beam.Pipeline(options=options) as pipeline:
        # Read from PubSub
        rows = (
            pipeline
            | "Read from PubSub"
            >> beam.io.ReadFromPubSub(
                subscription=input_subscription
            ).with_output_types(bytes)
            | "UTF-8 bytes to string" >> beam.Map(lambda msg: msg.decode("utf-8"))
        )

        verify = (
            rows | "Data Guard verify" >> beam.ParDo(
                DataGuardVerify()).with_outputs()
        )

        (
            verify.process
            | "Parse JSON messages RAW" >> beam.ParDo(ParseJson())
            | "Write to BigQuery RAW"
            >> beam.io.WriteToBigQuery(
                output_table_raw,
                create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                schema=get_schema_raw(),
            )
        )

        (
            verify.data_guard
            | "FlatMap" >> beam.FlatMap(lambda elements: elements)
            | "Parse JSON messages RAW data guard" >> beam.ParDo(ParseJson())
            | "Write to BigQuery RAW data guard"
            >> beam.io.WriteToBigQuery(
                output_table_raw,
                create_disposition=beam.io.BigQueryDisposition.CREATE_NEVER,
                write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
                schema=get_schema_raw(),
            )
        )

        (
            verify.process
            | "Writing .json to bucket"
            >> beam.ParDo(
                WriteElementsToGCS(output_path=output_path,
                                   origem="csf_client")
            )
        )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--input_subscription",
        required=True,
        help='Input PubSub subscription of the form "/subscriptions/<PROJECT>/<SUBSCRIPTION>".',
    )
    parser.add_argument(
        "--output_table_raw",
        required=True,
        help="Output BigQuery table for results specified as: PROJECT:DATASET.TABLE or DATASET.TABLE.",
    )
    parser.add_argument(
        "--output_path",
        required=True,
        help="Output Google Cloud Storage specified as: gs//<BUCKET>.",
    )
    known_args, pipeline_args = parser.parse_known_args()
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = True
    run(
        pipeline_options,
        known_args.input_subscription,
        known_args.output_table_raw,
        known_args.output_path,
    )
