import json
import logging

from google.cloud import pubsub_v1


def send_to_gr(request):
    request_json = request.get_json(force=True)
    resposta = {}
    try:
        resposta["cpf"] = request_json['CPF']

        resposta["data_adesao_mr"] = request_json['FIN_POLPRIV_GMT']

        data = json.dumps(resposta)
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path("br-apps-bi-customermdm-prd",
                                          "br-apps-bi-customermdm-prd-cliente-loyalty_person")

        future = publisher.publish(topic_path, data.encode("utf-8"))
        logging.info(f"Published messages response: {future.result()}.")

        return resposta
    except:
        logging.info(f'JSON NÂO SUPORTADO!')
