import json

from google.cloud import pubsub_v1


def send_to_gr(request):
    try:
        request_json = request.get_json(silent=True)

        data = json.dumps(request_json)
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path("br-apps-bi-customermdm-prd",
                                          "br-apps-bi-customermdm-prd-cliente-neoassist")

        future = publisher.publish(topic_path, data.encode("utf-8"))

        print(f"Published messages response: {future.result()}.")

        return request_json

    except:
        return f'JSON em formato inválido: {request}'
