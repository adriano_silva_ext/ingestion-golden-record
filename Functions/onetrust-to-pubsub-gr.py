import json
import logging

from google.cloud import pubsub_v1


def send_to_gr(request):
    request_json = request.get_json(silent=True)
    resposta = {}
    try:
        # if request_json:

        resposta["CPF"] = request_json['CPF']
        resposta["Email"] = request_json['Email']
        resposta["Nome"] = request_json['Nome']
        resposta["Sobrenome"] = request_json['Sobrenome']
        resposta["Telefone"] = request_json['Telefone']
        resposta["Genero"] = request_json['Genero']
        resposta["data_de_nascimento"] = request_json['Data_de_Nascimento']
        resposta["Origem"] = request_json['Origem']
        resposta["FIN_POLPRIV_CN"] = request_json['FIN_POLPRIV_CN']

        if request_json['FIN_POLPRIV_GMT'] != "":
            resposta["FIN_POLPRIV_GMT"] = request_json['FIN_POLPRIV_GMT']
        else:
            resposta["FIN_POLPRIV_GMT"] = ""

        resposta["FIN_OFERTASMAIL_CN"] = request_json['FIN_OFERTASMAIL_CN']

        if request_json['FIN_OFERTASMAIL_GMT'] != "":
            resposta["FIN_OFERTASMAIL_GMT"] = request_json['FIN_OFERTASMAIL_GMT']
        else:
            resposta["FIN_OFERTASMAIL_GMT"] = ""

        resposta["FIN_OFERTASSMS_CN"] = request_json['FIN_OFERTASSMS_CN']

        if request_json['FIN_OFERTASSMS_GMT'] != "":
            resposta["FIN_OFERTASSMS_GMT"] = request_json['FIN_OFERTASSMS_GMT']
        else:
            resposta["FIN_OFERTASSMS_GMT"] = ""

        resposta["FIN_OFERTASWHATS_CN"] = request_json['FIN_OFERTASWHATS_CN']

        if request_json['FIN_OFERTASWHATS_GMT'] != "":
            resposta["FIN_OFERTASWHATS_GMT"] = request_json['FIN_OFERTASWHATS_GMT']
        else:
            resposta["FIN_OFERTASWHATS_GMT"] = ""

        resposta["FIN_STATUSPEDWHATS_CN"] = request_json['FIN_STATUSPEDWHATS_CN']

        if request_json['FIN_STATUSPEDWHATS_GMT'] != "":
            resposta["FIN_STATUSPEDWHATS_GMT"] = request_json['FIN_STATUSPEDWHATS_GMT']
        else:
            resposta["FIN_STATUSPEDWHATS_GMT"] = ""

        resposta["FIN_TERCEIROS_CN"] = request_json['FIN_TERCEIROS_CN']

        if request_json['FIN_TERCEIROS_GMT'] != "":
            resposta["FIN_TERCEIROS_GMT"] = request_json['FIN_TERCEIROS_GMT']
        else:
            resposta["FIN_TERCEIROS_GMT"] = ""

        resposta["FIN_NEWSLETTER_CN"] = request_json['FIN_NEWSLETTER_CN']

        if request_json['FIN_NEWSLETTER_GMT'] != "":
            resposta["FIN_NEWSLETTER_GMT"] = request_json['FIN_NEWSLETTER_GMT']
        else:
            resposta["FIN_NEWSLETTER_GMT"] = ""

        resposta["FIN_CARINA_CN"] = request_json['FIN_CARINA_CN']

        if request_json['FIN_CARINA_GMT'] != "":
            resposta["FIN_CARINA_GMT"] = request_json['FIN_CARINA_GMT']
        else:
            resposta["FIN_CARINA_GMT"] = ""

        resposta["FIN_COMPARTILHA_CN"] = request_json['FIN_COMPARTILHA_CN']

        if request_json['FIN_COMPARTILHA_GMT'] != "":
            resposta["FIN_COMPARTILHA_GMT"] = request_json['FIN_COMPARTILHA_GMT']
        else:
            resposta["FIN_COMPARTILHA_GMT"] = ""

        data = json.dumps(resposta)
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path("br-apps-bi-customermdm-prd",
                                          "br-apps-bi-customermdm-prd-cliente-onetrust")

        future = publisher.publish(topic_path, data.encode("utf-8"))

        logging.info(f"Published messages response: {future.result()}.")

        return resposta
    except:
        # else:
        logging.info(f'JSON NÂO SUPORTADO!')
