from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.contrib.operators.bigquery_operator import BigQueryOperator
from airflow.models import Variable
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dagrun_operator import TriggerDagRunOperator
from google.cloud import storage

environment = Variable.get("environment")
dag_docs = """
## Base Única Golden Record - Camada Target
#### Objetivo
    Executar a partir de uma query a construção da camada target a partir da camada RAW
"""
local_tz = pendulum.timezone("America/Sao_Paulo")
default_args = {
    "owner": "[Engenheiro: Adriano Martins da Silva]",
    "email": [""],
    "email_on_failure": False,
    "email_on_retry": False,
    "retries": 0,
    "start_date": datetime(2022, 3, 7, 13, 20, 0, tzinfo=local_tz),
}

delta = timedelta(minutes=2)
time_delta = (datetime.utcnow()-delta-delta).strftime("%Y-%m-%d %H:%M:%S")

dag = DAG(
    "dag_target_ingestion",
    default_args=default_args,
    description="Ingestão camada Target a partir da RAW",
    schedule_interval=delta,
)

dag.doc_md = dag_docs

tbl_vtex_client = BigQueryOperator(
    task_id="tbl_vtex_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_vtex_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_vtex_address = BigQueryOperator(
    task_id="tbl_vtex_address",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_vtex_address.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_rh_client = BigQueryOperator(
    task_id="tbl_rh_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_rh_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_csf_client = BigQueryOperator(
    task_id="tbl_csf_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_csf_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_device_id_client = BigQueryOperator(
    task_id="tbl_device_id_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_device_id_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_sva_client = BigQueryOperator(
    task_id="tbl_sva_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_sva_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_sva_address = BigQueryOperator(
    task_id="tbl_sva_address",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_sva_address.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_propz_client = BigQueryOperator(
    task_id="tbl_propz_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_propz_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_onetrust_client = BigQueryOperator(
    task_id="tbl_onetrust_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_onetrust_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_qualibest_client = BigQueryOperator(
    task_id="tbl_qualibest_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_qualibest_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_loyalty_person = BigQueryOperator(
    task_id="tbl_loyalty_person",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_loyalty_person.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)

tbl_neoassist_client = BigQueryOperator(
    task_id="tbl_neoassist_client",
    use_legacy_sql=False,
    allow_large_results=True,
    sql="/queries/target/tbl_neoassist_client.sql",
    query_params=[{'name': 'LAST_DATE', 'parameterType': {
        'type': 'STRING'}, 'parameterValue': {'value': time_delta}}],
    dag=dag,
)


tbl_vtex_client
tbl_vtex_address
tbl_rh_client
tbl_csf_client
tbl_device_id_client
tbl_sva_client
tbl_sva_address
tbl_propz_client
tbl_onetrust_client
tbl_qualibest_client
tbl_loyalty_person
tbl_neoassist_client
