# Importando as bibliotecas que vamos usar nesse exemplo
from airflow.providers.google.cloud.operators.dataflow import (
    DataflowCreatePythonJobOperator,
)
import logging
from airflow import DAG
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from datetime import timedelta
from datetime import datetime
import pendulum

dag_docs = """
## Base Única Golden Record - Leitura dados Propz
#### Objetivo
    Leitura de API da Propz para envio de dados em Batch por PubSub
"""

local_tz = pendulum.timezone('America/Sao_Paulo')
environment = Variable.get('environment')
GCS_PYTHON = '/home/airflow/gcs/dags/api_propz_dataflow.py'

default_args = {
    'owner': '[Engenheiro: Rubens Funke Junior]',
    'email': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'start_date': datetime(2022,1,19, 12, 0, 0, tzinfo=local_tz)
}
 
dag = DAG(
    'dag_propz_ingestion',
    default_args=default_args,
    description="Ingestão camada Target a partir da RAW",
    schedule_interval="0 */6 * * *"
)

start_python_job = DataflowCreatePythonJobOperator(
    task_id="propz_load_raw_table",
    project_id=f'br-apps-bi-customermdm-{environment}',
    py_file=GCS_PYTHON,
    py_options=[],
    job_name='propz_load_raw_table',
    options={
        "env": environment,
        "job_name": "propz-batch",
        "temp_location": f"gs://br-apps-bi-customermdm-{environment}-client-integration/temp/",
        "staging_location": f"gs://br-apps-bi-customermdm-{environment}-client-integration/temp/",
        "region": "us-central1",
        "worker_machine_type": "n1-highmem-2",
        "max_num_workers": "3",
        "num_workers": "1",
        "autoscaling_algorithm": "THROUGHPUT_BASED",
        "requirements_file": '/home/airflow/gcs/dags/requirements_propz.txt'
    },
    py_requirements=[
        'requests',
        'google-cloud',
        'google-cloud-bigquery',
        'google-cloud-pubsub',
        'apache-beam',
        'google-apitools'
        ],
    py_interpreter='python3',
    py_system_site_packages=False,
    location='us-central1',
    dag=dag
)

start_python_job