VTEX CLIENT:
{
   "email":"celia@gmail.com",
   "birthDate":"null",
   "homePhone":"+55119525353534",
   "phone":"null",
   "businessPhone":"null",
   "document":"124141525235",
   "documentType":"cpf",
   "firstName":"Célia",
   "lastName":"Silva",
   "gender":"null",
   "isNewsletterOptIn":"false",
   "isSmsOptIn":"null",
   "id":"3ffe7b77-5868-11eb-82ac-1670be2e8cc3",
   "createdIn":"2021-01-17T02:04:01.131954Z",
   "updatedIn":"2021-09-28T20:48:00.785208Z",
   "channel":"DEFAULT",
   "isWhatsAppOptIn":"false"
}

vtex_client_pubsub_to_pubsub:
Produção:
python vtex_client_pubsub_to_pubsub.py --input_subscription=projects/br-digitalcomm-prod/subscriptions/br-digitalcomm-subscription-integrator-user-cadastrogeralmdmvtex --output_topic_exclusao=projects/br-apps-bi-customermdm-prd/topics/br-apps-bi-customermdm-prd-cliente-blacklist --output_topic_vtex=projects/br-apps-bi-customermdm-prd/topics/br-apps-bi-customermdm-prd-cliente-vtex-client  --streaming --project=br-apps-bi-customermdm-prd --service_account_email=vtex-cadastrogeral@br-apps-bi-customermdm-prd.iam.gserviceaccount.com --runner=DataflowRunner --temp_location=gs://br-apps-bi-customermdm-prd-client-integration/temp/ --staging_location=gs://br-apps-bi-customermdm-prd-client-integration/temp/ --region=us-central1 --job_name=vtex-to-client-mdm-prd --worker_machine_type=n1-highmem-2 --max_num_workers=3 --num_workers=1 --experiments=streaming_boot_disk_size_gb=20 --autoscaling_algorithm=THROUGHPUT_BASED


vtex_client:
Dev:
python vtex_client.py --input_subscription=projects/br-apps-bi-customermdm-dev/subscriptions/br-apps-bi-customermdm-qa-cliente-vtex-client-sub --output_table_raw br-apps-bi-customermdm-dev:db_dolphin_stage_client_base.tbl_vtex_client --output_path=br-apps-bi-customermdm-dev-dataguard --project=br-apps-bi-customermdm-dev --runner=DataflowRunner --temp_location=gs://dataflow_base_unica_gr_temp_dev/temp/ --staging_location=gs://dataflow_base_unica_gr_temp_dev/temp/ --region=us-central1 --job_name=vtex-client-dev --worker_machine_type=n1-highmem-2 --max_num_workers=3 --num_workers=1 --experiments=streaming_boot_disk_size_gb=20 --autoscaling_algorithm=THROUGHPUT_BASED --streaming

QA:
python vtex_client.py --input_subscription=projects/br-apps-bi-customermdm-qa/subscriptions/br-apps-bi-customermdm-qa-cliente-vtex-client-sub --output_table_raw=br-apps-bi-customermdm-qa:db_dolphin_stage_client_base.tbl_vtex_client --output_path=br-apps-bi-customermdm-qa-client-dataguard --project=br-apps-bi-customermdm-qa --runner=DataflowRunner --temp_location=gs://br-apps-bi-customermdm-qa-client-integration/temp/ --staging_location=gs://br-apps-bi-customermdm-qa-client-integration/temp/ --region=us-central1 --job_name=vtex-client-qa --worker_machine_type=n1-highmem-2 --max_num_workers=3 --num_workers=1 --experiments=streaming_boot_disk_size_gb=20 --autoscaling_algorithm=THROUGHPUT_BASED --streaming

PRD:
python vtex_client.py --input_subscription=projects/br-apps-bi-customermdm-prd/subscriptions/br-apps-bi-customermdm-prd-cliente-vtex-client-sub --output_table_raw=br-apps-bi-customermdm-prd:db_dolphin_stage_client_base.tbl_vtex_client --output_path=br-apps-bi-customermdm-prd-client-dataguard --project=br-apps-bi-customermdm-prd --runner=DataflowRunner --temp_location=gs://br-apps-bi-customermdm-prd-client-integration/temp/ --staging_location=gs://br-apps-bi-customermdm-prd-client-integration/temp/ --region=us-central1 --job_name=vtex-client-prd --worker_machine_type=n1-highmem-2 --max_num_workers=3 --num_workers=1 --experiments=streaming_boot_disk_size_gb=20 --autoscaling_algorithm=THROUGHPUT_BASED --streaming