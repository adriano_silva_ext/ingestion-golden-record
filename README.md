# Descrição técnica dos processos de ingestão e processamento Base Única
Este arquivo tem o objetivo a explicar um pouco mais técnico os processos construídos no processo de base única. Para que não ocorra problemas de atualizar aqui e não atualizar no confluence, neste documento é abordado somente as questões técnicas, mas detalhes de qual o nome da fila do pubsub, além do nome de tabelas e processos. O link da documentação do confluence é o: https://c4br.atlassian.net/wiki/spaces/DCDP/pages/2374041639

---
## Camadas
O projeto base única é dividido em 4 datasets, sendo eles:
1. Camada stage:
db_dolphin_stage_client_base - Neste dataset estão disponibilizados os dados da mesma forma como chegam dos sistemas origens, além de existir todas as versões dos registros (Exemplo: Se foi enviado às 9h um cadastro e às 10h uma atualização, nesta camada terá ambas as versões do registro), além de termos a nomenclatura dos campos conforme payload enviado pelos sistemas origens e o datatype é gravado sempre em String. Além dos campos disponibilizados pelo payload do sistema origem, há dois campos que são gravados em momento de ingestão que são dth_partition (a data da partição, utilizado para filtrar a tabela por uma partição específica afim de termos ganho de performance e custo) e dth_inclusion (data da carga), ambos os campos possuem valor da data que o processo dataflow que lê as mensagens do pubsub gravam na tabela stage.
2. Camada target:
db_dolphin_target_client_replica - Neste dataset estão disponibilizados a última versão dos dados de cada sistema origem da camada stage, a nomenclatura dos campos possui uma padronização seguindo os padrões do carrefour além de ter um datatype mais definido (exemplo, CPF que possui datatype numerico). Nesta camada também possui transformações básicas como no CPF (retirar mascara) e telefone (padronizando para seguirem um padrão)
3. Camada target mdm:
db_dolphin_target_clientmdm - Neste dataset são aplicadas as regras de negócio além de termos uma melhor divisão lógica dos dados do cliente, como tbl_client_address que possui dados de endereço do cliente, tbl_client_phone de telefones, etc e sabermos qual o melhor dado de cada cliente, exemplo: Caso o cliente tenha cadastro em diversos sistemas origens com um endereço cadastrado em cada um, qual devo considerar mais confiável? Esta regra foi implementada conforme prioridade de sistemas origens.
4. Camada metric:
db_dolphin_metric_clientmdm - Neste dataset está disponibilizado a tabela final de clientes, tbl_client, onde reúne os dados provenientes da camada target mdm (já com regras implementadas) e cada registro é um um dado de cliente.
5. Camada Fast Data:
tbl_customermdm_client_address - Esta camada não está disponibilizado no BigQuery, e sim no BigTable, esta tabela é uma replica da tabela tbl_client da camada metric e dispõe dados para consumo da API de clientes
---
## Ingestão camada stage

Os processos de ingestão da camada stage em geral seguem o seguinte fluxo: É publicado uma mensagem no tópico do pubsub e então essa mensagem publicada é gravada na respectiva tabela do BigQuery e no DataGuard.
O DataGuard é um repositório no Google Cloud Storage que tem o objetivo de, cada mensagem disponibilizada dos sistemas origens é gravado AS-IS em um arquivo de tal forma que possa ser reprocessado ao enviar novamente o conteúdo da mensagem novamente ao tópico do pubsub correspondente ao processo que deseja reprocessar.

Na pasta Dataflow deste repositório, temos todos os processos Dataflow desenvolvidos em Python que gravam na camada stage. Todos eles possuem mesmas características de funcionamento com pequenas diferenças dependendo da origem, além de que algumas delas por restrição do sistema origem que só consegue enviar mensagem por requisição POST, acionam uma function e a function envia a mensagem ao pubsub, a seguir temos uma breve descrição das pequenas diferenças de cada um e em seguida uma explicação em detalhe do código:

CSF: Processo csf_client_xml.py recebe mensagem em XML no pubsub, e grava no BigQuery e Storage.

RH: Processo rh_client_xml.py recebe mensagem em XML no pubsub, e grava no BigQuery e Storage.


Device Id: Processo device_id.py recebe mensagem em json no pubsub, e grava no BigQuery e Storage.

Qualibest: Processo qualibest_client.py recebe mensagem em json no pubsub, e grava no BigQuery e Storage.

SVA: Processo sva_client_address.py recebe mensagem em json no pubsub, e grava nas tabelas de client e address do SVA BigQuery e Storage.


VTEX: Há um processo tanto para o VTEX client (vtex_client_pubsub_to_pubsub.py) quanto para o VTEX address (vtex_address_pubsub_to_pubsub.py) que, busca a mensagem no tópico do pubsub da origem (VTEX) e então verifica os atributos da própria mensagem do Pubsub, caso o atributo seja de DELETE ele publicará em uma fila de deleção, caso constrário, publicará na fila do pubsub para ser lido em json pelos processos vtex_address e vtex_client então gravado nas tabelas client e address do VTEX no BigQuery e Storage.


Propz: A propz possui uma API que é atualizada a cada 6h, então há uma DAG na pasta de DAG deste repositório chamada dag_propz_ingestion, que aciona um job dataflow que se chama api_propz_dataflow.py que retorna os valores em json da API e publica na fila do pubsub, feito isso, um outro processo chamado propz_client.py lê a mensagem em json do pubsub e então grava no BigQuery e Storage.


Neoassist: Processo neoassist_client.py recebe mensagem em json no pubsub, e grava no BigQuery e Storage.

Loyalty Person(Minhas Recompensas): Processo loyalty_person.py recebe mensagem em json no pubsub, e grava no BigQuery e Storage.

Onetrust: Processo onetrust_client.py recebe mensagem em json no pubsub, e grava no BigQuery e Storage.


### Estrutura do código:
1. No final do código temos o if __name__ == '__main__', nesta parte estou recebendo todos os parâmetros que desejo e então os passo como referencia para o método run, que executará o pipeline.
2. É então criado um pipeline e então setado uma variável rows que terá o resultado da primeira parte do pipeline.
3. A primeira parte do pipeline consiste em usar a função ReadFromPubSub para ler da subscription que passei como referencia e então retornar o valor em bytes.
4. Uma vez que este valor é retornado, é então convertido de bytes para string em utf-8 usando a função decode.
5. Em seguida temos a variável verify, esta variável vai pegar esta mensagem recebida que foi atribuída a rows e então aplicar a classe DataGuardVerify, que converterá a mensagem em json.  
6. Em seguida, temos duas rotas que são executadas em paralelo, em alguns processos como sva que possui duas tabelas a serem gravadas 4 rotas mas basicamente funcionam da mesma forma, apenas mais tabelas a serem gravadas. A primeira rota ele gravará a mensagem na tabela através de um parser na classe ParseJson
7. A classe ParseJson vai então, ler o valor json armazenado na variável verify e então fazer um parser dos valores a serem gravados posteriormente na tabela, além de também pegar o valor da data atual para gravar nos campos dth_partition e dth_inclusion, referentes a data de inclusão da tabela.
8. Feito o parser, a mensagem vai ser então gravada na tabela que estamos passando como referencia na classe WriteToBigQuery que fará então a gravação na tabela.
9. Em paralelo a isso na segunda rota do processo ele acionará então a classe WriteElementsToGCS que armazenará a mensagem que chegou no bucket da dataguard.

### Deploy:

Para fazer o deploy dos processos dataflow, basta abrir a pasta DeployScripts que possui um arquivo txt referente a cada processo. O procedimento é basicamente:
1. Abrir o arquivo e copiar script de deploy do ambiente desejado.
2. Depois no terminal navegar até a pasta Dataflow
3. Colar e executar o script de deploy.
Obs.: 
1. Caso ao executar ele informe que está faltando alguma biblioteca do Python basta executar o comando pip install e o nome da biblioteca, e repetir o procedimento.
2. Todos os processos podem ser rodados simplesmente ao estar logado no terminal que então o dataflow executará com a conta default do dataflow, com exceção aos processos VTEX client (vtex_client_pubsub_to_pubsub.py) e VTEX address (vtex_address_pubsub_to_pubsub.py) que leem de uma fila do pubsub de um projeto e gravam em outro projeto. Para este, deve ser feito então a autenticação com a conta que possui acesso:
Aponte para o diretório onde está o json de service account da conta de serviço vtex-cadastrogeral@br-digitalcomm-prod.iam.gserviceaccount.com e sete as credenciais, feito isso só executar o conforma outros sistemas.
gcloud auth activate-service-account vtex-cadastrogeral@br-digitalcomm-prod.iam.gserviceaccount.com --key-file=C:\Users\adriano.silva\Downloads\br-digitalcomm-prod-7ced8034ab97.json
SET GOOGLE_APPLICATION_CREDENTIALS=C:\Users\adriano.silva\Downloads\br-digitalcomm-prod-7ced8034ab97.json
3. Dentro de cada arquivo txt há no começo um exemplo de payload teste caso queira efetuar testes em ambiente de desenvolvimento.

---
## Ingestão camada target

Os processos de construção da camada target funcionam em torno da DAG dag_target_ingestion.py 

Esta DAG executa em paralelo uma query para cada sistema origem a cada 2min para que seja atualizada a tabela target.

As tabelas stage são particionadas pelo campo dth_partition, desta forma, para que a cada 2min não fosse consultada a tabela inteira, é passado como referencia o filtro LAST_DATE que informa este delta de tempo para ser realizado o filtro.

Estas queries estão localizadas na pasta DML/prd/target, para fazer o deploy para o ambiente produtivo basta copiar o conteúdo da pasta DML/prd/target para a pasta dentro de queries/target no bucket de dags do composer, exemplo, produção: us-east1-br-apps-bi-custome-e41f0c34-bucket/dags/queries/target/

As queries como um todo executam uma query que vai buscar a última versão de cada registro, também formatando para o formato correto que chegou nessa diferença de tempo informada pelo filtro e então é feito um Merge com a tabela target destino, caso o registro exista ele irá atualizar com os valores mais atuais, caso contrário, irá inserir um registro novo.

---
## Ingestão camada target MDM

Os processos de construção da camada target funcionam em torno da DAG dag_gr.py 

Assim como a DAG da camada target, esta executa queries e com o resultado, é então atualizada a tabela destino

Estas queries estão localizadas na pasta DML/prd/gr, para fazer o deploy para o ambiente produtivo basta copiar o conteúdo da pasta DML/prd/target para a pasta dentro de queries/target no bucket de dags do composer, exemplo, produção: us-east1-br-apps-bi-custome-e41f0c34-bucket/dags/queries/gr/

As queries seguem uma sequencia de execução que pode ser visto no final da DAG onde mostra a sequencia e dependencia dos processos quando temos por exemplo tbl_tmp_last_update_source >> tbl_last_update_source, vemos uma dependencia da execução com sucesso do processo tbl_tmp_last_update_source para que o processo tbl_last_update_source seja executado, além de que quando temos uma sequencia de >> [], todos os itens da lista são executados em paralelo.

Quando a DAG finaliza a última atividade inicia ela mesmo, isto previne que a DAG fique inativa quando definimos um tempo fixo ou que encavale dois processos.

As tabelas possuem uma ordem de execução por conta que primeiro devem ser criadas as informações de clientes, pois neste processo é gerado o NUC e este é utilizado para as tabelas seguintes para fazer join entre tabelas.
Então primeiro é gerado a tbl_client_personal_information que contém informações dos clientes, em seguida, a tabela de e-mail, tbl_client_email, assim por diante.

Antes do início do processo é feito uma cópia das tabelas para uma tabela temporária para cada tabela principal, o intuito destas tabelas é para que seja mantido algumas informações das versões anteriores, como por exemplo, o NUC, a data de criação do cliente entre outros, além de também ser responsável por auxiliar a tabela de logs.

Nas queries estão aplicadas todas as regras de negócio definidas.

O nome das queries segue uma linha que auxilie a identificar o que a query faz. Muitas das queries executam cargas com cpf, e sem cpf, o motivo é por conta que uma das premissas do projeto é que seriam cpfs únicos e e-mails unicos, porém havendo a possibilidade de cpfs nulos, e como o cpf é a chave mais forte de união entre as tabelas, é então feito a carga com o cpf e depois adicionando os registros faltando que não tinham cpf mas tinham email.

As pastas deste repositório possuem uma estrutura lógica de tal forma que facilita identificar quais queries são de dev, qa ou prd, sendo assim, uma vez que precisar atualizar uma query ou dag do processo que está rodando em produção basta copiar a dag desejada atualizada para a pasta gs://us-east1-br-apps-bi-custome-e41f0c34-bucket/dags/ do composer, e as queries estão localizadas em gs://us-east1-br-apps-bi-custome-e41f0c34-bucket/dags/queries/ onde lá temos 3 pastas, uma para as queries da camada target, outra para as queries que o processo descrito a seguir (log) lê, e por fim, a pasta gr, que possui todas as queries que a dag dag_gr utiliza para construir os dados de golden record.

## Em caso de erro
No caso em que o processo da DAG apresente erro, abra a interface do airflow na dag dag_gr https://m0c1b301de9d9e51ep-tp.appspot.com/admin/airflow/tree?dag_id=dag_gr, clique em cima do quadrado que estiver em vermelho da última execução e então em View Log e ele informará o motivo do erro, caso tenha sido um erro do próprio airflow, basta apenas ir em Trigger DAG e clicar em Trigger para iniciar a DAG novamente e seguir o processamento.

Além do processo de construção das tabelas, no final do processo esta DAG publica a data que exeução anterior em uma fila do pubsub que serve de trigger para dois processos, um é a carga da tabela de log e a outra do processo que carrega a tabela do bigtable.

O processo log.py localizado na pasta Dataflow tem o intuíto de executar uma seguencia de queries, basicamente duas para cada tabela principal da client mdm (tbl_client_address, tbl_client_phone, tbl_client_email, etc), sendo uma query para identificar se são registros novos, então neste processo ele pega todos os registros que tem na tabela atual e não tem na tabela temporária (versão anterior) e para cada campo, inserir na tabela de log. A outra query é relacionada a identificar, campo a campo, quais campos foram alterados, e então registrar na tabela de log estes campos novos cada um em um novo registro.

---
## Ingestão camada metric
A ingestão da camada metric do BigQuery segue na mesma DAG que a informada no processo anterior, é uma tabela de cliente chamada tbl_client que reúne informações de todas as tabelas formando um único registro, tendo o melhor dado do cliente.

Construída a camada metric do BigQuery, temos um processo dataflow, que da mesma forma que os processos da camada stage, encontra-se na pasta Dataflow e tem nome de bigtable.py. Este processo é responsável de replicar as informações da camada metrics do bigquery para o bigtable. O processo consiste em executar uma query a partir do periodo entre a execução anterior da dag de construção do golden record com a execução atual, e então inserir os registros que foram incluídos ou alterados neste período de tempo no bigtable.

Este é um processo em streaming que fica olhando para uma fila do pubsub, a dag dag_gr no final publica a data da última execução nesta fila, uma vez que chega a fila ele então executa o processo, utilizando essa mensagem como filtro do processo.

Esta camada do bigtable então possui as informações para que a API de clientes possa consultar os dados dela e retornar no menor tempo possível.

---